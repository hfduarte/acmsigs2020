import numpy as np
import random
from shapely.geometry import Point, Polygon, LineString, MultiPolygon
import shapely.wkt
import math
from MCoords import MCoords
from SupportLine import SupportLine
import sys

"""

 * Authors: Jose Duarte        (hfduarte@ua.pt)
 * 			     Mark Mckenney 	(marmcke@siue.edu)
 * 
 * Version: 1.0 (2020-05-29)
 * 
 
"""

"""

    Example:
    python PrimitiveEventsGenerator.py '50' '100' '150' '100' '50' '50' '11' '1' '1'
    
"""

def get_input():
    if len(sys.argv) != 10:
        print('usage: python support-line3.py <n_vertices> <min_seg_len> <max_seg_len> <num_observations> <dx> <dy> <rot> <change_topology> <op> ')
        sys.exit()

    n_vertices = int(sys.argv[1])
    
    min_seg_len = int(sys.argv[2])
    max_seg_len = int(sys.argv[3])
    
    num_observations = int(sys.argv[4])
    
    dx = float(sys.argv[5])
    dy = float(sys.argv[6])
    rot = float(sys.argv[7])
    
    change_topology = int(sys.argv[8])
    
    op = int(sys.argv[9])
    
    return n_vertices, min_seg_len, max_seg_len, num_observations, dx, dy, rot, change_topology, op

def get_polygon(n_vertices, min_len, max_len):

    random.seed()

    angles = []

    if n_vertices < 360:
        population = []
        
        i = 0
        while i < 360:
            population += [i]
            i += 1

        angles = random.sample(population, n_vertices)
        angles.sort()

    # need to check if two consecutive angles are separated by >= 180 degrees
    # that situation can cause self-intersections.

    edge_lens = []

    i = 0
    while i < n_vertices:
        num = random.uniform(min_len, max_len)
        edge_lens += [num]
        i += 1

    coords = []
    i = 0
    for e in edge_lens:
        line = LineString([(0, 0), (e, 0)])
        line = shapely.affinity.rotate(line, angles[i], (0, 0), False)
        coords += [(line.coords[1][0], line.coords[1][1])]
        
        i += 1

    coords += [(coords[0][0], coords[0][1])]
    poly = Polygon(coords)

    return poly

def generate_line(num_segs, seg_min_len, seg_max_len, up_down_min_len, up_down_max_len):   
    segs = []
    
    dy = 0
    i = 0
    x = 0
    y = 0
    
    while i < num_segs:
        seg_len = random.uniform(seg_min_len, seg_max_len)
        
        seg = LineString([(x, y), (x + seg_len, y)])
        segs += [seg]
        
        x += seg_len
        
        rnum = random.uniform(0, 1)
        
        up = 0
        if rnum >= 0.5:
            up = 1
        
        dy = random.uniform(up_down_min_len, up_down_max_len)
        
        if up == 0:
            y -= dy
        else:
            y += dy
        
        i += 1

    return segs

def generate_line2(num_segs, seg_min_len, seg_max_len, up_down_min_len, up_down_max_len):   
    segs = []
    
    dy = 0
    i = 0
    x = 0
    y = 0
    
    while i < num_segs:
        seg_len = random.uniform(seg_min_len, seg_max_len)
        
        seg = LineString([(x, y), (x + seg_len, y)])
        segs += [seg]
        
        x += seg_len
        
        """
        rnum = random.uniform(0, 1)
        
        up = 0
        if rnum >= 0.5:
            up = 1
        
        dy = random.uniform(up_down_min_len, up_down_max_len)
        
        if up == 0:
            y -= dy
        else:
            y += dy
        """
        
        i += 1

    return segs

def get_coords_from_upper_and_lower_vertices(up_coords, down_coords):
    _len = len(down_coords) - 1
    
    while _len >= 0:
        up_coords += [down_coords[_len]]
        _len -= 1
       
    up_coords += [(up_coords[0][0], up_coords[0][1])]
    return up_coords

def get_simple_split_coords_1(vertices, dxx1, dyy1, dxx2, dyy2, min_percent, max_percent):
    
    t_up_coords = []
    t_down_coords = []
    
    s_up_coords = []
    s_down_coords = []
    
    _h = 0
    _n = len(vertices)
    
    for v in vertices:
        up = v[0]
        down = v[1]
        
        _g = 0
        _m = len(up)
        
        for _l in up:
            if _h == _n - 1 and _g == _m - 1:
                line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx1, _l.coords[1][1] + dyy1)])
                #sys.exit()
            else:
                dx = _l.coords[1][0] - _l.coords[0][0]
                dy = _l.coords[1][1] - _l.coords[0][1]
                
                dxx = random.uniform(dx / min_percent, dx / max_percent)
                dyy = random.uniform(dy / min_percent, dy / max_percent)
                
                r_num = random.uniform(0, 1)
                
                if r_num >= 0.5:
                    line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])
                else:
                    line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])             
            
            t_up_coords += [(line.coords[1][0], line.coords[1][1])]
            s_up_coords += [(_l.coords[1][0], _l.coords[1][1])]

            _g += 1
        
        _g = 0
        _m = len(down)
        
        for _l in down:
            if _h == _n - 1 and _g == _m - 1:
                line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx2, _l.coords[1][1] - dyy2)])
                #sys.exit()
            else:
                dx = _l.coords[1][0] - _l.coords[0][0]
                dy = _l.coords[1][1] - _l.coords[0][1]
                        
                dxx = random.uniform(dx / min_percent, dx / max_percent)
                dyy = random.uniform(dy / min_percent, dy / max_percent)
                
                r_num = random.uniform(0, 1)
                
                if r_num >= 0.5:
                    line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])
                else:
                    line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])             

            t_down_coords += [(line.coords[1][0], line.coords[1][1])]
            s_down_coords += [(_l.coords[1][0], _l.coords[1][1])]
     
            _g += 1
        
        _h += 1

    return s_up_coords, s_down_coords, t_up_coords, t_down_coords

def get_simple_split_coords_2(vertices, dxx1, dyy1, dxx2, dyy2, min_percent, max_percent):
    
    t_up_coords = []
    t_down_coords = []
    
    s_up_coords = []
    s_down_coords = []
    
    _h = 0
    _n = len(vertices)
    
    for v in vertices:
        up = v[0]
        down = v[1]
        
        _g = 0
        #_m = len(up)
        
        for _l in up:
            if _h == 0 and _g == 0:
                line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx1, _l.coords[1][1] + dyy1)])
            else:
                dx = _l.coords[1][0] - _l.coords[0][0]
                dy = _l.coords[1][1] - _l.coords[0][1]
                
                dxx = random.uniform(dx / min_percent, dx / max_percent)
                dyy = random.uniform(dy / min_percent, dy / max_percent)
                
                r_num = random.uniform(0, 1)
                
                if r_num >= 0.5:
                    line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])
                else:
                    line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])             
            
            t_up_coords += [(line.coords[1][0], line.coords[1][1])]
            s_up_coords += [(_l.coords[1][0], _l.coords[1][1])]

            _g += 1
        
        _g = 0
        #_m = len(down)
        
        for _l in down:
            if _h == 0 and _g == 0:
                line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx2, _l.coords[1][1] - dyy2)])
            else:
                dx = _l.coords[1][0] - _l.coords[0][0]
                dy = _l.coords[1][1] - _l.coords[0][1]
                        
                dxx = random.uniform(dx / min_percent, dx / max_percent)
                dyy = random.uniform(dy / min_percent, dy / max_percent)
                
                r_num = random.uniform(0, 1)
                
                if r_num >= 0.5:
                    line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])
                else:
                    line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])             

            t_down_coords += [(line.coords[1][0], line.coords[1][1])]
            s_down_coords += [(_l.coords[1][0], _l.coords[1][1])]
     
            _g += 1
        
        _h += 1

    return s_up_coords, s_down_coords, t_up_coords, t_down_coords

def interpolate(s_coords, t_coords, num_samples):

    i = 0
    t = 0
    
    n = num_samples - 1
    num_invalid_geoms = 0
    num_complex_geoms = 0
    
    geoms = []
    
    z = 1
    
    while i < num_samples:
        t = float(i) / n

        if t == 0:
            poly = Polygon(s_coords)
            
            if not poly.is_valid:
                num_invalid_geoms += 1
            
            if not poly.is_simple:
                num_complex_geoms += 1
            
            geoms += [poly]
        elif t == 1:
            poly = Polygon(t_coords)
            
            if not poly.is_valid:
                num_invalid_geoms += 1
            
            if not poly.is_simple:
                num_complex_geoms += 1
            
            geoms += [poly]
        else:            
            _k = 0
            _len = len(s_coords)
            coords = []
            
            while _k < _len - 1:
                dx = s_coords[_k][0] - t_coords[_k][0]
                dy = s_coords[_k][1] - t_coords[_k][1]
                
                dx /= float(num_samples - 1)
                dy /= float(num_samples - 1)
                
                coords += [(s_coords[_k][0] - dx * z, s_coords[_k][1] - dy * z)]
                
                _k += 1
            
            z += 1
            coords += [(coords[0][0], coords[0][1])]

            poly = Polygon(coords)
            geoms += [poly]
            
            if not poly.is_valid:
                num_invalid_geoms += 1
            
            if not poly.is_simple:
                num_complex_geoms += 1
            
        i += 1

    return geoms, num_invalid_geoms, num_complex_geoms

def interpolate_2(coords, num_samples):

    i = 0
    t = 0
    
    n = num_samples - 1
    num_invalid_geoms = 0
    num_complex_geoms = 0
    
    geoms = []
    
    #z = 1
    
    while i < num_samples:
        t = float(i) / n

        polys = []

        for coord in coords:
            poly, inv, cpx = coord.at(t)


            if poly == None:
                #print(coord)
                #print(coord.at(t))
                pass
            elif not poly.is_valid:
                print(poly.wkt + ';', t)
                sys.exit()
            
            if poly != None:
                polys += [poly]
                num_invalid_geoms += inv
                num_complex_geoms += cpx
                
                """
                print('')
                print(poly.wkt + ';')
                print('')
                """
                
        m = len(polys)
        
        if m == 1:
            geoms += [polys[0]]
        elif m > 1:
            poly = polys[0]
            
            j = 1
            k = len(polys)
            
            while j < k:
                poly = poly.union(polys[j])
                j += 1
        
            geoms += [poly]
        
        i += 1

    return geoms, num_invalid_geoms, num_complex_geoms

def interpolate_3(coords, num_samples, dx = 0, dy = 0, rot = 0):

    if coords == None:
        return None, 'interpolate3() >>> No coords to Interpolate!', 0

    #print('>>')

    i = 0
    t = 0
    
    n = num_samples - 1
    num_invalid_geoms = 0
    num_complex_geoms = 0
    
    geoms = []
    
    ref_point = Point(0, 0)
    
    while i < num_samples:
        t = float(i) / n

        #dxx = dx * t
        #dyy = dy * t

        daa = rot * t

        holes = []
        faces = []

        for coord in coords:
            poly, inv, cpx = coord.at(t)

            if poly != None and not poly.is_valid:
                #print(poly.wkt + ';', t)
                #sys.exit()
                return None, poly.wkt, t

            if poly != None:
                if coord.get_geom_type() == 1:
                    faces += [[poly, coord.get_geom_id(), coord.get_geom_parent_id()]]
                else:
                    holes += [[poly, coord.get_geom_id(), coord.get_geom_parent_id()]]
                
                num_invalid_geoms += inv
                num_complex_geoms += cpx
        
        _n = len(faces)
        _m = len(holes)
        
        if _n == 0 and _m == 0:
            #print('Instant', t)
            #print('interpolate3() >>> No Geometry to Interpolate!')
            #sys,exit()
            return None, 'interpolate3() >>> No Geometry to Interpolate!', t
            #i += 1
            #continue
            
        elif _n == 1 and _m == 0:
            geoms += [faces[0][0]]
        elif _n == 1 and _m == 1:
            poly = faces[0][0].difference(holes[0][0])
            geoms += [poly]
        else:
            _faces = []
            
            for f in faces:
                _id = f[1]
                
                for h in holes:
                    if h[2] == _id:
                        f[0] = f[0].difference(h[0])
                        
                _faces += [f[0]]
            
            poly = _faces[0]

            j = 1
            k = len(_faces)
            
            while j < k:
                poly = poly.union(_faces[j])
                j += 1
            
            geoms += [poly]        
        i += 1

        # apply rotation
        if rot > 0:
            n_geoms = len(geoms)
            geom = geoms[n_geoms - 1]
            c = geom.centroid
            
            xoff = 0 - c.x
            yoff = 0 - c.y
            zoff = 0.0
            
            geom = shapely.affinity.translate(geom, xoff, yoff, zoff)
            geom = shapely.affinity.rotate(geom, daa, ref_point, False)
            geom = shapely.affinity.translate(geom, -xoff, -yoff, zoff)
            geoms[n_geoms - 1] = geom

    return geoms, num_invalid_geoms, num_complex_geoms

def print_result(geoms, num_samples, num_invalid_geoms, num_complex_geoms):
    print(num_samples)
    print(num_invalid_geoms)
    print(num_complex_geoms)
    
    for g in geoms:
        print(g.wkt)

def print_result_error(error_message):
    print('[]')
    print('1')
    print('0')
    print('0')
    print(error_message)

def generate_vertices(segs, num_vertices_per_seg, min_len, max_len):
    i = len(segs)
    k = 0
    n_vertices_created = 0
    vertices = []
    
    while k < i:
        _seg_up_vertices = []
        _seg_down_vertices = []
        
        seg = segs[k]
        coords = seg.coords

        if k == 0:
            n_v = num_vertices_per_seg - 2
            seg_len = seg.length

            ref_p = Point(seg.coords[0][0], seg.coords[0][1])
            
            # 1
            p_len = random.uniform(min_len, max_len)
            p_angle = random.uniform(91, 179)

            line = LineString([(ref_p.x, ref_p.y), (ref_p.x + p_len, ref_p.y)])
            line = shapely.affinity.rotate(line, p_angle, ref_p, False)
            n_vertices_created += 1
            
            _seg_up_vertices += [line]
            
            # 2
            p_len = random.uniform(min_len, max_len)
            p_angle = random.uniform(91, 179)
            
            line = LineString([(ref_p.x, ref_p.y), (ref_p.x + p_len, ref_p.y)])
            line = shapely.affinity.rotate(line, -p_angle, ref_p, False)
            n_vertices_created += 1
            
            _seg_down_vertices += [line]
            
            # 
            i_p = Point(seg.coords[0][0], seg.coords[0][1])
            f_p = Point(seg.coords[1][0], seg.coords[1][1])
       
            dx = seg_len / (n_v + 1)
            
            _j = 0
            _d = 0
            
            while _j < n_v:
                p_len = random.uniform(min_len, max_len)

                x = i_p.x + dx * (_j + 1)
                y = i_p.y
                
                if _d == 0:
                    y -= p_len
                else:
                    y += p_len
                
                line = LineString([(x, i_p.y), (x, y)])
                n_vertices_created += 1
                
                if _d == 0:
                    _seg_down_vertices += [line]
                    _d = 1
                else:
                    _seg_up_vertices += [line]
                    _d = 0
                
                _j += 1
        elif k == i - 1:
            seg_len = seg.length
            
            ref_p = Point(seg.coords[1][0], seg.coords[1][1])
            
            #
            
            m = n_vertices - n_vertices_created + 2
            
            i_p = Point(seg.coords[0][0], seg.coords[0][1])
            f_p = Point(seg.coords[1][0], seg.coords[1][1])
       
            dx = seg_len / (m + 1)
            
            _j = 0
            _d = 0
            
            while _j < m:
                p_len = random.uniform(min_len, max_len)

                x = i_p.x + dx * (_j + 1)
                y = i_p.y
                
                if _d == 0:
                    y -= p_len
                else:
                    y += p_len
                
                line = LineString([(x, i_p.y), (x, y)])
                n_vertices_created += 1
                
                if _d == 0:
                    _seg_down_vertices += [line]
                    _d = 1
                else:
                    _seg_up_vertices += [line]
                    _d = 0
                
                _j += 1
            
            # 1
            p_len = random.uniform(min_len, max_len)
            p_angle = random.uniform(1, 79)
            
            line = LineString([(ref_p.x, ref_p.y), (ref_p.x + p_len, ref_p.y)])
            line = shapely.affinity.rotate(line, p_angle, ref_p, False)
            n_vertices_created += 1
            
            _seg_up_vertices += [line]
            
            # 2
            p_len = random.uniform(min_len, max_len)
            p_angle = random.uniform(1, 79)
            
            line = LineString([(ref_p.x, ref_p.y), (ref_p.x + p_len, ref_p.y)])
            line = shapely.affinity.rotate(line, -p_angle, ref_p, False)
            n_vertices_created += 1
            
            _seg_down_vertices += [line]
        else:
            i_p = Point(seg.coords[0][0], seg.coords[0][1])
            f_p = Point(seg.coords[1][0], seg.coords[1][1])
       
            seg_len = seg.length
            dx = seg_len / (num_vertices_per_seg + 1)
            
            _j = 0
            _d = 0
            
            while _j < num_vertices_per_seg:
                p_len = random.uniform(min_len, max_len)

                x = i_p.x + dx * (_j + 1)
                y = i_p.y
                
                if _d == 0:
                    y -= p_len
                else:
                    y += p_len
                
                line = LineString([(x, i_p.y), (x, y)])
                n_vertices_created += 1
                
                if _d == 0:
                    _seg_down_vertices += [line]
                    _d = 1
                else:
                    _seg_up_vertices += [line]
                    _d = 0
                
                _j += 1
       
        vertices += [(_seg_up_vertices, _seg_down_vertices)]
        k += 1

    return vertices

def find_two_points_for_splitting(vertices, seg_idx):
    seg_vertices = vertices[seg_idx]
    
    up = seg_vertices[0]
    down = seg_vertices[1]
    
    v_up_idx = np.random.randint(1, len(up))
    v_up = up[v_up_idx]
    
    v_dw_idx = -1
    v_dw = None
    
    _h = 0
    _n = len(down)
    
    while _h < _n:
        _seg = down[_h]
    
        if _seg.coords[0][0] < v_up.coords[0][0]:
            v_dw_idx = _h
        else:
            break
        
        _h += 1

    v_dw = down[v_dw_idx]

    return v_up, v_up_idx, v_dw, v_dw_idx

def find_two_points_for_splitting_2(vertices, segs):
    seg_idx_1 = np.random.randint(1, len(segs))
    seg_idx_2 = np.random.randint(1, len(segs))
    
    seg_vertices_1 = vertices[seg_idx_1]
    seg_vertices_2 = vertices[seg_idx_2]

    up = seg_vertices_1[0]
    down = seg_vertices_2[1]
    
    v_up_idx = np.random.randint(1, len(up))
    #v_up = up[v_up_idx]
    
    v_dw_idx = np.random.randint(1, len(down))
    #v_dw = down[v_dw_idx]

    #return seg_idx_1, seg_idx_2, v_up, v_up_idx, v_dw, v_dw_idx
    return seg_idx_1, seg_idx_2, v_up_idx, v_dw_idx

def get_up_dx(vertices, seg_idx_1, v_up_idx, x_up):
    _n = len(vertices[seg_idx_1][1])
    #_k = v_up_idx - 1
    
    _prev_up = None
    _prev_dw = None
    
    if v_up_idx - 1 >= 0:
        _prev_up = vertices[seg_idx_1][0][v_up_idx - 1]
    
    _i = _n - 1
    
    while _i >= 0:
        _prev_dw = vertices[seg_idx_1][1][_i]
        
        if _prev_dw.coords[0][0] < x_up:
            if _prev_up == None:
                return _prev_dw
            elif _prev_up.coords[0][0] > _prev_dw.coords[0][0]:
                return _prev_up
            else:
                return _prev_dw
        
        _i -= 1
    
    if not _prev_up == None:
        return _prev_up
    
    # 2. prev segment.
    
    _prev_seg = vertices[seg_idx_1 - 1]
    
    _up_vertices = _prev_seg[0]
    _down_vertices = _prev_seg[1]
    
    _n_up_vertices = len(_up_vertices)
    _prev_up = _up_vertices[_n_up_vertices - 1]
    
    _n_down_vertices = len(_down_vertices)
    _prev_dw = _down_vertices[_n_down_vertices - 1]
    
    if _prev_up.coords[0][0] > _prev_dw.coords[0][0]:
        return _prev_up
    else:
        return _prev_dw

def get_down_dx(vertices, seg_idx_2, v_down_idx, x_dw):
    _next_up = None
    _next_dw = None
    
    _n = len(vertices[seg_idx_2][1])
    
    if v_down_idx + 1 < _n:
        _next_dw = vertices[seg_idx_2][1][v_down_idx + 1]
    
    _i = 0
    _m = len(vertices[seg_idx_2][0])
    
    while _i < _m:
    
        _next_up = vertices[seg_idx_2][0][_i]
        
        if _next_up.coords[0][0] > x_dw:
            if _next_dw == None:
                return _next_up
            elif _next_up.coords[0][0] < _next_dw.coords[0][0]:
                return _next_up
            else:
                return _next_dw
        
        _i += 1
    
    if not _next_dw == None:
        return _next_dw
    
    # 2. next segment.
    
    _next_seg = vertices[seg_idx_2 + 1]
    
    _up_vertices = _next_seg[0]
    _down_vertices = _next_seg[1]
    
    _n_up_vertices = len(_up_vertices)
    _next_up = _up_vertices[0]
    
    _n_down_vertices = len(_down_vertices)
    _next_dw = _down_vertices[0]
    
    if _next_up.coords[0][0] < _next_dw.coords[0][0]:
        return _next_up
    else:
        return _next_dw

def print_segs(vertices):      
    for v in vertices:
        u = v[0]
        d = v[1]
        
        for l in u:
            print(l.wkt + ';')

        for l in d:
            print(l.wkt + ';')

    print('')

def get_params(n_vertices, min_len, max_len):
    #NUM_VERTICES_PER_SEG = 5

    MIN_NUM_VERTICES_IN_EXT_SEG = 5
    MIN_NUM_VERTICES_IN_INT_SEG = 4

    num_vertices_ext = MIN_NUM_VERTICES_IN_EXT_SEG * 2

    if n_vertices < num_vertices_ext:
        num_segs = 1
    elif n_vertices < (num_vertices_ext + MIN_NUM_VERTICES_IN_INT_SEG):
        num_segs = 2
    else:
        num_segs = 2
        
        _n_vertices = n_vertices - num_vertices_ext
        r = _n_vertices % MIN_NUM_VERTICES_IN_INT_SEG
        
        if r == 0:
            num_segs += (_n_vertices / MIN_NUM_VERTICES_IN_INT_SEG)
        else:
            delta = MIN_NUM_VERTICES_IN_INT_SEG - r
            _n_vertices += delta
            
            num_segs += (_n_vertices / MIN_NUM_VERTICES_IN_INT_SEG) - 1

    #num_segs = int(n_vertices / NUM_VERTICES_PER_SEG)
    
    seg_min_len = min_len / 2
    seg_max_len = max_len / 2
    
    up_down_min_len = min_len / 8
    up_down_max_len = min_len / 4

    return num_segs, seg_min_len, seg_max_len, up_down_min_len, up_down_max_len

def print_params(num_segs, seg_min_len, seg_max_len, up_down_min_len, up_down_max_len):
    print('')

    print('------------------------------------------------------')
    print('Params')
    print('------------------------------------------------------')

    print('')

    print('num segs: ' + str(num_segs))
    print('seg min len: ' + str(seg_min_len))
    print('seg max len: ' + str(seg_max_len))
    print('up down min len: ' + str(up_down_min_len))
    print('up down max len: ' + str(up_down_max_len))
    
    print('')

def print_support_line(support_line):
    print('')

    print('------------------------------------------------------')
    print('Support Line')
    print('------------------------------------------------------')
    
    print('')
    
    support_line.print_segs_csv()
    
    print('')

def print_support_line_vertices(support_line):
    print('')

    print('------------------------------------------------------')
    print('Support Line Vertices')
    print('------------------------------------------------------')
    
    print('')
    
    support_line.print_vertices_csv()
    
    print('')

def insert_vertices(s_vertices, t_vertices):
    
    _n = len(s_vertices)
    _m = len(t_vertices)
    
    if _n != _m:
        return None
    
    _i = 0
    
    _s_vertices = []
    _t_vertices = []
    
    lines1 = []
    lines2 = []
    
    while _i < _n:
        seg1 = s_vertices[_i]
        seg2 = t_vertices[_i]
        
        up1 = seg1[0]
        down1 = seg1[1]
        
        up2 = seg2[0]
        down2 = seg2[1]
        
        up_coords1 = []
        down_coords1 = []
        
        up_coords2 = []
        down_coords2 = []
        
        # up
        
        _k = len(up1)
        _h = 0
        
        prob = 0.48
        step = 0.16
        
        while _h < _k:
            v1 = up1[_h]
            v2 = up2[_h]
            
            x0 = v1[0]
            y0 = v1[1]
            
            x1 = v2[0]
            y1 = v2[1]
            
            insert = False
            
            if _h < _k - 1:
                p = random.uniform(0, 1)
                
                if p <= prob:
                    prob -= step
                    insert = True
            
            up_coords1 += [(x0, y0)]
            up_coords2 += [(x1, y1)]
            
            if insert:
                _v1 = up1[_h + 1]
                _v2 = up2[_h + 1]
                
                _x0 = _v1[0]
                _y0 = _v1[1]
            
                _x1 = _v2[0]
                _y1 = _v2[1]
            
                _dxx = _x0 - x0
                _dyy = _y0 - y0
            
                _f = random.uniform(0.22, 0.88)
            
                x = x0 + _dxx * _f
                y = y0 + _dyy * _f
            
                _dxx = _x1 - x1
                _dyy = _y1 - y1
            
                xx = x1 + _dxx * _f
                yy = y1 + _dyy * _f
            
                up_coords1 += [(x, y)]
                up_coords2 += [(xx, yy)]
                
                #lines2 += [LineString([(x, y), (xx, yy)])]
            
            #lines1 += [LineString([(x0, y0), (x1, y1)])]
            
            _h += 1

        # down
        
        _k = len(down1)
        _h = 0
        
        prob = 0.48
        step = 0.16
        
        while _h < _k:
            v1 = down1[_h]
            v2 = down2[_h]
            
            x0 = v1[0]
            y0 = v1[1]
            
            x1 = v2[0]
            y1 = v2[1]
            
            insert = False
            
            if _h < _k - 1:
                p = random.uniform(0, 1)
                
                if p <= prob:
                    prob -= step
                    insert = True
            
            down_coords1 += [(x0, y0)]
            down_coords2 += [(x1, y1)]
            
            if insert:
                _v1 = down1[_h + 1]
                _v2 = down2[_h + 1]
                
                _x0 = _v1[0]
                _y0 = _v1[1]
            
                _x1 = _v2[0]
                _y1 = _v2[1]
            
                _dxx = _x0 - x0
                _dyy = _y0 - y0
            
                _f = random.uniform(0.33, 0.67)
            
                x = x0 + _dxx * _f
                y = y0 + _dyy * _f
            
                _dxx = _x1 - x1
                _dyy = _y1 - y1
            
                xx = x1 + _dxx * _f
                yy = y1 + _dyy * _f
            
                down_coords1 += [(x, y)]
                down_coords2 += [(xx, yy)]                
                
                #lines2 += [LineString([(x, y), (xx, yy)])]
            """
            down_coords1 += [(x0, y0)]
            down_coords2 += [(x1, y1)]
            """
            #lines1 += [LineString([(x0, y0), (x1, y1)])]
            
            _h += 1

        _s_vertices += [[up_coords1, down_coords1]]
        _t_vertices += [[up_coords2, down_coords2]]
        
        _i += 1

    """
    print('')
    for l in lines1:
        print(l.wkt + ';')
        
    print('')
    for l in lines2:
        print(l.wkt + ';')

    sys.exit()
    """
    
    return _s_vertices, _t_vertices

def insert_vertices_2(s_vertices, t_vertices):
    
    _n = len(s_vertices)
    _m = len(t_vertices)
    
    if _n != _m:
        return None
    
    _i = 0
    
    _s_vertices = []
    _t_vertices = []
    
    lines1 = []
    lines2 = []
    
    while _i < _n:
        seg1 = s_vertices[_i]
        seg2 = t_vertices[_i]
        
        up1 = seg1[0]
        down1 = seg1[1]
        
        up2 = seg2[0]
        down2 = seg2[1]
        
        up_coords1 = []
        down_coords1 = []
        
        up_coords2 = []
        down_coords2 = []
        
        # up
        
        _k = len(up1)
        _h = 0
        
        #prob = 0.48
        #step = 0.16
        
        prob = 0.49
        step = 0.7
        
        while _h < _k:
            v1 = up1[_h]
            v2 = up2[_h]
            
            x0 = v1.coords[1][0]
            y0 = v1.coords[1][1]
            
            x1 = v2[0]
            y1 = v2[1]
            
            insert = False
            
            if _h < _k - 1:
                p = random.uniform(0, 1)
                
                if p <= prob:
                    prob -= step
                    insert = True
            
            up_coords1 += [(x0, y0)]
            up_coords2 += [(x1, y1)]
            
            if insert:
                _v1 = up1[_h + 1]
                _v2 = up2[_h + 1]
                
                _x0 = _v1.coords[1][0]
                _y0 = _v1.coords[1][1]
            
                _x1 = _v2[0]
                _y1 = _v2[1]
            
                _dxx = _x0 - x0
                _dyy = _y0 - y0
            
                _f = random.uniform(0.22, 0.88)
            
                x = x0 + _dxx * _f
                y = y0 + _dyy * _f
            
                _dxx = _x1 - x1
                _dyy = _y1 - y1
            
                xx = x1 + _dxx * _f
                yy = y1 + _dyy * _f
            
                up_coords1 += [(x, y)]
                up_coords2 += [(xx, yy)]
            
            _h += 1

        # down
        
        _k = len(down1)
        _h = 0
        
        #prob = 0.48
        #step = 0.16
        
        prob = 0.49
        step = 0.7
        
        while _h < _k:
            v1 = down1[_h]
            v2 = down2[_h]
            
            x0 = v1.coords[1][0]
            y0 = v1.coords[1][1]
            
            x1 = v2[0]
            y1 = v2[1]
            
            insert = False
            
            if _h < _k - 1:
                p = random.uniform(0, 1)
                
                if p <= prob:
                    prob -= step
                    insert = True
            
            down_coords1 += [(x0, y0)]
            down_coords2 += [(x1, y1)]
            
            if insert:
                _v1 = down1[_h + 1]
                _v2 = down2[_h + 1]
                
                _x0 = _v1.coords[1][0]
                _y0 = _v1.coords[1][1]
            
                _x1 = _v2[0]
                _y1 = _v2[1]
            
                _dxx = _x0 - x0
                _dyy = _y0 - y0
            
                _f = random.uniform(0.33, 0.67)
            
                x = x0 + _dxx * _f
                y = y0 + _dyy * _f
            
                _dxx = _x1 - x1
                _dyy = _y1 - y1
            
                xx = x1 + _dxx * _f
                yy = y1 + _dyy * _f
            
                down_coords1 += [(x, y)]
                down_coords2 += [(xx, yy)]                
                        
            _h += 1

        _s_vertices += [[up_coords1, down_coords1]]
        _t_vertices += [[up_coords2, down_coords2]]
        
        _i += 1
    
    return _s_vertices, _t_vertices

def transform(s_v, t_v, mint, maxt, end):
        s_up_coords = []
        t_up_coords = []
            
        s_down_coords = []
        t_down_coords = []
        
        _n = len(s_v)
        _m = len(t_v)
        
        _i = 0
        
        _t_vertices = []
        
        while _i < _n:
            seg1 = s_v[_i]
            seg2 = t_v[_i]
            
            up1 = seg1[0]
            down1 = seg1[1]
            
            up2 = seg2[0]
            down2 = seg2[1]

            _h = 0
            _k = len(up1)
            
            up_v = []
            down_v = []
            
            while _h < _k:
                v1 = up1[_h]
                v2 = up2[_h]
                
                x0 = v1[0]
                y0 = v1[1]
                
                x1 = v2[0]
                y1 = v2[1]
                
                if end:
                    s_up_coords += [(x0, y0)]
                    t_up_coords += [(x1, y1)]
                else:
                    _x = 0
                    _y = 0
                    
                    dxx = x1 - x0
                    dyy = y1 - y0
                    
                    _f = random.uniform(mint, maxt)
                    _x = x0 + dxx * _f
                    _y = y0 + dyy * _f

                    s_up_coords += [(x0, y0)]
                    t_up_coords += [(_x, _y)]
                    
                    up_v += [(_x, _y)]

                _h += 1
            
            _h = 0
            _k = len(down1)
            
            while _h < _k:
                v1 = down1[_h]
                v2 = down2[_h]
                
                x0 = v1[0]
                y0 = v1[1]
                
                x1 = v2[0]
                y1 = v2[1]
                
                if end:
                    s_down_coords += [(x0, y0)]
                    t_down_coords += [(x1, y1)]
                else:
                    _x = 0
                    _y = 0
                    
                    dxx = x1 - x0
                    dyy = y1 - y0
                    
                    _f = random.uniform(mint, maxt)
                    
                    _x = x0 + dxx * _f
                    _y = y0 + dyy * _f

                    s_down_coords += [(x0, y0)]
                    t_down_coords += [(_x, _y)]
                    
                    down_v += [(_x, _y)]
                
                _h += 1
        
            _t_vertices += [(up_v, down_v)]
            _i += 1
        
        ###
        
        _len = len(s_down_coords) - 1
                
        while _len >= 0:
            s_up_coords += [s_down_coords[_len]]
            _len -= 1
        
        s_up_coords += [(s_up_coords[0][0], s_up_coords[0][1])]

        ###

        _len = len(t_down_coords) - 1
        
        while _len >= 0:
            t_up_coords += [t_down_coords[_len]]
            _len -= 1
        
        t_up_coords += [(t_up_coords[0][0], t_up_coords[0][1])]

        ###

        return s_up_coords, t_up_coords, _t_vertices

def get_line_from_vertices(vertices):
        up_coords = []
        down_coords = []
        
        for seg in vertices:
            up = seg[0]
            down = seg[1]
            
            for v in up:
                up_coords += [(v[0], v[1])]

            for v in down:
                down_coords += [(v[0], v[1])]
        
        _len = len(down_coords) - 1
        
        while _len >= 0:
            up_coords += [down_coords[_len]]
            _len -= 1
        
        up_coords += [(up_coords[0][0], up_coords[0][1])]

        return LineString(up_coords)

def burst(s, t, mint, maxt):
        _s_coords = []
        _t_coords = []

        _n = len(s)
        _i = 0
        
        #_n_b_v = np.random.randint(4, 9)
        _n_b_v = np.random.randint(4, _n / 2)
        burst = False
        bursted = 0
        
        b_idx = np.random.randint(0, _n - _n_b_v)
        
        while _i < _n:
            s_coord = s[_i]
            t_coord = t[_i]
            
            x0 = s_coord[0]
            y0 = s_coord[1]
            
            x1 = t_coord[0]
            y1 = t_coord[1]
            
            if _i == b_idx:
                burst = True
            
            if burst:
                _s_coords += [(x0, y0)]
                _t_coords += [(x1, y1)]
                
                bursted += 1
                
                if bursted == _n_b_v:
                    burst = False
            else:
                dxx = x1 - x0
                dyy = y1 - y0
                    
                _f = random.uniform(mint, maxt)
                    
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f
            
                _s_coords += [(x0, y0)]
                _t_coords += [(_x, _y)]
                
            _i += 1

        return _s_coords, _t_coords

def burst_2(s, t, mint, maxt):
        _s_coords = []
        _t_coords = []

        _n = len(s)
        _i = 0
        
        #_n_b_v = np.random.randint(4, 9)
        _n_b_v = np.random.randint(4, _n / 2)
        burst = False
        bursted = 0
        
        b_idx = np.random.randint(0, _n - _n_b_v)
        
        _idx_1 = b_idx
        _idx_2 = 0
        
        while _i < _n:
            s_coord = s[_i]
            t_coord = t[_i]
            
            x0 = s_coord[0]
            y0 = s_coord[1]
            
            x1 = t_coord[0]
            y1 = t_coord[1]
            
            if _i == b_idx:
                burst = True
            
            if burst:
                _s_coords += [(x0, y0)]
                _t_coords += [(x1, y1)]
                
                bursted += 1
                _idx_2 = _i
                
                if bursted == _n_b_v:
                    burst = False
            else:
                dxx = x1 - x0
                dyy = y1 - y0
                    
                _f = random.uniform(mint, maxt)
                    
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f
            
                _s_coords += [(x0, y0)]
                _t_coords += [(_x, _y)]
                
            _i += 1

        return _s_coords, _t_coords, _idx_1, _idx_2

def transform_after_event(s, t, mint, maxt, idx_1, idx_2):
        _s_coords = []
        _t_coords = []

        _n = len(s)
        _i = 0
                        
        while _i < _n:
            s_coord = s[_i]
            t_coord = t[_i]
            
            x0 = s_coord[0]
            y0 = s_coord[1]
            
            x1 = t_coord[0]
            y1 = t_coord[1]
            
            if _i >= idx_1 and _i <= idx_2:
                _s_coords += [(x0, y0)]
                _t_coords += [(x1, y1)]
            else:
                dxx = x1 - x0
                dyy = y1 - y0
                    
                _f = random.uniform(mint, maxt)
                    
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f
            
                _s_coords += [(x0, y0)]
                _t_coords += [(_x, _y)]
                
            _i += 1

        return _s_coords, _t_coords

def hole_split_face(s, t, mint, maxt):
        _s_coords = []
        _t_coords = []

        _n = len(s)
        _i = 0
        
        _m = _n / 2
        
        _min = 4
        _n_b_v_1 = np.random.randint(_min, _min + int(_min / 2))
        b_idx_1 = np.random.randint(0, _m - _n_b_v_1)
        
        ini_idx = b_idx_1 + _n_b_v_1 + 3

        _min = 3
        _n_b_v_2 = np.random.randint(_min, _min + int(_min / 2))
        b_idx_2 = np.random.randint(ini_idx, _n - _n_b_v_2)

        burst = False
        bursted = 0
        
        flag = 0        
        while _i < _n:
            s_coord = s[_i]
            t_coord = t[_i]
            
            x0 = s_coord[0]
            y0 = s_coord[1]
            
            x1 = t_coord[0]
            y1 = t_coord[1]
            
            if _i == b_idx_1:
                burst = True
            elif _i == b_idx_2:
                burst = True
                flag = 1
            
            if burst:
                _s_coords += [(x0, y0)]
                _t_coords += [(x1, y1)]
                
                bursted += 1
                
                if flag == 0 and bursted == _n_b_v_1:
                    burst = False
                    bursted = 0
                elif flag == 1 and bursted == _n_b_v_2:
                    burst = False
            else:
                dxx = x1 - x0
                dyy = y1 - y0
                    
                _f = random.uniform(mint, maxt)
                    
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f
            
                _s_coords += [(x0, y0)]
                _t_coords += [(_x, _y)]
                
            _i += 1

        return _s_coords, _t_coords

def geom_consume_geom(s, t):
        #_s_coords = []
        _t_coords = []

        _n = len(s)
        _i = 0
        
        """
        _m = _n / 2
        
        _min = 4
        _n_b_v_1 = np.random.randint(_min, _min + int(_min / 2))
        b_idx_1 = np.random.randint(0, _m - _n_b_v_1)
        
        ini_idx = b_idx_1 + _n_b_v_1 + 3
        
        _min = 3
        _n_b_v_2 = np.random.randint(_min, _min + int(_min / 2))
        b_idx_2 = np.random.randint(ini_idx, _n - _n_b_v_2)
        
        burst = False
        bursted = 0
        
        flag = 0
        """
        
        while _i < _n:
            s_coord = s[_i]
            t_coord = t[_i]
            
            x0 = s_coord[0]
            y0 = s_coord[1]
            
            x1 = t_coord[0]
            y1 = t_coord[1]
            
            """
            if _i == b_idx_1:
                burst = True
            elif _i == b_idx_2:
                burst = True
                flag = 1
            
            if burst:
            """
            
            dxx = x1 - x0
            dyy = y1 - y0
                
            dxx *= 0.05
            dyy *= 0.05
                
            _t_coords += [(x1, y1)]
            
            """
                bursted += 1
                
                if flag == 0 and bursted == _n_b_v_1:
                    burst = False
                    bursted = 0
                elif flag == 1 and bursted == _n_b_v_2:
                    burst = False
            else:
                dxx = x1 - x0
                dyy = y1 - y0
                    
                _f = random.uniform(mint, maxt)
                    
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f
            
                _s_coords += [(x0, y0)]
                _t_coords += [(_x, _y)]
            """
            
            _i += 1

        #return _s_coords, _t_coords
        return _t_coords

def geom_split_geom(s, t, mint, maxt):
        _s_coords = []
        _t_coords = []

        _n = len(s)
        _i = 0
        
        _m = _n / 2
        
        _min = 4
        _n_b_v_1 = np.random.randint(_min, _min + int(_min / 2))
        b_idx_1 = np.random.randint(0, _m - _n_b_v_1)
        
        ini_idx = b_idx_1 + _n_b_v_1 + 3
        
        _min = 3
        _n_b_v_2 = np.random.randint(_min, _min + int(_min / 2))
        b_idx_2 = np.random.randint(ini_idx, _n - _n_b_v_2)
        
        burst = False
        bursted = 0
        
        flag = 0
        
        while _i < _n:
            s_coord = s[_i]
            t_coord = t[_i]
            
            x0 = s_coord[0]
            y0 = s_coord[1]
            
            x1 = t_coord[0]
            y1 = t_coord[1]
            
            if _i == b_idx_1:
                burst = True
            elif _i == b_idx_2:
                burst = True
                flag = 1
            
            if burst:

                dxx = x1 - x0
                dyy = y1 - y0
                
                dxx *= 0.05
                dyy *= 0.05
                
                _s_coords += [(x0, y0)]
                _t_coords += [(x1 + dxx, y1 + dyy)]
                
                """
                _s_coords += [(x0, y0)]
                _t_coords += [(x1, y1)]
                """
                
                bursted += 1
                
                if flag == 0 and bursted == _n_b_v_1:
                    burst = False
                    bursted = 0
                elif flag == 1 and bursted == _n_b_v_2:
                    burst = False
            else:
                dxx = x1 - x0
                dyy = y1 - y0
                    
                _f = random.uniform(mint, maxt)
                    
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f
            
                _s_coords += [(x0, y0)]
                _t_coords += [(_x, _y)]
                
            _i += 1

        return _s_coords, _t_coords

def geom_burst_geom(s, t, mint, maxt, numv, up, grow, _seg_id):
        # burst (merge) in one place
        # return a sl
        
        vertices = []
        
        """
        _n = len(t.get_segs())
        _seg_id = np.random.randint(1, _n)
        """
        
        burst = False
        bursted = 0
        
        vs = s.get_vertices()
        vt = t.get_vertices()
        
        i = 0
        while i < _n:
            v_seg_s = vs[i]
            v_seg_t = vt[i]
            
            v_seg_s_up = v_seg_s[0]
            v_seg_s_down = v_seg_s[1]
            
            v_seg_t_up = v_seg_t[0]
            v_seg_t_down = v_seg_t[1]
            
            _up = []
            _down = []
            
            if i < _seg_id or i > _seg_id + 1:
                for l in v_seg_s_up:
                    x0 = l.coords[0][0]
                    y0 = l.coords[0][1]
                    
                    x1 = l.coords[1][0]
                    y1 = l.coords[1][1]
                
                    dxx = x1 - x0
                    dyy = y1 - y0
                
                    _f = random.uniform(mint, maxt)
                    
                    if grow:
                        _up += [LineString([(x0, y0), (x1 + dxx * _f, y1 + dyy * _f)])]
                    else:
                        _up += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]
                        
                for l in v_seg_s_down:
                    x0 = l.coords[0][0]
                    y0 = l.coords[0][1]
                    
                    x1 = l.coords[1][0]
                    y1 = l.coords[1][1]
                
                    dxx = x1 - x0
                    dyy = y1 - y0
                
                    _f = random.uniform(mint, maxt)
                    
                    if grow:
                        _down += [LineString([(x0, y0), (x1 + dxx * _f, y1 + dyy * _f)])]
                    else:
                        _down += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]
            elif i == _seg_id:
                burst = True
                
                if up:
                    _m = len(v_seg_s_up)
                    j = 0
                    
                    while j < _m:
                        ls = v_seg_s_up[j]
                        lt = v_seg_t_up[j]

                        if burst:
                            x0 = ls.coords[1][0]
                            y0 = ls.coords[1][1]
                            
                            x1 = lt.coords[1][0]
                            y1 = lt.coords[1][1]
                        
                            _up += [LineString([(x0, y0), (x1, y1)])]
                            
                            bursted += 1
                            if bursted == numv:
                                burst = False
                        else:
                            x0 = ls.coords[0][0]
                            y0 = ls.coords[0][1]
                            
                            x1 = ls.coords[1][0]
                            y1 = ls.coords[1][1]
                
                            dxx = x1 - x0
                            dyy = y1 - y0
                
                            _f = random.uniform(mint, maxt)
                            
                            if grow:
                                _up += [LineString([(x0, y0), (x1 + dxx * _f, y1 + dyy * _f)])]
                            else:
                                _up += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]
                        
                        j += 1
                
                    for l in v_seg_s_down:
                        x0 = l.coords[0][0]
                        y0 = l.coords[0][1]
                        
                        x1 = l.coords[1][0]
                        y1 = l.coords[1][1]
                    
                        dxx = x1 - x0
                        dyy = y1 - y0
                    
                        _f = random.uniform(mint, maxt)
                        
                        if grow:
                            _down += [LineString([(x0, y0), (x1 + dxx * _f, y1 + dyy * _f)])]
                        else:
                            _down += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]
                else:
                    for l in v_seg_s_up:
                        x0 = l.coords[0][0]
                        y0 = l.coords[0][1]
                        
                        x1 = l.coords[1][0]
                        y1 = l.coords[1][1]
                    
                        dxx = x1 - x0
                        dyy = y1 - y0
                    
                        _f = random.uniform(mint, maxt)
                        
                        if grow:
                            _up += [LineString([(x0, y0), (x1 + dxx * _f, y1 + dyy * _f)])]
                        else:
                            _up += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]
                    
                    _m = len(v_seg_s_down)
                    j = 0
                
                    while j < _m:
                        ls = v_seg_s_down[j]
                        lt = v_seg_t_down[j]

                        if burst:
                            x0 = ls.coords[1][0]
                            y0 = ls.coords[1][1]
                            
                            x1 = lt.coords[1][0]
                            y1 = lt.coords[1][1]
                        
                            _down += [LineString([(x0, y0), (x1, y1)])]
                            
                            bursted += 1
                            if bursted == numv:
                                burst = False
                        else:
                            x0 = ls.coords[0][0]
                            y0 = ls.coords[0][1]
                            
                            x1 = ls.coords[1][0]
                            y1 = ls.coords[1][1]
                
                            dxx = x1 - x0
                            dyy = y1 - y0
                
                            _f = random.uniform(mint, maxt)
                            
                            if grow:
                                _down += [LineString([(x0, y0), (x1 + dxx * _f, y1 + dyy * _f)])]
                            else:
                                _down += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]
                        
                        j += 1          
            else:
                if burst:
                    if up:
                        _m = len(v_seg_s_up)
                        j = 0
                        
                        while j < _m:
                            ls = v_seg_s_up[j]
                            lt = v_seg_t_up[j]

                            if burst:
                                x0 = ls.coords[1][0]
                                y0 = ls.coords[1][1]
                                
                                x1 = lt.coords[1][0]
                                y1 = lt.coords[1][1]
                            
                                _up += [LineString([(x0, y0), (x1, y1)])]
                                
                                bursted += 1
                                if bursted == numv:
                                    burst = False
                            else:
                                x0 = ls.coords[0][0]
                                y0 = ls.coords[0][1]
                                
                                x1 = ls.coords[1][0]
                                y1 = ls.coords[1][1]
                    
                                dxx = x1 - x0
                                dyy = y1 - y0
                    
                                _f = random.uniform(mint, maxt)
                                
                                if grow:
                                    _up += [LineString([(x0, y0), (x1 + dxx * _f, y1 + dyy * _f)])]
                                else:
                                    _up += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]
                            
                            j += 1
                    
                        for l in v_seg_s_down:
                            x0 = l.coords[0][0]
                            y0 = l.coords[0][1]
                            
                            x1 = l.coords[1][0]
                            y1 = l.coords[1][1]
                        
                            dxx = x1 - x0
                            dyy = y1 - y0
                        
                            _f = random.uniform(mint, maxt)
                            
                            if grow:
                                _down += [LineString([(x0, y0), (x1 + dxx * _f, y1 + dyy * _f)])]
                            else:
                                _down += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]
                    else:
                        for l in v_seg_s_up:
                            x0 = l.coords[0][0]
                            y0 = l.coords[0][1]
                            
                            x1 = l.coords[1][0]
                            y1 = l.coords[1][1]
                        
                            dxx = x1 - x0
                            dyy = y1 - y0
                        
                            _f = random.uniform(mint, maxt)
                            
                            if grow:
                                _up += [LineString([(x0, y0), (x1 + dxx * _f, y1 + dyy * _f)])]
                            else:
                                _up += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]
                        
                        _m = len(v_seg_s_down)
                        j = 0
                    
                        while j < _m:
                            ls = v_seg_s_down[j]
                            lt = v_seg_t_down[j]

                            if burst:
                                x0 = ls.coords[1][0]
                                y0 = ls.coords[1][1]
                                
                                x1 = lt.coords[1][0]
                                y1 = lt.coords[1][1]
                            
                                _down += [LineString([(x0, y0), (x1, y1)])]
                                
                                bursted += 1
                                if bursted == numv:
                                    burst = False
                            else:
                                x0 = ls.coords[0][0]
                                y0 = ls.coords[0][1]
                                
                                x1 = ls.coords[1][0]
                                y1 = ls.coords[1][1]
                    
                                dxx = x1 - x0
                                dyy = y1 - y0
                    
                                _f = random.uniform(mint, maxt)
                                
                                if grow:
                                    _down += [LineString([(x0, y0), (x1 + dxx * _f, y1 + dyy * _f)])]
                                else:
                                    _down += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]
                            
                            j += 1  
                else:
                    for l in v_seg_s_up:
                        x0 = l.coords[0][0]
                        y0 = l.coords[0][1]
                        
                        x1 = l.coords[1][0]
                        y1 = l.coords[1][1]
                    
                        dxx = x1 - x0
                        dyy = y1 - y0
                    
                        _f = random.uniform(mint, maxt)
                        
                        if grow:
                            _up += [LineString([(x0, y0), (x1 + dxx * _f, y1 + dyy * _f)])]
                        else:
                            _up += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]
                            
                    for l in v_seg_s_down:
                        x0 = l.coords[0][0]
                        y0 = l.coords[0][1]
                        
                        x1 = l.coords[1][0]
                        y1 = l.coords[1][1]
                    
                        dxx = x1 - x0
                        dyy = y1 - y0
                    
                        _f = random.uniform(mint, maxt)
                        
                        if grow:
                            _down += [LineString([(x0, y0), (x1 + dxx * _f, y1 + dyy * _f)])]
                        else:
                            _down += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]
            
            vertices += [[_up, _down]]
            
            i += 1
        
        sl = SupportLine(len(s.get_segs()), 0, 0, 0, 0, s.get_segs(), vertices)

        s.generate_coords_from_vertices()
        s_coords = s.get_coords()
        
        sl.generate_coords_from_vertices()
        t_coords = sl.get_coords()

        return sl, s_coords, t_coords, _seg_id

def generate_coords_from_vertices(vertices):
        _up_coords = []
        _down_coords = []
        
        for v in vertices:
            up = v[0]
            down = v[1]
            
            for _l in up:
                _up_coords += [(_l.coords[1][0], _l.coords[1][1])]

            for _l in down:
                _down_coords += [(_l.coords[1][0], _l.coords[1][1])]
         
         # >>>
         
        _len = len(_down_coords) - 1
        
        while _len >= 0:
            _up_coords += [_down_coords[_len]]
            _len -= 1
           
        _up_coords += [(_up_coords[0][0], _up_coords[0][1])]
        return _up_coords

def geom_internal_merge(s, t, mint, maxt, up):
        vertices = []

        vs = s.get_vertices()
        vt = t.get_vertices()

        eps = 0.0

        _n = len(s.get_segs())
        _i = 0
        while _i < _n:
            _s_coords = []
            _t_coords = []
        
            _vs = vs[_i]
            _vt = vt[_i]
            
            vs_up = _vs[0]
            vs_down = _vs[1]
            
            vt_up = _vt[0]
            vt_down = _vt[1]
            
            _m = len(vs_up)
            
            _j = 0
            while _j < _m:
                if up:
                    x0 = vs_up[_j].coords[1][0]
                    y0 = vs_up[_j].coords[1][1]
                    
                    x1 = vt_up[_j].coords[1][0]
                    y1 = vt_up[_j].coords[1][1]
                    
                    dxx = x1 - x0
                    dyy = y1 - y0
                    
                    _s_coords += [LineString([(x0, y0), (x1 + dxx * eps, y1 + dyy * eps)])]
                else:
                    x0 = vs_up[_j].coords[0][0]
                    y0 = vs_up[_j].coords[0][1]
                    
                    x1 = vs_up[_j].coords[1][0]
                    y1 = vs_up[_j].coords[1][1]
                    
                    dxx = x1 - x0
                    dyy = y1 - y0
                        
                    _f = random.uniform(mint, maxt)
                    _s_coords += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]

                _j += 1
            
            _m = len(vs_down)
            
            _j = 0
            while _j < _m:
                if up:
                    x0 = vs_down[_j].coords[0][0]
                    y0 = vs_down[_j].coords[0][1]
                    
                    x1 = vs_down[_j].coords[1][0]
                    y1 = vs_down[_j].coords[1][1]
                    
                    dxx = x1 - x0
                    dyy = y1 - y0
                        
                    _f = random.uniform(mint, maxt)
                    _t_coords += [LineString([(x0, y0), (x1 - dxx * _f, y1 - dyy * _f)])]
                else:
                    x0 = vs_down[_j].coords[1][0]
                    y0 = vs_down[_j].coords[1][1]
                    
                    x1 = vt_down[_j].coords[1][0]
                    y1 = vt_down[_j].coords[1][1]
                    
                    dxx = x1 - x0
                    dyy = y1 - y0
                    
                    _t_coords += [LineString([(x0, y0), (x1 + dxx * eps, y1 + dyy * eps)])]
                
                _j += 1
                        
            vertices += [[_s_coords, _t_coords]]
            _i += 1
                
        sl = SupportLine(len(s.get_segs()), 0, 0, 0, 0, s.get_segs(), vertices)
        
        s.generate_coords_from_vertices()
        s_coords = s.get_coords()
        
        sl.generate_coords_from_vertices()
        t_coords = sl.get_coords()
        
        return sl, s_coords, t_coords

def hole_split_face_n(s, t, mint, maxt, n_parts):
        _s_coords = []
        _t_coords = []

        _n = len(s)
        _i = 0
        
        _m = int(_n / n_parts) - 1
        _ini_idx = 0
        
        _n_b_v = []
        _b_idx = []
        
        while _i < n_parts:
            _min = np.random.randint(3, 7)
            
            _num = np.random.randint(_min, _min + int(_min / 2))
            
            if _i == n_parts - 1:
                _idx = np.random.randint(_ini_idx, _n - _num)
            else:
                _idx = np.random.randint(_ini_idx, _m - _num)
            
            _n_b_v += [_num]
            _b_idx += [_idx]
            
            _ini_idx = _idx + _num + 3
            _m = _ini_idx + _m
            _i += 1
                
        burst = False
        bursted = 0
        
        flag = 0

        _i = 0
        while _i < _n:
            s_coord = s[_i]
            t_coord = t[_i]
            
            x0 = s_coord[0]
            y0 = s_coord[1]
            
            x1 = t_coord[0]
            y1 = t_coord[1]
            
            if flag < len(_b_idx):
                if _i == _b_idx[flag]:
                    burst = True
            
            if burst:
                _s_coords += [(x0, y0)]
                _t_coords += [(x1, y1)]
                
                bursted += 1
                
                if _n_b_v[flag] == bursted:
                    burst = False
                    bursted = 0
                    flag += 1
            else:
                dxx = x1 - x0
                dyy = y1 - y0
                    
                _f = random.uniform(mint, maxt)
                    
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f
            
                _s_coords += [(x0, y0)]
                _t_coords += [(_x, _y)]
                
            _i += 1

        return _s_coords, _t_coords

def get_hole(ref_geom, n_vertices):
    S = ref_geom.get_segs()
    V = ref_geom.get_vertices()
    m = len(S)
    
    rnd_seg_idx = np.random.randint(0, m - 1)

    x0 = S[rnd_seg_idx].coords[0][0]
    y0 = S[rnd_seg_idx].coords[0][1]
    
    x1 = S[rnd_seg_idx + 1].coords[1][0]
    y1 = S[rnd_seg_idx + 1].coords[1][1]
    
    y = (y1 + y0) / 2
    
    seg = LineString([(x0, y), (x1, y)])
    hole_segs = [seg]
    
    min_len_seg = sys.float_info.max
    
    i = rnd_seg_idx
    while i <= rnd_seg_idx + 1:
        v = V[i]
        
        up = v[0]
        down = v[1]
        
        for l in up:
            if l.length < min_len_seg:
                min_len_seg = l.length
        
        for l in down:
            if l.length < min_len_seg:
                min_len_seg = l.length
        
        i += 1
        
    _min = min_len_seg / 4
    _max = min_len_seg - (min_len_seg * 0.1)
    
    hsl = SupportLine(len(hole_segs), _min, _max, 0, 0, hole_segs, None, True)
    hsl.generate_vertices(n_vertices, 
                                        MIN_NUM_VERTICES_IN_EXT_SEG, 
                                        MIN_NUM_VERTICES_IN_INT_SEG, 
                                        -1, 
                                        -1, 
                                        -1, 
                                        _min, 
                                        _max, 
                                        False)
    
    return hsl, rnd_seg_idx

def get_engulf_params(ref_geom, hsl, rnd_seg_idx, up):

    S = ref_geom.get_segs()
    V = ref_geom.get_vertices()
    m = len(S)
    
    _seg_idxs = [(), ()]
    
    up = 1
    p = np.random.uniform(0, 1)
    if p <= 0.5:
        up = 0
    
    VH = hsl.get_vertices()
    
    k = 0
    v_idx = -1
    pos = 1
    vertex = None

    if rnd_seg_idx == m - 2:
        if up == 1:
            k = len(VH[0][0])
        else:
            k = len(VH[0][1])

        v_idx = np.random.randint(1, k - 1)
        
        if up == 1:
            vertex = VH[0][0][v_idx]
        else:
            vertex = VH[0][1][v_idx] 
        
        search = True
        
        i = rnd_seg_idx - 1
        while i <= rnd_seg_idx + 1 and search:
            v = V[i]
            vt = None
            
            if up == 1:
                vt = v[0]
            else:
                vt = v[1]
            
            k = len(vt)
            p = 0
            
            while p < k:
                vertx = vt[p]
                
                if vertx.coords[0][0] < vertex.coords[0][0]:
                    _seg_idxs[0] = (i, p)
                else:
                    _seg_idxs[1] = (i, p)
                    search = False
                    break
                
                p += 1
            
            i += 1

        pos = 3
    elif rnd_seg_idx > 0:
        if up == 1:
            k = len(VH[0][0])
        else:
            k = len(VH[0][1])

        v_idx = np.random.randint(1, k - 1)
        
        if up == 1:
            vertex = VH[0][0][v_idx]
        else:
            vertex = VH[0][1][v_idx] 
        
        search = True
        
        i = rnd_seg_idx - 1
        while i <= rnd_seg_idx + 2 and search:
            v = V[i]
            vt = None
            
            if up == 1:
                vt = v[0]
            else:
                vt = v[1]
            
            k = len(vt)
            p = 0
            
            while p < k:
                vertx = vt[p]
                
                if vertx.coords[0][0] < vertex.coords[0][0]:
                    _seg_idxs[0] = (i, p)
                else:
                    _seg_idxs[1] = (i, p)
                    search = False
                    break
                
                p += 1
            
            i += 1
        
        pos = 2
    else:
        if up == 1:
            k = len(VH[0][0])
        else:
            k = len(VH[0][1])

        v_idx = np.random.randint(1, k - 1)
        
        if up == 1:
            vertex = VH[0][0][v_idx]
        else:
            vertex = VH[0][1][v_idx] 
        
        search = True
        
        i = rnd_seg_idx
        while i <= rnd_seg_idx + 2 and search:
            v = V[i]
            vt = None
            
            if up == 1:
                vt = v[0]
            else:
                vt = v[1]
            
            k = len(vt)
            p = 0
            
            while p < k:
                vertx = vt[p]
                
                if vertx.coords[0][0] < vertex.coords[0][0]:
                    _seg_idxs[0] = (i, p)
                else:
                    _seg_idxs[1] = (i, p)
                    search = False
                    break
                
                p += 1
            
            i += 1
        
        pos = 1
    
    return up, _seg_idxs, v_idx, pos

def get_engulf_params_2(ref_geom, hsl, rnd_seg_idx, up, v_idx):

    S = ref_geom.get_segs()
    V = ref_geom.get_vertices()
    m = len(S)
    
    _seg_idxs = [(), ()]
    
    """
    up = 1
    p = np.random.uniform(0, 1)
    if p <= 0.5:
        up = 0
    """
    
    VH = hsl.get_vertices()
    
    #k = 0
    #v_idx = -1
    #pos = 1
    vertex = None

    if rnd_seg_idx == m - 2:
        """
        if up == 1:
            k = len(VH[0][0])
        else:
            k = len(VH[0][1])

        v_idx = np.random.randint(1, k - 1)
        """
        
        if up == 1:
            vertex = VH[0][0][v_idx]
        else:
            vertex = VH[0][1][v_idx] 
        
        search = True
        
        i = rnd_seg_idx - 1
        while i <= rnd_seg_idx + 1 and search:
            v = V[i]
            vt = None
            
            if up == 1:
                vt = v[0]
            else:
                vt = v[1]
            
            k = len(vt)
            p = 0
            
            while p < k:
                vertx = vt[p]
                
                if vertx.coords[0][0] < vertex.coords[0][0]:
                    _seg_idxs[0] = (i, p)
                else:
                    _seg_idxs[1] = (i, p)
                    search = False
                    break
                
                p += 1
            i += 1
        #pos = 3
    elif rnd_seg_idx > 0:
        """
        if up == 1:
            k = len(VH[0][0])
        else:
            k = len(VH[0][1])

        v_idx = np.random.randint(1, k - 1)
        """
        
        if up == 1:
            vertex = VH[0][0][v_idx]
        else:
            vertex = VH[0][1][v_idx] 
        
        search = True
        
        i = rnd_seg_idx - 1
        while i <= rnd_seg_idx + 2 and search:
            v = V[i]
            vt = None
            
            if up == 1:
                vt = v[0]
            else:
                vt = v[1]
            
            k = len(vt)
            p = 0
            
            while p < k:
                vertx = vt[p]
                
                if vertx.coords[0][0] < vertex.coords[0][0]:
                    _seg_idxs[0] = (i, p)
                else:
                    _seg_idxs[1] = (i, p)
                    search = False
                    break
                
                p += 1
            i += 1
        #pos = 2
    else:
        """
        if up == 1:
            k = len(VH[0][0])
        else:
            k = len(VH[0][1])

        v_idx = np.random.randint(1, k - 1)
        """
        
        if up == 1:
            vertex = VH[0][0][v_idx]
        else:
            vertex = VH[0][1][v_idx] 
        
        search = True
        
        i = rnd_seg_idx
        while i <= rnd_seg_idx + 2 and search:
            v = V[i]
            vt = None
            
            if up == 1:
                vt = v[0]
            else:
                vt = v[1]
            
            k = len(vt)
            p = 0
            
            while p < k:
                vertx = vt[p]
                
                if vertx.coords[0][0] < vertex.coords[0][0]:
                    _seg_idxs[0] = (i, p)
                else:
                    _seg_idxs[1] = (i, p)
                    search = False
                    break
                
                p += 1
            i += 1
        #pos = 1
    
    #return up, _seg_idxs, v_idx, pos
    return _seg_idxs

"""

    Prints the annotations.
    
    Example output for a face with id = 1:
    
    (face, 1, shrink, 0, 1, 1);

"""
def print_annotations(annotations):
    n_annotations = len(annotations)

    if n_annotations == 0:
        print('[]')
        return

    out = ''
    i = 0
    for annotation in annotations:
        geom_type = annotation[0]
        
        geom_type_str = None
        
        if geom_type == 1:
            geom_type_str = 'face'
        else:
            geom_type_str = 'hole'
        
        geom_id = annotation[1]
        event_type = annotation[2]
        start = annotation[3]
        end = annotation[4]
        unit_id = annotation[5]
        
        out += "(" + geom_type_str + ", id: " + str(geom_id) + ", " +  event_type + ", start: " + str(start) + ", end: " + str(end) + ", unit id: " + str(unit_id) + ")"
        
        if i != n_annotations - 1:
            out += ";"
        
        #print(out)
        #print(geom_type_str, geom_id, event_type, start, end, unit_id)
        i += 1
    
    print(out)
    
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

MIN_NUM_VERTICES_IN_EXT_SEG = 5
MIN_NUM_VERTICES_IN_INT_SEG = 4

min_num_inactive_vertices_per_seg = 3
max_num_inactive_vertices_per_seg = min_num_inactive_vertices_per_seg * 2  

use_inactive_points = True

# 1. Get input.

n_vertices, min_len, max_len, num_observations, Dx, Dy, Rot, change_topology, op = get_input()
#print(n_vertices, min_seg_len, max_seg_len, num_observations, Dx, Dy, Rot, change_topology, op)

if n_vertices < 3:
    print_result_error('Invalid number of vertices. Must have 3 or more vertices!')
    sys.exit()
    
# 2. Get params for the support line.
    
num_segs, seg_min_len, seg_max_len, up_down_min_len, up_down_max_len = get_params(n_vertices, min_len, max_len)
#print_params(num_segs, seg_min_len, seg_max_len, up_down_min_len, up_down_max_len)

# 3. Generate the support line.
    
support_line = SupportLine(num_segs, seg_min_len, seg_max_len, up_down_min_len, up_down_max_len)
#print_support_line(support_line)

# 4. Generate vertices.

support_line.generate_vertices( n_vertices, 
                                                   MIN_NUM_VERTICES_IN_EXT_SEG,
                                                   MIN_NUM_VERTICES_IN_INT_SEG,
                                                   -1,
                                                   min_num_inactive_vertices_per_seg,
                                                   max_num_inactive_vertices_per_seg,
                                                   min_len, 
                                                   max_len, 
                                                   use_inactive_points)

#print_support_line_vertices(support_line)
#support_line.print_structure()

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

# Operations.

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

### Notes

"""

    insert_vertices() in SP may have bug when adding a lot of vertices!
                               can cause invalid geometries and clone points at the boundary
    
"""

isl = support_line
_exec = True

max_tries = 10
counter = 0

while _exec:
    support_line = isl
    final_geom = None
    coords = []
    annotations = []

    # Non-Continuous Shrink (Non-Continuous because remove vertices does not respect the main events occuring).
    if op == 1:
        low = 1
        high = 10
        
        N = np.random.randint(low, high)
        
        dt = float(1) / (N + 1)
        
        start_t =  0
        end_t = 0
        
        inclusive = False
        end_as_line = None
        
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        xstep = Dx / N
        ystep = Dy / N
        
        insert_vertices = False
        annotations += [[1, 1, 'shrink' , 0, 1, 1]]
        
        i = 0
        while i < N: 
            if change_topology == 0:
                min_percent = 1 / np.random.uniform(0.05, 0.11)
                max_percent = 1 / np.random.uniform(0.13, 0.21)
            
                sl, s_coords, t_coords = support_line.shrink(min_percent, max_percent)
                    
                if i == N - 1:
                    dx = Dx - xstep * (i)
                    dy = Dy - ystep * (i)
                
                    end_t = 1
                    inclusive = True
                else:
                    dx = xstep
                    dy = ystep
                    
                    end_t += dt

                x = xstep * (i)
                y = ystep * (i)

                coords += [MCoords(s_coords, t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
            else:
                if insert_vertices:
                    min_percent = 1 / np.random.uniform(0.05, 0.11)
                    max_percent = 1 / np.random.uniform(0.13, 0.21)
                
                    sl, s_coords, t_coords = support_line.shrink(min_percent, max_percent)
                        
                    if i == N - 1:
                        dx = Dx - xstep * (i)
                        dy = Dy - ystep * (i)
                    
                        end_t = 1
                        inclusive = True
                    else:
                        dx = xstep
                        dy = ystep
                        
                        end_t += dt

                    x = xstep * (i)
                    y = ystep * (i)

                    coords += [MCoords(s_coords, t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
                    insert_vertices = False
                else:
                    rand = np.random.uniform(0, 1)
                    if rand <= 0.5:
                        insert_vertices = True
                
                    if insert_vertices:
                        min_percent = 1 / np.random.uniform(0.05, 0.11)
                        max_percent = 1 / np.random.uniform(0.13, 0.21)
                    
                        sl, s_coords, t_coords = support_line.shrink(min_percent, max_percent)
                            
                        if i == N - 1:
                            dx = Dx - xstep * (i)
                            dy = Dy - ystep * (i)
                        
                            end_t = 1
                            inclusive = True
                        else:
                            dx = xstep
                            dy = ystep
                            
                            end_t += dt

                        x = xstep * (i)
                        y = ystep * (i)

                        coords += [MCoords(s_coords, t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

                        _min_vertices = 5
                        _max_vertices = np.random.randint(6, 9)
                        _num_pos = _max_vertices
                        
                        sl.insert_vertices(_min_vertices, _max_vertices, _num_pos)
                    else:
                        _min = np.random.uniform(0.05, 0.08)
                        _max = np.random.uniform(0.1, 0.13)
                        sl, s_coords, t_coords = support_line.remove_vertices(_min, _max, 0)
        
                        if i == N - 1:
                            dx = Dx - xstep * (i)
                            dy = Dy - ystep * (i)
                        
                            end_t = 1
                            inclusive = True
                        else:
                            dx = xstep
                            dy = ystep
                            
                            end_t += dt

                        x = xstep * (i)
                        y = ystep * (i)

                        coords += [MCoords(s_coords, t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
            
            support_line = sl
            start_t = end_t
            i += 1
    # Continuous Shrink (Using insert vertices only).
    elif op == 2:
        low = 1
        high = 10
        
        N = np.random.randint(low, high)
        
        dt = float(1) / (N + 1)
        
        start_t =  0
        end_t = 0
        
        inclusive = False
        end_as_line = None
        
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        xstep = Dx / N
        ystep = Dy / N
        
        annotations += [[1, 1, 'shrink' , 0, 1, 1]]
        
        i = 0
        while i < N:
            if change_topology == 0:
                min_percent = 1 / np.random.uniform(0.05, 0.11)
                max_percent = 1 / np.random.uniform(0.13, 0.21)
            
                sl, s_coords, t_coords = support_line.shrink(min_percent, max_percent)
                    
                if i == N - 1:
                    dx = Dx - xstep * (i)
                    dy = Dy - ystep * (i)
                
                    end_t = 1
                    inclusive = True
                else:
                    dx = xstep
                    dy = ystep
                    
                    end_t += dt

                x = xstep * (i)
                y = ystep * (i)

                coords += [MCoords(s_coords, t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
            else:
                min_percent = 1 / np.random.uniform(0.05, 0.11)
                max_percent = 1 / np.random.uniform(0.13, 0.21)
                    
                sl, s_coords, t_coords = support_line.shrink(min_percent, max_percent)
                            
                if i == N - 1:
                    dx = Dx - xstep * (i)
                    dy = Dy - ystep * (i)
                        
                    end_t = 1
                    inclusive = True
                else:
                    dx = xstep
                    dy = ystep
                            
                    end_t += dt

                x = xstep * (i)
                y = ystep * (i)

                coords += [MCoords(s_coords, t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

                # insert vertices
                
                _min_vertices = 5
                _max_vertices = np.random.randint(6, 9)
                _num_pos = _max_vertices
                        
                sl.insert_vertices(_min_vertices, _max_vertices, _num_pos)
                
            support_line = sl
            start_t = end_t
            i += 1
    # Disappear to line.
    elif op == 3:
        min_percent = 10                       # 10%
        max_percent = (1 / 0.5)              # 50%
        
        supp_line_1, source_coords_1, target_coords_1 = support_line.shrink(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.25
        
        dx = Dx * end_t
        dy = Dy * end_t
        
        x = 0
        y = 0
        
        inclusive = False
        end_as_line = None
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        # Transform 2 (Insert Vertices + Shrink) >>>>>>>>>>>>>>>
                
        supp_line_1.insert_vertices(0, 11, 19)
        
        min_percent = (1 / 0.20)               # 10%
        max_percent = (1 / 0.45)              # 50%
        
        supp_line_2, source_coords_2, target_coords_2 = supp_line_1.shrink(min_percent, max_percent)
        
        start_t = 0.25
        end_t = 0.5
        
        dx1 = Dx * (end_t - start_t)
        dy1 = Dy * (end_t - start_t)
        
        x1 = Dx * start_t
        y1 = Dy * start_t
        
        coords += [MCoords(source_coords_2, target_coords_2, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        
        # Transform 3 (Disappear to Line) >>>>>>>>>>>>>>>

        annotations += [[1, 1, 'shrink' , 0, 1, 1]]
        annotations += [[1, 1, 'start disappearing' , 0.5, 1, 1]]
        annotations += [[1, 1, 'disappear' , 1, 1, 1]]
        
        #supp_line_3, source_coords_3, target_coords_3 = supp_line_2.disappear_to_line()
        sl, source_coords_3, target_coords_3 = supp_line_2.disappear_to_line2()
        
        start_t = 0.5
        end_t = 1
        
        dx2 = Dx * (end_t - start_t)
        dy2 = Dy * (end_t - start_t)
        
        x2 = Dx * start_t
        y2 = Dy * start_t
        
        inclusive = True
        
        #line = supp_line_3.get_support_line()
        #line = shapely.affinity.translate(line, Dx, Dy, 0.0)
        
        line = sl.get_line_from_sl()
        line = shapely.affinity.translate(line, Dx, Dy, 0.0)

        end_as_line = line
        
        coords += [MCoords(source_coords_3, target_coords_3, start_t, end_t, dx2, dy2, x2, y2, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
    # Disappear to Seg.
    elif op == 4:
        min_percent = (1 / 0.05)               # 10%
        max_percent = (1 / 0.17)              # 50%

        _exec = True
        while _exec:
            supp_line_1, source_coords_1, target_coords_1 = support_line.shrink(min_percent, max_percent)
            
            start_t = 0
            end_t = 0.25
            
            dx = Dx * end_t
            dy = Dy * end_t
            
            x = 0
            y = 0
            
            inclusive = False
            end_as_line = None
            geom_type = 1
            geom_id = 1
            geom_parent_id = 0
            
            coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

            # Transform 2 (Insert Vertices + Shrink) >>>>>>>>>>>>>>>
           
            supp_line_1.insert_vertices(0, 11, 19)
                    
            min_percent = (1 / 0.07)               # 10%
            max_percent = (1 / 0.13)              # 50%
            
            supp_line_2, source_coords_2, target_coords_2 = supp_line_1.shrink(min_percent, max_percent)
            
            start_t = 0.25
            end_t = 0.5
            
            dx1 = Dx * (end_t - start_t)
            dy1 = Dy * (end_t - start_t)
            
            x1 = Dx * start_t
            y1 = Dy * start_t
            
            coords += [MCoords(source_coords_2, target_coords_2, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

            # Transform 3 (Disappear to Seg) >>>>>>>>>>>>>>>
            
            annotations += [[1, 1, 'shrink' , 0, 1, 1]]

            supp_line_3, source_coords_3, target_coords_3 = supp_line_2.disappear_to_seg()
            
            if supp_line_3 == None:
                coords = None
                break
            
            start_t = 0.5
            end_t = 1
            
            annotations += [[1, 1, 'disappear to seg' , start_t, end_t, 1]]
            annotations += [[1, 1, 'disappear' , 1, 1, 1]]
            
            dx2 = Dx * (end_t - start_t)
            dy2 = Dy * (end_t - start_t)
            
            x2 = Dx * start_t
            y2 = Dy * start_t
            
            inclusive = True
            
            line = supp_line_3.get_support_line()
            line = shapely.affinity.translate(line, x2 + dx2, y2 + dy2, 0.0)
            
            end_as_line = line
            
            coords += [MCoords(source_coords_3, target_coords_3, start_t, end_t, dx2, dy2, x2, y2, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
            _exec = False
    # Hole from Open Line + Remove/Insert Vertices.
    elif op == 5:
        if change_topology == 1:
            min_percent = (1 / 0.05)               # 10%
            max_percent = (1 / 0.25)              # 25%
            
            xstep = Dx / 2
            ystep = Dy / 2
            
            supp_line_1, source_coords_1, target_coords_1 = support_line.shrink(min_percent, max_percent)
            
            start_t = 0
            end_t = 0.5
            
            dx = xstep
            dy = ystep
            
            x = 0
            y = 0
            
            inclusive = False
            end_as_line = None
            geom_type = 1
            geom_id = 1
            geom_parent_id = 0
            
            coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

            _min = np.random.uniform(0.05, 0.08)
            _max = np.random.uniform(0.1, 0.13)
            
            sl, s_coords, t_coords = supp_line_1.remove_vertices(_min, _max, 0)
            
            start_t = 0.5
            end_t = 1
            
            dx = Dx - xstep
            dy = Dy - ystep
            
            x = xstep
            y = ystep
            
            inclusive = True
            coords += [MCoords(s_coords, t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
            
            annotations += [[1, 1, 'shrink' , 0, end_t, 1]]
            
            # >> Generate Hole
            
            _min_up_percent = 0.05
            _max_up_percent = 0.1
            
            _min_down_percent = 0.05
            _max_down_percent = 0.1
            
            hsl, hs_coords, ht_coords = support_line.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)
            
            start_t = 0.2
            end_t = 0.6
            
            annotations += [[0, 2, 'appear' , start_t, start_t, 1]]
            annotations += [[0, 2, 'grow' , start_t, 1, 1]]
            
            dx1 = (Dx * 0.4)
            dy1 = (Dy * 0.4)
            
            x1 = Dx * 0.2
            y1 = Dy * 0.2
            
            inclusive = False
            end_as_line = None
            
            geom_type = 0
            geom_id = 2
            geom_parent_id = 1
            
            coords += [MCoords(hs_coords, ht_coords, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
            
            """
            _min = np.random.uniform(0.05, 0.08)
            _max = np.random.uniform(0.1, 0.13)
            hsl, hs_coords, ht_coords = hsl.remove_vertices(_min, _max, 0)
            """
            
            _min_vertices = 7
            _max_vertices = np.random.randint(9, 11)
            _num_pos = _max_vertices
            
            min_percent = (1 / 0.1)                 # 10%
            max_percent = (1 / 0.75)              # 25%
            
            hsl.insert_vertices(_min_vertices, _max_vertices, _num_pos)
            hsl, hs_coords, ht_coords = hsl.grow(min_percent, max_percent)
            
            start_t = 0.6
            end_t = 1
            
            dx1 = Dx - (Dx * 0.6)
            dy1 = Dy - (Dy * 0.6)
            
            x1 = Dx * 0.6
            y1 = Dy * 0.6
            
            inclusive = True
            coords += [MCoords(hs_coords, ht_coords, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        else:
            min_percent = (1 / 0.05)               # 10%
            max_percent = (1 / 0.25)              # 25%
            
            supp_line_1, source_coords_1, target_coords_1 = support_line.shrink(min_percent, max_percent)
            
            start_t = 0
            end_t = 1
            
            dx = Dx
            dy = Dy
            
            x = 0
            y = 0
            
            inclusive = True
            end_as_line = None
            geom_type = 1
            geom_id = 1
            geom_parent_id = 0
            
            coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

            annotations += [[1, 1, 'shrink' , start_t, end_t, 1]]

            # >> Generate Hole
            
            _min_up_percent = 0.1
            _max_up_percent = 0.2
            
            _min_down_percent = 0.05
            _max_down_percent = 0.2
            
            hsl, hs_coords, ht_coords = supp_line_1.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)
                        
            start_t = 0.2
            end_t = 1
            
            annotations += [[0, 2, 'appear' , start_t, start_t, 1]]
            annotations += [[0, 2, 'grow' , start_t, end_t, 1]]
            
            dx1 = Dx - (Dx * 0.2)
            dy1 = Dy - (Dy * 0.2)
            
            x1 = Dx * 0.2
            y1 = Dy * 0.2
            
            inclusive = True
            end_as_line = None
            
            geom_type = 0
            geom_id = 2
            geom_parent_id = 1
            
            coords += [MCoords(hs_coords, ht_coords, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
    # Hole from Open Line + Insert Vertices.
    elif op == 6:
        x_step = Dx / 2
        y_step = Dy / 2
        
        min_percent = (1 / 0.05)
        max_percent = (1 / 0.25)
        
        supp_line_1, source_coords_1, target_coords_1 = support_line.shrink(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.5
        
        dx = x_step
        dy = y_step
        
        x = 0
        y = 0
        
        inclusive = False
        end_as_line = None
        
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        
        min_percent = (1 / 0.07)
        max_percent = (1 / 0.33)
        
        supp_line_1.insert_vertices(0, 9, 25)
        supp_line_2, source_coords_2, target_coords_2 = supp_line_1.shrink(min_percent, max_percent)
        
        start_t = 0.5
        end_t = 1
        
        dx1 = Dx - x_step
        dy1 = Dy - y_step
        
        x1 = dx
        y1 = dy
        
        inclusive = True        
        coords += [MCoords(source_coords_2, target_coords_2, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        annotations += [[1, 1, 'shrink' , 0, 1, 1]]

        # Hole
        
        _min_up_percent = 0.11
        _max_up_percent = 0.27
            
        _min_down_percent = 0.07
        _max_down_percent = 0.29
        
        hsl, source_coords_3, target_coords_3 = supp_line_1.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)
        
        start_t = 0.2
        end_t = 1
        
        annotations += [[0, 2, 'appear' , start_t, start_t, 1]]
        annotations += [[0, 2, 'grow' , start_t, end_t, 1]]
        
        dx1 = Dx - (Dx * 0.2)
        dy1 = Dy - (Dy * 0.2)
        
        x1 = Dx * 0.2
        y1 = Dy * 0.2
        
        inclusive = True
        end_as_line = None
        
        geom_type = 0
        geom_id = 2
        geom_parent_id = 1
        
        coords += [MCoords(source_coords_3, target_coords_3, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
    # Hole consume face. (TODO: Change Topology)!
    elif op == 7:
        """
        min_percent = (1 / 0.07)
        max_percent = (1 / 0.25)
        
        f_sl, f_s_coords, f_t_coords = support_line.shrink(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.5
        
        dx = (Dx * end_t)
        dy = (Dy * end_t)
        
        x = 0
        y = 0
        
        inclusive = False
        end_as_line = None
        
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        coords += [MCoords(f_s_coords, f_t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        _min_vertices = 5
        _max_vertices = np.random.randint(6, 9)
        _num_pos = _max_vertices
                        
        f_sl.insert_vertices(_min_vertices, _max_vertices, _num_pos)
        
        min_percent = 1 / 0.07
        max_percent = 1 / 0.13

        f_sl, f_s_coords, f_t_coords = f_sl.shrink(min_percent, max_percent)
        
        start_t = 0.5
        end_t = 1
        
        dx = (Dx * end_t)
        dy = (Dy * end_t)
        
        x = 0
        y = 0

        inclusive = True
        #line = get_line_from_vertices(f_t_coords)
        line = LineString(f_t_coords)
        line = shapely.affinity.translate(line, Dx, Dy, 0.0)
        end_as_line = line

        coords += [MCoords(f_s_coords, f_t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        annotations += [[1, 1, 'shrink' , 0, 1, 1]]
    
        # Hole
        
        segs = f_sl.get_segs()
        
        hsl = SupportLine(len(segs), seg_min_len, seg_max_len, up_down_min_len, up_down_max_len, segs)

        hsl.generate_vertices( n_vertices + np.random.randint(3, 9), 
                                           MIN_NUM_VERTICES_IN_EXT_SEG,
                                           MIN_NUM_VERTICES_IN_INT_SEG,
                                           -1,
                                           min_num_inactive_vertices_per_seg,
                                           max_num_inactive_vertices_per_seg,
                                           min_len, 
                                           max_len, 
                                           use_inactive_points)

        

        #min_percent = (1 / 0.21)
        #max_percent = (1 / 0.39)
        
        _min_up_percent = 0.07
        _max_up_percent = 0.31
            
        _min_down_percent = 0.07
        _max_down_percent = 0.33
        
        hsl, hs_coords, ht_coords = hsl.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)

        start_t = 0.2
        end_t = 0.6
        
        annotations += [[0, 2, 'appear' , start_t, start_t, 1]]
        annotations += [[0, 2, 'grow' , start_t, 1, 1]]
        
        dx1 = Dx * (end_t - start_t)
        dy1 = Dy * (end_t - start_t)
        
        x1 = Dx * start_t
        y1 = Dy * start_t
        
        inclusive = False
        end_as_line = None
        geom_type = 0
        geom_id = 2
        geom_parent_id = 1

        coords += [MCoords(hs_coords, ht_coords, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        #hsl.print_structure()
        #f_sl.print_structure()

        s_coords, t_coords = hsl.grow_to_line(f_sl)

        start_t = 0.6
        end_t = 1

        dx1 = Dx * (end_t - start_t)
        dy1 = Dy * (end_t - start_t)
        
        x1 = Dx * start_t
        y1 = Dy * start_t
    
        inclusive = True
        coords += [MCoords(s_coords, t_coords, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        """
        # >>>>>>>>>>>>>>>>>>>>>>
        # A face.
        
        min_percent = (1 / 0.07)
        max_percent = (1 / 0.25)
        
        supp_line_1, source_coords_1, target_coords_1 = support_line.shrink(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.7
        
        dx = (Dx * 0.7)
        dy = (Dy * 0.7)
        
        x = 0
        y = 0
        
        inclusive = False
        end_as_line = None
        
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        annotations += [[1, 1, 'shrink' , 0, 1, 1]]

        # Hole from open line.
        
        _min_up_percent = 0.21
        _max_up_percent = 0.47
            
        _min_down_percent = 0.27
        _max_down_percent = 0.39
        
        hsl, hs_coords, ht_coords = supp_line_1.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)
                
        start_t = 0.2
        end_t = 0.6
        
        annotations += [[0, 2, 'appear' , start_t, start_t, 1]]
        annotations += [[0, 2, 'grow' , start_t, 1, 1]]
        
        dx1 = Dx * (end_t - start_t)
        dy1 = Dy * (end_t - start_t)
        
        x1 = Dx * start_t
        y1 = Dy * start_t
        
        inclusive = False
        end_as_line = None
        geom_type = 0
        geom_id = 2
        geom_parent_id = 1

        coords += [MCoords(hs_coords, ht_coords, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        
        # Hole consume face.
                
        consume_line_vertices = supp_line_1.get_consume_line_coords_2(hsl.get_vertices(), 0.5, 0.75)

        _s_v, _t_v = insert_vertices_2(hsl.get_vertices(), consume_line_vertices)
       
        _coords = supp_line_1.get_transformed_coords()

        # Face evolution.
        
        end = True
        _s_f1_v1, _t_f1_v1, t_f1_vertices_1 = transform(_coords, consume_line_vertices, -1, -1, end)
        
        start_t = 0.7
        end_t = 1
        
        dx = Dx - (Dx * start_t)
        dy = Dy - (Dy * start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        inclusive = True
        
        line = get_line_from_vertices(_t_v)
        line = shapely.affinity.translate(line, Dx, Dy, 0.0)
        end_as_line = line

        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        coords += [MCoords(_s_f1_v1, _t_f1_v1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        # Hole evolution.
        
        _s_v1, _t_v1, t_vertices_1 = transform(_s_v, _t_v, 0.25, 0.55, False)

        start_t = 0.6
        end_t = 0.8
        
        dx1 = (Dx * 0.2)
        dy1 = (Dy * 0.2)
        
        x1 = Dx * start_t
        y1 = Dy * start_t
        
        inclusive = False
        end_as_line = None
        
        geom_type = 0
        geom_id = 2
        geom_parent_id = 1
        end = True
        
        coords += [MCoords(_s_v1, _t_v1, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        _s_v2, _t_v2, t_vertices_2 = transform(t_vertices_1, _t_v, -1, -1, end)
        
        start_t = 0.8
        end_t = 1
        
        annotations += [[1, 1, 'disappear' , end_t, end_t, 1]]
        annotations += [[0, 2, 'disappear' , end_t, end_t, 1]]
        
        dx1 = Dx - (Dx * 0.8)
        dy1 = Dy - (Dy * 0.8)
        
        x1 = Dx * start_t
        y1 = Dy * start_t
        
        inclusive = True
        poly = Polygon()
        end_as_line = poly
        
        coords += [MCoords(_s_v2, _t_v2, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        final_geom = Polygon()
    # Burst. (Remove. Repeated with 11).
    elif op == 8:
        # Face.
        
        min_percent = (1 / 0.05)
        max_percent = (1 / 0.25)
        
        supp_line_1, s_f1_coords_1, t_f1_coords_1 = support_line.shrink(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.5
        
        dx = Dx * end_t
        dy = Dy * end_t
        
        x = 0
        y = 0
        
        inclusive = False
        end_as_line = None
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        coords += [MCoords(s_f1_coords_1, t_f1_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        
        supp_line_1.insert_vertices(0, 9, 17)
        supp_line_2, s_f1_coords_2, t_f1_coords_2 = supp_line_1.shrink(min_percent, max_percent)

        start_t = 0.5
        end_t = 1
        
        dx = Dx - (Dx * start_t)
        dy = Dy - (Dy * start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        inclusive = True
        coords += [MCoords(s_f1_coords_2, t_f1_coords_2, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        
        annotations += [[1, 1, 'shrink' , 0, end_t, 1]]
        
        # Hole.
        
        _min_up_percent = 0.21
        _max_up_percent = 0.47
            
        _min_down_percent = 0.27
        _max_down_percent = 0.39
        
        hsl, s_h1_coords_1, t_h1_coords_1 = supp_line_2.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)
        
        start_t = 0.2
        end_t = 0.5
        
        annotations += [[0, 2, 'appear' , start_t, start_t, 1]]
        annotations += [[0, 2, 'grow' , start_t, end_t, 1]]
        
        dx1 = Dx * 0.3
        dy1 = Dy * 0.3
        
        x1 = Dx * 0.2
        y1 = Dy * 0.2
        
        inclusive = False
        end_as_line = None
        geom_type = 0
        geom_id = 2
        geom_parent_id = 1
        
        coords += [MCoords(s_h1_coords_1, t_h1_coords_1, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        
        s_h1_coords_2, t_h1_coords_2 = burst(t_h1_coords_1, t_f1_coords_2, 0.1, 0.35)
        
        start_t = 0.5
        end_t = 1
        
        annotations += [[0, 2, 'grow' , start_t, end_t, 1]]
        annotations += [[0, 2, 'burst' , end_t, end_t, 1]]
        annotations += [[0, 2, 'disappear' , end_t, end_t, 1]]
        
        dx1 = Dx - (Dx * start_t)
        dy1 = Dy - (Dy * start_t)
        
        x1 = Dx * start_t
        y1 = Dy * start_t
        
        inclusive = True
        coords += [MCoords(s_h1_coords_2, t_h1_coords_2, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
    # Hole split face.
    elif op == 9:
        # Face.
        
        min_percent = (1 / 0.05)
        max_percent = (1 / 0.25)
        
        supp_line_1, s_f1_coords_1, t_f1_coords_1 = support_line.shrink(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.5
        
        dx = Dx * end_t
        dy = Dy * end_t
        
        x = 0
        y = 0
        
        inclusive = False
        end_as_line = None
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        coords += [MCoords(s_f1_coords_1, t_f1_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
                
        supp_line_1.insert_vertices(0, 9, 17)
        supp_line_2, s_f1_coords_2, t_f1_coords_2 = supp_line_1.shrink(min_percent, max_percent)

        start_t = 0.5
        end_t = 1
        
        dx = Dx * start_t
        dy = Dy * start_t
        
        x = Dx * start_t
        y = Dy * start_t
        
        inclusive = True       
        coords += [MCoords(s_f1_coords_2, t_f1_coords_2, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        
        annotations += [[1, 1, 'shrink' , 0, end_t, 1]]
        
        # Hole
        
        _min_up_percent = 0.21
        _max_up_percent = 0.47
            
        _min_down_percent = 0.27
        _max_down_percent = 0.39
        
        hsl, s_h1_coords_1, t_h1_coords_1 = supp_line_2.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)

        start_t = 0.2
        end_t = 0.5
        
        annotations += [[0, 2, 'appear' , start_t, start_t, 1]]

        dx1 = Dx * 0.3
        dy1 = Dy * 0.3
        
        x1 = Dx * 0.2
        y1 = Dy * 0.2
        
        inclusive = False
        end_as_line = None
        geom_type = 0
        geom_id = 2
        geom_parent_id = 1
        
        coords += [MCoords(s_h1_coords_1, t_h1_coords_1, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        s_h1_coords_2, t_h1_coords_2 = hole_split_face(t_h1_coords_1, t_f1_coords_2, 0.1, 0.35)
        
        start_t = 0.5
        end_t = 1
        
        annotations += [[0, 2, 'grow' , 0, end_t, 1]]
        annotations += [[0, 2, 'disappear' , end_t, end_t, 1]]
        
        annotations += [[1, 1, 'split' , end_t, end_t, 1]]
        annotations += [[1, 1, 'disappear' , end_t, end_t, 1]]
        
        annotations += [[1, 3, 'appear' , end_t, end_t, 1]]
        annotations += [[1, 4, 'appear' , end_t, end_t, 1]]
        
        dx1 = Dx * start_t
        dy1 = Dy * start_t
        
        x1 = Dx * start_t
        y1 = Dy * start_t
        
        inclusive = True
        coords += [MCoords(s_h1_coords_2, t_h1_coords_2, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
    # Hole split face in n parts.
    elif op == 10:        
        min_percent = (1 / 0.05)
        max_percent = (1 / 0.25)
        
        supp_line_1, s_f1_coords_1, t_f1_coords_1 = support_line.shrink(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.5
        
        dx = Dx * end_t
        dy = Dy * end_t
        
        x = 0
        y = 0
        
        inclusive = False
        end_as_line = None
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        coords += [MCoords(s_f1_coords_1, t_f1_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        
        supp_line_1.insert_vertices(0, 9, 17)

        min_percent = (1 / 0.07)
        max_percent = (1 / 0.19)
        
        supp_line_2, s_f1_coords_2, t_f1_coords_2 = supp_line_1.shrink(min_percent, max_percent)

        start_t = 0.5
        end_t = 1
        
        dx = Dx - (Dx * start_t)
        dy = Dy - (Dy * start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        inclusive = True
        coords += [MCoords(s_f1_coords_2, t_f1_coords_2, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        
        annotations += [[1, 1, 'shrink' , 0, 1, 1]]
        
        # Hole
        
        _min_up_percent = 0.21
        _max_up_percent = 0.47
            
        _min_down_percent = 0.27
        _max_down_percent = 0.39
        
        hsl, s_h1_coords_1, t_h1_coords_1 = supp_line_2.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)
 
        start_t = 0.2
        end_t = 0.5
        
        annotations += [[0, 2, 'appear' , start_t, start_t, 1]]

        dx1 = Dx * 0.3
        dy1 = Dy * 0.3
        
        x1 = Dx * start_t
        y1 = Dy * start_t
        
        inclusive = False
        end_as_line = None
        geom_type = 0
        geom_id = 2
        geom_parent_id = 1
        
        coords += [MCoords(s_h1_coords_1, t_h1_coords_1, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        
        _n_parts = np.random.randint(3, 5)
        
        s_h1_coords_2, t_h1_coords_2 = hole_split_face_n(t_h1_coords_1, t_f1_coords_2, 0.1, 0.35, _n_parts)
        
        start_t = 0.5
        end_t = 1
        
        annotations += [[0, 2, 'grow' , 0.2, end_t, 1]]
        annotations += [[0, 2, 'disappear' , end_t, end_t, 1]]
        
        annotations += [[1, 1, 'split' , end_t, end_t, 1]]
        annotations += [[1, 1, 'disappear' , end_t, end_t, 1]]
        
        annotations += [[1, 3, 'appear' , end_t, end_t, 1]]
        annotations += [[1, 4, 'appear' , end_t, end_t, 1]]
        
        dx1 = Dx - (Dx * start_t)
        dy1 = Dy - (Dy * start_t)
        
        x1 = Dx * start_t
        y1 = Dy * start_t
        
        inclusive = True
        coords += [MCoords(s_h1_coords_2, t_h1_coords_2, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
    # Burst.
    elif op == 11:
        min_percent = (1 / 0.05)
        max_percent = (1 / 0.15)
        
        supp_line_1, s_f1_coords_1, t_f1_coords_1 = support_line.shrink(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.5
        
        dx = Dx * end_t
        dy = Dy * end_t
        
        x = 0
        y = 0
        
        inclusive = False
        end_as_line = None
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        coords += [MCoords(s_f1_coords_1, t_f1_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        # 2.
        
        supp_line_1.insert_vertices(0, 9, 17)

        min_percent = (1 / 0.04)
        max_percent = (1 / 0.17)

        supp_line_2, s_f1_coords_2, t_f1_coords_2 = supp_line_1.shrink(min_percent, max_percent)

        start_t = 0.5
        end_t = 0.8
        
        dx = Dx * 0.3
        dy = Dy * 0.3
        
        x = Dx * start_t
        y = Dy * start_t
                
        coords += [MCoords(s_f1_coords_2, t_f1_coords_2, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        # 3.
        
        min_percent = (1 / 0.05)
        max_percent = (1 / 0.15)

        supp_line_3, s_f1_coords_3, t_f1_coords_3 = supp_line_2.shrink(min_percent, max_percent)

        start_t = 0.8
        end_t = 1
        
        dx = Dx * 0.2
        dy = Dy * 0.2
        
        x = Dx * start_t
        y = Dy * start_t
        
        inclusive = True
        coords += [MCoords(s_f1_coords_3, t_f1_coords_3, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        
        annotations += [[1, 1, 'shrink' , 0, 1, 1]]
        
        # Hole
        
        # 1.
        
        _min_up_percent = 0.21
        _max_up_percent = 0.47
            
        _min_down_percent = 0.27
        _max_down_percent = 0.39
        
        hsl, s_h1_coords_1, t_h1_coords_1 = supp_line_2.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)

        start_t = 0.2
        end_t = 0.5
        
        annotations += [[0, 2, 'appear' , start_t, start_t, 1]]
        #annotations += [[0, 2, 'grow' , start_t, end_t, 1]]
        
        dx1 = Dx * 0.3
        dy1 = Dy * 0.3
        
        x1 = Dx * start_t
        y1 = Dy * start_t
        
        inclusive = False
        end_as_line = None
        geom_type = 0
        geom_id = 2
        geom_parent_id = 1
        
        coords += [MCoords(s_h1_coords_1, t_h1_coords_1, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        
        # 2. 
        
        s_h1_coords_2, t_h1_coords_2, _idx_1, _idx_2 = burst_2(t_h1_coords_1, t_f1_coords_2, 0.05, 0.2)
        
        start_t = 0.5
        end_t = 0.8
        
        annotations += [[0, 2, 'grow' , 0.2, end_t, 1]]
        annotations += [[0, 2, 'burst' , end_t, end_t, 1]]
        annotations += [[0, 2, 'disappear' , end_t, end_t, 1]]
        
        dx1 = Dx * 0.3
        dy1 = Dy * 0.3
        
        x1 = Dx * start_t
        y1 = Dy * start_t

        coords += [MCoords(s_h1_coords_2, t_h1_coords_2, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        # 3.
        
        _min_t = 0.05
        _max_t = 0.13
        
        s_h1_coords_3, t_h1_coords_3 = transform_after_event(t_h1_coords_2, t_f1_coords_3, _min_t, _max_t, _idx_1, _idx_2)

        start_t = 0.8
        end_t = 1
        
        #annotations += [[0, 2, 'grow' , start_t, end_t, 1]]
        
        dx1 = Dx * 0.2
        dy1 = Dy * 0.2
        
        x1 = Dx * start_t
        y1 = Dy * start_t
        
        inclusive = True
        coords += [MCoords(s_h1_coords_3, t_h1_coords_3, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
    # Grow.
    elif op == 12:
        low = 1
        high = 10
        
        N = np.random.randint(low, high)
            
        min_percent =  0.02
        max_percent = 0.05
        
        if N < 3:
            min_percent =  0.035
            max_percent = 0.169
        
        min_percent = 1 / min_percent
        max_percent = 1 / max_percent
        
        dt = float(1) / (N + 1)
        
        start_t =  0
        end_t = 0
        
        end_as_line = None
        
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        annotations += [[1, 1, 'grow' , 0, 1, 1]]
        
        xstep = Dx / N
        ystep = Dy / N
        
        i = 0
        while i < N:
            f1_supp_line, s_f1_coords, t_f1_coords = support_line.grow(min_percent, max_percent)
        
            inclusive = False
            
            if i == N - 1:
                dx = Dx - xstep * (i)
                dy = Dy - ystep * (i)
                
                end_t = 1
                inclusive = True
            else:
                dx = xstep
                dy = ystep
            
                end_t += dt
            
            x = xstep * (i)
            y = ystep * (i)

            coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
            
            if change_topology == 1:
                _max_num_vertices = 11
                _add_min_vertices = 0
                _add_max_vertices = np.random.randint(_add_min_vertices + 1, _max_num_vertices)
                _num_pos = np.random.randint(_add_max_vertices + 1, _max_num_vertices + _max_num_vertices - 1)
                
                f1_supp_line.insert_vertices(_add_min_vertices, _add_max_vertices, _num_pos)
            
            support_line = f1_supp_line
            start_t = end_t
            i += 1
    # Evolve.
    elif op == 13:
        low = 1
        high = 10
        
        N = np.random.randint(low, high)
            
        min_percent =  0.02
        max_percent = 0.05
        
        if N < 3:
            min_percent =  0.035
            max_percent = 0.169
        
        min_percent = 1 / min_percent
        max_percent = 1 / max_percent
        
        dt = float(1) / (N + 1)
        
        end_as_line = None
        
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        start_t =  0
        end_t = 0
        
        annotations += [[1, 1, 'evolve' , 0, 1, 1]]
        
        xstep = Dx / N
        ystep = Dy / N
        
        i = 0
        while i < N:
            f1_supp_line, s_f1_coords, t_f1_coords = support_line.evolve(min_percent, max_percent)
        
            inclusive = False
            
            if i == N - 1:
                dx = Dx - xstep * (i)
                dy = Dy - ystep * (i)
            
                end_t = 1
                inclusive = True
            else:
                dx = xstep
                dy = ystep
            
                end_t += dt
        
            x = xstep * (i)
            y = ystep * (i)

            coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

            if change_topology == 1:
                _max_num_vertices = 11
                _add_min_vertices = 0
                _add_max_vertices = np.random.randint(_add_min_vertices + 1, _max_num_vertices)
                _num_pos = np.random.randint(_add_max_vertices + 1, _max_num_vertices + _max_num_vertices - 1)
                f1_supp_line.insert_vertices(_add_min_vertices, _add_max_vertices, _num_pos)
            
            support_line = f1_supp_line
            start_t = end_t
            i += 1
    # Hole split face from closed line.
    elif op == 14:
        # Transform 1 (Hole develops inside face)   
        N = 3
            
        min_percent =  0.02
        max_percent = 0.05
                
        min_percent = 1 / min_percent
        max_percent = 1 / max_percent
        
        dt = float(1) / (N + 1)
        
        start_t =  0
        end_t = 0
        
        xstep = Dx / N
        ystep = Dy / N
        
        end_as_line = None
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        annotations += [[1, 1, 'evolve' , 0, 1, 1]]
        
        i = 0
        while i < N:
            f1_supp_line, s_f1_coords, t_f1_coords = support_line.evolve(min_percent, max_percent)
        
            inclusive = False
            
            if i == N - 1:
                dx = Dx - xstep * (i)
                dy = Dy - ystep * (i)
            
                end_t = 1
                inclusive = True
            else:
                dx = xstep
                dy = ystep
            
                end_t += dt
        
            x = xstep * (i)
            y = ystep * (i)
        
            coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

            if change_topology == 1:
                _max_num_vertices = 11
                _add_min_vertices = 0
                _add_max_vertices = np.random.randint(_add_min_vertices + 1, _max_num_vertices)              
                _num_pos = np.random.randint(_add_max_vertices + 1, _max_num_vertices + _max_num_vertices - 1)
                
                f1_supp_line.insert_vertices(_add_min_vertices, _add_max_vertices, _num_pos)
            
            support_line = f1_supp_line
            start_t = end_t        
            i += 1

        # Hole
        
        seg_max_len = seg_min_len - seg_min_len / 8
        seg_min_len /= 2

        up_down_min_len = 3
        up_down_max_len = 6
        num_segs = 4
        
        h1_support_line = SupportLine(num_segs, seg_min_len, seg_max_len, up_down_min_len, up_down_max_len)
      
        n_vertices = np.random.randint(18, 48)
      
        max_len = min_len - min_len * 0.55
        min_len /= 4
        
        h1_support_line.generate_vertices( n_vertices, 
                                                           MIN_NUM_VERTICES_IN_EXT_SEG,
                                                           MIN_NUM_VERTICES_IN_INT_SEG,
                                                           -1,
                                                           min_num_inactive_vertices_per_seg,
                                                           max_num_inactive_vertices_per_seg,
                                                           min_len, 
                                                           max_len, 
                                                           use_inactive_points)
        
        # >>>
        
        segs1 = support_line.get_segs()
        segs2 = h1_support_line.get_segs()
        
        x0 = segs1[0].coords[0][0]
        x1 = segs1[len(segs1) - 1].coords[1][0]
        
        _x0 = segs2[0].coords[0][0]
        _x1 = segs2[len(segs2) - 1].coords[1][0]

        dxx = np.random.uniform(0, x1 - (_x1 - _x0))
        
        # >>>
        
        min_percent =  1 / 0.03
        max_percent = 1 / 0.07
        
        h1_supp_line, s_h1_coords, t_h1_coords = h1_support_line.grow(min_percent, max_percent)
        
        start_t = 0.2
        end_t = 0.4

        annotations += [[1, 1, 'internal split (caused by a closed fissure)' , start_t, start_t, 1]]
        annotations += [[0, 2, 'appear' , start_t, start_t, 1]]
        annotations += [[0, 2, 'grow' , start_t, end_t, 1]]
            
        dx = Dx * 0.2
        dy = Dy * 0.2
            
        x = Dx * start_t
        y = Dy * start_t
            
        end_as_line = None
        inclusive = False
        geom_type = 0
        geom_id = 2
        geom_parent_id = 1
        
        coords += [MCoords(s_h1_coords, t_h1_coords, start_t, end_t, dx, dy, x + dxx, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        _max_num_vertices = 9
        _add_min_vertices = 4
        _add_max_vertices = np.random.randint(_add_min_vertices + 1, _max_num_vertices)
        _num_pos = np.random.randint(_add_max_vertices + 1, _max_num_vertices + _max_num_vertices - 1)
        
        h1_supp_line.insert_vertices(_add_min_vertices, _add_max_vertices, _num_pos)
        
        min_percent =  1 / 0.05
        max_percent = 1 / 0.1

        h1_supp_line, s_h1_coords, t_h1_coords = h1_supp_line.evolve(min_percent, max_percent)
        
        start_t = 0.4
        end_t = 1
    
        annotations += [[0, 2, 'evolve' , start_t, end_t, 1]]

        dx = Dx * 0.6
        dy = Dy * 0.6
            
        x = Dx * start_t
        y = Dy * start_t

        inclusive = True
        coords += [MCoords(s_h1_coords, t_h1_coords, start_t, end_t, dx, dy, x + dxx, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        ## Face Inside Hole

        min_percent =  1 / 0.35
        max_percent = 1 / 0.47
        
        f2_supp_line, s_f2_coords, t_f2_coords = h1_support_line.shrink(min_percent, max_percent)
        
        start_t = 0.2
        end_t = 0.5
        
        annotations += [[1, 3, 'appear' , start_t, start_t, 1]]
        annotations += [[1, 3, 'shrink' , start_t, 1, 1]]
        
        dx = Dx * 0.3
        dy = Dy * 0.3
            
        x = Dx * start_t
        y = Dy * start_t
            
        end_as_line = None
        inclusive = False
        geom_type = 1
        geom_id = 3
        geom_parent_id = 2
        
        coords += [MCoords(s_f2_coords, t_f2_coords, start_t, end_t, dx, dy, x + dxx, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        
        _max_num_vertices = 9
        _add_min_vertices = 6
        _add_max_vertices = np.random.randint(_add_min_vertices + 1, _max_num_vertices)
        
        _num_pos = np.random.randint(_add_max_vertices + 1, _max_num_vertices + _max_num_vertices - 1)
        
        f2_supp_line.insert_vertices(_add_min_vertices, _add_max_vertices, _num_pos)
        
        min_percent =  1 / 0.2
        max_percent = 1 / 0.45
        
        f2_supp_line, s_f2_coords, t_f2_coords = f2_supp_line.shrink(min_percent, max_percent)
        
        start_t = 0.5
        end_t = 1
        
        dx = Dx * 0.5
        dy = Dy * 0.5
            
        x = Dx * start_t
        y = Dy * start_t

        inclusive = True
        coords += [MCoords(s_f2_coords, t_f2_coords, start_t, end_t, dx, dy, x + dxx, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
    # Split (Fragment).
    elif op == 15:
        N = 2
        
        min_percent =  0.05
        max_percent = 0.11

        min_percent = 1 / min_percent
        max_percent = 1 / max_percent
        
        dt = float(1) / (N)
        
        start_t =  0
        end_t = 0
        
        end_as_line = None
        
        max_num_vertices = 11
        add_min_vertices = 0
        
        debri_1_supp_line = None
        
        annotations += [[1, 1, 'shrink' , 0, 1, 1]]
        
        xstep = Dx / N
        ystep = Dy / N
        
        i = 0
        while i < N:
            f1_supp_line, s_f1_coords, t_f1_coords = support_line.shrink(min_percent, max_percent)
        
            # Cut
                
            if i == 0:
                # Choose a seg to cut not at the extremities.
                
                num_segs = len(f1_supp_line.get_segs())
                seg_id = np.random.randint(1, num_segs)
                
                p = np.random.uniform(0, 1)
            
                up = 0
                if p >= 0.5:
                     up = 1
            
                f1_supp_line.insert_vertices_on_segs(seg_id, 4, 6, 6, up)
                
                # Cut the geom.
                
                debri_1_supp_line = f1_supp_line.cut(seg_id, up)
                support_line = f1_supp_line
            
            ### >>> End Cut

            inclusive = False
            
            if i == N - 1:
                dx = Dx - xstep * (i)
                dy = Dy - ystep * (i)
            
                end_t = 1
                inclusive = True
            else:
                dx = xstep
                dy = ystep
            
                end_t += dt
        
            x = xstep * (i)
            y = ystep * (i)

            coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]
            start_t = end_t
            i += 1

        ### Cut Interpolation ###

        min_percent = 1 / 0.17
        max_percent = 1 / 0.33

        cut1_supp_line, s_cut1_coords, t_cut1_coords = debri_1_supp_line.shrink(min_percent, max_percent)
        
        start_t = 0.5
        end_t = 1
        
        annotations += [[1, 1, 'split' , start_t, start_t, 1]]
        annotations += [[1, 2, 'appear' , start_t, start_t, 1]]
        annotations += [[1, 2, 'shrink' , start_t, end_t, 1]]
        
        dx = Dx * 0.5
        dy = Dy * 0.5
        
        x = Dx * 0.5
        y = Dy * 0.5
        
        inclusive = True
        coords += [MCoords(s_cut1_coords, t_cut1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 2, 0)]
    # Split (Fragment) 2.
    elif op == 16:
        N = 3
        
        min_percent =  0.05
        max_percent = 0.11

        min_percent = 1 / min_percent
        max_percent = 1 / max_percent
        
        dt = float(1) / (N)
        
        start_t =  0
        end_t = 0
        
        end_as_line = None
        
        max_num_vertices = 11
        add_min_vertices = 0
        
        starting_times = []
        cuts = []
        segs_ids = []
        
        annotations += [[1, 1, 'shrink' , 0, 1, 1]]
        
        xstep = Dx / N
        ystep = Dy / N
        
        i = 0
        while i < N:
            f1_supp_line, s_f1_coords, t_f1_coords = support_line.shrink(min_percent, max_percent)
        
            inclusive = False
            
            if i == N - 1:
                dx = Dx - xstep * (i)
                dy = Dy - ystep * (i)
            
                end_t = 1
                inclusive = True
            else:
                dx = xstep
                dy = ystep
            
                end_t += dt
        
            x = xstep * (i)
            y = ystep * (i)
        
            # Cut
                
            if i == 0:
                # Choose a seg to cut not at the extremities.
                
                num_segs = len(f1_supp_line.get_segs())
                seg_id = np.random.randint(1, num_segs)
                
                p = np.random.uniform(0, 1)
            
                up = 0
                if p >= 0.5:
                     up = 1
            
                f1_supp_line.insert_vertices_on_segs(seg_id, 4, 6, 6, up)
                
                # Cut the geom.
                
                cuts += [f1_supp_line.cut(seg_id, up)]
                starting_times += [end_t]
                segs_ids = [seg_id]
                
                support_line = f1_supp_line
            elif i == 1:
                # Choose a seg to cut not at the extremities.
                
                num_segs = len(f1_supp_line.get_segs())
                
                seg_id = segs_ids[0]
                while seg_id == segs_ids[0]:
                    seg_id = np.random.randint(1, num_segs)
                
                p = np.random.uniform(0, 1)
            
                up = 0
                if p >= 0.5:
                     up = 1
            
                f1_supp_line.insert_vertices_on_segs(seg_id, 4, 6, 6, up)
                
                # Cut the geom.
                
                cuts += [f1_supp_line.cut(seg_id, up)]
                starting_times += [end_t]
                segs_ids = [seg_id]
                
                support_line = f1_supp_line
            
            ### >>> End Cut

            coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]
            
            ### >>> Insert Vertices ###
            
            if change_topology == 1:
                _add_max_vertices = np.random.randint(add_min_vertices + 1, max_num_vertices)
                _num_pos = np.random.randint(_add_max_vertices + 1, max_num_vertices + max_num_vertices - 1)
                f1_supp_line.insert_vertices(add_min_vertices, _add_max_vertices, _num_pos)
                
            ### >>> End Insert Vertices ###
            
            support_line = f1_supp_line
            start_t = end_t
            i += 1

        ### >>> Cut Interpolation ###
        
        add_min_vertices = 6
        
        i = 0
        for cut in cuts:
            min_percent =  0.17
            max_percent = 0.33

            min_percent = 1 / min_percent
            max_percent = 1 / max_percent

            if change_topology == 1:
                _add_max_vertices = np.random.randint(add_min_vertices, max_num_vertices)
                _num_pos = np.random.randint(_add_max_vertices + 1, max_num_vertices + max_num_vertices - 1)
                cut.insert_vertices(add_min_vertices, _add_max_vertices, _num_pos)
            
            cut_supp_line, s_cut_coords, t_cut_coords = cut.shrink(min_percent, max_percent)
        
            start_t = starting_times[i]
            end_t = 1
            inclusive = True
            
            annotations += [[1, 1, 'split' , start_t, start_t, 1]]
            annotations += [[1, i + 2, 'appear' , start_t, start_t, 1]]
            annotations += [[1, i + 2, 'shrink' , start_t, end_t, 1]]
            
            dx = Dx * (end_t - start_t)
            dy = Dy * (end_t - start_t)
            
            x = Dx * start_t
            y = Dy * start_t
            
            coords += [MCoords(s_cut_coords, t_cut_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, i + 2, 0)]
            i+= 1
    # Split (Fragment) 3.
    elif op == 17:
        N = 3
        
        min_percent =  0.05
        max_percent = 0.11

        min_percent = 1 / min_percent
        max_percent = 1 / max_percent
        
        dt = float(1) / (N)
        
        start_t =  0
        end_t = 0
        
        end_as_line = None
        
        max_num_vertices = 11
        add_min_vertices = 0
        
        starting_times = []
        cuts = []
        segs_ids = []
        
        xstep = Dx / N
        ystep = Dy / N
        
        annotations += [[1, 1, 'shrink' , 0, 1, 1]]
        
        i = 0
        while i < N:
            f1_supp_line, s_f1_coords, t_f1_coords = support_line.shrink(min_percent, max_percent)
        
            inclusive = False
            
            if i == N - 1:
                dx = Dx - xstep * (i)
                dy = Dy - ystep * (i)
            
                end_t = 1
                inclusive = True
            else:
                dx = xstep
                dy = ystep
            
                end_t += dt
        
            x = xstep * (i)
            y = ystep * (i)
        
            # Cut
                
            if i == 0:
                # Cut 1

                num_segs = len(f1_supp_line.get_segs())
                seg_id = np.random.randint(1, num_segs)
                
                p = np.random.uniform(0, 1)
            
                up = 0
                if p >= 0.5:
                     up = 1
            
                f1_supp_line.insert_vertices_on_segs(seg_id, 4, 6, 6, up)
                
                # Cut the geom.
                
                cuts += [f1_supp_line.cut(seg_id, up)]
                starting_times += [end_t]
                segs_ids = [seg_id]
                
                # Cut 2

                num_segs = len(f1_supp_line.get_segs())
                
                seg_id = segs_ids[0]
                while seg_id == segs_ids[0]:
                    seg_id = np.random.randint(1, num_segs)
                
                p = np.random.uniform(0, 1)
            
                up = 0
                if p >= 0.5:
                     up = 1
            
                f1_supp_line.insert_vertices_on_segs(seg_id, 4, 6, 6, up)
                
                # Cut the geom.
                
                cuts += [f1_supp_line.cut(seg_id, up)]
                starting_times += [end_t]
                segs_ids = [seg_id]
                
                support_line = f1_supp_line
            
            ### >>> End Cut
                
            coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

            ### >>> End Interpolate ###
            
            ### >>> Insert Vertices ###
            
            if change_topology == 1:
                _add_max_vertices = np.random.randint(add_min_vertices + 1, max_num_vertices)
                _num_pos = np.random.randint(_add_max_vertices + 1, max_num_vertices + max_num_vertices - 1)
                f1_supp_line.insert_vertices(add_min_vertices, _add_max_vertices, _num_pos)
                
            support_line = f1_supp_line
            start_t = end_t
            i += 1

        ### >>> Cut Interpolation ###
        
        add_min_vertices = 6
        
        i = 0
        for cut in cuts:
            min_percent =  0.17
            max_percent = 0.33

            min_percent = 1 / min_percent
            max_percent = 1 / max_percent

            if change_topology == 1:
                _add_max_vertices = np.random.randint(add_min_vertices, max_num_vertices)
                _num_pos = np.random.randint(_add_max_vertices + 1, max_num_vertices + max_num_vertices - 1)
                cut.insert_vertices(add_min_vertices, _add_max_vertices, _num_pos)
            
            cut_supp_line, s_cut_coords, t_cut_coords = cut.shrink(min_percent, max_percent)
        
            start_t = starting_times[i]
            end_t = 1
            inclusive = True
            
            annotations += [[1, 1, 'split' , start_t, start_t, 1]]
            annotations += [[1, i + 2, 'appear' , start_t, start_t, 1]]
            annotations += [[1, i + 2, 'shrink' , start_t, end_t, 1]]
            
            dx = Dx * (end_t - start_t)
            dy = Dy * (end_t - start_t)
            
            x = Dx * start_t
            y = Dy * start_t
            
            coords += [MCoords(s_cut_coords, t_cut_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, i + 2, 0)]
            i+= 1
    # Split at Point ('Stay connected').
    elif op == 18:
        min_percent =  0.05
        max_percent = 0.11

        min_percent = 1 / min_percent
        max_percent = 1 / max_percent
                
        start_t =  0
        end_t = 0
        
        end_as_line = None
        
        max_num_vertices = 11
        add_min_vertices = 0

        unit_id = 1
        
        parts = []
        times = []
        
        i = 0
        seg_id = -1
        inclusive = False
        
        end_t = 0.25
        
        f1_supp_line, s_f1_coords, t_f1_coords = support_line.shrink(min_percent, max_percent)
        annotations += [[1, 1, 'shrink' , start_t, end_t, unit_id]]
        
        dx = Dx * end_t
        dy = Dy * end_t
        
        x = 0
        y = 0
        
        coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]
      
        # split.
        
        num_segs = len(f1_supp_line.get_segs())
        seg_id = np.random.randint(1, num_segs)
                
        #f1_supp_line.insert_vertices_on_segs(seg_id, 7, 9, 11, 1)
        #f1_supp_line.insert_vertices_on_segs(seg_id, 6, 7, 9, 0)
        
        f1_supp_line.insert_vertices_on_segs(seg_id, 15, 19, 21, 1)
        f1_supp_line.insert_vertices_on_segs(seg_id, 16, 17, 26, 0)
          
        #f1_supp_line, s_f1_coords, t_f1_coords, idx_1_up, idx_1_down, idx_2_up, idx_2_down = f1_supp_line.split_to_point_preproccess(seg_id, min_percent, max_percent)
        
        f1_supp_line, s_f1_coords, t_f1_coords, idx_1_up, idx_1_down, idx_2_up, idx_2_down = f1_supp_line.split_to_point_preproccessing(seg_id, min_percent, max_percent)
        
        start_t = end_t
        end_t = 0.5
        
        annotations += [[1, 1, 'shrink' , start_t, end_t, unit_id]]
        annotations += [[1, 1, 'initiate split' , start_t, end_t, unit_id]]
        annotations += [[1, 1, 'split' , end_t, end_t, unit_id]]
        annotations += [[1, 1, 'disappear' , end_t, end_t, unit_id]]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]
      
        parts = f1_supp_line.split_to_parts(seg_id, idx_1_up, idx_1_down, idx_2_up, idx_2_down)
        
        start_t = end_t
        end_t = 1
        
        inclusive = True
        
        j = 0
        for part in parts:
            dx = Dx * (end_t - start_t)
            dy = Dy * (end_t - start_t)
            
            x = Dx * start_t
            y = Dy * start_t

            p_supp_line, s_p_coords, t_p_coords = part.shrink(min_percent, max_percent)
            coords += [MCoords(s_p_coords, t_p_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, j + 2, 0)]
            
            annotations += [[1, j + 2, 'appear' , start_t, start_t, unit_id]]
            annotations += [[1, j + 2, 'shrink' , start_t, end_t, unit_id]]
            j += 1
    # Split to Point ('Move away from each other').
    elif op == 19:
        min_percent =  0.05
        max_percent = 0.11

        min_percent = 1 / min_percent
        max_percent = 1 / max_percent
        
        start_t =  0
        end_t = 0
        
        end_as_line = None
        
        max_num_vertices = 11
        add_min_vertices = 0
        
        unit_id = 1
        parts = []
        i = 0      
        seg_id = -1
               
        inclusive = False
        end_t = 0.25
        
        f1_supp_line, s_f1_coords, t_f1_coords = support_line.shrink(min_percent, max_percent)
        annotations += [[1, 1, 'shrink' , start_t, end_t, unit_id]]
        
        dx = Dx * end_t
        dy = Dy * end_t
        
        x = 0
        y = 0
        
        coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]
      
        # split.
        
        num_segs = len(f1_supp_line.get_segs())
        seg_id = np.random.randint(1, num_segs)
                
        f1_supp_line.insert_vertices_on_segs(seg_id, 4, 6, 11, 1)
        f1_supp_line.insert_vertices_on_segs(seg_id, 4, 7, 9, 0)
                
        f1_supp_line, s_f1_coords, t_f1_coords, idx_1_up, idx_1_down, idx_2_up, idx_2_down = f1_supp_line.split_to_point_preproccess(seg_id, min_percent, max_percent)
        
        start_t = end_t
        end_t = 0.5
        
        annotations += [[1, 1, 'shrink' , start_t, end_t, unit_id]]
        annotations += [[1, 1, 'initiate split' , start_t, end_t, unit_id]]
        annotations += [[1, 1, 'split' , end_t, end_t, unit_id]]
        annotations += [[1, 1, 'disappear' , end_t, end_t, unit_id]]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]
        
        # >>>
        
        parts = f1_supp_line.split_to_parts(seg_id, idx_1_up, idx_1_down, idx_2_up, idx_2_down)
        
        start_t = end_t
        end_t = 1
        
        inclusive = True
        translation = [[-np.random.randint(50, 177), np.random.randint(25, 100)], [np.random.randint(60, 121), -np.random.randint(25, 100)]]
        
        insert_vertices = True
        add_min_vertices = 17
        max_num_vertices = 23
        
        j = 0
        for part in parts:
            if change_topology == 1:
                _add_max_vertices = np.random.randint(add_min_vertices + 1, max_num_vertices)
                _num_pos = np.random.randint(_add_max_vertices + 1, max_num_vertices + 7)
                part.insert_vertices(add_min_vertices, _add_max_vertices, _num_pos)
        
            dx = translation[j][0]
            dy = translation[j][1]
            
            x = Dx * start_t
            y = Dy * start_t
        
            min_percent = 1 / 0.17
            max_percent = 1 / 0.31
        
            p_supp_line, s_p_coords, t_p_coords = part.shrink(min_percent, max_percent)
            coords += [MCoords(s_p_coords, t_p_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, j + 2, 0)]
            
            annotations += [[1, j + 2, 'appear' , start_t, start_t, unit_id]]
            annotations += [[1, j + 2, 'shrink' , start_t, end_t, unit_id]]
            j += 1
    # Appear Hole + Hole split.
    elif op == 20:
        unit_id = 1
        
        min_percent = (1 / 0.05)               # 5%
        max_percent = (1 / 0.17)              # 17%
        
        inclusive = False
        end_as_line = None
        
        supp_line_1, source_coords_1, target_coords_1 = support_line.evolve(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.4
        
        annotations += [[1, 1, 'evolve' , start_t, end_t, unit_id]]
        
        dx = Dx * end_t
        dy = Dy * end_t
        
        x = 0
        y = 0
        
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        max_num_vertices = 11
        add_min_vertices = 7
        
        _add_max_vertices = np.random.randint(add_min_vertices, max_num_vertices)
        _num_pos = _add_max_vertices
       
        supp_line_1.insert_vertices(add_min_vertices, _add_max_vertices, _num_pos)
        
        min_percent = (1 / 0.07)               # 5%
        max_percent = (1 / 0.21)              # 17%
        
        supp_line_1, source_coords_1, target_coords_1 = supp_line_1.shrink(min_percent, max_percent)
        
        start_t = 0.4
        end_t = 1
        
        annotations += [[1, 1, 'shrink' , start_t, end_t, unit_id]]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        inclusive = True
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        #
        
        _min_up_percent = 0.10
        _max_up_percent = 0.20
            
        _min_down_percent = 0.05
        _max_down_percent = 0.20

        h1_sl, h1_s_coords, h1_t_coords = supp_line_1.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)
        
        start_t = 0.4
        end_t = 0.6
        
        annotations += [[0, 2, 'appear' , start_t, start_t, unit_id]]
        annotations += [[0, 2, 'grow' , start_t, end_t, unit_id]]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        inclusive = False
        coords += [MCoords(h1_s_coords, h1_t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, 2, 1)]
        
        num_segs = len(h1_sl.get_segs())
        seg_id = np.random.randint(1, num_segs)
                
        h1_sl.insert_vertices_on_segs(seg_id, 4, 6, 7, 1)
        h1_sl.insert_vertices_on_segs(seg_id, 6, 9, 9, 0)

        min_percent = 1 / 0.05
        max_percent = 1 / 0.11
        
        h1_sl, h1_s_coords, h1_t_coords, idx_1_up, idx_1_down, idx_2_up, idx_2_down = h1_sl.split_to_point_preproccess(seg_id, min_percent, max_percent)
        
        start_t = 0.6
        end_t = 0.75
        
        annotations += [[0, 2, 'initiate split' , start_t, end_t, unit_id]]
        annotations += [[0, 2, 'split' , end_t, end_t, unit_id]]
        annotations += [[0, 2, 'disappear' , end_t, end_t, unit_id]]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        coords += [MCoords(h1_s_coords, h1_t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, 1, 1)]
      
        parts = h1_sl.split_to_parts(seg_id, idx_1_up, idx_1_down, idx_2_up, idx_2_down)
        
        start_t = 0.75
        end_t = 1
        
        min_percent = 1 / 0.05
        max_percent = 1 / 0.11
        
        inclusive = True
        translation = [[-np.random.randint(10, 17), np.random.randint(5, 11)], [np.random.randint(5, 11), -np.random.randint(7, 13)]]
        
        _dx = Dx * (end_t - start_t)
        _dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        j = 0
        for part in parts:
            min_percent = 1 / 0.09
            max_percent = 1 / 0.21
            sl, s_coords, t_coords = part.shrink(min_percent, max_percent)
            
            dx = translation[j][0] + _dx
            dy = translation[j][1] + _dy
            
            coords += [MCoords(s_coords, t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, j + 1, 1)]
            
            annotations += [[0, j + 3, 'appear' , end_t, end_t, unit_id]]
            annotations += [[0, j + 3, 'shrink' , start_t, end_t, unit_id]]
            j += 1
    # Hole disappear to open line.
    elif op == 21:
        unit_id = 1
        
        min_percent = (1 / 0.05)               # 5%
        max_percent = (1 / 0.17)              # 17%
        
        inclusive = False
        end_as_line = None
        
        supp_line_1, source_coords_1, target_coords_1 = support_line.evolve(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.4
        
        annotations += [[1, 1, 'evolve' , start_t, end_t, unit_id]]
        
        dx = Dx * end_t
        dy = Dy * end_t
        
        x = 0
        y = 0
        
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        max_num_vertices = 11
        add_min_vertices = 7
        
        _add_max_vertices = np.random.randint(add_min_vertices, max_num_vertices)
        _num_pos = _add_max_vertices
        supp_line_1.insert_vertices(add_min_vertices, _add_max_vertices, _num_pos)
        
        min_percent = (1 / 0.07)               # 5%
        max_percent = (1 / 0.21)              # 17%
        
        supp_line_1, source_coords_1, target_coords_1 = supp_line_1.shrink(min_percent, max_percent)
        
        start_t = 0.4
        end_t = 1
        
        annotations += [[1, 1, 'shrink' , start_t, end_t, unit_id]]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        inclusive = True
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        # >>> Generate hole
        
        _min_up_percent = 0.10
        _max_up_percent = 0.20
            
        _min_down_percent = 0.05
        _max_down_percent = 0.20

        h1_sl, h1_s_coords, h1_t_coords = supp_line_1.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)
        
        num_segs = len(h1_sl.get_segs())
        seg_id = np.random.randint(1, num_segs)
                
        h1_sl.insert_vertices_on_segs(seg_id, 4, 6, 7, 1)
        h1_sl.insert_vertices_on_segs(seg_id, 6, 9, 9, 0)

        min_percent = 1 / 0.05
        max_percent = 1 / 0.11
        
        h1_sl, h1_s_coords, h1_t_coords, idx_1_up, idx_1_down, idx_2_up, idx_2_down = h1_sl.split_to_point_preproccess(seg_id, min_percent, max_percent)

        parts = h1_sl.split_to_parts(seg_id, idx_1_up, idx_1_down, idx_2_up, idx_2_down)
        
        min_percent = 1 / 0.05
        max_percent = 1 / 0.21
        
        inclusive = False
        start_t = 0
        end_t = 0.6
        
        geom_id = -1
        rand = np.random.uniform(0, 1)
        
        if rand <= 0.5:
            geom_id = 0
        else:
            geom_id = 1
        
        annotations += [[0, 2, 'shrink' , start_t, end_t, unit_id]]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        hsl, hs_coords, ht_coords = parts[geom_id].shrink(min_percent, max_percent)
        coords += [MCoords(hs_coords, ht_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, 1, 1)]
        
        inclusive = True
        start_t = 0.6
        end_t = 1
        
        annotations += [[0, 2, 'initiate disappear' , start_t, end_t, unit_id]]
        annotations += [[0, 2, 'disappear' , end_t, end_t, unit_id]]
        
        end_as_line = Polygon()
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        hsl, hs_coords, ht_coords = hsl.disappear_to_line()
        coords += [MCoords(hs_coords, ht_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, 1, 1)]
    # Face split hole in 2 parts.
    elif op == 22:
        unit_id = 1
        
        min_percent = (1 / 0.05)               # 5%
        max_percent = (1 / 0.17)              # 17%
        
        inclusive = False
        end_as_line = None
        
        supp_line_1, source_coords_1, target_coords_1 = support_line.evolve(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.4
        
        annotations += [[1, 1, 'evolve' , start_t, end_t, unit_id]]
        
        dx = Dx * end_t
        dy = Dy * end_t
        
        x = 0
        y = 0
        
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        max_num_vertices = 11
        add_min_vertices = 7
        
        _add_max_vertices = np.random.randint(add_min_vertices, max_num_vertices)
        _num_pos = _add_max_vertices
    
        supp_line_1.insert_vertices(add_min_vertices, _add_max_vertices, _num_pos)
        
        min_percent = (1 / 0.07)               # 5%
        max_percent = (1 / 0.21)              # 17%
        
        supp_line_1, source_coords_1, target_coords_1 = supp_line_1.shrink(min_percent, max_percent)
        
        start_t = 0.4
        end_t = 1
        
        annotations += [[1, 1, 'shrink' , start_t, end_t, unit_id]]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        inclusive = True
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        # >>> Generate hole
                
        _min_up_percent = 0.19
        _max_up_percent = 0.37
            
        _min_down_percent = 0.21
        _max_down_percent = 0.39

        h1_sl, h1_s_coords, h1_t_coords = supp_line_1.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)
        
        num_segs = len(h1_sl.get_segs())
        seg_id = np.random.randint(1, num_segs)
                
        h1_sl.insert_vertices_on_segs(seg_id, 4, 6, 7, 1)
        h1_sl.insert_vertices_on_segs(seg_id, 6, 9, 9, 0)

        min_percent = 1 / 0.05
        max_percent = 1 / 0.11
        
        h1_sl, h1_s_coords, h1_t_coords, idx_1_up, idx_1_down, idx_2_up, idx_2_down = h1_sl.split_to_point_preproccess(seg_id, min_percent, max_percent)
                
        parts = h1_sl.split_to_parts(seg_id, idx_1_up, idx_1_down, idx_2_up, idx_2_down)
        
        min_percent = 1 / 0.11
        max_percent = 1 / 0.21
        
        inclusive = True
        start_t = 0
        end_t = 1
        
        geom_id = -1
        rand = np.random.uniform(0, 1)
        
        if rand <= 0.5:
            geom_id = 0
        else:
            geom_id = 1
        
        annotations += [[0, 2, 'grow' , start_t, end_t, unit_id]]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        hsl, hs_coords, ht_coords = parts[geom_id].grow(min_percent, max_percent)
        coords += [MCoords(hs_coords, ht_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, 2, 1)]
        
        min1 = 0.11
        max1 = 0.27
        min2 = 0.09
        max2 = 0.22
        
        gsl = parts[geom_id].generate_geom_from_sp_line(min1, max1, min2, max2)
        
        min_percent = 1 / 0.11
        max_percent = 1 / 0.21
        
        gsl, gs_coords, gt_coords = gsl.grow(min_percent, max_percent)
        
        inclusive = False
        start_t = 0
        end_t = 0.5
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        coords += [MCoords(gs_coords, gt_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 3, 2)]
        
        inclusive = True
        start_t = 0.5
        end_t = 1
        
        _min_t = 0.1
        _max_t = 0.35
        
        annotations += [[1, 3, 'grow' , 0, end_t, unit_id]]
        annotations += [[1, 3, 'merge(1, 3)' , start_t, end_t, unit_id]]
        annotations += [[0, 2, 'split' , end_t, end_t, unit_id]]
        annotations += [[0, 2, 'disappear' , end_t, end_t, unit_id]]
        annotations += [[1, 3, 'disappear' , end_t, end_t, unit_id]]
        annotations += [[0, 4, 'appear' , end_t, end_t, unit_id]]
        annotations += [[0, 5, 'appear' , end_t, end_t, unit_id]]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        s_coords, t_coords_ = hole_split_face(gt_coords, ht_coords, _min_t, _max_t)
        coords += [MCoords(s_coords, t_coords_, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 2, 1)]
    # Face split hole and consumes it.
    elif op == 23:
        unit_id = 1
        
        min_percent = (1 / 0.05)               # 5%
        max_percent = (1 / 0.17)              # 17%
        
        inclusive = False
        end_as_line = None
        
        supp_line_1, source_coords_1, target_coords_1 = support_line.evolve(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.4
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        annotations += [[1, 1, 'evolve' , start_t, end_t, unit_id]]
        
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        max_num_vertices = 11
        add_min_vertices = 7
        
        _add_max_vertices = np.random.randint(add_min_vertices, max_num_vertices)
        _num_pos = _add_max_vertices
                
        supp_line_1.insert_vertices(add_min_vertices, _add_max_vertices, _num_pos)
        
        min_percent = (1 / 0.07)               # 5%
        max_percent = (1 / 0.21)              # 17%
        
        supp_line_1, source_coords_1, target_coords_1 = supp_line_1.shrink(min_percent, max_percent)
        
        start_t = 0.4
        end_t = 1
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        annotations += [[1, 1, 'shrink' , start_t, end_t, unit_id]]
        
        inclusive = True
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        # Generate Hole
        
        _min_up_percent = 0.19
        _max_up_percent = 0.37
            
        _min_down_percent = 0.17
        _max_down_percent = 0.39

        h1_sl, h1_s_coords, h1_t_coords = supp_line_1.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)
        
        num_segs = len(h1_sl.get_segs())
        seg_id = np.random.randint(1, num_segs)
                
        h1_sl.insert_vertices_on_segs(seg_id, 4, 6, 7, 1)
        h1_sl.insert_vertices_on_segs(seg_id, 6, 9, 9, 0)

        min_percent = 1 / 0.05
        max_percent = 1 / 0.11
        
        h1_sl, h1_s_coords, h1_t_coords, idx_1_up, idx_1_down, idx_2_up, idx_2_down = h1_sl.split_to_point_preproccess(seg_id, min_percent, max_percent)

        parts = h1_sl.split_to_parts(seg_id, idx_1_up, idx_1_down, idx_2_up, idx_2_down)
        
        min_percent = 1 / 0.11
        max_percent = 1 / 0.21
        
        inclusive = False
        start_t = 0
        end_t = 0.5
        
        geom_id = -1
        rand = np.random.uniform(0, 1)
        
        if rand <= 0.5:
            geom_id = 0
        else:
            geom_id = 1
        
        geom = parts[geom_id]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        hsl, hs_coords, ht_coords = geom.grow(min_percent, max_percent)
        coords += [MCoords(hs_coords, ht_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, 2, 1)]
        
        min_percent = 1 / 0.09
        max_percent = 1 / 0.17
        
        hsl, hs_coords1, ht_coords1 = hsl.grow(min_percent, max_percent)
        
        inclusive = True
        end_as_line = Polygon()
        
        start_t = 0.5
        end_t = 1
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        coords += [MCoords(hs_coords1, ht_coords1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, 2, 1)]

        end_as_line = None
        
        min1 = 0.11
        max1 = 0.27
        min2 = 0.09
        max2 = 0.22
        
        gsl = geom.generate_geom_from_sp_line(min1, max1, min2, max2)
        
        min_percent = 1 / 0.11
        max_percent = 1 / 0.21
        
        gsl, gs_coords, gt_coords = gsl.grow(min_percent, max_percent)
        
        inclusive = False
        start_t = 0
        end_t = 0.5
        
        annotations += [[1, 3, 'grow' , start_t, end_t, unit_id]]
        annotations += [[0, 2, 'grow' , start_t, end_t, unit_id]]
        annotations += [[0, 2, 'disappear' , end_t, end_t, unit_id]]
        annotations += [[0, 4, 'appear' , end_t, end_t, unit_id]]
        annotations += [[0, 5, 'appear' , end_t, end_t, unit_id]]
        annotations += [[1, 3, 'merge(1, 3)' , end_t, end_t, unit_id]]
        annotations += [[1, 3, 'disappear' , end_t, end_t, unit_id]]

        _min_t = 0.1
        _max_t = 0.35
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        s_coords, t_coords_ = geom_split_geom(gt_coords, ht_coords, _min_t, _max_t)
        coords += [MCoords(s_coords, t_coords_, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 3, 2)]
        
        inclusive = True
        end_as_line = Polygon()
        start_t = 0.5
        end_t = 1
        
        _min_t = 0.99
        _max_t = 1
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        annotations += [[0, 4, 'shrink' , start_t, end_t, unit_id]]
        annotations += [[0, 5, 'shrink' , start_t, end_t, unit_id]]
        annotations += [[0, 4, 'disappear' , end_t, end_t, unit_id]]
        annotations += [[0, 5, 'disappear' , end_t, end_t, unit_id]]

        s_coords, t_coords_ = geom_split_geom(t_coords_, ht_coords1, _min_t, _max_t)
        coords += [MCoords(s_coords, t_coords_, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 3, 2)]
    # Face internal Merge.
    elif op == 24:       
        unit_id = 1
        
        min_percent = (1 / 0.05)               # 5%
        max_percent = (1 / 0.17)              # 17%
        
        inclusive = False
        end_as_line = None
        
        supp_line_1, source_coords_1, target_coords_1 = support_line.evolve(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.4
        
        annotations += [[1, 1, 'evolve' , start_t, end_t, unit_id]]
       
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        max_num_vertices = 11
        add_min_vertices = 7
        
        _add_max_vertices = np.random.randint(add_min_vertices, max_num_vertices)
        _num_pos = _add_max_vertices
                
        supp_line_1.insert_vertices(add_min_vertices, _add_max_vertices, _num_pos)
        
        min_percent = (1 / 0.07)               # 5%
        max_percent = (1 / 0.21)              # 17%
        
        supp_line_1, source_coords_1, target_coords_1 = supp_line_1.shrink(min_percent, max_percent)
        
        start_t = 0.4
        end_t = 1
        
        annotations += [[1, 1, 'shrink' , start_t, end_t, unit_id]]
       
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
       
        inclusive = True
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        # >>> Generate hole

        min1 = 0.10
        max1 = 0.20
        min2 = 0.05
        max2 = 0.20
        
        h1_s_coords, h1_t_coords, h1_sl = supp_line_1.generate_hole_from_sp_line_2(min1, max1, min2, max2)
            
        num_segs = len(h1_sl.get_segs())
        seg_id = np.random.randint(1, num_segs)
                
        h1_sl.insert_vertices_on_segs(seg_id, 4, 6, 7, 1)
        h1_sl.insert_vertices_on_segs(seg_id, 6, 9, 9, 0)

        min_percent = 1 / 0.05
        max_percent = 1 / 0.11
        
        h1_sl, h1_s_coords, h1_t_coords, idx_1_up, idx_1_down, idx_2_up, idx_2_down = h1_sl.split_to_point_preproccess(seg_id, min_percent, max_percent)

        parts = h1_sl.split_to_parts(seg_id, idx_1_up, idx_1_down, idx_2_up, idx_2_down)
        
        min_percent = 1 / 0.11
        max_percent = 1 / 0.21
        
        inclusive = False
        start_t = 0
        end_t = 0.5
        
        geom_id = -1
        rand = np.random.uniform(0, 1)
        
        if rand <= 0.5:
            geom_id = 0
        else:
            geom_id = 1
        
        geom = parts[geom_id]
        _n = len(geom.get_segs())
        _seg_id = np.random.randint(1, _n)
        
        ### >>> <<< ###
        V = geom.get_vertices()
        M = len(geom.get_segs())
        i = 0
        
        while i < M:
            if i < _seg_id or i >  _seg_id + 1:
                
                p = np.random.uniform(0, 1)
                up = 0
                if p >= 0.5:
                     up = 1
            
                _npos = 7
                _min = np.random.randint(3, 5)
                _max = np.random.randint(5, 7)
                
                geom.insert_vertices_on_segs(i, _min, _max, _npos, up)
                
            i += 1
        ### >>> <<< ###
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        hsl, hs_coords, ht_coords = geom.grow(min_percent, max_percent)
        coords += [MCoords(hs_coords, ht_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, 2, 1)]
        
        annotations += [[0, 2, 'grow' , start_t, 1, unit_id]]
        
        min_percent = 1 / 0.07
        max_percent = 1 / 0.27
        
        ### >>> <<< ###
        V = hsl.get_vertices()
        M = len(hsl.get_segs())
        i = 0
        
        while i < M:
            if i < _seg_id or i >  _seg_id + 1:
                
                p = np.random.uniform(0, 1)
                up = 0
                if p >= 0.5:
                     up = 1
            
                _npos = 11
                _min = np.random.randint(6, 9)
                _max = np.random.randint(9, 11)
                
                hsl.insert_vertices_on_segs(i, _min, _max, _npos, up)
                
            i += 1
        ### >>> <<< ###
        
        hsl, hs_coords1, ht_coords1 = hsl.grow(min_percent, max_percent)
        
        inclusive = True

        start_t = 0.5
        end_t = 1
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        coords += [MCoords(hs_coords1, ht_coords1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, 2, 1)]
        
        # >>>
        
        end_as_line = None
        
        min1 = 0.11
        max1 = 0.27
        min2 = 0.09
        max2 = 0.22
        
        gsl = geom.generate_geom_from_sp_line(min1, max1, min2, max2)
                
        # >>>>>>>>>>>>>>>>>>>>>
        V = gsl.get_vertices()
        M = len(gsl.get_segs())
        i = 0
        
        while i < M:
            if i < _seg_id or i >  _seg_id + 1:
                
                p = np.random.uniform(0, 1)
                up = 0
                if p >= 0.5:
                     up = 1
            
                _npos = 7
                _min = np.random.randint(3, 5)
                _max = np.random.randint(5, 7)
                
                gsl.insert_vertices_on_segs(i, _min, _max, _npos, up)
                
            i += 1
        # >>>>>>>>>>>>>>>>>>>>>
                
        min_percent = 1 / 0.06
        max_percent = 1 / 0.21
        
        gsl, gs_coords, gt_coords = gsl.grow(min_percent, max_percent)
        
        inclusive = False
        start_t = 0
        end_t = 0.5

        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t

        annotations += [[1, 3, 'grow' , start_t, end_t, unit_id]]
        annotations += [[1, 3, 'merge(1, 3)' , end_t, end_t, unit_id]]
        annotations += [[1, 3, 'disappear' , end_t, end_t, unit_id]]

        coords += [MCoords(gs_coords, gt_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 3, 2)]

        inclusive = True
        start_t = 0.5
        end_t = 1
        
        _min_t = 0.1
        _max_t = 0.47
        
        # insert vertices with a restriction
        
        numv = np.random.randint(1, 6)
        
        up = True
        rnd = np.random.uniform(0, 1)
        if rnd <= 0.5:
            up = False
            
        grow = True
        rnd = np.random.uniform(0, 1)
        if rnd <= 0.5:
            grow = False
          
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
          
        sl, s_coords, t_coords, b_seg_id = geom_burst_geom(gsl, hsl, _min_t, _max_t, numv, up, grow, _seg_id)
        coords += [MCoords(s_coords, t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 3, 2)]      
    # Hole Consume.
    elif op == 25:
        unit_id = 1
        
        min_percent = (1 / 0.05)               # 5%
        max_percent = (1 / 0.17)              # 17%
        
        inclusive = False
        end_as_line = None
        
        supp_line_1, source_coords_1, target_coords_1 = support_line.evolve(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.4
        
        annotations += [[1, 1, 'evolve' , start_t, end_t, unit_id]]
       
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        max_num_vertices = 11
        add_min_vertices = 7
        
        _add_max_vertices = np.random.randint(add_min_vertices, max_num_vertices)
        _num_pos = _add_max_vertices
                
        supp_line_1.insert_vertices(add_min_vertices, _add_max_vertices, _num_pos)
        
        min_percent = (1 / 0.07)               # 5%
        max_percent = (1 / 0.21)              # 17%
        
        supp_line_1, source_coords_1, target_coords_1 = supp_line_1.shrink(min_percent, max_percent)
        
        start_t = 0.4
        end_t = 1
        
        annotations += [[1, 1, 'shrink' , start_t, end_t, unit_id]]
       
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        inclusive = True
        
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        # >>> Generate hole

        min1 = 0.10
        max1 = 0.20
        min2 = 0.05
        max2 = 0.20
        
        h1_s_coords, h1_t_coords, h1_sl = supp_line_1.generate_hole_from_sp_line_2(min1, max1, min2, max2)
            
        num_segs = len(h1_sl.get_segs())
        seg_id = np.random.randint(1, num_segs)
                
        h1_sl.insert_vertices_on_segs(seg_id, 4, 6, 7, 1)
        h1_sl.insert_vertices_on_segs(seg_id, 6, 9, 9, 0)

        min_percent = 1 / 0.05
        max_percent = 1 / 0.11
        
        h1_sl, h1_s_coords, h1_t_coords, idx_1_up, idx_1_down, idx_2_up, idx_2_down = h1_sl.split_to_point_preproccess(seg_id, min_percent, max_percent)

        parts = h1_sl.split_to_parts(seg_id, idx_1_up, idx_1_down, idx_2_up, idx_2_down)
        
        min_percent = 1 / 0.11
        max_percent = 1 / 0.21
        
        inclusive = False
        start_t = 0
        end_t = 0.5
        
        geom_id = -1
        rand = np.random.uniform(0, 1)
        
        if rand <= 0.5:
            geom_id = 0
        else:
            geom_id = 1
        
        geom = parts[geom_id]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        hsl, hs_coords, ht_coords = geom.grow(min_percent, max_percent)
        coords += [MCoords(hs_coords, ht_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, 2, 1)]
        
        annotations += [[0, 2, 'grow' , start_t, 0.8, unit_id]]
        
        min_percent = 1 / 0.09
        max_percent = 1 / 0.17
        
        hsl, hs_coords1, ht_coords1 = hsl.grow(min_percent, max_percent)
        
        inclusive = True
        end_as_line = Polygon()
        
        start_t = 0.5
        end_t = 0.8
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        coords += [MCoords(hs_coords1, ht_coords1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, 2, 1)]
        
        # >>>
        
        end_as_line = None
        
        min1 = 0.11
        max1 = 0.27
        min2 = 0.09
        max2 = 0.22
        
        gsl = geom.generate_geom_from_sp_line(min1, max1, min2, max2)
        
        min_percent = 1 / 0.11
        max_percent = 1 / 0.21
        
        gsl, gs_coords, gt_coords = gsl.grow(min_percent, max_percent)
                
        inclusive = True
        end_as_line = Polygon()
        
        start_t = 0
        end_t = 0.8

        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t

        annotations += [[1, 3, 'grow' , start_t, end_t, unit_id]]
        annotations += [[1, 3, 'merge(1, 3)' , end_t, end_t, unit_id]]
        annotations += [[0, 2, 'disappear' , end_t, end_t, unit_id]]

        #t_coords_ = geom_consume_geom(gt_coords, ht_coords1)
        coords += [MCoords(gt_coords, ht_coords1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 3, 2)]
    # Internal face split.
    elif op == 26:
        unit_id = 1
        
        min_percent = (1 / 0.05)               # 5%
        max_percent = (1 / 0.17)              # 17%
        
        inclusive = False
        end_as_line = None
        
        supp_line_1, source_coords_1, target_coords_1 = support_line.evolve(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.4
        
        annotations += [[1, 1, 'evolve' , start_t, end_t, unit_id]]
       
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        max_num_vertices = 11
        add_min_vertices = 7
        
        _add_max_vertices = np.random.randint(add_min_vertices, max_num_vertices)
        _num_pos = _add_max_vertices
                
        supp_line_1.insert_vertices(add_min_vertices, _add_max_vertices, _num_pos)
        
        min_percent = (1 / 0.07)               # 5%
        max_percent = (1 / 0.21)              # 17%
        
        supp_line_1, source_coords_1, target_coords_1 = supp_line_1.shrink(min_percent, max_percent)
        
        start_t = 0.4
        end_t = 1
        
        annotations += [[1, 1, 'shrink' , start_t, end_t, unit_id]]
       
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        inclusive = True
        coords += [MCoords(source_coords_1, target_coords_1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 1, 0)]

        # >>> Generate hole

        min1 = 0.10
        max1 = 0.20
        min2 = 0.05
        max2 = 0.20
        
        h1_s_coords, h1_t_coords, h1_sl = supp_line_1.generate_hole_from_sp_line_2(min1, max1, min2, max2)
            
        num_segs = len(h1_sl.get_segs())
        seg_id = np.random.randint(1, num_segs)
                
        h1_sl.insert_vertices_on_segs(seg_id, 4, 6, 7, 1)
        h1_sl.insert_vertices_on_segs(seg_id, 6, 9, 9, 0)

        min_percent = 1 / 0.05
        max_percent = 1 / 0.11
        
        h1_sl, h1_s_coords, h1_t_coords, idx_1_up, idx_1_down, idx_2_up, idx_2_down = h1_sl.split_to_point_preproccess(seg_id, min_percent, max_percent)

        parts = h1_sl.split_to_parts(seg_id, idx_1_up, idx_1_down, idx_2_up, idx_2_down)
        
        min_percent = 1 / 0.11
        max_percent = 1 / 0.21
        
        inclusive = False
        start_t = 0
        end_t = 0.5
        
        geom_id = -1
        rand = np.random.uniform(0, 1)
        
        if rand <= 0.5:
            geom_id = 0
        else:
            geom_id = 1
        
        geom = parts[geom_id]
        
        annotations += [[0, 2, 'grow' , start_t, 1, unit_id]]
       
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        hsl, hs_coords, ht_coords = geom.grow(min_percent, max_percent)
        coords += [MCoords(hs_coords, ht_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, 2, 1)]
        
        min_percent = 1 / 0.09
        max_percent = 1 / 0.17
        
        hsl, hs_coords1, ht_coords1 = hsl.grow(min_percent, max_percent)
        
        inclusive = True

        start_t = 0.5
        end_t = 1
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        coords += [MCoords(hs_coords1, ht_coords1, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 0, 2, 1)]
        
        # >>>
        
        end_as_line = None
        
        min1 = 0.11
        max1 = 0.27
        min2 = 0.09
        max2 = 0.22
        
        gsl = geom.generate_geom_from_sp_line(min1, max1, min2, max2)
        
        min_percent = 1 / 0.11
        max_percent = 1 / 0.21
        
        gsl, gs_coords, gt_coords = gsl.grow(min_percent, max_percent)
        
        inclusive = True

        start_t = 0
        end_t = 1

        annotations += [[1, 3, 'grow' , start_t, end_t, unit_id]]
        annotations += [[1, 3, 'merge(1, 3)' , end_t, end_t, unit_id]]
        annotations += [[1, 3, 'disappear' , end_t, end_t, unit_id]]
       
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t

        _min_t = 0.1
        _max_t = 0.35

        up = False
        rnd = np.random.uniform(0, 1)
        if rnd > 0.5:
            up = True

        sl, s, t = geom_internal_merge(gsl, hsl, _min_t, _max_t, up)
        coords += [MCoords(s, t, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, 1, 3, 2)]
    # Remove vertices.
    elif op == 27: 
        unit_id = 1
        min_percent =  0.02
        max_percent = 0.05
        
        min_percent = 1 / min_percent
        max_percent = 1 / max_percent
        
        rem_vertices = True
        end_as_line = None
        
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0

        f1_supp_line, s_f1_coords, t_f1_coords = support_line.evolve(min_percent, max_percent)
        inclusive = False
                
        start_t = 0
        end_t = 0.25
     
        annotations += [[1, 1, 'evolve' , 0, 1, unit_id]]
       
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
     
        coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        sl, s, t = f1_supp_line.remove_vertices(0.07, 0.13, 2)
        
        start_t = 0.25
        end_t = 0.5

        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t

        coords += [MCoords(s, t, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        support_line = sl
        f1_supp_line, s_f1_coords, t_f1_coords = support_line.evolve(min_percent, max_percent)
       
        start_t = 0.5
        end_t = 0.75

        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t

        coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        sl, s, t = f1_supp_line.remove_vertices(0.05, 0.07, 2)
            
        inclusive = True
        start_t = 0.75
        end_t = 1

        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t

        coords += [MCoords(s, t, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
    # Engulf.
    elif op == 28:
        unit_id = 1
        min_percent =  0.02
        max_percent = 0.05
        
        min_percent = 1 / min_percent
        max_percent = 1 / max_percent
        
        rem_vertices = True
        end_as_line = None
        
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0

        ref_geom = None
        ref_geom1 = support_line

        f1_supp_line, s_f1_coords, t_f1_coords = support_line.evolve(min_percent, max_percent)
        inclusive = False
                
        start_t = 0
        end_t = 0.25
     
        annotations += [[1, 1, 'evolve' , start_t, end_t, unit_id]]
     
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
     
        coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        ref_geom2 = f1_supp_line

        sl, s, t = f1_supp_line.remove_vertices(0.07, 0.13, 2)
        
        start_t = 0.25
        end_t = 0.5

        annotations += [[1, 1, 'remove vertices' , start_t, end_t, unit_id]]

        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t

        coords += [MCoords(s, t, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        support_line = sl
        f1_supp_line, s_f1_coords, t_f1_coords = support_line.evolve(min_percent, max_percent)
       
        start_t = 0.5
        end_t = 0.75

        annotations += [[1, 1, 'evolve' , start_t, end_t, unit_id]]

        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t

        coords += [MCoords(s_f1_coords, t_f1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        sl, s, t = f1_supp_line.remove_vertices(0.05, 0.07, 2)
            
        inclusive = True
        start_t = 0.75
        end_t = 1

        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t

        coords += [MCoords(s, t, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        annotations += [[1, 1, 'remove vertices' , start_t, end_t, unit_id]]

        # Hole
        
        n_vertices = 37
        hsl, rnd_seg_idx = get_hole(ref_geom1, n_vertices)
        
        up = 1
        p = np.random.uniform(0, 1)
        if p <= 0.5:
            up = 0
        
        VH = hsl.get_vertices()
        
        k = -1
        if up == 1:
            k = len(VH[0][0])
        else:
            k = len(VH[0][1])

        v_idx = np.random.randint(1, k - 1)
        
        _seg_idxs = get_engulf_params_2(ref_geom1, hsl, rnd_seg_idx, up, v_idx)

        # get positions
        
        l1 = []
        V = ref_geom1.get_vertices()
        for e in _seg_idxs:
            if up == 1:
                l = V[e[0]][0][e[1]]
                l1 += [(l.coords[1][0], l.coords[1][1])]
            else:
                l = V[e[0]][1][e[1]]
                l1 += [(l.coords[1][0], l.coords[1][1])]
        
        _seg_idxs = get_engulf_params_2(ref_geom2, hsl, rnd_seg_idx, up, v_idx)
        
        l2 = []
        V = ref_geom2.get_vertices()
        for e in _seg_idxs:
            if up == 1:
                l = V[e[0]][0][e[1]]
                l2 += [(l.coords[1][0], l.coords[1][1])]
            else:
                l = V[e[0]][1][e[1]]
                l2 += [(l.coords[1][0], l.coords[1][1])]

        x = 0
        y = 0

        if up == 1:
            l = VH[0][0][v_idx]
            _coords = l.coords
                    
            x0 = l1[0][0]
            y0 = l1[0][1]
                        
            x1 = l1[1][0]
            y1 = l1[1][1]
                        
            x = _coords[0][0]
                        
            dx = x1 - x0
            dy = y1 - y0
                        
            f = (x - x0) / dx
            y = y0 + dy * f

            y += y * np.random.uniform(0.1, 0.15)
            
            VH[0][0][v_idx] = LineString([(_coords[0][0], _coords[0][1]), (x, y)])
        else:
            l = VH[0][1][v_idx]
            _coords = l.coords
            
            x0 = l1[0][0]
            y0 = l1[0][1]
                        
            x1 = l1[1][0]
            y1 = l1[1][1]
                        
            x = _coords[0][0]
                        
            dx = x1 - x0
            dy = y1 - y0
                        
            f = (x - x0) / dx
            y = y0 + dy * f

            y += y * np.random.uniform(0.1, 0.2)
        
            VH[0][1][v_idx] = LineString([(_coords[0][0], _coords[0][1]), (x, y)])
        
        # transform hole
        
        if up == 1:
            l = VH[0][0][v_idx]
            _coords = l.coords
                    
            x0 = l2[0][0]
            y0 = l2[0][1]
                        
            x1 = l2[1][0]
            y1 = l2[1][1]
                        
            x = _coords[0][0]
                        
            dx = x1 - x0
            dy = y1 - y0
                        
            f = (x - x0) / dx
            y = y0 + dy * f
        else:
            l = VH[0][1][v_idx]
            _coords = l.coords
            
            x0 = l2[0][0]
            y0 = l2[0][1]
                        
            x1 = l2[1][0]
            y1 = l2[1][1]
                        
            x = _coords[0][0]
                        
            dx = x1 - x0
            dy = y1 - y0
                        
            f = (x - x0) / dx
            y = y0 + dy * f
                
        min_percent =  1 / 0.13
        max_percent = 1 / 0.27
        
        up_r = None
        d_r = None
        
        if up == 1:
            up_r = [0, v_idx, x, y]
        else:
            d_r = [0, v_idx, x, y]
        
        hsl, s_h1_coords, t_h1_coords = hsl.shrink_with_restrictions(min_percent, max_percent, up_r, d_r)

        start_t = 0
        end_t = 0.25
            
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
            
        end_as_line = None
        inclusive = False
        
        geom_type = 0
        geom_id = 2
        geom_parent_id = 1
        
        annotations += [[1, 1, 'engulf' , end_t, end_t, unit_id]]
        annotations += [[0, 2, 'appear' , end_t, end_t, unit_id]]
        
        coords += [MCoords(s_h1_coords, t_h1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        min_percent =  1 / 0.27
        max_percent = 1 / 0.47
        
        """
        if change_topology == 1:
            _max_num_vertices = 30
            _add_min_vertices = 20
            _add_max_vertices = np.random.randint(_add_min_vertices, _max_num_vertices)
            _num_pos = _max_num_vertices
        
            hsl.insert_vertices(_add_min_vertices, _add_max_vertices, _num_pos)
        """
        
        _max_num_vertices = 30
        _add_min_vertices = 20
        _add_max_vertices = np.random.randint(_add_min_vertices, _max_num_vertices)
        _num_pos = _max_num_vertices
        
        hsl.insert_vertices(_add_min_vertices, _add_max_vertices, _num_pos)
        
        hsl, s_h1_coords, t_h1_coords = hsl.shrink(min_percent, max_percent)
        
        start_t = 0.25
        end_t = 0.5
        
        annotations += [[0, 2, 'insert vertices' , start_t, start_t, unit_id]]
        annotations += [[0, 2, 'shrink' , start_t, end_t, unit_id]]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        inclusive = False
        coords += [MCoords(s_h1_coords, t_h1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        hsl, s, t = hsl.remove_vertices(0.05, 0.07, 2)
            
        start_t = 0.5
        end_t = 0.75

        annotations += [[0, 2, 'remove vertices' , start_t, end_t, unit_id]]

        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t

        coords += [MCoords(s, t, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        #min_percent =  1 / 0.47
        #max_percent = 1 / 0.61

        hsl, s_h1_coords, t_h1_coords = hsl.shrink(min_percent, max_percent)
        
        start_t = 0.75
        end_t = 1
        
        annotations += [[0, 2, 'shrink' , start_t, end_t, unit_id]]
        
        dx = Dx * (end_t - start_t)
        dy = Dy * (end_t - start_t)
        
        x = Dx * start_t
        y = Dy * start_t
        
        inclusive = True
        coords += [MCoords(s_h1_coords, t_h1_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
    # Hole consume face Non-Deterministic.
    elif op == 29:
        min_percent = (1 / 0.07)
        max_percent = (1 / 0.25)
        
        f_sl, f_s_coords, f_t_coords = support_line.shrink(min_percent, max_percent)
        
        start_t = 0
        end_t = 0.5
        
        dx = (Dx * end_t)
        dy = (Dy * end_t)
        
        x = 0
        y = 0
        
        inclusive = False
        end_as_line = None
        
        geom_type = 1
        geom_id = 1
        geom_parent_id = 0
        
        coords += [MCoords(f_s_coords, f_t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        _min_vertices = 5
        _max_vertices = np.random.randint(6, 9)
        _num_pos = _max_vertices
                        
        f_sl.insert_vertices(_min_vertices, _max_vertices, _num_pos)
        
        min_percent = 1 / 0.07
        max_percent = 1 / 0.13

        f_sl, f_s_coords, f_t_coords = f_sl.shrink(min_percent, max_percent)
        
        start_t = 0.5
        end_t = 1
        
        dx = (Dx * end_t)
        dy = (Dy * end_t)
        
        x = 0
        y = 0

        inclusive = True
        line = LineString(f_t_coords)
        line = shapely.affinity.translate(line, Dx, Dy, 0.0)
        end_as_line = line

        coords += [MCoords(f_s_coords, f_t_coords, start_t, end_t, dx, dy, x, y, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        annotations += [[1, 1, 'shrink' , 0, 1, 1]]
    
        # Hole
       
        segs = f_sl.get_segs()
        
        hsl = SupportLine(len(segs), seg_min_len, seg_max_len, up_down_min_len, up_down_max_len, segs)

        hsl.generate_vertices( n_vertices + np.random.randint(3, 9), 
                                           MIN_NUM_VERTICES_IN_EXT_SEG,
                                           MIN_NUM_VERTICES_IN_INT_SEG,
                                           -1,
                                           min_num_inactive_vertices_per_seg,
                                           max_num_inactive_vertices_per_seg,
                                           min_len, 
                                           max_len, 
                                           use_inactive_points)
        
        _min_up_percent = 0.07
        _max_up_percent = 0.31
            
        _min_down_percent = 0.07
        _max_down_percent = 0.33
        
        hsl, hs_coords, ht_coords = hsl.generate_hole_from_sl_2(_min_up_percent, _max_up_percent, _min_down_percent, _max_down_percent)

        start_t = 0.2
        end_t = 0.6
        
        annotations += [[0, 2, 'appear' , start_t, start_t, 1]]
        annotations += [[0, 2, 'grow' , start_t, 1, 1]]
        
        dx1 = Dx * (end_t - start_t)
        dy1 = Dy * (end_t - start_t)
        
        x1 = Dx * start_t
        y1 = Dy * start_t
        
        inclusive = False
        end_as_line = None
        geom_type = 0
        geom_id = 2
        geom_parent_id = 1

        coords += [MCoords(hs_coords, ht_coords, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]

        s_coords, t_coords = hsl.grow_to_line(f_sl)

        start_t = 0.6
        end_t = 1

        dx1 = Dx * (end_t - start_t)
        dy1 = Dy * (end_t - start_t)
        
        x1 = Dx * start_t
        y1 = Dy * start_t
    
        inclusive = True
        coords += [MCoords(s_coords, t_coords, start_t, end_t, dx1, dy1, x1, y1, inclusive, end_as_line, geom_type, geom_id, geom_parent_id)]
        final_geom = Polygon()
    else:
        print_result_error('Invalid operation.')
        sys.exit()
    
    # Output >>>
    
    geoms, num_invalid_geoms, num_complex_geoms = interpolate_3(coords, num_observations, 0, 0, Rot)

    if geoms == None:
        counter += 1
        
        if counter == max_tries:
            print_result_error('Unable to generate a valid interpolation in the max amount of tries specified: ' + str(max_tries) + "!")
            sys.exit()
        
        continue

    if final_geom != None:
        geoms += [final_geom]
        num_observations += 1

    print_annotations(annotations)
    print_result(geoms, num_observations, num_invalid_geoms, num_complex_geoms)
    
    _exec = False
