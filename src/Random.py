import numpy as np
import random

"""

 * Authors: Jose Duarte        (hfduarte@ua.pt)
 * 			     Mark Mckenney 	(marmcke@siue.edu)
 * 
 * Version: 1.0 (2020-05-29)
 * 
 
"""

"""
    
    A Module to generate random objects: numbers, sequences of numbers, 2D points, etc.

    x = [randint(0, 9) for p in range(0, 10)]
    Generates 10 pseudorandom integers in range 0 to 9 inclusive.
    
"""

"""

    Generates a random number in [a, b[

"""
def generate_random_number(a, b):
    return random.randrange(a, b, 1)

def generate_random_int_number(a, b):
    return random.randint(a, b)

"""

    Generates a random integer in the interval [low, high).

"""
def numpy_generate_random_int_number(low, high):
    return np.random.randint(low, high)

def numpy_generate_random_float_number(low, high):
    return np.random.uniform(low, high, size = (1, ))[0]

def numpy_generate_one_random_float_number(low, high):
    return np.random.uniform(low, high)

"""

    As n gets bigger the convex hull tends to be a square in the plane x = 1.0, y = 1.0.
    This is a problem for generating polygons. If n > 150 this effect is already seen.
    
    Need another function to generate a random set of 2D points!

"""
def generate_n_random_2D_points(n):
    return np.random.rand(n, 2)

def tests():
    print('')
    print('--------------------------------------------')
    print('Tests')
    print('--------------------------------------------')
    
    print('')
    print('A random number between 0 and 360.')
    print('')
    
    a = 0
    b = 361
    
    m = generate_random_number(a, b)
    print(m)
    
    print('')
    print('A random number between 0 and 360 using numpy.')
    print('')
    
    a = 0
    b = 361
    
    m = numpy_generate_random_float_number(a, b)
    print(m)
    
    """
    print('')
    print('A random float number between 0 and 360.')
    print('')
    
    a = 0
    b = 361
    
    m = generate_random_float_number(a, b)
    print(m)
    """
    
    print('')
    print('A random integer number between 0 and 100.')
    print('')

    a = 0
    b = 100

    m = generate_random_int_number(a, b)
    print(m)
    
    print('')
    print('A random integer number between 0 and 100 using numpy.')
    print('')

    a = 0
    b = 100

    m = numpy_generate_random_int_number(a, b)
    print(m)
    
    n = 5
    
    print('')
    print('A random set of ' + str(n) + ' 2D points.')
    print('')

    m = generate_n_random_2D_points(n)
    print(m)
    
    print('')
    print('--------------------------------------------')

#tests()
