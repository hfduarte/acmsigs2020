import numpy as np
from shapely.geometry import Point, Polygon, LineString, MultiPolygon
import shapely.wkt
import math
import sys

"""

 * Authors: Jose Duarte        (hfduarte@ua.pt)
 * 			     Mark Mckenney 	(marmcke@siue.edu)
 * 
 * Version: 1.0 (2020-05-29)
 * 
 
"""

"""

    MCoords A class used to get the in-between geometries (observations) of the evolution
    of a moving region in an interval of time defined in [0, 1].

"""

class MCoords:
    
    """
    
        geom_type = 1 > face
        geom_type = 2 > hole
        
    """
    
    def __init__(self, s_coords, t_coords, starting_time = 0, ending_time = 1, dx = 0, dy = 0, idx = 0, idy = 0, inclusive = False, end_as_line = None, geom_type = 1, geom_id = 0, geom_parent_id = 0):
        self.s_coords = s_coords
        self.t_coords = t_coords
        self.dx = dx
        self.dy = dy
        
        self.idx = idx
        self.idy = idy
        
        self.starting_time = float(starting_time)
        self.ending_time = float(ending_time)
        self.inclusive = inclusive
        
        self.end_as_line = end_as_line
        
        self.geom_type = geom_type
        self.geom_id = geom_id
        self.geom_parent_id = geom_parent_id
        
    def at(self, t):
        num_invalid_geoms = 0
        num_complex_geoms = 0
        
        if self.starting_time < 0 or self.ending_time > 1:
            print('Invalid starting_time : ending_time.')
            sys.exit()

        if t < self.starting_time:
            return None, num_invalid_geoms, num_complex_geoms
        
        if t > self.ending_time:
            return None, num_invalid_geoms, num_complex_geoms
        
        if t == self.ending_time and not self.inclusive:
            return None, num_invalid_geoms, num_complex_geoms
        
        if t == self.ending_time and self.end_as_line != None:
            poly = self.end_as_line
            
            if not poly.is_valid:
                num_invalid_geoms = 1
                
            if not poly.is_simple:
                num_complex_geoms = 1
            
            #print(poly.wkt + ';')
            #sys.exit()
            
            return poly, num_invalid_geoms, num_complex_geoms
    
        if self.starting_time == 0 and self.ending_time == 1:            
            _k = 0
            _len = len(self.s_coords)
            coords = []
            
            _dxx = self.dx * t + self.idx
            _dyy = self.dy * t + self.idy
            
            while _k < _len - 1:
                dxx = self.t_coords[_k][0] - self.s_coords[_k][0]
                dyy = self.t_coords[_k][1] - self.s_coords[_k][1]
                
                dxx *= t
                dyy *= t

                coords += [(self.s_coords[_k][0] + dxx + _dxx, self.s_coords[_k][1] + dyy + _dyy)]
                
                _k += 1
                      
            coords += [(coords[0][0], coords[0][1])]
            poly = Polygon(coords)
            
            if not poly.is_valid:
                num_invalid_geoms = 1
            
            if not poly.is_simple:
                num_complex_geoms = 1
            
            return poly, num_invalid_geoms, num_complex_geoms
        elif self.starting_time == self.ending_time:
            """
            _k = 0
            _len = len(self.s_coords)
            coords = []
            
            _dxx = self.dx * t + self.idx
            _dyy = self.dy * t + self.idy
            
            while _k < _len - 1:
                dxx = self.t_coords[_k][0] - self.s_coords[_k][0]
                dyy = self.t_coords[_k][1] - self.s_coords[_k][1]
                
                dxx *= t
                dyy *= t

                coords += [(self.s_coords[_k][0] + dxx + _dxx, self.s_coords[_k][1] + dyy + _dyy)]
                
                _k += 1
            
            coords += [(coords[0][0], coords[0][1])]
            poly = Polygon(coords)
            """
            
            poly = Polygon(self.s_coords + [(self.s_coords[0][0]. self.s_coords[0][1])])
            
            if not poly.is_valid:
                num_invalid_geoms = 1
            
            if not poly.is_simple:
                num_complex_geoms = 1
            
            return poly, num_invalid_geoms, num_complex_geoms
        else:
            if self.starting_time != 0 and self.ending_time == 1:
                w1_n = (1 - t)
                w1_d = (1 - self.starting_time)
                    
                dt = float(w1_n) / w1_d
                t = float(t - self.starting_time) / w1_d           
            elif self.starting_time == 0 and self.ending_time != 1:
                w1_n = (self.ending_time - t)
                w1_d = self.ending_time

                dt = float(w1_n) / w1_d
                t = float(t) / w1_d
            else:
                w1_n = (self.ending_time - t)
                w1_d = self.ending_time - self.starting_time

                dt = float(w1_n) / w1_d
                t = float(t - self.starting_time) / w1_d

            _k = 0
            _len = len(self.s_coords)
            coords = []
            
            dxx = self.dx * t + self.idx
            dyy = self.dy * t + self.idy
                        
            while _k < _len - 1:
                coords += [(dt * self.s_coords[_k][0] + t * self.t_coords[_k][0] + dxx, dt * self.s_coords[_k][1] + t * self.t_coords[_k][1] + dyy)]
                _k += 1
                        
            coords += [(coords[0][0], coords[0][1])]
            poly = Polygon(coords)
            
            if not poly.is_valid:
                num_invalid_geoms = 1
                
            if not poly.is_simple:
                num_complex_geoms = 1
            
            return poly, num_invalid_geoms, num_complex_geoms
    
    def get_geom_type(self):
        return self.geom_type
    
    def get_geom_id(self):
        return self.geom_id
    
    def get_geom_parent_id(self):
        return self.geom_parent_id
    