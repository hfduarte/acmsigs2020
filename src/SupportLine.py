import numpy as np
from shapely.geometry import Point, Polygon, LineString, MultiPolygon
import shapely.wkt
from Utils import distance
import math
import random
import sys

EPS = 1e-9

"""

 * Authors: Jose Duarte        (hfduarte@ua.pt)
 * 			     Mark Mckenney 	(marmcke@siue.edu)
 * 
 * Version: 1.0 (2020-05-29)
 * 
 
"""

"""

    SupportLine Class to generate and manipulate support lines.
    

"""

class SupportLine:
    
    def __init__(self, num_segs, seg_min_len, seg_max_len, up_down_min_len, up_down_max_len, segs = None, vertices = None, gen_vertices = False):
        self.num_segs = num_segs
        self.seg_min_len = seg_min_len
        self.seg_max_len = seg_max_len
        self.up_down_min_len = up_down_min_len
        self.up_down_max_len = up_down_max_len
        self.segs = []
        self.vertices = []
        self.coords = []

        if segs == None:
            self.generate_line()
        else:
             self.segs = segs
        
        if not vertices == None:
            self.vertices = vertices
        
        #if gen_vertices:
        #    self.generate_vertices()  
        
    def generate_line(self):
        i = 0
        
        x = 0
        y = 0
    
        while i < self.num_segs:
            seg_len = random.uniform(self.seg_min_len, self.seg_max_len)
            
            seg = LineString([(x, y), (x + seg_len, y)])
            self.segs += [seg]
            
            x += seg_len
            dy = random.uniform(self.up_down_min_len, self.up_down_max_len)
           
            p = random.uniform(0, 1)
            
            if p < 0.5:
                y -= dy
            else:
                y += dy
            
            i += 1

    def get_vertex(self, ref_p, min_angle, max_angle, min_len, max_len, positive_rot = True):
        p_len = random.uniform(min_len, max_len)
        p_angle = random.uniform(min_angle, max_angle)

        line = LineString([(ref_p.x, ref_p.y), (ref_p.x + p_len, ref_p.y)])
        
        if positive_rot:
            line = shapely.affinity.rotate(line, p_angle, ref_p, False)
        else:
            line = shapely.affinity.rotate(line, -p_angle, ref_p, False)

        return line

    def generate_vertices(self, n_vertices, n_vertices_ext_seg, n_vertices_int_seg, r, min_n_inactive_vertices_per_seg, max_n_inactive_vertices_per_seg, min_len, max_len, use_inactive_vertices = False):
        i = len(self.segs)
        k = 0
        n_vertices_created = 0
        
        if i == 0:
            return 2
        
        self.vertices = []
        
        if i == 1:
            _seg_up_vertices = []
            _seg_down_vertices = []
                
            _seg = self.segs[k]
            _coords = _seg.coords

            _ref_p = Point(_coords[0][0], _coords[0][1])
                    
            _min_angle = 91
            _max_angle = 179
                    
            # up
            _line = self.get_vertex(_ref_p, _min_angle, _max_angle, min_len, max_len, True)
            _seg_up_vertices += [_line]
                    
            # down                
            _line = self.get_vertex(_ref_p, _min_angle, _max_angle, min_len, max_len, False)
            _seg_down_vertices += [_line]
                    
            # interior
            _r_v = n_vertices - 4
            _seg_len = _seg.length
                    
            i_p = Point(_coords[0][0], _coords[0][1])
            _dx = _seg_len / (_r_v + 1)
                    
            _j = 0
            while _j < _r_v:
                _p_len = random.uniform(min_len, max_len)

                x = i_p.x + _dx * (_j + 1)
                y = i_p.y
                        
                _p = random.uniform(0, 1)
                        
                if _p >= 0.5:
                    y -= _p_len
                            
                    _line = LineString([(x, i_p.y), (x, y)])
                    _seg_down_vertices += [_line]
                else:
                    y += _p_len
                            
                    _line = LineString([(x, i_p.y), (x, y)])
                    _seg_up_vertices += [_line]

                _j += 1
                    
            ## >>
                    
            ref_p = Point(_coords[1][0], _coords[1][1])
                    
            _min_angle = 1
            _max_angle = 79
                    
            # 1
            _line = self.get_vertex(ref_p, _min_angle, _max_angle, min_len, max_len, True)
            _seg_up_vertices += [_line]
                    
            # 2
            _line = self.get_vertex(ref_p, _min_angle, _max_angle, min_len, max_len, False)
            _seg_down_vertices += [_line]

            self.vertices += [[_seg_up_vertices, _seg_down_vertices]]
        elif i == 2:
            return 10
        elif i == 3:
            return 10
        else:
            while k < i:
                _seg_up_vertices = []
                _seg_down_vertices = []
                
                _seg = self.segs[k]
                _coords = _seg.coords

                # first seg
                if k == 0:
                    _ref_p = Point(_coords[0][0], _coords[0][1])
                    
                    _min_angle = 91
                    _max_angle = 179
                    
                    # up
                    _line = self.get_vertex(_ref_p, _min_angle, _max_angle, min_len, max_len, True)
                    _seg_up_vertices += [_line]
                    
                    # down                
                    _line = self.get_vertex(_ref_p, _min_angle, _max_angle, min_len, max_len, False)
                    _seg_down_vertices += [_line]
                    
                    n_vertices_created += 2
                    
                    # interior
                    _r_v = n_vertices_ext_seg - 3
                    _seg_len = _seg.length
                    
                    i_p = Point(_coords[0][0], _coords[0][1])
                    _dx = _seg_len / (_r_v + 1)
                    
                    _j = 0
                    while _j < _r_v:
                        _p_len = random.uniform(min_len, max_len)

                        x = i_p.x + _dx * (_j + 1)
                        y = i_p.y
                        
                        _p = random.uniform(0, 1)
                        
                        if _p >= 0.5:
                            y -= _p_len
                            
                            _line = LineString([(x, i_p.y), (x, y)])
                            _seg_down_vertices += [_line]
                        else:
                            y += _p_len
                            
                            _line = LineString([(x, i_p.y), (x, y)])
                            _seg_up_vertices += [_line]
                        
                        n_vertices_created += 1
                        _j += 1
                    
                    # last
                    _curr_y = _coords[0][1]
                    _next_y = self.segs[1].coords[0][1]
                    
                    _p_len = random.uniform(min_len, max_len)
                    
                    x = _coords[1][0]
                    y = _coords[1][1]
                        
                    if _curr_y > _next_y:
                        _line = LineString([(x, y), (x, y + _p_len)])
                        _seg_up_vertices += [_line]
                    else:
                        _line = LineString([(x, y), (x, y - _p_len)])
                        _seg_down_vertices += [_line]
                    
                    n_vertices_created += 1
                elif k == i - 1:
                    # first
                    _curr_y = _coords[0][1]
                    _prev_y = self.segs[k - 1].coords[0][1]
                
                    _p_len = random.uniform(min_len, max_len)
                    
                    x = _coords[0][0]
                    y = _coords[0][1]
                    
                    if _curr_y > _prev_y:
                        _line = LineString([(x, y), (x, y + _p_len)])
                        _seg_up_vertices += [_line]
                    else:
                        _line = LineString([(x, y), (x, y - _p_len)])
                        _seg_down_vertices += [_line]
                    
                    n_vertices_created += 1
                
                    # interior
                    seg_len = _seg.length
                    #ref_p = Point(seg.coords[1][0], seg.coords[1][1])
                    
                    m = n_vertices - (n_vertices_created + 2)
                    
                    i_p = Point(_coords[0][0], _coords[0][1])
                    #f_p = Point(_coords[1][0], _coords[1][1])
               
                    dx = seg_len / (m + 1)
                    
                    _j = 0
                    while _j < m:
                        p_len = random.uniform(min_len, max_len)

                        x = i_p.x + dx * (_j + 1)
                        
                        if x >= _coords[1][0] - 0.01:
                            _j += 1
                            continue
                            
                        if x <= _coords[0][0] + 0.01:
                            _j += 1
                            continue
                        
                        y = i_p.y
                        
                        _p = random.uniform(0, 1)
                        
                        if _p >= 0.5:
                            y -= p_len
                            
                            _line = LineString([(x, i_p.y), (x, y)])
                            _seg_down_vertices += [_line]
                        else:
                            y += p_len
                            
                            _line = LineString([(x, i_p.y), (x, y)])
                            _seg_up_vertices += [_line]

                        n_vertices_created += 1
                        _j += 1
                    
                    ref_p = Point(_coords[1][0], _coords[1][1])
                    
                    #p_len = random.uniform(min_len, max_len)
                    _min_angle = 1
                    _max_angle = 79
                    
                    # 1
                    _line = self.get_vertex(ref_p, _min_angle, _max_angle, min_len, max_len, True)
                    _seg_up_vertices += [_line]
                    
                    # 2
                    _line = self.get_vertex(ref_p, _min_angle, _max_angle, min_len, max_len, False)
                    _seg_down_vertices += [_line]

                    n_vertices_created += 2
                else:
                    # first
                    _curr_y = _coords[0][1]
                    _prev_y = self.segs[k - 1].coords[0][1]
                
                    _p_len = random.uniform(min_len, max_len)
                    
                    x = _coords[0][0]
                    y = _coords[0][1]
                    
                    if _curr_y > _prev_y:
                        _line = LineString([(x, y), (x, y + _p_len)])
                        _seg_up_vertices += [_line]
                    else:
                        _line = LineString([(x, y), (x, y - _p_len)])
                        _seg_down_vertices += [_line]
                    
                    n_vertices_created += 1
                
                    # interior
                    i_p = Point(_coords[0][0], _coords[0][1])

                    seg_len = _seg.length
                    _nv = n_vertices_int_seg - 2
                    dx = seg_len / (_nv + 1)
                    
                    _j = 0
                    while _j < _nv:
                        p_len = random.uniform(min_len, max_len)

                        x = i_p.x + dx * (_j + 1)
                        y = i_p.y
                        
                        _p = random.uniform(0, 1)
                        
                        if _p >= 0.5:
                            y -= p_len
                            
                            _line = LineString([(x, i_p.y), (x, y)])
                            _seg_down_vertices += [_line]
                        else:
                            y += p_len
                            
                            _line = LineString([(x, i_p.y), (x, y)])
                            _seg_up_vertices += [_line]
                        
                        n_vertices_created += 1                   
                        _j += 1
               
                    # last
                    _curr_y = _coords[0][1]
                    _next_y = self.segs[k + 1].coords[0][1]
                    
                    _p_len = random.uniform(min_len, max_len)
                    
                    x = _coords[1][0]
                    y = _coords[1][1]
                        
                    if _curr_y > _next_y:
                        _line = LineString([(x, y), (x, y + _p_len)])
                        _seg_up_vertices += [_line]
                    else:
                        _line = LineString([(x, y), (x, y - _p_len)])
                        _seg_down_vertices += [_line]
                    
                    n_vertices_created += 1
                    
                self.vertices += [[_seg_up_vertices, _seg_down_vertices]]
                k += 1
    
    def generate_coords_from_vertices(self):
        _up_coords = []
        _down_coords = []
        
        for v in self.vertices:
            up = v[0]
            down = v[1]
            
            for _l in up:
                _up_coords += [(_l.coords[1][0], _l.coords[1][1])]

            for _l in down:
                _down_coords += [(_l.coords[1][0], _l.coords[1][1])]
         
         # >>>
         
        _len = len(_down_coords) - 1
        
        while _len >= 0:
            _up_coords += [_down_coords[_len]]
            _len -= 1
           
        _up_coords += [(_up_coords[0][0], _up_coords[0][1])]
        self.coords = _up_coords

    def shrink(self, min_percent, max_percent):
        # generate a new support line.
        
        vertices = []
                        
        for v in self.vertices:
            up_vertices = v[0]
            down_vertices = v[1]
            
            _seg_up_vertices = []
            _seg_down_vertices = []
            
            for _l in up_vertices:
                dx = _l.coords[1][0] - _l.coords[0][0]
                dy = _l.coords[1][1] - _l.coords[0][1]
                
                dxx = random.uniform(dx / min_percent, dx / max_percent)
                dyy = random.uniform(dy / min_percent, dy / max_percent)
                 
                _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                _seg_up_vertices += [_line]
            
            for _l in down_vertices:
                dx = _l.coords[1][0] - _l.coords[0][0]
                dy = _l.coords[1][1] - _l.coords[0][1]
                        
                dxx = random.uniform(dx / min_percent, dx / max_percent)
                dyy = random.uniform(dy / min_percent, dy / max_percent)
                 
                _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                _seg_down_vertices += [_line]
            
            vertices += [[_seg_up_vertices, _seg_down_vertices]]
        
        supp_line = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
    
        self.generate_coords_from_vertices()
        source_coords = self.get_coords()
    
        supp_line.generate_coords_from_vertices()
        target_coords = supp_line.get_coords()
    
        return supp_line, source_coords, target_coords

    def shrink_with_restrictions(self, min_percent, max_percent, up_restriction, d_restriction):
        # generate a new support line.
        
        vertices = []
        counter = 0
        k = 0
        for v in self.vertices:
            up_vertices = v[0]
            down_vertices = v[1]
            
            _seg_up_vertices = []
            _seg_down_vertices = []
            
            j = 0
            for _l in up_vertices:
            
                if up_restriction != None and k == up_restriction[0] and j == up_restriction[1]:
                    """
                    x0 = up_restriction[2][0]
                    y0 = up_restriction[2][1]
                    
                    x1 = up_restriction[3][0]
                    y1 = up_restriction[3][1]
                    
                    x = _l.coords[0][0]
                    
                    dx = x1 - x0
                    dy = y1 - y0
                    
                    f = (x - x0) / dx
                    y = y0 + dy * f

                    if up_restriction[4] == 1:
                        y += y * np.random.uniform(0.1, 0.15)
                    """
                    
                    x = up_restriction[2]
                    y = up_restriction[3]
                    
                    _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (x, y)])
                    _seg_up_vertices += [_line]
                else:
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                    
                    dxx = random.uniform(dx / min_percent, dx / max_percent)
                    dyy = random.uniform(dy / min_percent, dy / max_percent)
                     
                    _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                    _seg_up_vertices += [_line]
                
                j += 1
            
            j = 0
            for _l in down_vertices:
            
                if d_restriction != None and k == d_restriction[0] and j == d_restriction[1]:
                    """
                    x0 = d_restriction[2][0]
                    y0 = d_restriction[2][1]
                    
                    x1 = d_restriction[3][0]
                    y1 = d_restriction[3][1]
                    
                    x = _l.coords[0][0]
                    
                    dx = x1 - x0
                    dy = y1 - y0
                    
                    f = (x - x0) / dx
                    y = y0 + dy * f
                    
                    if d_restriction[4] == 1:
                        y -= y * np.random.uniform(0.1, 0.2)
                    """
                    
                    x = d_restriction[2]
                    y = d_restriction[3]
                    
                    _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (x, y)])
                    _seg_down_vertices += [_line]
                else:
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                            
                    dxx = random.uniform(dx / min_percent, dx / max_percent)
                    dyy = random.uniform(dy / min_percent, dy / max_percent)
                     
                    _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                    _seg_down_vertices += [_line]
                
                j += 1
            
            vertices += [[_seg_up_vertices, _seg_down_vertices]]
            k += 1
        
        supp_line = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
    
        self.generate_coords_from_vertices()
        source_coords = self.get_coords()
    
        supp_line.generate_coords_from_vertices()
        target_coords = supp_line.get_coords()
    
        return supp_line, source_coords, target_coords

    def grow(self, min_percent, max_percent):
        # generate a new support line.
        
        vertices = []
                        
        for v in self.vertices:
            up_vertices = v[0]
            down_vertices = v[1]
            
            _seg_up_vertices = []
            _seg_down_vertices = []
            
            for _l in up_vertices:
                dx = _l.coords[1][0] - _l.coords[0][0]
                dy = _l.coords[1][1] - _l.coords[0][1]
                
                dxx = random.uniform(dx / min_percent, dx / max_percent)
                dyy = random.uniform(dy / min_percent, dy / max_percent)
                 
                _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])
                _seg_up_vertices += [_line]
            
            for _l in down_vertices:
                dx = _l.coords[1][0] - _l.coords[0][0]
                dy = _l.coords[1][1] - _l.coords[0][1]
                        
                dxx = random.uniform(dx / min_percent, dx / max_percent)
                dyy = random.uniform(dy / min_percent, dy / max_percent)
                 
                _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])
                _seg_down_vertices += [_line]
            
            vertices += [[_seg_up_vertices, _seg_down_vertices]]
        
        supp_line = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
    
        self.generate_coords_from_vertices()
        source_coords = self.get_coords()
    
        supp_line.generate_coords_from_vertices()
        target_coords = supp_line.get_coords()
    
        return supp_line, source_coords, target_coords

    def grow2(self, u_min_percent, u_max_percent, d_min_percent, d_max_percent):
        vertices = []
                        
        for v in self.vertices:
            up_vertices = v[0]
            down_vertices = v[1]
            
            _seg_up_vertices = []
            _seg_down_vertices = []
            
            for _l in up_vertices:
                dx = _l.coords[1][0] - _l.coords[0][0]
                dy = _l.coords[1][1] - _l.coords[0][1]
                
                dxx = random.uniform(dx / u_min_percent, dx / u_max_percent)
                dyy = random.uniform(dy / u_min_percent, dy / u_max_percent)
                 
                _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])
                _seg_up_vertices += [_line]
            
            for _l in down_vertices:
                dx = _l.coords[1][0] - _l.coords[0][0]
                dy = _l.coords[1][1] - _l.coords[0][1]
                        
                dxx = random.uniform(dx / d_min_percent, dx / d_max_percent)
                dyy = random.uniform(dy / d_min_percent, dy / d_max_percent)
                 
                _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])
                _seg_down_vertices += [_line]
            
            vertices += [[_seg_up_vertices, _seg_down_vertices]]
        
        supp_line = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
    
        self.generate_coords_from_vertices()
        source_coords = self.get_coords()
    
        supp_line.generate_coords_from_vertices()
        target_coords = supp_line.get_coords()
    
        return supp_line, source_coords, target_coords

    def grow_to_line(self, target):
        N = len(self.vertices)
        i = 0
        
        s_vertices = self.vertices
        t_vertices = target.get_vertices()
        
        u_prev_v1 = None
        d_prev_v1 = None
        
        u_prev_v2 = None
        d_prev_v2 = None
        
        u_next_v1 = None
        d_next_v1 = None
        
        u_next_v2 = None
        d_next_v2 = None
        
        s_u_coords = []
        s_d_coords = []
        
        t_u_coords = []
        t_d_coords = []
        
        while i < N:
            u_v_1 = s_vertices[i][0]
            d_v_1 = s_vertices[i][1]
            
            u_v_2 = t_vertices[i][0]
            d_v_2 = t_vertices[i][1]
            
            #print(i, N)
            
            M = len(u_v_1)
            K = len(u_v_2)
            
            j = 0
            g = 0
            
            while j < M and g < K:
                v1 = u_v_1[j]
                v2 = u_v_2[g]
                
                v1_advance = False
                v2_advance = False
                
                if round(v1.coords[0][0], 3) == round(v2.coords[0][0], 3):# and v1.coords[0][1] == v2.coords[0][1]:
                    s_u_coords += [(v1.coords[1][0], v1.coords[1][1])]
                    t_u_coords += [(v2.coords[1][0], v2.coords[1][1])]
                    j += 1
                    g += 1
                    
                    v1_advance = True
                    v2_advance = True
                elif v1.coords[0][0] > v2.coords[0][0]:
                    _x0 = u_prev_v1.coords[1][0]
                    _y0 = u_prev_v1.coords[1][1]
                    
                    _x1 = v1.coords[1][0]
                    _y1 = v1.coords[1][1]
                    
                    dxx = _x1 - _x0
                    dyy = _y1 - _y0
                    
                    x1 = v2.coords[1][0]
                    y1 = _y0 + dyy * ((x1 - _x0) / dxx)

                    s_u_coords += [( x1, y1)]
                    t_u_coords += [(v2.coords[1][0], v2.coords[1][1])]

                    v2_advance = True
                    g += 1
                else:
                    _x0 = u_prev_v2.coords[1][0]
                    _y0 = u_prev_v2.coords[1][1]
                    
                    _x1 = v2.coords[1][0]
                    _y1 = v2.coords[1][1]
                    
                    dxx = _x1 - _x0
                    dyy = _y1 - _y0
                    
                    x1 = v1.coords[1][0]
                    y1 = _y0 + dyy * ((x1 - _x0) / dxx)
                
                    s_u_coords += [(v1.coords[1][0], v1.coords[1][1])]
                    t_u_coords += [(x1, y1)]
                    
                    v1_advance = True
                    j += 1
            
                if u_prev_v1 == None or v1_advance:
                    u_prev_v1 = v1
                
                if u_prev_v2 == None or v2_advance:
                    u_prev_v2 = v2
                
                """
                if i == N - 1:
                    print(v1.coords[0][0], v2.coords[0][0], M, K, j, g)
                """
            """
            if i == N - 1:
                print(M, K, j, g)
            """
            while j < M:
                v1 = u_v_1[j]
                """
                if i == N - 1:
                    print(v1.coords[0][0])
                """
                u_next_v2 = t_vertices[i + 1][0][0]
                    
                _x0 = u_prev_v2.coords[1][0]
                _y0 = u_prev_v2.coords[1][1]
                    
                _x1 = u_next_v2.coords[1][0]
                _y1 = u_next_v2.coords[1][1]
                    
                dxx = _x1 - _x0
                dyy = _y1 - _y0
                    
                x1 = v1.coords[1][0]
                y1 = _y0 + dyy * ((x1 - _x0) / dxx)
                
                s_u_coords += [(v1.coords[1][0], v1.coords[1][1])]
                t_u_coords += [(x1, y1)]
                
                j += 1
            
            while g < K:
                v2 = u_v_2[g]
                """
                if i == N - 1:
                    print(v2.coords[0][0])
                """
                u_next_v1 = s_vertices[i + 1][0][0]
                    
                _x0 = u_prev_v1.coords[1][0]
                _y0 = u_prev_v1.coords[1][1]
                    
                _x1 = u_next_v1.coords[1][0]
                _y1 = u_next_v1.coords[1][1]
                    
                dxx = _x1 - _x0
                dyy = _y1 - _y0
                    
                x1 = v2.coords[1][0]
                y1 = _y0 + dyy * ((x1 - _x0) / dxx)
                
                s_u_coords += [( x1, y1)]
                t_u_coords += [(v2.coords[1][0], v2.coords[1][1])]

                g += 1
            
            # >
            
            M = len(d_v_1)
            K = len(d_v_2)
                        
            j = 0
            g = 0
            
            while j < M and g < K:
                v1 = d_v_1[j]
                v2 = d_v_2[g]
                
                v1_advance = False
                v2_advance = False
                
                if round(v1.coords[0][0], 3) == round(v2.coords[0][0], 3):#and v1.coords[0][1] == v2.coords[0][1]:
                    s_d_coords += [(v1.coords[1][0], v1.coords[1][1])]
                    t_d_coords += [(v2.coords[1][0], v2.coords[1][1])]
                    
                    v1_advance = True
                    v2_advance = True
                    j += 1
                    g += 1
                elif v1.coords[0][0] > v2.coords[0][0]:
                    _x0 = d_prev_v1.coords[1][0]
                    _y0 = d_prev_v1.coords[1][1]
                    
                    _x1 = v1.coords[1][0]
                    _y1 = v1.coords[1][1]
                    
                    dxx = _x1 - _x0
                    dyy = _y1 - _y0
                    
                    x1 = v2.coords[1][0]
                    y1 = _y0 + dyy * ((x1 - _x0) / dxx)

                    s_d_coords += [( x1, y1)]
                    t_d_coords += [(v2.coords[1][0], v2.coords[1][1])]

                    v2_advance = True
                    g += 1
                else:
                    _x0 = d_prev_v2.coords[1][0]
                    _y0 = d_prev_v2.coords[1][1]
                    
                    _x1 = v2.coords[1][0]
                    _y1 = v2.coords[1][1]
                    
                    dxx = _x1 - _x0
                    dyy = _y1 - _y0
                    
                    x1 = v1.coords[1][0]
                    y1 = _y0 + dyy * ((x1 - _x0) / dxx)
                
                    s_d_coords += [(v1.coords[1][0], v1.coords[1][1])]
                    t_d_coords += [(x1, y1)]
                    
                    v1_advance = True
                    j += 1
            
                if d_prev_v1 == None or v1_advance:
                    d_prev_v1 = v1
                
                if d_prev_v2 == None or v2_advance:
                    d_prev_v2 = v2
                """
                if i == N - 1:
                    print(v1.coords[0][0], v2.coords[0][0], M, K, j, g)
                """
            """
            if i == N - 1:
                print(M, K, j, g)
            """
            while j < M:
                v1 = d_v_1[j]
                """
                if i == N - 1:
                    print(v1.coords[0][0])
                """
                d_next_v2 = t_vertices[i + 1][1][0]
                    
                _x0 = d_prev_v2.coords[1][0]
                _y0 = d_prev_v2.coords[1][1]
                    
                _x1 = d_next_v2.coords[1][0]
                _y1 = d_next_v2.coords[1][1]
                    
                dxx = _x1 - _x0
                dyy = _y1 - _y0
                    
                x1 = v1.coords[1][0]
                y1 = _y0 + dyy * ((x1 - _x0) / dxx)
                
                s_d_coords += [(v1.coords[1][0], v1.coords[1][1])]
                t_d_coords += [(x1, y1)]
                
                j += 1
            
            while g < K:
                v2 = d_v_2[g]
                """
                if i == N - 1:
                    print(v2.coords[0][0])
                """
                d_next_v1 = s_vertices[i + 1][1][0]
                    
                _x0 = d_prev_v1.coords[1][0]
                _y0 = d_prev_v1.coords[1][1]
                    
                _x1 = d_next_v1.coords[1][0]
                _y1 = d_next_v1.coords[1][1]
                    
                dxx = _x1 - _x0
                dyy = _y1 - _y0
                    
                x1 = v2.coords[1][0]
                y1 = _y0 + dyy * ((x1 - _x0) / dxx)
                
                s_d_coords += [( x1, y1)]
                t_d_coords += [(v2.coords[1][0], v2.coords[1][1])]

                g += 1
            
            i += 1

        _len = len(s_d_coords) - 1
        
        while _len >= 0:
            s_u_coords += [s_d_coords[_len]]
            _len -= 1
           
        s_u_coords += [(s_u_coords[0][0], s_u_coords[0][1])]

        _len = len(t_d_coords) - 1
        
        while _len >= 0:
            t_u_coords += [t_d_coords[_len]]
            _len -= 1
           
        t_u_coords += [(t_u_coords[0][0], t_u_coords[0][1])]

        return s_u_coords, t_u_coords

    def evolve(self, min_percent, max_percent):
        # generate a new support line.
        
        vertices = []
                        
        for v in self.vertices:
            up_vertices = v[0]
            down_vertices = v[1]
            
            _seg_up_vertices = []
            _seg_down_vertices = []
            
            for _l in up_vertices:
                dx = _l.coords[1][0] - _l.coords[0][0]
                dy = _l.coords[1][1] - _l.coords[0][1]
                
                dxx = random.uniform(dx / min_percent, dx / max_percent)
                dyy = random.uniform(dy / min_percent, dy / max_percent)
                
                prob = random.uniform(0, 1)
                
                if prob <= 0.5:
                    _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])
                else:
                    _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])

                _seg_up_vertices += [_line]
            
            for _l in down_vertices:
                dx = _l.coords[1][0] - _l.coords[0][0]
                dy = _l.coords[1][1] - _l.coords[0][1]
                        
                dxx = random.uniform(dx / min_percent, dx / max_percent)
                dyy = random.uniform(dy / min_percent, dy / max_percent)
                
                prob = random.uniform(0, 1)
                
                if prob <= 0.5:
                    _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])               
                else:
                    _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])

                _seg_down_vertices += [_line]
            
            vertices += [[_seg_up_vertices, _seg_down_vertices]]
        
        supp_line = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
    
        self.generate_coords_from_vertices()
        source_coords = self.get_coords()
    
        supp_line.generate_coords_from_vertices()
        target_coords = supp_line.get_coords()
    
        return supp_line, source_coords, target_coords

    def find_two_points_for_splitting(self):
        seg_idx_1 = np.random.randint(1, len(self.segs))
        seg_idx_2 = np.random.randint(1, len(self.segs))
        
        seg_vertices_1 = self.vertices[seg_idx_1]
        seg_vertices_2 = self.vertices[seg_idx_2]

        up = seg_vertices_1[0]
        down = seg_vertices_2[1]
        
        v_up_idx = np.random.randint(1, len(up))       
        v_dw_idx = np.random.randint(1, len(down))

        return seg_idx_1, seg_idx_2, v_up_idx, v_dw_idx

    def get_up_dx(self, seg_idx_1, v_up_idx, x_up):
        _n = len(self.vertices[seg_idx_1][1])
        #_k = v_up_idx - 1
        
        _prev_up = None
        _prev_dw = None
        
        if v_up_idx - 1 >= 0:
            _prev_up = self.vertices[seg_idx_1][0][v_up_idx - 1]
        
        _i = _n - 1
        
        while _i >= 0:
            _prev_dw = self.vertices[seg_idx_1][1][_i]
            
            if _prev_dw.coords[0][0] < x_up:
                if _prev_up == None:
                    return _prev_dw
                elif _prev_up.coords[0][0] > _prev_dw.coords[0][0]:
                    return _prev_up
                else:
                    return _prev_dw
            
            _i -= 1
        
        if not _prev_up == None:
            return _prev_up
        
        # 2. prev segment.
        
        _prev_seg = self.vertices[seg_idx_1 - 1]
        
        _up_vertices = _prev_seg[0]
        _down_vertices = _prev_seg[1]
        
        _n_up_vertices = len(_up_vertices)
        _prev_up = _up_vertices[_n_up_vertices - 1]
        
        _n_down_vertices = len(_down_vertices)
        _prev_dw = _down_vertices[_n_down_vertices - 1]
        
        if _prev_up.coords[0][0] > _prev_dw.coords[0][0]:
            return _prev_up
        else:
            return _prev_dw

    def get_down_dx(self, seg_idx_2, v_down_idx, x_dw):
        _next_up = None
        _next_dw = None
        
        _n = len(self.vertices[seg_idx_2][1])
        
        if v_down_idx + 1 < _n:
            _next_dw = self.vertices[seg_idx_2][1][v_down_idx + 1]
        
        _i = 0
        _m = len(self.vertices[seg_idx_2][0])
        
        while _i < _m:
        
            _next_up = self.vertices[seg_idx_2][0][_i]
            
            if _next_up.coords[0][0] > x_dw:
                if _next_dw == None:
                    return _next_up
                elif _next_up.coords[0][0] < _next_dw.coords[0][0]:
                    return _next_up
                else:
                    return _next_dw
            
            _i += 1
        
        if not _next_dw == None:
            return _next_dw
        
        # 2. next segment.
        
        _next_seg = self.vertices[seg_idx_2 + 1]
        
        _up_vertices = _next_seg[0]
        _down_vertices = _next_seg[1]
        
        _n_up_vertices = len(_up_vertices)
        _next_up = _up_vertices[0]
        
        _n_down_vertices = len(_down_vertices)
        _next_dw = _down_vertices[0]
        
        if _next_up.coords[0][0] < _next_dw.coords[0][0]:
            return _next_up
        else:
            return _next_dw

    def get_coords_from_upper_and_lower_vertices(self, up_coords, down_coords):
        _len = len(down_coords) - 1
        
        while _len >= 0:
            up_coords += [down_coords[_len]]
            _len -= 1
           
        up_coords += [(up_coords[0][0], up_coords[0][1])]
        return up_coords

    def crack(self):
        _x_off_up = []
        _x_off_down = []
        
        _support_line_coords = []
        
        seg_idx_1 = -1
        seg_idx_2 = -1
        
        v_up_idx = -1
        v_dw_idx = -1
        
        _up_line = None
        _dw_line = None
        
        x_up = None
        x_dw = None
        
        """
        """
        
        up_idx = -1
        down_idx = -1
        
        up_dx = 0
        down_dx = 0
    
        while True:
            seg_idx_1, seg_idx_2, v_up_idx, v_dw_idx = self.find_two_points_for_splitting()
            
            _up_line = self.vertices[seg_idx_1][0][v_up_idx]
            _dw_line = self.vertices[seg_idx_2][1][v_dw_idx]
        
            x_up = _up_line.coords[0][0]
            x_dw = _dw_line.coords[0][0]
        
            if x_up > x_dw:
                break

        if x_up > x_dw:
        
            # endpoint vertices coordinates
            
            up_dx = self.get_up_dx(seg_idx_1, v_up_idx, x_up)
        
            down_dx = self.get_down_dx(seg_idx_2, v_dw_idx, x_dw)
                
            _i = seg_idx_2

            while _i <= seg_idx_1:
                _list_seg_vertices = self.vertices[_i]
                
                _up_vertices = _list_seg_vertices[0]
                _down_vertices = _list_seg_vertices[1]
                
                _min_up_len = sys.float_info.max
                _min_down_len = sys.float_info.max
                
                # first seg
                
                if _i == seg_idx_2:
                    # up
                    
                    _k = 0
                    for v in _up_vertices:
                        if v.coords[0][0] > x_dw:
                            if up_idx == -1:
                                up_idx = _k
                            
                            if v.length < _min_up_len:
                                _min_up_len = v.length
                    
                        _k += 1
                    
                    # down
                    
                    _n = len(_down_vertices)
                    _k = v_dw_idx
                    
                    while _k < _n:
                        _v = _down_vertices[_k]
                        
                        if _v.length < _min_down_len:
                            _min_down_len = _v.length
                                
                        _k += 1
                    
                    # store results
                    
                    _x_off_up += [_min_up_len / 2]
                    _x_off_down += [_min_down_len / 2]
                    
                    if _up_vertices[len(_up_vertices) - 1].coords[0][0] > _down_vertices[_n - 1].coords[0][0]:
                        _support_line_coords += [_up_vertices[len(_up_vertices) - 1]]
                    else:
                        _support_line_coords += [_down_vertices[_n - 1]]
                
                # last seg
                
                elif _i == seg_idx_1:
                    # up
                    
                    _k = v_up_idx
                    _h = 0
                    
                    while _h <= _k:
                        _v = _up_vertices[_h]
                        
                        if _v.length < _min_up_len:
                            _min_up_len = _v.length
                                
                        _h += 1
                    
                    # down
                    
                    _k = 0
                    for v in _down_vertices:
                        if v.coords[0][0] < x_up:
                            down_idx = _k
                            
                            if v.length < _min_down_len:
                                _min_down_len = v.length
                    
                            _k += 1
                        else:
                            break
                    
                    # store results
                    
                    _x_off_up += [_min_up_len / 2]
                    _x_off_down += [_min_down_len / 2]
                    
                # Interior seg
                
                else:
                    # up
                    
                    for v in _up_vertices:
                        if v.length < _min_up_len:
                            _min_up_len = v.length

                    for v in _down_vertices:
                        if v.length < _min_down_len:
                            _min_down_len = v.length

                    # store results
                    
                    _x_off_up += [_min_up_len / 2]
                    _x_off_down += [_min_down_len / 2]
                    
                    _m = len(_up_vertices) - 1
                    _n = len(_down_vertices) - 1
                    
                    if _up_vertices[_m].coords[0][0] > _down_vertices[_n].coords[0][0]:
                        _support_line_coords += [_up_vertices[_m]]
                    else:
                        _support_line_coords += [_down_vertices[_n]]
                
                _i += 1
        else:
            pass
    
        # 1 . choose a random target position
    
        s_coords = []
        s_down_coords = []
        
        t_coords = []
        t_down_coords = []
    
        min_percent = 10
        max_percent = 8
        line = None
        _h = 0
    
        vertices_1 = []
        vertices_2 = []
        
        supp_line_1 = []
        supp_line_2 = []
        
        flag_up = 0
        flag_dw = 0
        
        dxx1 = 0
        dyy1 = 0
        
        dxx2 = 0
        dyy2 = 0
    
        for v in self.vertices:
            up = v[0]
            down = v[1]
            
            _seg_up_vertices_1 = []
            _seg_down_vertices_1 = []
            
            _seg_up_vertices_2 = []
            _seg_down_vertices_2 = []
            
            if x_up > x_dw:
            
                if _h == 0:
                    
                    dxx2 = _dw_line.coords[0][0]
                    dxx2 += (down_dx.coords[0][0] - _dw_line.coords[0][0]) * 0.8
                    dyy2 = _dw_line.coords[0][1] - _x_off_down[0]
                    
                    _line = LineString([(dxx2, dyy2), (_dw_line.coords[0][0], _dw_line.coords[0][1])])
                    _seg_up_vertices_2 += [_line]

                _g = 0
                
                for _l in up:
                    # transformation from source to target
                    """
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                    
                    dxx = random.uniform(dx / min_percent, dx / max_percent)
                    dyy = random.uniform(dy / min_percent, dy / max_percent)
                        
                    # Choose randomly if the vertex grows or shrinks.
                        
                    r_num = random.uniform(0, 1)
                        
                    if r_num >= 0.5:
                        line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])
                    else:
                        line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])             
                             
                    t_coords += [(line.coords[1][0], line.coords[1][1])]
                    s_coords += [(_l.coords[1][0], _l.coords[1][1])]
                    """

                    if _h == seg_idx_2 and _l.coords[0][0] > x_dw:
                        _line = LineString([(_l.coords[0][0], _l.coords[0][1] + _x_off_up[_h - seg_idx_2]), (line.coords[1][0], line.coords[1][1])])
                        #_line.coords[0][1] += _x_off_up[_h - seg_idx_2]
                        _seg_up_vertices_1 += [_line]
                        
                        _line = LineString([(line.coords[0][0], line.coords[0][1] - _x_off_down[_h - seg_idx_2]), (line.coords[0][0], line.coords[0][1])])
                        _seg_up_vertices_2 += [_line]
                    elif _h > seg_idx_2 and _h < seg_idx_1:
                        #_line = LineString(line)
                        #_line.coords[0][1] += _x_off_up[_h - seg_idx_2]
                        _line = LineString([(line.coords[0][0], line.coords[0][1] + _x_off_up[_h - seg_idx_2]), (line.coords[1][0], line.coords[1][1])])
                        _seg_up_vertices_1 += [_line]

                        _line = LineString([(line.coords[0][0], line.coords[0][1] - _x_off_down[_h - seg_idx_2]), (line.coords[0][0], line.coords[0][1])])
                        _seg_up_vertices_2 += [_line]
                    elif _h == seg_idx_1 and _g <= v_up_idx:
                        #_line = LineString(line)
                        #_line.coords[0][1] += _x_off_up[_h - seg_idx_2]
                        
                        _x = line.coords[0][0]
                        _y = line.coords[0][1] + _x_off_up[_h - seg_idx_2]
                        
                        if _g == v_up_idx:
                            #_dx = (line.coords[0][0] - up_dx.coords[0][0]) / 4
                            _dx = (line.coords[0][0] - up_dx.coords[0][0]) * 0.8
                            #_line.coords[0][0] -= _dx
                            _x -= _dx
                        
                            dxx1 = _x
                            dyy1 = _y
                        
                        _line = LineString([(_x, _y), (line.coords[1][0], line.coords[1][1])])
                        _seg_up_vertices_1 += [_line]
                        
                        if not _g == v_up_idx:
                            _line = LineString([(line.coords[0][0], line.coords[0][1] - _x_off_down[_h - seg_idx_2]), (line.coords[0][0], line.coords[0][1])])
                            _seg_up_vertices_2 += [_line]
                        else:
                            _seg_up_vertices_2 += [line]

                        flag_up = 1
                    else:       
                        if flag_up == 0:
                            _seg_up_vertices_1 += [line]
                        else:
                            _seg_up_vertices_2 += [line]

                    _g += 1
                    
                _g = 0
                
                for _l in down:
                    """
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                                
                    dxx = random.uniform(dx / min_percent, dx / max_percent)
                    dyy = random.uniform(dy / min_percent, dy / max_percent)
                        
                    # Choose randomly if the vertex grows or shrinks.
                        
                    r_num = random.uniform(0, 1)
                        
                    if r_num >= 0.5:
                        line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])
                    else:
                        line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])             
                                
                    t_down_coords += [(line.coords[1][0], line.coords[1][1])]
                    s_down_coords += [(_l.coords[1][0], _l.coords[1][1])]
                    """
                    
                    # end transformation from source to target
                
                    if _h == seg_idx_2 and _g >= v_dw_idx:
                        if _g == v_dw_idx:
                            #sys.exit()
                            _seg_down_vertices_1 += [line]
                            
                            _line = LineString([(dxx2, dyy2), (line.coords[1][0], line.coords[1][1])])
                            _seg_down_vertices_2 += [_line]
                        else:
                            _line = LineString([(line.coords[0][0], line.coords[0][1] + _x_off_up[_h - seg_idx_2]), (line.coords[0][0], line.coords[0][1])])
                            _seg_down_vertices_1 += [_line]
                    
                            #_line = LineString(line)
                            #_line.coords[0][1] -= _x_off_down[_h - seg_idx_2]
                            _line = LineString([(line.coords[0][0], line.coords[0][1] - _x_off_down[_h - seg_idx_2]), (line.coords[1][0], line.coords[1][1])])
                            _seg_down_vertices_2 += [_line]
                    elif _h > seg_idx_2 and _h < seg_idx_1:
                        _line = LineString([(line.coords[0][0], line.coords[0][1] + _x_off_up[_h - seg_idx_2]), (line.coords[0][0], line.coords[0][1])])
                        _seg_down_vertices_1 += [_line]
                    
                        #_line = LineString(line)
                        #_line.coords[0][1] -= _x_off_down[_h - seg_idx_2]
                        _line = LineString([(line.coords[0][0], line.coords[0][1] - _x_off_down[_h - seg_idx_2]), (line.coords[1][0], line.coords[1][1])])
                        _seg_down_vertices_2 += [_line]
                    elif _h == seg_idx_1 and line.coords[0][0] < x_up:
                        _line = LineString([(line.coords[0][0], line.coords[0][1] + _x_off_up[_h - seg_idx_2]), (line.coords[0][0], line.coords[0][1])])
                        _seg_down_vertices_1 += [_line]
                    
                        _line = LineString([(line.coords[0][0], line.coords[0][1] - _x_off_down[_h - seg_idx_2]), (line.coords[1][0], line.coords[1][1])])
                        _seg_down_vertices_2 += [_line]
                        
                        flag_dw = 1
                    else:       
                        if flag_dw == 0:
                            _seg_down_vertices_1 += [line]
                        else:
                            _seg_down_vertices_2 += [line]

                    _g += 1
            else:
            
                """
                _g = 0
                
                for _l in up:
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                        
                    dxx = random.uniform(dx / min_percent, dx / max_percent)
                    dyy = random.uniform(dy / min_percent, dy / max_percent)
                        
                    # Choose randomly if the vertex grows or shrinks.
                        
                    r_num = random.uniform(0, 1)
                        
                    if r_num >= 0.5:
                        line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])
                    else:
                        line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])             
                             
                    t_coords += [(line.coords[1][0], line.coords[1][1])]
                    s_coords += [(_l.coords[1][0], _l.coords[1][1])]
                
                    if _h == seg_idx_1 and _g == v_up_idx:
                        _line = LineString([(line.coords[0][0], line.coords[0][1]), (line.coords[0][0], line.coords[0][1])])
                        
                        _seg_up_vertices_1 += [line]
                        _seg_up_vertices_1 += [_line]
                        
                        _seg_up_vertices_2 += [_line]
                        _seg_up_vertices_2 += [line]
                        
                        flag_up = 1                
                    else:       
                        if flag_up == 0:
                            _seg_up_vertices_1 += [line]
                        else:
                            _seg_up_vertices_2 += [line]

                    _g += 1
                
                _g = 0
                
                for _l in down:
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                                
                    dxx = random.uniform(dx / min_percent, dx / max_percent)
                    dyy = random.uniform(dy / min_percent, dy / max_percent)
                        
                    # Choose randomly if the vertex grows or shrinks.
                        
                    r_num = random.uniform(0, 1)
                        
                    if r_num >= 0.5:
                        line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] + dxx, _l.coords[1][1] + dyy)])
                    else:
                        line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])             
                                
                    t_down_coords += [(line.coords[1][0], line.coords[1][1])]
                    s_down_coords += [(_l.coords[1][0], _l.coords[1][1])]
                

                    if _h == seg_idx_2 and _g == v_dw_idx:
                        _line = LineString([(line.coords[0][0], line.coords[0][1]), (line.coords[0][0], line.coords[0][1])])
                        
                        _seg_down_vertices_1 += [line]
                        _seg_down_vertices_1 += [_line]
                        
                        _seg_down_vertices_2 += [_line]
                        _seg_down_vertices_2 += [line]
                        
                        flag_dw = 1
                    else:
                        if flag_dw == 0:
                            _seg_down_vertices_1 += [line]
                        else:
                            _seg_down_vertices_2 += [line]
                    
                    _g += 1
                
                """
                
                pass
            
            if len(_seg_up_vertices_1) > 0 or len(_seg_down_vertices_1) > 0:
                vertices_1 += [[_seg_up_vertices_1, _seg_down_vertices_1]]
            
            if len(_seg_up_vertices_2) > 0 or len(_seg_down_vertices_2) > 0:
                vertices_2 += [[_seg_up_vertices_2, _seg_down_vertices_2]]
            
            _h += 1
    
        if x_up > x_dw:
            # adjust the last down vertex on the last seg of the first poly.
            
            dwv = vertices_1[len(vertices_1) - 1][1]

            _line = LineString([(dxx1, dyy1), (_up_line.coords[0][0], _up_line.coords[0][1])])
            dwv += [_line]
            
            # if it has only one seg up / down in the first seg (second poly generated by split).
            
            _upv = vertices_2[0][0]
            _downv = vertices_2[0][1]
            
            if len(_upv) == len(_downv) and  len(_upv) == 1:
                _y = vertices_2[1][0][0].coords[0][1]
                line = vertices_2[0][0][0]
                
                _line = LineString([(line.coords[0][0], _y), (line.coords[1][0], line.coords[1][1])])
                vertices_2[0][0] = [_line]
                
                line = vertices_2[0][1][0]
                _line = LineString([(line.coords[0][0], _y), (line.coords[1][0], line.coords[1][1])])
                vertices_2[0][1] = [_line]

            #__n = len(self.segs)
        
        for v in vertices_1:
            up = v[0]
            down = v[1]
            
            for l in up:
                print(l.wkt + ';')
            
            for l in down:
                print(l.wkt + ';')
        
        sys.exit()
        
        """
        # .
        s_coords = self.get_coords_from_upper_and_lower_vertices(s_coords, s_down_coords)
        t_coords = self.get_coords_from_upper_and_lower_vertices(t_coords, t_down_coords)
        
        # After split transform.
        
        dxx1 = 35
        dyy1 = random.uniform(seg_min_len, seg_max_len) / 2
        
        dxx2 = 25
        dyy2 = random.uniform(seg_min_len, seg_max_len) / 3

        min_percent = 10
        max_percent = 5
    
        s1_up_coords, s1_down_coords, t1_up_coords, t1_down_coords = get_simple_split_coords_1(vertices_1, dxx1, dyy1, dxx2, dyy2, min_percent, max_percent)

        s1_coords = get_coords_from_upper_and_lower_vertices(s1_up_coords, s1_down_coords)
        t1_coords = get_coords_from_upper_and_lower_vertices(t1_up_coords, t1_down_coords)
            
        min_percent = 10
        max_percent = 5
        
        s2_up_coords, s2_down_coords, t2_up_coords, t2_down_coords = get_simple_split_coords_2(vertices_2, dxx1, dyy1, dxx2, dyy2, min_percent, max_percent)

        s2_coords = get_coords_from_upper_and_lower_vertices(s2_up_coords, s2_down_coords)
        t2_coords = get_coords_from_upper_and_lower_vertices(t2_up_coords, t2_down_coords)
        """
      
    def cut(self, seg_id, up):
        cut_spp_line = None

        seg_vertices = self.vertices[seg_id]
        
        vertices = seg_vertices[1]
        #_vertices = seg_vertices[0]
        
        if up == 1:
            vertices = seg_vertices[0]
            #_vertices = seg_vertices[1]
        
        # get y
        
        min_seg_len = sys.float_info.max
        
        for v in vertices:
            if v.length < min_seg_len:
                min_seg_len = v.length
        
        y0 = vertices[0].coords[0][1]
        #y1 = vertices[0].coords[1][1]
        
        _dyy = min_seg_len / 4
        
        y = 0
        if up == 1:
            y = y0 + _dyy * 2
        else:
            y = y0 - _dyy * 2
        
        # get cut sl
        
        m = len(vertices)
        
        segs = []
        
        x0 = vertices[0].coords[0][0]
        x1 = vertices[1].coords[0][0]

        x = x0 + (x1 - x0) / 2

        x0 = vertices[m - 2].coords[0][0]
        x1 = vertices[m - 1].coords[0][0]
        
        xx = x0 + (x1 - x0) / 2
        
        _seg = LineString([(x, y), (xx, y)])
        segs += [_seg]
        
        # get vertices
        
        cut_vertices = []
        
        up_vertices = []
        down_vertices = []
        
        i = 0
        _line = None
        
        while i < m:
            v = vertices[i]
            coords = v.coords

            if i == 0:
                _line = LineString([(x, y), (coords[1][0], coords[1][1])])
            elif i == m - 1:
                _line = LineString([(xx, y), (coords[1][0], coords[1][1])])
            else:
                _line = LineString([(coords[0][0], y), (coords[1][0], coords[1][1])])
        
            if up == 1:
                up_vertices += [_line]
            else:
                down_vertices += [_line]
            
            i += 1
        
        i = 1
        while i < m - 1:
            v = vertices[i]
            coords = v.coords
            
            x0 = coords[0][0]
            y0 = coords[0][1]
            
            x1 = coords[1][0]
            
            _yy = np.random.uniform(_dyy / 4, _dyy)
            
            if up == 1:
                _yy = y - _yy
                _line = LineString([(x0, y), (x0, _yy)])
                down_vertices += [_line]
            else:
                _yy = y + _yy
                _line = LineString([(x0, y), (x0, _yy)])
                up_vertices += [_line]
            
            vertices[i] = LineString([(x0, y0), (x1, _yy)])
            i += 1
        
        if up == 1:
            ref_x = vertices[0].coords[0][0]
            step = (x - ref_x) / 8
            
            _xx = np.random.uniform(ref_x + step, x - step)
            _yy = np.random.uniform(y - _dyy / 2, y)
            
            _line = LineString([(x, y), (_xx, _yy)])
            down_vertices = [_line] + down_vertices
            
            ref_x = vertices[m - 1].coords[0][0]
            step = (ref_x - xx) / 8
            
            _xx = np.random.uniform(xx + step, ref_x - step)
            _yy = np.random.uniform(y - _dyy / 2, y)
            
            _line = LineString([(xx, y), (_xx, _yy)])
            
            #_n = len(down_vertices)
            #last = down_vertices[_n - 1]
            #down_vertices[_n - 1] = _line
            down_vertices += [_line]
            
            x0 = down_vertices[0].coords[1][0]
            y0 = vertices[0].coords[0][1]
            y1 = down_vertices[0].coords[1][1]
            
            _line1 = LineString([(x0, y0), (x0, y1)])
            
            _n = len(down_vertices) - 1
            x0 = down_vertices[_n].coords[1][0]
            y0 = vertices[0].coords[0][1]
            y1 = down_vertices[_n].coords[1][1]
            
            _line2 = LineString([(x0, y0), (x0, y1)])
        else:
            ref_x = vertices[0].coords[0][0]
            step = (x - ref_x) / 8
            
            _xx = np.random.uniform(ref_x + step, x - step)
            _yy = np.random.uniform(y, y + _dyy / 2)
            
            _line = LineString([(x, y), (_xx, _yy)])
            up_vertices = [_line] + up_vertices
            
            ref_x = vertices[m - 1].coords[0][0]
            step = (ref_x - xx) / 8
            
            _xx = np.random.uniform(xx + step, ref_x - step)
            _yy = np.random.uniform(y, y + _dyy / 2)
            
            _line = LineString([(xx, y), (_xx, _yy)])
            
            #_n = len(up_vertices)
            #last = up_vertices[_n - 1]
            #up_vertices[_n - 1] = _line
            up_vertices += [_line]
        
            x0 = up_vertices[0].coords[1][0]
            y0 = vertices[0].coords[0][1]
            y1 = up_vertices[0].coords[1][1]
            
            _line1 = LineString([(x0, y0), (x0, y1)])
            
            _n = len(up_vertices) - 1
            x0 = up_vertices[_n].coords[1][0]
            y0 = vertices[0].coords[0][1]
            y1 = up_vertices[_n].coords[1][1]
            
            _line2 = LineString([(x0, y0), (x0, y1)])
        
        _v = []
        i = 0
        _n = len(vertices)
        
        while i < _n:
            _v += [vertices[i]]
            
            if i == 0:
                _v += [_line1]
            elif i == _n - 2:
                _v += [_line2]
            
            i += 1
        
        vertices = _v
        
        if up == 1:
            seg_vertices[0] = vertices
        else:
            seg_vertices[1] = vertices
        
        """
        print('')
        print(_seg)
        #print('')
        
        #print(up_vertices)
        #print(down_vertices)
        
        for l in up_vertices:
            print(l)
            print(l.wkt + ';')
        
        for l in down_vertices:
            print(l.wkt + ';')
        """
        
        #print(min_seg_len, y0, y1, y, m)
        
        #print('')
        #self.print_structure()

        """
        _min1 = min_seg_len = sys.float_info.max
        _min2 = min_seg_len = sys.float_info.max
        
        for v in up_vertices:
            if v.length < _min1:
                _min1 = v.length
        
        for v in down_vertices:
            if v.length < _min2:
                _min2 = v.length
        
        y = (_min1 - _min2) / 2
        
        #_seg = LineString([(x, y), (xx, y)])
        #segs += [_seg]
        
        __seg = LineString([(_seg.coords[0][0], y), (_seg.coords[1][0], y)])
        
        print('')
        print(__seg.wkt + ';')
        
        _up_vertices = []
        _down_vertices = []
        
        for l in up_vertices:
            _up_vertices += [LineString([(l.coords[0][0], y), (l.coords[0][1], l.coords[1][1])])]
        
        for l in down_vertices:
            _down_vertices += [LineString([(l.coords[0][0], y), (l.coords[0][1], l.coords[1][1])])]
        
        for l in up_vertices:
            print(l)
            print(l.wkt + ';')
        
        for l in down_vertices:
            print(l.wkt + ';')
        
        print('')
        print(_seg.wkt + ';')
        
        for l in up_vertices:
            print(l)
            print(l.wkt + ';')
        
        for l in down_vertices:
            print(l.wkt + ';')
        
        sys.exit()
        """
        
        cut_spp_line = SupportLine(1, self.seg_min_len, self.seg_max_len, 0, 0, segs, [[up_vertices, down_vertices]])
        return cut_spp_line
    
    def split_to_parts(self, seg_id, idx_1_up, idx_1_down, idx_2_up, idx_2_down):
        parts = []
        
        segs1 = []
        vertices1 = []
        
        segs2 = []
        vertices2 = []
        
        n = len(self.segs)
        i = 0
        
        while i < n:
            v = self.vertices[i]
            
            up = v[0]
            down = v[1]
            
            up_v_1 = []
            down_v_1 = []
            
            up_v_2 = []
            down_v_2 = []
            
            if i < seg_id:
                segs1 += [self.segs[i]]
                up_v_1 = up
                down_v_1 = down
                #vertices1 += [[up, down]]
            elif i > seg_id:
                segs2 += [self.segs[i]]
                up_v_2 = up
                down_v_2 = down
                #vertices2 += [[up, down]]
            else:
                m = len(up)
                j = 0
                
                while j < m:
                    if j < idx_1_up:
                        up_v_1 += [up[j]]
                    elif j == idx_1_up:
                        x1 = up[idx_1_up].coords[0][0]
                        x2 = up[idx_1_up - 1].coords[0][0]
                        x3 = down[idx_1_down].coords[0][0]
                        
                        if x1 < x3:
                            up_v_1 += [up[j]]
                            segs1 += [LineString([(self.segs[i].coords[0][0], self.segs[i].coords[0][1]), (up[j].coords[0][0], self.segs[i].coords[0][1])])]
                        else:
                            if x2 > x3:
                                up_v_1 += [LineString([((x1 + x2) / 2, up[idx_1_up].coords[0][1]), (up[idx_1_up].coords[1][0], up[idx_1_up].coords[1][1])])]
                                segs1 += [LineString([(self.segs[i].coords[0][0], self.segs[i].coords[0][1]), ((x1 + x2) / 2, self.segs[i].coords[0][1])])]
                            else:
                                up_v_1 += [LineString([(down[idx_1_down].coords[0][0], up[idx_1_up].coords[0][1]), (up[idx_1_up].coords[1][0], up[idx_1_up].coords[1][1])])]
                                segs1 += [LineString([(self.segs[i].coords[0][0], self.segs[i].coords[0][1]), (down[idx_1_down].coords[0][0], self.segs[i].coords[0][1])])]
                    elif j > idx_2_up:
                        up_v_2 += [up[j]]
                    elif j == idx_2_up:
                        x1 = up[idx_2_up].coords[0][0]
                        x2 = up[idx_2_up + 1].coords[0][0]
                        x3 = down[idx_2_down].coords[0][0]
                        
                        if x1 > x3:
                            #up_v_2 += [up[j]]
                            #segs2 += [LineString([(up[j].coords[0][0], self.segs[i].coords[0][1]), (self.segs[i].coords[1][0], self.segs[i].coords[1][1])])]
                            print('unexpected x1 > x3')
                            sys.exit()
                        else:
                            if x2 < x3:
                                up_v_2 += [LineString([((x1 + x2) / 2, up[idx_2_up].coords[0][1]), (up[idx_2_up].coords[1][0], up[idx_2_up].coords[1][1])])]
                                segs2 += [LineString([((x1 + x2) / 2, self.segs[i].coords[0][1]), (self.segs[i].coords[1][0], self.segs[i].coords[1][1])])]
                            else:
                                #up_v_2 += [up[j]]
                                up_v_2 += [LineString([(down[idx_2_down].coords[0][0], up[idx_2_up].coords[0][1]), (up[idx_2_up].coords[1][0], up[idx_2_up].coords[1][1])])]
                                segs2 += [LineString([(down[idx_2_down].coords[0][0], self.segs[i].coords[0][1]), (self.segs[i].coords[1][0], self.segs[i].coords[1][1])])]
                        
                    j += 1
            
                m = len(down)
                j = 0
                
                while j < m:
                    if j < idx_1_down:
                        down_v_1 += [down[j]]
                    elif j == idx_1_down:
                        x1 = up[idx_1_up].coords[0][0]
                        x2 = up[idx_1_up - 1].coords[0][0]
                        x3 = down[idx_1_down].coords[0][0]
                        
                        #print(x1, x2)
                        #print((x1 + x2) / 2)
                        
                        if x1 < x3:
                            down_v_1 += [LineString([(up[idx_1_up].coords[0][0], down[idx_1_down].coords[0][1]), (down[idx_1_down].coords[1][0], down[idx_1_down].coords[1][1])])]
                        else:
                            if x2 > x3:
                                down_v_1 += [LineString([((x1 + x2) / 2, down[idx_1_down].coords[0][1]), (down[idx_1_down].coords[1][0], down[idx_1_down].coords[1][1])])]
                            else:
                                down_v_1 += [down[j]]
                    elif j > idx_2_down:
                        down_v_2 += [down[j]]
                    elif j == idx_2_down:
                        x1 = up[idx_2_up].coords[0][0]
                        x2 = up[idx_2_up + 1].coords[0][0]
                        x3 = down[idx_2_down].coords[0][0]
                        
                        if x1 > x3:
                            #down_v_2 += [LineString([(up[idx_2_up].coords[0][0], down[idx_2_down].coords[0][1]), (down[idx_2_down].coords[1][0], down[idx_2_down].coords[1][1])])]
                            print('unexpected x1 > x3')
                            sys.exit()
                        else:
                            if x2 < x3:
                                down_v_2 += [LineString([((x1 + x2) / 2, down[idx_2_down].coords[0][1]), (down[idx_2_down].coords[1][0], down[idx_2_down].coords[1][1])])]
                            else:
                                down_v_2 += [down[j]]

                    j += 1
            
            if len(up_v_1) > 0 or len(down_v_1) > 0:
                vertices1 += [[up_v_1, down_v_1]]
            
            if len(up_v_2) > 0 or len(down_v_2) > 0:
                vertices2 += [[up_v_2, down_v_2]]
            
            i += 1

        parts += [SupportLine(len(vertices1), self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, segs1, vertices1)]
        parts += [SupportLine(len(vertices2), self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, segs2, vertices2)]

        #print(parts[0].print_structure())
        #sys.exit()
        return parts
    
    def split_to_point_preproccess(self, seg_id, min_percent, max_percent):
        ### >>>
        
        seg_vertices = self.vertices[seg_id]
        
        up_vertices = seg_vertices[0]
        down_vertices = seg_vertices[1]
        
        idx_1_up = -1
        idx_1_down = -1
        
        idx_2_up = -1
        idx_2_down = -1
        
        split_point_x = 0
        split_point_y = 0
        
        ### >>>
        
        n = len(up_vertices)
        
        idx_2_up = int(n / 2)

        x2 = up_vertices[idx_2_up].coords[0][0]
        x1 = -1
        
        idx_1_up = idx_2_up - 1
        
        prev_x = up_vertices[idx_1_up].coords[0][0]
        
        i = 0
        m = len(down_vertices)
        
        while i < m:
            v = down_vertices[i]
            
            if v.coords[0][0] < x2:
                x1 = v.coords[0][0]
                idx_1_down = i
            else:
                idx_2_down = i
                break
            
            i += 1

        if prev_x > x1:
            x1 = prev_x
        
        split_point_x = (x1 + x2) / 2
        split_point_y = up_vertices[idx_1_up].coords[0][1]
        
        ### >>>
        
        vertices = []
        i = 0
        
        step = 0.25
        step_i = 0.1
        _max = 0.8
        
        for v in self.vertices:
            up_vertices = v[0]
            down_vertices = v[1]
            
            _seg_up_vertices = []
            _seg_down_vertices = []
            
            if i == seg_id:
            
                j = 0
                for _l in up_vertices:
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                    
                    if j == idx_1_up or j == idx_2_up:
                        _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (split_point_x, split_point_y)])
                        _seg_up_vertices += [_line]
                    elif j < idx_1_up:
                        ds_min = step + step_i * j
                        ds_max = step + step + step_i * j
                    
                        if ds_min > 0.65:
                            ds_min = 0.65
                        
                        if ds_max > 0.8:
                            ds_max = 0.8

                        #ds_min = min_percent - step_i * j
                        #ds_max = max_percent + step_i * j
                        
                        #print(ds_min, ds_max)
                        
                        """
                        if ds_max > 0.8:
                            ds_max = 0.8
                        
                        while ds_min < ds_max:
                            ds_min += step_i

                        print(ds_min, ds_max)
                        """
                        
                        #dxx = random.uniform(dx / ds_min, dx / ds_max)
                        #dyy = random.uniform(dy / ds_min, dy / ds_max)
                         
                        dxx = random.uniform(dx * ds_min, dx * ds_max)
                        dyy = random.uniform(dy * ds_min, dy * ds_max)
                         
                        _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                        _seg_up_vertices += [_line]
                    else:
                        ds_min = 0.65 - step_i * (j - idx_2_up)
                        ds_max = 0.8 - step_i * (j - idx_2_up)
                        
                        """
                        if ds_max > 0.8:
                            ds_max = 0.8
                        
                        while ds_min < ds_max:
                            ds_min += step_i
                        """

                        if ds_min < 0.25:
                            ds_min = 0.25
                        
                        if ds_max < 0.5:
                            ds_max = 0.5

                        #print(ds_min, ds_max)

                        #dxx = random.uniform(dx / ds_min, dx / ds_max)
                        #dyy = random.uniform(dy / ds_min, dy / ds_max)
                        
                        dxx = random.uniform(dx * ds_min, dx * ds_max)
                        dyy = random.uniform(dy * ds_min, dy * ds_max)
                        
                        _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                        _seg_up_vertices += [_line]
                    
                    j += 1
                
                j = 0
                for _l in down_vertices:
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                    
                    if j == idx_1_down or j == idx_2_down:
                        _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (split_point_x, split_point_y)])
                        _seg_down_vertices += [_line]
                    elif j < idx_1_down:
                        """
                        ds_min = min_percent + step_i * j
                        ds_max = max_percent + step_i * j
                        
                        if ds_max > 0.8:
                            ds_max = 0.8
                        
                        while ds_min > ds_max:
                            ds_min -= step_i

                        dxx = random.uniform(dx / ds_min, dx / ds_max)
                        dyy = random.uniform(dy / ds_min, dy / ds_max)
                         """
                         
                        ds_min = step + step_i * j
                        ds_max = step + step + step_i * j
                    
                        if ds_min > 0.65:
                            ds_min = 0.65
                        
                        if ds_max > 0.8:
                            ds_max = 0.8
                         
                        dxx = random.uniform(dx * ds_min, dx * ds_max)
                        dyy = random.uniform(dy * ds_min, dy * ds_max)
                         
                        _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                        _seg_down_vertices += [_line]
                    else:
                        """
                        ds_min = min_percent + step_d - step_i * (j - idx_2_up)
                        ds_max = max_percent + step_d - step_i * (j - idx_2_up)
                        
                        if ds_max > 0.8:
                            ds_max = 0.8
                        
                        while ds_min > ds_max:
                            ds_min -= step_i

                        dxx = random.uniform(dx / ds_min, dx / ds_max)
                        dyy = random.uniform(dy / ds_min, dy / ds_max)
                         """
                         
                        ds_min = 0.65 - step_i * (j - idx_2_up)
                        ds_max = 0.8 - step_i * (j - idx_2_up)
                         
                        if ds_min < 0.25:
                            ds_min = 0.25
                        
                        if ds_max < 0.5:
                            ds_max = 0.5
                         
                        dxx = random.uniform(dx * ds_min, dx * ds_max)
                        dyy = random.uniform(dy * ds_min, dy * ds_max)
                         
                        _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                        _seg_down_vertices += [_line]
                    
                    j += 1
                
            else:
                # up
                for _l in up_vertices:
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                    
                    dxx = random.uniform(dx / min_percent, dx / max_percent)
                    dyy = random.uniform(dy / min_percent, dy / max_percent)
                     
                    _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                    _seg_up_vertices += [_line]
            
                # down
                for _l in down_vertices:
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                            
                    dxx = random.uniform(dx / min_percent, dx / max_percent)
                    dyy = random.uniform(dy / min_percent, dy / max_percent)
                     
                    _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                    _seg_down_vertices += [_line]
            
            vertices += [[_seg_up_vertices, _seg_down_vertices]]
            i += 1
        
        supp_line = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
    
        self.generate_coords_from_vertices()
        source_coords = self.get_coords()
    
        supp_line.generate_coords_from_vertices()
        target_coords = supp_line.get_coords()

        return supp_line, source_coords, target_coords, idx_1_up, idx_1_down, idx_2_up, idx_2_down
        
        ### >>>
        
        ### >>> (x1 / x2)
        """
        print(x1, x2, split_point_x, split_point_y)
        print(idx_1_up, idx_2_up)
        print(n, m)
        """
 
    def split_to_point_preproccessing(self, seg_id, min_percent, max_percent):
        ### >>>
        
        seg_vertices = self.vertices[seg_id]
        
        up_vertices = seg_vertices[0]
        down_vertices = seg_vertices[1]
        
        idx_1_up = -1
        idx_1_down = -1
        
        idx_2_up = -1
        idx_2_down = -1
        
        split_point_x = 0
        split_point_y = 0
        
        ### >>>
        
        n = len(up_vertices)
        
        idx_2_up = int(n / 2)

        x2 = up_vertices[idx_2_up].coords[0][0]
        x1 = -1
        
        #idx_1_up = idx_2_up - 1
        idx_1_up = idx_2_up
        
        prev_x = up_vertices[idx_1_up].coords[0][0]
        
        i = 0
        m = len(down_vertices)
        
        while i < m:
            v = down_vertices[i]
            
            if v.coords[0][0] < x2:
                x1 = v.coords[0][0]
                idx_1_down = i
            else:
                idx_2_down = i
                break
            
            i += 1

        if prev_x > x1:
            x1 = prev_x
        
        split_point_x = (x1 + x2) / 2
        split_point_y = up_vertices[idx_1_up].coords[0][1]
        
        ### >>>
        
        vertices = []
        i = 0
        
        step = 0.2
        step_i = 0.15
        _max = 0.8
        
        for v in self.vertices:
            up_vertices = v[0]
            down_vertices = v[1]
            
            _seg_up_vertices = []
            _seg_down_vertices = []
            
            if i == seg_id:
            
                j = 0
                for _l in up_vertices:
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                    
                    if j == idx_1_up or j == idx_2_up:
                        _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (split_point_x, split_point_y)])
                        _seg_up_vertices += [_line]
                    elif j < idx_1_up:
                        ds_min = step + step_i * j
                        ds_max = step + step + step_i * j
                    
                        """
                        if ds_min > 0.65:
                            ds_min = 0.65
                        
                        if ds_max > 0.8:
                            ds_max = 0.8
                        """
                        
                        ds_min = 0.6
                        ds_max = 0.75
                        
                        dxx = random.uniform(dx * ds_min, dx * ds_max)
                        dyy = random.uniform(dy * ds_min, dy * ds_max)
                         
                        _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                        _seg_up_vertices += [_line]
                    else:
                        ds_min = 0.65 - step_i * (j - idx_2_up)
                        ds_max = 0.8 - step_i * (j - idx_2_up)

                        if ds_min < 0.25:
                            ds_min = 0.25
                        
                        if ds_max < 0.5:
                            ds_max = 0.5

                        dxx = random.uniform(dx * ds_min, dx * ds_max)
                        dyy = random.uniform(dy * ds_min, dy * ds_max)
                        
                        _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                        _seg_up_vertices += [_line]
                    
                    j += 1
                
                j = 0
                for _l in down_vertices:
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                    
                    if j == idx_1_down or j == idx_2_down:
                        _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (split_point_x, split_point_y)])
                        _seg_down_vertices += [_line]
                    elif j < idx_1_down:                         
                        ds_min = step + step_i * j
                        ds_max = step + step + step_i * j
                    
                        if ds_min > 0.65:
                            ds_min = 0.65
                        
                        if ds_max > 0.8:
                            ds_max = 0.8
                         
                        dxx = random.uniform(dx * ds_min, dx * ds_max)
                        dyy = random.uniform(dy * ds_min, dy * ds_max)
                         
                        _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                        _seg_down_vertices += [_line]
                    else:                         
                        ds_min = 0.65 - step_i * (j - idx_2_up)
                        ds_max = 0.8 - step_i * (j - idx_2_up)
                         
                        if ds_min < 0.25:
                            ds_min = 0.25
                        
                        if ds_max < 0.5:
                            ds_max = 0.5
                         
                        dxx = random.uniform(dx * ds_min, dx * ds_max)
                        dyy = random.uniform(dy * ds_min, dy * ds_max)
                         
                        _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                        _seg_down_vertices += [_line]
                    
                    j += 1
                
            else:
                # up
                for _l in up_vertices:
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                    
                    dxx = random.uniform(dx / min_percent, dx / max_percent)
                    dyy = random.uniform(dy / min_percent, dy / max_percent)
                     
                    _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                    _seg_up_vertices += [_line]
            
                # down
                for _l in down_vertices:
                    dx = _l.coords[1][0] - _l.coords[0][0]
                    dy = _l.coords[1][1] - _l.coords[0][1]
                            
                    dxx = random.uniform(dx / min_percent, dx / max_percent)
                    dyy = random.uniform(dy / min_percent, dy / max_percent)
                     
                    _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[1][0] - dxx, _l.coords[1][1] - dyy)])
                    _seg_down_vertices += [_line]
            
            vertices += [[_seg_up_vertices, _seg_down_vertices]]
            i += 1
        
        supp_line = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
    
        self.generate_coords_from_vertices()
        source_coords = self.get_coords()
    
        supp_line.generate_coords_from_vertices()
        target_coords = supp_line.get_coords()

        return supp_line, source_coords, target_coords, idx_1_up, idx_1_down, idx_2_up, idx_2_down
 
    """
        Input 
        
        Output
                        sl                  .support line of the new geometry
                        s_coords      .source coordinates
                        t_coords       .target coordinates
    """
    def remove_vertices(self, min_percent, max_percent, grow):
        """
            Todo:
                This function should respect the event that is occuring.
                I.e., if a shrink is occuring, the removal of vertices should respect that!
                This is not the case in the current implementation.
                
                Refactor: lots of repeated code.
        """
        
        vertices = []
        
        s_coords = []
        t_coords = []
        
        s_d_coords = []
        t_d_coords = []
        
        for v in self.vertices:
            _seg_up_vertices = []
            _seg_down_vertices = []
 
            nex_coords = None
            prev = None
            remove = False
 
            up = v[0]
            down = v[1]
            
            n = len(up)
            i = 0
            
            while i < n:
                coords = up[i].coords
                
                x0 = coords[0][0]
                y0 = coords[0][1]  
                    
                x1 = coords[1][0]
                y1 = coords[1][1]  
                
                if i == 0:
                    dx = x1 - x0
                    dy = y1 - y0
                    
                    f = random.uniform(min_percent, max_percent)
                    
                    if grow != 0 and grow != 1:
                        grow = 1
                        
                        p = random.uniform(0, 1)
                        if p <= 0.5:
                            grow = 0
                    
                    _x = 0
                    _y = 0
                    
                    if grow == 1:
                        _x = x1 + dx * f
                        _y = y1 + dy * f
                    else:
                        _x = x1 - dx * f
                        _y = y1 - dy * f

                    s_coords += [(x1, y1)]
                    t_coords += [(_x, _y)]
                    
                    _line = LineString([(x0, y0), (_x, _y)])
                    _seg_up_vertices += [_line]
                    prev = _line
                    remove = True
                elif i == n - 1:
                    _x = 0
                    _y = 0
                
                    if nex_coords != None:
                        _x = nex_coords[0]
                        _y = nex_coords[1]
                        
                        nex_coords = None
                    else:
                        dx = x1 - x0
                        dy = y1 - y0
                        
                        f = random.uniform(min_percent, max_percent)

                        if grow != 0 and grow != 1:
                            grow = 1
                            
                            p = random.uniform(0, 1)
                            if p <= 0.5:
                                grow = 0

                        if grow == 1:
                            _x = x1 + dx * f
                            _y = y1 + dy * f
                        else:
                            _x = x1 - dx * f
                            _y = y1 - dy * f

                    s_coords += [(x1, y1)]
                    t_coords += [(_x, _y)]
                    
                    _line = LineString([(x0, y0), (_x, _y)])
                    _seg_up_vertices += [_line]
                    remove = False 
                else:
                    if remove:
                        _x = 0
                        _y = 0
                        
                        p = random.uniform(0, 1)

                         # to prev vertex
                        if p <= 0.33:
                            _x = prev.coords[1][0]
                            _y = prev.coords[1][1]
                            
                            s_coords += [(x1, y1)]
                            t_coords += [(_x, _y)]
                        # collinear
                        if  p > 0.33 and p <= 0.66:
                            next_coords = up[i + 1].coords
                            
                            x0 = next_coords[0][0]
                            y0 = next_coords[0][1]
                        
                            x1 = next_coords[1][0]
                            y1 = next_coords[1][1]
                        
                            dx = x1 - x0
                            dy = y1 - y0
                            
                            f = random.uniform(min_percent, max_percent)

                            if grow != 0 and grow != 1:
                                grow = 1
                                
                                p = random.uniform(0, 1)
                                if p <= 0.5:
                                    grow = 0

                            if grow == 1:
                                _x = x1 + dx * f
                                _y = y1 + dy * f
                            else:
                                _x = x1 - dx * f
                                _y = y1 - dy * f
                            
                            nex_coords = (_x, _y)
                            
                            x0 = prev.coords[1][0]
                            y0 = prev.coords[1][1]

                            x1 = _x
                            y1 = _y
                            
                            dx = x1 - x0
                            dy = y1 - y0

                            x1 = coords[1][0]
                            y1 = coords[1][1]  
                            
                            _x = x1
                            f = float(x1 - x0) / dx
                            _y = y0 + dy * f
                            
                            s_coords += [(x1, y1)]
                            t_coords += [(_x, _y)]
                        # to next vertex
                        elif p > 0.66:
                            next_coords = up[i + 1].coords
                            
                            x0 = next_coords[0][0]
                            y0 = next_coords[0][1]
                        
                            x1 = next_coords[1][0]
                            y1 = next_coords[1][1]
                        
                            dx = x1 - x0
                            dy = y1 - y0
                            
                            f = random.uniform(min_percent, max_percent)

                            if grow != 0 and grow != 1:
                                grow = 1
                                
                                p = random.uniform(0, 1)
                                if p <= 0.5:
                                    grow = 0

                            if grow == 1:
                                _x = x1 + dx * f
                                _y = y1 + dy * f
                            else:
                                _x = x1 - dx * f
                                _y = y1 - dy * f
                            
                            nex_coords = (_x, _y)
                            
                            s_coords += [(coords[1][0], coords[1][1])]
                            t_coords += [(_x, _y)]

                        remove = False
                    else:
                        _x = 0
                        _y = 0
                    
                        if nex_coords != None:
                            _x = nex_coords[0]
                            _y = nex_coords[1]
                            nex_coords = None
                        else:
                            dx = x1 - x0
                            dy = y1 - y0
                            
                            f = random.uniform(min_percent, max_percent)

                            if grow != 0 and grow != 1:
                                grow = 1
                                
                                p = random.uniform(0, 1)
                                if p <= 0.5:
                                    grow = 0

                            if grow == 1:
                                _x = x1 + dx * f
                                _y = y1 + dy * f
                            else:
                                _x = x1 - dx * f
                                _y = y1 - dy * f

                        s_coords += [(x1, y1)]
                        t_coords += [(_x, _y)]
                        
                        _line = LineString([(x0, y0), (_x, _y)])
                        _seg_up_vertices += [_line]
                        prev = _line
                        remove = True
                
                i += 1
 
            nex_coords = None
            prev = None
            remove = False
 
            n = len(down)
            i = 0
 
            while i < n:
                coords = down[i].coords
                
                x0 = coords[0][0]
                y0 = coords[0][1]  
                    
                x1 = coords[1][0]
                y1 = coords[1][1]  
                
                if i == 0:
                    dx = x1 - x0
                    dy = y1 - y0
                    
                    f = random.uniform(min_percent, max_percent)
                    
                    if grow != 0 and grow != 1:
                        grow = 1
                        
                        p = random.uniform(0, 1)
                        if p <= 0.5:
                            grow = 0
                    
                    _x = 0
                    _y = 0
                    
                    if grow == 1:
                        _x = x1 + dx * f
                        _y = y1 + dy * f
                    else:
                        _x = x1 - dx * f
                        _y = y1 - dy * f

                    s_d_coords += [(x1, y1)]
                    t_d_coords += [(_x, _y)]
                    
                    _line = LineString([(x0, y0), (_x, _y)])
                    _seg_down_vertices += [_line]
                    prev = _line
                    remove = True
                elif i == n - 1:
                    _x = 0
                    _y = 0
                
                    if nex_coords != None:
                        _x = nex_coords[0]
                        _y = nex_coords[1]
                        
                        nex_coords = None
                    else:
                        dx = x1 - x0
                        dy = y1 - y0
                        
                        f = random.uniform(min_percent, max_percent)

                        if grow != 0 and grow != 1:
                            grow = 1
                            
                            p = random.uniform(0, 1)
                            if p <= 0.5:
                                grow = 0

                        if grow == 1:
                            _x = x1 + dx * f
                            _y = y1 + dy * f
                        else:
                            _x = x1 - dx * f
                            _y = y1 - dy * f

                    s_d_coords += [(x1, y1)]
                    t_d_coords += [(_x, _y)]
                    
                    _line = LineString([(x0, y0), (_x, _y)])
                    _seg_down_vertices += [_line]
                    remove = False 
                else:
                    if remove:
                        _x = 0
                        _y = 0
                        
                        p = random.uniform(0, 1)

                         # to prev vertex
                        if p <= 0.33:
                            _x = prev.coords[1][0]
                            _y = prev.coords[1][1]
                            
                            s_d_coords += [(x1, y1)]
                            t_d_coords += [(_x, _y)]
                        # collinear
                        if  p > 0.33 and p <= 0.66:
                            next_coords = down[i + 1].coords
                            
                            x0 = next_coords[0][0]
                            y0 = next_coords[0][1]
                        
                            x1 = next_coords[1][0]
                            y1 = next_coords[1][1]
                        
                            dx = x1 - x0
                            dy = y1 - y0
                            
                            f = random.uniform(min_percent, max_percent)

                            if grow != 0 and grow != 1:
                                grow = 1
                                
                                p = random.uniform(0, 1)
                                if p <= 0.5:
                                    grow = 0

                            if grow == 1:
                                _x = x1 + dx * f
                                _y = y1 + dy * f
                            else:
                                _x = x1 - dx * f
                                _y = y1 - dy * f
                            
                            nex_coords = (_x, _y)
                            
                            x0 = prev.coords[1][0]
                            y0 = prev.coords[1][1]

                            x1 = _x
                            y1 = _y
                            
                            dx = x1 - x0
                            dy = y1 - y0

                            x1 = coords[1][0]
                            y1 = coords[1][1]  
                            
                            _x = x1
                            f = float(x1 - x0) / dx
                            _y = y0 + dy * f
                            
                            s_d_coords += [(x1, y1)]
                            t_d_coords += [(_x, _y)]
                        # to next vertex
                        elif p > 0.66:
                            next_coords = down[i + 1].coords
                            
                            x0 = next_coords[0][0]
                            y0 = next_coords[0][1]
                        
                            x1 = next_coords[1][0]
                            y1 = next_coords[1][1]
                        
                            dx = x1 - x0
                            dy = y1 - y0
                            
                            f = random.uniform(min_percent, max_percent)

                            if grow != 0 and grow != 1:
                                grow = 1
                                
                                p = random.uniform(0, 1)
                                if p <= 0.5:
                                    grow = 0

                            if grow == 1:
                                _x = x1 + dx * f
                                _y = y1 + dy * f
                            else:
                                _x = x1 - dx * f
                                _y = y1 - dy * f
                            
                            nex_coords = (_x, _y)
                            
                            s_d_coords += [(coords[1][0], coords[1][1])]
                            t_d_coords += [(_x, _y)]

                        remove = False
                    else:
                        _x = 0
                        _y = 0
                    
                        if nex_coords != None:
                            _x = nex_coords[0]
                            _y = nex_coords[1]
                            nex_coords = None
                        else:
                            dx = x1 - x0
                            dy = y1 - y0
                            
                            f = random.uniform(min_percent, max_percent)

                            if grow != 0 and grow != 1:
                                grow = 1
                                
                                p = random.uniform(0, 1)
                                if p <= 0.5:
                                    grow = 0

                            if grow == 1:
                                _x = x1 + dx * f
                                _y = y1 + dy * f
                            else:
                                _x = x1 - dx * f
                                _y = y1 - dy * f

                        s_d_coords += [(x1, y1)]
                        t_d_coords += [(_x, _y)]
                        
                        _line = LineString([(x0, y0), (_x, _y)])
                        _seg_down_vertices += [_line]
                        prev = _line
                        remove = True
                
                i += 1
             
            vertices += [[_seg_up_vertices, _seg_down_vertices]]
 
        #
 
        _len = len(s_d_coords) - 1
        
        while _len >= 0:
            s_coords += [s_d_coords[_len]]
            _len -= 1
           
        s_coords += [(s_coords[0][0], s_coords[0][1])]

        #
    
        _len = len(t_d_coords) - 1
        
        while _len >= 0:
            t_coords += [t_d_coords[_len]]
            _len -= 1
           
        t_coords += [(t_coords[0][0], t_coords[0][1])]
 
        #
 
        sl = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
        return sl, s_coords, t_coords
 
    def insert_vertices(self, nmin, nmax, npos):
        i = len(self.segs)
        k = 0

        while k < i:
            _up_vertices_pos = []
            _down_vertices_pos = []
                
            _seg = self.segs[k]
            _seg_len = _seg.length
            
            _x = _seg.coords[0][0]
            _y = _seg.coords[0][1]
            
            _dx = _seg_len / (npos + 1)
            
            _k = 1
            _pop = []
            while _k <= npos:
                _pop += [_x + (_dx * _k)]
                _k += 1
            
            _nv= np.random.randint(nmin, nmax + 1)
            _vpos = random.sample(_pop, _nv)
            _vpos.sort()
                
            for _pos in _vpos:
                _p = random.uniform(0, 1)
                    
                if _p >= 0.5:
                    _down_vertices_pos += [_pos]
                else:
                    _up_vertices_pos += [_pos]
            
            # >>>>>>>>>>>
            
            """
            print('')
            print(_nv, _seg.coords[0][0], _seg.coords[1][0])
            print(_up_vertices_pos)
            print(_down_vertices_pos)
            print('')

            sys.exit()
            """
            
            if _nv != 0:
                _new_up = []
                _new_down = []
                
                _seg_vertices = self.vertices[k]
                _up = _seg_vertices[0]
                _down = _seg_vertices[1]
                
                _n = len(_up_vertices_pos)
                if _n > 0:
                    _m = len(_up)
                    
                    if _m == 0:                    
                        _prev_seg_vertices = self.vertices[k - 1]
                        _prev_up = _prev_seg_vertices[0]
                        
                        x0 = _prev_up[len(_prev_up) - 1].coords[1][0]
                        y0 = _prev_up[len(_prev_up) - 1].coords[1][1]
                      
                        _next_seg_vertices = self.vertices[k + 1]
                        _next_up = _next_seg_vertices[0]
                        
                        x1 = _next_up[0].coords[1][0]
                        y1 = _next_up[0].coords[1][1]
                        
                        dxx = x1 - x0
                        dyy = y1 - y0
                        
                        _j = 0
                        while _j < _n:
                            _x2 = _up_vertices_pos[_j]
                            
                            _f = (_x2 - x0) / dxx
                            _y2 = y0 + dyy * _f
                            
                            _line = LineString([(_x2, _y), (_x2, _y2)])
                            _new_up += [_line]
                            
                            _j += 1
                    else:
                        _j = 0
                        temp = []
                        
                        while _j < _n:
                            _idx_1 = -1
                            _idx_2 = -1
                            
                            ignore = False
                            
                            _x1 = _up_vertices_pos[_j]
                    
                            _i = 0
                            while _i < _m:
                                x0 = _up[_i].coords[0][0]
                                
                                if x0 < _x1:
                                    _idx_1 = _i
                                elif x0 > _x1:
                                    _idx_2 = _i
                                    break
                                else:
                                    ignore = True
                                    break
                                    
                                _i += 1
                            
                            _j += 1
                    
                            if not ignore:
                                x0 = 0
                                y0 = 0
                            
                                x1 = 0
                                y1 = 0
                            
                                if _idx_1 != -1 and _idx_2 != -1:
                                    x0 = _up[_idx_1].coords[1][0]
                                    y0 = _up[_idx_1].coords[1][1]
                                    
                                    x1 = _up[_idx_2].coords[1][0]
                                    y1 = _up[_idx_2].coords[1][1]
                                elif _idx_1 != -1 and _idx_2 == -1:
                                    x0 = _up[_idx_1].coords[1][0]
                                    y0 = _up[_idx_1].coords[1][1]
                                    
                                    _next_seg_vertices = self.vertices[k + 1]
                                    _next_up = _next_seg_vertices[0]
                                    
                                    x1 = _next_up[0].coords[1][0]
                                    y1 = _next_up[0].coords[1][1]
                                elif _idx_1 == -1 and _idx_2 != -1:
                                    _prev_seg_vertices = self.vertices[k - 1]
                                    _prev_up = _prev_seg_vertices[0]
                                    
                                    x0 = _prev_up[len(_prev_up) - 1].coords[1][0]
                                    y0 = _prev_up[len(_prev_up) - 1].coords[1][1]
                                  
                                    x1 = _up[_idx_2].coords[1][0]
                                    y1 = _up[_idx_2].coords[1][1]
                                else:
                                    print('Unexpected!')
                                    sys.exit()
                                
                                dxx = x1 - x0
                                dyy = y1 - y0

                                _f = (_x1 - x0) / dxx
                                _y1 = y0 + dyy * _f
                                
                                _line = LineString([(_x1, _y), (_x1, _y1)])
                                temp += [_line]
                        
                        _c1 = 0
                        _c2 = 0
                        
                        if temp != []:
                            
                            _nn = len(temp)
                            
                            #print(k, _m, _nn)
                            
                            while _c1 < _m and _c2 < _nn:
                                x0 = _up[_c1].coords[0][0]
                                x1 = temp[_c2].coords[0][0]
                                
                                if x0 < x1:
                                    _new_up += [_up[_c1]]
                                    _c1 += 1
                                elif x0 > x1:
                                    _new_up += [temp[_c2]]
                                    _c2 += 1
                                else:
                                    print('Unexpected Error. (Two vertices with the same coords.)')
                                    sys.exit()
                            
                            #print(k, _c1, _c2, _m, _nn, len(_new_up))
                            
                            while _c1 < _m:
                                #print(_c1, _m)
                                _new_up += [_up[_c1]]
                                _c1 += 1
                            
                            while _c2 < _nn:
                                #print(_c2, _nn)
                                _new_up += [temp[_c2]]
                                _c2 += 1
                    
                            #print(len(_new_up))
                    
                    if _new_up != []:                    
                        self.vertices[k][0] = _new_up

                _n = len(_down_vertices_pos)
                if _n > 0:
                    _m = len(_down)
                    
                    if _m == 0:
                        _prev_seg_vertices = self.vertices[k - 1]
                        _prev = _prev_seg_vertices[1]
                        
                        x0 = _prev[len(_prev) - 1].coords[1][0]
                        y0 = _prev[len(_prev) - 1].coords[1][1]
                      
                        _next_seg_vertices = self.vertices[k + 1]
                        _next = _next_seg_vertices[1]
                        
                        x1 = _next[0].coords[1][0]
                        y1 = _next[0].coords[1][1]
                        
                        dxx = x1 - x0
                        dyy = y1 - y0
                        
                        _j = 0
                        while _j < _n:
                            _x2 = _down_vertices_pos[_j]
                            
                            _f = (_x2 - x0) / dxx
                            _y2 = y0 + dyy * _f
                            
                            _line = LineString([(_x2, _y), (_x2, _y2)])
                            _new_down += [_line]
                            
                            _j += 1
                    else:
                        _j = 0
                        temp = []
                        
                        while _j < _n:
                        
                            _idx_1 = -1
                            _idx_2 = -1
                            
                            ignore = False
                            
                            _x1 = _down_vertices_pos[_j]
                    
                            _i = 0
                            while _i < _m:
                                x0 = _down[_i].coords[0][0]
                                
                                if x0 < _x1:
                                    _idx_1 = _i
                                elif x0 > _x1:
                                    _idx_2 = _i
                                    break
                                else:
                                    ignore = True
                                    break
                                    
                                _i += 1
                            
                            _j += 1
                    
                            if not ignore:
                                x0 = 0
                                y0 = 0
                            
                                x1 = 0
                                y1 = 0
                            
                                if _idx_1 != -1 and _idx_2 != -1:
                                    x0 = _down[_idx_1].coords[1][0]
                                    y0 = _down[_idx_1].coords[1][1]
                                    
                                    x1 = _down[_idx_2].coords[1][0]
                                    y1 = _down[_idx_2].coords[1][1]
                                elif _idx_1 != -1 and _idx_2 == -1:
                                    x0 = _down[_idx_1].coords[1][0]
                                    y0 = _down[_idx_1].coords[1][1]
                                    
                                    _next_seg_vertices = self.vertices[k + 1]
                                    _next = _next_seg_vertices[1]
                                    
                                    x1 = _next[0].coords[1][0]
                                    y1 = _next[0].coords[1][1]
                                elif _idx_1 == -1 and _idx_2 != -1:
                                    _prev_seg_vertices = self.vertices[k - 1]
                                    _prev = _prev_seg_vertices[1]
                                    
                                    x0 = _prev[len(_prev) - 1].coords[1][0]
                                    y0 = _prev[len(_prev) - 1].coords[1][1]
                                  
                                    x1 = _down[_idx_2].coords[1][0]
                                    y1 = _down[_idx_2].coords[1][1]
                                else:
                                    print('Unexpected!')
                                    sys.exit()
                                
                                dxx = x1 - x0
                                dyy = y1 - y0

                                _f = (_x1 - x0) / dxx
                                _y1 = y0 + dyy * _f
                                
                                _line = LineString([(_x1, _y), (_x1, _y1)])
                                temp += [_line]
                            
                        if temp != []:
                            _c1 = 0
                            _c2 = 0
                            
                            _n = len(temp)
                            
                            while _c1 < _m and _c2 < _n:
                                x0 = _down[_c1].coords[0][0]
                                x1 = temp[_c2].coords[0][0]
                                
                                if x0 < x1:
                                    _new_down += [_down[_c1]]
                                    _c1 += 1
                                elif x0 > x1:
                                    _new_down += [temp[_c2]]
                                    _c2 += 1
                                else:
                                    print('Unexpected Error. (Two vertices with the same coords.)')
                                    sys.exit()
                            
                            while _c1 < _m:
                                _new_down += [_down[_c1]]
                                _c1 += 1
                            
                            while _c2 < _n:
                                _new_down += [temp[_c2]]
                                _c2 += 1
                    
                    if _new_down != []:
                        self.vertices[k][1] = _new_down
                
            k += 1

    def insert_vertices_on_segs(self, seg_id, nmin, nmax, npos, up):
            i = len(self.segs)
            k = seg_id

            _up_vertices_pos = []
            _down_vertices_pos = []
                
            _seg = self.segs[k]
            _seg_len = _seg.length
            
            _x = _seg.coords[0][0]
            _y = _seg.coords[0][1]
            
            _dx = _seg_len / (npos + 1)
            
            _k = 1
            _pop = []
            while _k <= npos:
                _pop += [_x + (_dx * _k)]
                _k += 1
            
            _nv= np.random.randint(nmin, nmax + 1)
            _vpos = random.sample(_pop, _nv)
            _vpos.sort()
                
            for _pos in _vpos:
            
                if up == 1:
                    _up_vertices_pos += [_pos]
                elif up == 0:
                    _down_vertices_pos += [_pos]
                else:
                    _p = random.uniform(0, 1)
                        
                    if _p >= 0.5:
                        _down_vertices_pos += [_pos]
                    else:
                        _up_vertices_pos += [_pos]
            
            # >>>>>>>>>>>
            
            if _nv != 0:
                _new_up = []
                _new_down = []
                
                _seg_vertices = self.vertices[k]
                _up = _seg_vertices[0]
                _down = _seg_vertices[1]
                
                _n = len(_up_vertices_pos)
                if _n > 0:
                    _m = len(_up)
                    
                    if _m == 0:                    
                        _prev_seg_vertices = self.vertices[k - 1]
                        _prev_up = _prev_seg_vertices[0]
                        
                        x0 = _prev_up[len(_prev_up) - 1].coords[1][0]
                        y0 = _prev_up[len(_prev_up) - 1].coords[1][1]
                      
                        _next_seg_vertices = self.vertices[k + 1]
                        _next_up = _next_seg_vertices[0]
                        
                        x1 = _next_up[0].coords[1][0]
                        y1 = _next_up[0].coords[1][1]
                        
                        dxx = x1 - x0
                        dyy = y1 - y0
                        
                        _j = 0
                        while _j < _n:
                            _x2 = _up_vertices_pos[_j]
                            
                            _f = (_x2 - x0) / dxx
                            _y2 = y0 + dyy * _f
                            
                            _line = LineString([(_x2, _y), (_x2, _y2)])
                            _new_up += [_line]
                            
                            _j += 1
                    else:
                        _j = 0
                        temp = []
                        
                        while _j < _n:
                            _idx_1 = -1
                            _idx_2 = -1
                            
                            ignore = False
                            
                            _x1 = _up_vertices_pos[_j]
                    
                            _i = 0
                            while _i < _m:
                                x0 = _up[_i].coords[0][0]
                                
                                if x0 < _x1:
                                    _idx_1 = _i
                                elif x0 > _x1:
                                    _idx_2 = _i
                                    break
                                else:
                                    ignore = True
                                    break
                                    
                                _i += 1
                            
                            _j += 1
                    
                            if not ignore:
                                x0 = 0
                                y0 = 0
                            
                                x1 = 0
                                y1 = 0
                            
                                if _idx_1 != -1 and _idx_2 != -1:
                                    x0 = _up[_idx_1].coords[1][0]
                                    y0 = _up[_idx_1].coords[1][1]
                                    
                                    x1 = _up[_idx_2].coords[1][0]
                                    y1 = _up[_idx_2].coords[1][1]
                                elif _idx_1 != -1 and _idx_2 == -1:
                                    x0 = _up[_idx_1].coords[1][0]
                                    y0 = _up[_idx_1].coords[1][1]
                                    
                                    _next_seg_vertices = self.vertices[k + 1]
                                    _next_up = _next_seg_vertices[0]
                                    
                                    x1 = _next_up[0].coords[1][0]
                                    y1 = _next_up[0].coords[1][1]
                                elif _idx_1 == -1 and _idx_2 != -1:
                                    _prev_seg_vertices = self.vertices[k - 1]
                                    _prev_up = _prev_seg_vertices[0]
                                    
                                    x0 = _prev_up[len(_prev_up) - 1].coords[1][0]
                                    y0 = _prev_up[len(_prev_up) - 1].coords[1][1]
                                  
                                    x1 = _up[_idx_2].coords[1][0]
                                    y1 = _up[_idx_2].coords[1][1]
                                else:
                                    print('Unexpected!')
                                    sys.exit()
                                
                                dxx = x1 - x0
                                dyy = y1 - y0

                                _f = (_x1 - x0) / dxx
                                _y1 = y0 + dyy * _f
                                
                                _line = LineString([(_x1, _y), (_x1, _y1)])
                                temp += [_line]
                        
                        _c1 = 0
                        _c2 = 0
                        
                        if temp != []:
                            
                            _nn = len(temp)
                            while _c1 < _m and _c2 < _nn:
                                x0 = _up[_c1].coords[0][0]
                                x1 = temp[_c2].coords[0][0]
                                
                                if x0 < x1:
                                    _new_up += [_up[_c1]]
                                    _c1 += 1
                                elif x0 > x1:
                                    _new_up += [temp[_c2]]
                                    _c2 += 1
                                else:
                                    print('Unexpected Error. (Two vertices with the same coords.)')
                                    sys.exit()
                            
                            while _c1 < _m:
                                _new_up += [_up[_c1]]
                                _c1 += 1
                            
                            while _c2 < _nn:
                                _new_up += [temp[_c2]]
                                _c2 += 1
                    
                    #print(len(_new_up))
                    
                    if _new_up != []:              
                        #print(len(self.vertices[k][0]), len(_new_up))
                        self.vertices[k][0] = _new_up

                _n = len(_down_vertices_pos)
                if _n > 0:
                    _m = len(_down)
                    
                    if _m == 0:
                        _prev_seg_vertices = self.vertices[k - 1]
                        _prev = _prev_seg_vertices[1]
                        
                        x0 = _prev[len(_prev) - 1].coords[1][0]
                        y0 = _prev[len(_prev) - 1].coords[1][1]
                      
                        _next_seg_vertices = self.vertices[k + 1]
                        _next = _next_seg_vertices[1]
                        
                        x1 = _next[0].coords[1][0]
                        y1 = _next[0].coords[1][1]
                        
                        dxx = x1 - x0
                        dyy = y1 - y0
                        
                        _j = 0
                        while _j < _n:
                            _x2 = _down_vertices_pos[_j]
                            
                            _f = (_x2 - x0) / dxx
                            _y2 = y0 + dyy * _f
                            
                            _line = LineString([(_x2, _y), (_x2, _y2)])
                            _new_down += [_line]
                            
                            _j += 1
                    else:
                        _j = 0
                        temp = []
                        
                        while _j < _n:
                        
                            _idx_1 = -1
                            _idx_2 = -1
                            
                            ignore = False
                            
                            _x1 = _down_vertices_pos[_j]
                    
                            _i = 0
                            while _i < _m:
                                x0 = _down[_i].coords[0][0]
                                
                                if x0 < _x1:
                                    _idx_1 = _i
                                elif x0 > _x1:
                                    _idx_2 = _i
                                    break
                                else:
                                    ignore = True
                                    break
                                    
                                _i += 1
                            
                            _j += 1
                    
                            if not ignore:
                                x0 = 0
                                y0 = 0
                            
                                x1 = 0
                                y1 = 0
                            
                                if _idx_1 != -1 and _idx_2 != -1:
                                    x0 = _down[_idx_1].coords[1][0]
                                    y0 = _down[_idx_1].coords[1][1]
                                    
                                    x1 = _down[_idx_2].coords[1][0]
                                    y1 = _down[_idx_2].coords[1][1]
                                elif _idx_1 != -1 and _idx_2 == -1:
                                    x0 = _down[_idx_1].coords[1][0]
                                    y0 = _down[_idx_1].coords[1][1]
                                    
                                    _next_seg_vertices = self.vertices[k + 1]
                                    _next = _next_seg_vertices[1]
                                    
                                    x1 = _next[0].coords[1][0]
                                    y1 = _next[0].coords[1][1]
                                elif _idx_1 == -1 and _idx_2 != -1:
                                    _prev_seg_vertices = self.vertices[k - 1]
                                    _prev = _prev_seg_vertices[1]
                                    
                                    x0 = _prev[len(_prev) - 1].coords[1][0]
                                    y0 = _prev[len(_prev) - 1].coords[1][1]
                                  
                                    x1 = _down[_idx_2].coords[1][0]
                                    y1 = _down[_idx_2].coords[1][1]
                                else:
                                    print('Unexpected!')
                                    sys.exit()
                                
                                dxx = x1 - x0
                                dyy = y1 - y0

                                _f = (_x1 - x0) / dxx
                                _y1 = y0 + dyy * _f
                                
                                _line = LineString([(_x1, _y), (_x1, _y1)])
                                temp += [_line]
                            
                        if temp != []:
                            _c1 = 0
                            _c2 = 0
                            
                            _n = len(temp)
                            
                            while _c1 < _m and _c2 < _n:
                                x0 = _down[_c1].coords[0][0]
                                x1 = temp[_c2].coords[0][0]
                                
                                if x0 < x1:
                                    _new_down += [_down[_c1]]
                                    _c1 += 1
                                elif x0 > x1:
                                    _new_down += [temp[_c2]]
                                    _c2 += 1
                                else:
                                    print('Unexpected Error. (Two vertices with the same coords.)')
                                    sys.exit()
                            
                            while _c1 < _m:
                                _new_down += [_down[_c1]]
                                _c1 += 1
                            
                            while _c2 < _n:
                                _new_down += [temp[_c2]]
                                _c2 += 1
                    
                    #print(len(_new_down))
                    
                    if _new_down != []:
                        #print(len(self.vertices[k][1]), len(_new_down))
                        self.vertices[k][1] = _new_down

    def disappear_to_point(self, point):
        pass
    
    def get_line_in_corner_up(self, curr_seg, i, k):
        x = curr_seg.coords[k][0]
        y = curr_seg.coords[k][1]
                
        xx = 0
        yy = 0
                
        _prev_v = None
        _next_v = None
        
        _f = 0
        
        if k == 0:
            _m = len(self.vertices[i][k])
            
            if _m > 0:
                _next_v = self.vertices[i][k][0]
            else:
                _next = self.vertices[i + 1][k]
                _next_v = _next[0]
            
            _m = len(self.vertices[i - 1][k])
            _prev_v = self.vertices[i - 1][k][_m - 1]
            
            _f = random.uniform(0.15, 0.45)
        else:
            _m = len(self.vertices[i][k])
                    
            if _m > 0:
                _prev_v = self.vertices[i][k][_m - 1]
            else:
                _prev = self.vertices[i - 1][k]
                        
                _m = len(_prev)
                _prev_v = _prev[_m - 1]
                    
            _next_v = self.vertices[i + 1][k][0]
            
            _f = random.uniform(0.5, 0.85)
        
        x0 = _prev_v.coords[1][0]
        y0 = _prev_v.coords[1][1]
                            
        x1 = _next_v.coords[1][0]
        y1 = _next_v.coords[1][1]
                
        dxx = x1 - x0
        dyy = y1 - y0

        #_f = random.uniform(0.2, 0.8)
                
        xx = x0 + dxx * _f
        yy = y0 + dyy * _f
                            
        line = LineString([(x, y), (xx, yy)])
        return line
    
    def get_line_in_corner_down(self, curr_seg, i, k):
        if k == 0:
            x = curr_seg.coords[1][0]
            y = curr_seg.coords[1][1]
        else:
            x = curr_seg.coords[0][0]
            y = curr_seg.coords[0][1]
        
        xx = 0
        yy = 0
                
        _prev_v = None
        _next_v = None
        
        _f = 0
        
        if k == 0:
            _m = len(self.vertices[i][k])
            
            if _m > 0:
                _prev_v = self.vertices[i][k][_m - 1]
            else:
                _prev = self.vertices[i - 1][k]
                _m = len(_prev)
                _prev_v = _prev[_m - 1]
            
            _next_v = self.vertices[i + 1][k][0]
            _f = random.uniform(0.55, 0.88)
        else:
            _m = len(self.vertices[i][k])
                    
            if _m > 0:
                _next_v = self.vertices[i][k][0]
            else:
                _next = self.vertices[i + 1][k]
                _next_v = _next[0]
             
            _m = len(self.vertices[i - 1][k])
            _prev_v = self.vertices[i - 1][k][_m - 1]
            _f = random.uniform(0.15, 0.5)
                
        x0 = _prev_v.coords[1][0]
        y0 = _prev_v.coords[1][1]
                            
        x1 = _next_v.coords[1][0]
        y1 = _next_v.coords[1][1]
                
        dxx = x1 - x0
        dyy = y1 - y0

        #_f = random.uniform(0.2, 0.8)
                
        xx = x0 + dxx * _f
        yy = y0 + dyy * _f
                            
        line = LineString([(x, y), (xx, yy)])
        return line
    
    """
        Disappears to the support line.
    """
    def disappear_to_line(self):
        # insert vertices to avoid invalid geometries
    
        _n = len(self.segs)
        _i = 0
        
        _vertices = []
        _len = len(self.vertices)
        _k = 0
        
        while _k < _len:
            up = self.vertices[_k][0]
            down = self.vertices[_k][1]
            
            _vertices += [[up, down]]
            _k += 1
        
        while _i < _n - 1:
            curr_seg = self.segs[_i]
            next_seg = self.segs[_i + 1]
            
            y0 = curr_seg.coords[0][1]
            y1 = next_seg.coords[0][1]
                        
            if y0 > y1:
                line1 = self.get_line_in_corner_up(curr_seg, _i, 1)
                _vertices[_i][1] += [line1]

                line2 = self.get_line_in_corner_up(next_seg, _i + 1, 0)
                _vertices[_i + 1][0] = [line2] + _vertices[_i + 1][0]
            else:
                line1 = self.get_line_in_corner_down(curr_seg, _i, 0)
                _vertices[_i][0] += [line1]

                line2 = self.get_line_in_corner_down(next_seg, _i + 1, 1)
                _vertices[_i + 1][1] = [line2] + _vertices[_i + 1][1]

            _i += 1
        
        vertices = []
        
        self.vertices = _vertices
        #self.print_vertices_csv()
        #sys.exit()
        
        for v in self.vertices:
            up_vertices = v[0]
            down_vertices = v[1]
            
            _seg_up_vertices = []
            _seg_down_vertices = []
            
            for _l in up_vertices:
                _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[0][0], _l.coords[0][1])])
                _seg_up_vertices += [_line]
            
            for _l in down_vertices:
                _line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[0][0], _l.coords[0][1])])
                _seg_down_vertices += [_line]
            
            vertices += [[_seg_up_vertices, _seg_down_vertices]]
        
        supp_line = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
    
        self.generate_coords_from_vertices()
        source_coords = self.get_coords()
    
        supp_line.generate_coords_from_vertices()
        target_coords = supp_line.get_coords()
    
        return supp_line, source_coords, target_coords

    """
    Disappears to a line based on the support line.
    """
    def disappear_to_line2(self):       
        vertices = []
        N = len(self.segs)
        i = 0
        
        segs = self.segs
        
        for v in self.vertices:
            up_vertices = v[0]
            down_vertices = v[1]
            
            seg_up_vertices = []
            seg_down_vertices = []
            
            x0 = 0
            y0 = 0
            
            x1 = 0
            y1 = 0
            
            for _l in up_vertices:
                if i == N - 1:
                    x0 = _l.coords[0][0]
                    y0 = _l.coords[0][1]
                    
                    x1 = _l.coords[0][0]
                    y1 = _l.coords[0][1]
                else:
                    s0 = segs[i]
                    s1 = segs[i + 1]
                    
                    _x0 = s0.coords[0][0]
                    _y0 = s0.coords[0][1]
                    
                    _x1 = s1.coords[0][0]
                    _y1 = s1.coords[0][1]
                
                    dxx = _x1 - _x0
                    dyy = _y1 - _y0

                    x0 = _l.coords[0][0]
                    y0 = _l.coords[0][1]
                    
                    x1 = _l.coords[0][0]
                    y1 = _y0 + dyy * ((x1 - _x0) / dxx)
                
                line = LineString([(x0, y0), (x1, y1)])
                seg_up_vertices += [line]
            
            # >>>
                        
            for _l in down_vertices:
                if i == N - 1:
                    x0 = _l.coords[0][0]
                    y0 = _l.coords[0][1]
                    
                    x1 = _l.coords[0][0]
                    y1 = _l.coords[0][1]
                else:
                    s0 = segs[i]
                    s1 = segs[i + 1]
                    
                    _x0 = s0.coords[0][0]
                    _y0 = s0.coords[0][1]
                    
                    _x1 = s1.coords[0][0]
                    _y1 = s1.coords[0][1]
                
                    dxx = _x1 - _x0
                    dyy = _y1 - _y0

                    x0 = _l.coords[0][0]
                    y0 = _l.coords[0][1]
                    
                    x1 = _l.coords[0][0]
                    y1 = _y0 + dyy * ((x1 - _x0) / dxx)
                
                line = LineString([(x0, y0), (x1, y1)])
                seg_down_vertices += [line]

            i += 1
            vertices += [[seg_up_vertices, seg_down_vertices]]
        
        sl = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
    
        self.generate_coords_from_vertices()
        source_coords = self.get_coords()
    
        sl.generate_coords_from_vertices()
        target_coords = sl.get_coords()
    
        return sl, source_coords, target_coords

    def get_line_from_sl(self):
        coords = []
       
        N = len(self.segs)
        i = 0

        for seg in self.segs:
            if i == N - 1:
                coords += [(seg.coords[0][0], seg.coords[0][1])]
                coords += [(seg.coords[1][0], seg.coords[1][1])]
            else:
                coords += [(seg.coords[0][0], seg.coords[0][1])]
            
            i += 1
        
        return LineString(coords)

    def disappear_to_seg(self):
        # insert vertices to avoid invalid geometries
        """
        _n = len(self.segs)
        _i = 0
        
        _vertices = []
        _len = len(self.vertices)
        _k = 0
        
        while _k < _len:
            up = self.vertices[_k][0]
            down = self.vertices[_k][1]
            
            _vertices += [[up, down]]
            _k += 1        
        
        while _i < _n - 1:
            curr_seg = self.segs[_i]
            next_seg = self.segs[_i + 1]
            
            y0 = curr_seg.coords[0][1]
            y1 = next_seg.coords[0][1]
                        
            if y0 > y1:
                line1 = self.get_line_in_corner_up(curr_seg, _i, 1)
                _vertices[_i][1] += [line1]

                line2 = self.get_line_in_corner_up(next_seg, _i + 1, 0)
                _vertices[_i + 1][0] = [line2] + _vertices[_i + 1][0]
            else:
                line1 = self.get_line_in_corner_down(curr_seg, _i, 0)
                _vertices[_i][0] += [line1]

                line2 = self.get_line_in_corner_down(next_seg, _i + 1, 1)
                _vertices[_i + 1][1] = [line2] + _vertices[_i + 1][1]

            _i += 1
        
        vertices = []
        
        self.vertices = _vertices
        """
        
        _n = len(self.segs)
        
        x0 = self.segs[0].coords[0][0]
        y0 = self.segs[0].coords[0][1]
        
        x1 = self.segs[_n - 1].coords[1][0]
        y1 = self.segs[_n - 1].coords[1][1]
        
        seg = LineString([(x0, y0), (x1, y1)])
        poly = self.get_poly()
        
        #print(seg.distance(poly))
        #print(poly.distance(seg))
        
        if not seg.within(poly):
            """
            print(seg.wkt + ';')
            print('')
            print(poly.wkt + ';')
            print(seg.distance(poly))
            """
            return None, None, None
        
        segs = [seg]
        vertices = []
        
        dxx = x1 - x0
        dyy = y1 - y0
        
        for v in self.vertices:
            up_vertices = v[0]
            down_vertices = v[1]
            
            _seg_up_vertices = []
            _seg_down_vertices = []
            
            for _l in up_vertices:
                _x = _l.coords[0][0]
                
                _f = (_x - x0) / dxx
                _y = y0 + dyy * _f
                 
                _line = LineString([(_x, _y), (_x, _y)])
                _seg_up_vertices += [_line]
            
            for _l in down_vertices:
                _x = _l.coords[0][0]
                
                _f = (_x - x0) / dxx
                _y = y0 + dyy * _f
                 
                _line = LineString([(_x, _y), (_x, _y)])
                #_line = LineString([(_l.coords[0][0], _l.coords[0][1]), (_l.coords[0][0], _l.coords[0][1])])
                _seg_down_vertices += [_line]
            
            vertices += [[_seg_up_vertices, _seg_down_vertices]]
        
        supp_line = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, segs, vertices)
    
        self.generate_coords_from_vertices()
        source_coords = self.get_coords()
    
        supp_line.generate_coords_from_vertices()
        target_coords = supp_line.get_coords()
    
        return supp_line, source_coords, target_coords

    def disappear_to_point(self):
        _n = len(self.segs)
        
        seg_id = np.random.randint(0, _n)
        seg_len = self.segs[seg_id]. length
        dx = random.uniform(0, seg_len)

        _x = self.segs[seg_id].coords[0][0] + dx
        _y = self.segs[seg_id].coords[0][1]
        
        seg = LineString([(_x, _y), (_x, _y)])
        segs = [seg]
        
        #print(_x, _y)
        
        vertices = []
               
        for v in self.vertices:
            up_vertices = v[0]
            down_vertices = v[1]
            
            _seg_up_vertices = []
            _seg_down_vertices = []
            
            for _l in up_vertices:
                _line = LineString([(_x, _y), (_x, _y)])
                _seg_up_vertices += [_line]
            
            for _l in down_vertices:
                _line = LineString([(_x, _y), (_x, _y)])
                _seg_down_vertices += [_line]
            
            vertices += [[_seg_up_vertices, _seg_down_vertices]]
        
        supp_line = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, segs, vertices)
    
        self.generate_coords_from_vertices()
        source_coords = self.get_coords()
    
        supp_line.generate_coords_from_vertices()
        target_coords = supp_line.get_coords()
    
        return supp_line, source_coords, target_coords
    
    def get_consume_line_coords(self, other_vertices, minl, maxl):
        vertices = []
        
        _n = len(self.vertices)
        _m = len(other_vertices)
        
        if _n != _m:
            print('get_consume_line_coords different number of vertices.')
            return None
        
        _i = 0
                
        while _i < _n:
            seg1 = self.vertices[_i]
            seg2 = other_vertices[_i]
            
            up_coords = []
            down_coords = []
            
            up1 = seg1[0]
            down1 = seg1[1]
        
            up2 = seg2[0]
            down2 = seg2[1]
        
            _k = len(up1)
            _h = 0
            
            while _h < _k:
                v1 = up1[_h]
                v2 = up2[_h]
                
                x0 = v2[0]
                y0 = v2[1]
                
                x1 = v1.coords[1][0]
                y1 = v1.coords[1][1]
                
                dxx = x1 - x0
                dyy = y1 - y0
                
                _f = random.uniform(minl, maxl)
                
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f
                
                up_coords += [(_x, _y)]
                _h += 1

            _k = len(down1)
            _h = 0
            
            while _h < _k:
                v1 = down1[_h]
                v2 = down2[_h]
                
                x0 = v2[0]
                y0 = v2[1]
                
                x1 = v1.coords[1][0]
                y1 = v1.coords[1][1]
                
                dxx = x1 - x0
                dyy = y1 - y0
                
                _f = random.uniform(minl, maxl)
                
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f
                
                down_coords += [(_x, _y)]
                _h += 1

            vertices += [[up_coords, down_coords]]
        
            _i += 1
        
        return vertices

    def get_consume_line_coords_2(self, other_vertices, minl, maxl):
        vertices = []
        
        _n = len(self.vertices)
        _m = len(other_vertices)
        
        if _n != _m:
            print('get_consume_line_coords different number of vertices.')
            return None
        
        _i = 0
                
        while _i < _n:
            seg1 = self.vertices[_i]
            seg2 = other_vertices[_i]
            
            up_coords = []
            down_coords = []
            
            up1 = seg1[0]
            down1 = seg1[1]
        
            up2 = seg2[0]
            down2 = seg2[1]
        
            _k = len(up1)
            _h = 0
            
            while _h < _k:
                v1 = up1[_h]
                v2 = up2[_h]
                
                x0 = v2.coords[1][0]
                y0 = v2.coords[1][1]
                
                x1 = v1.coords[1][0]
                y1 = v1.coords[1][1]
                
                dxx = x1 - x0
                dyy = y1 - y0
                
                _f = random.uniform(minl, maxl)
                
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f
                
                up_coords += [(_x, _y)]
                _h += 1

            _k = len(down1)
            _h = 0
            
            while _h < _k:
                v1 = down1[_h]
                v2 = down2[_h]
                
                x0 = v2.coords[1][0]
                y0 = v2.coords[1][1]
                
                x1 = v1.coords[1][0]
                y1 = v1.coords[1][1]
                
                dxx = x1 - x0
                dyy = y1 - y0
                
                _f = random.uniform(minl, maxl)
                
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f
                
                down_coords += [(_x, _y)]
                _h += 1

            vertices += [[up_coords, down_coords]]
        
            _i += 1
        
        return vertices

    def generate_hole_from_sp_line_2(self, min1, max1, min2, max2):
    
        # 0.10, 0.20, 0.05, 0.20
        
        vertices = []
        
        s_up_coords = []
        t_up_coords = []
            
        s_down_coords = []
        t_down_coords = []
        
        for seg in self.vertices:
            up = seg[0]
            down = seg[1]
            
            _t_up_coords = []
            _t_down_coords = []
            
            for v in up:
                x0 = v.coords[0][0]
                y0 = v.coords[0][1]
                
                x1 = v.coords[1][0]
                y1 = v.coords[1][1]
                
                _x = 0
                _y = 0
                
                dxx = x1 - x0
                dyy = y1 - y0
                
                _f = random.uniform(min1, max1)
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f

                s_up_coords += [(x0, y0)]
                t_up_coords += [(_x, _y)]
                
                _t_up_coords += [LineString([(x0, y0), (_x, _y)])]            
            for v in down:
                x0 = v.coords[0][0]
                y0 = v.coords[0][1]
                
                x1 = v.coords[1][0]
                y1 = v.coords[1][1]
                
                _x = 0
                _y = 0
                
                dxx = x1 - x0
                dyy = y1 - y0
                
                _f = random.uniform(min2, max2)
                
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f

                s_down_coords += [(x0, y0)]
                t_down_coords += [(_x, _y)]
                
                _t_down_coords += [LineString([(x0, y0), (_x, _y)])]
        
            vertices += [[_t_up_coords, _t_down_coords]]
        
        ###
        
        _len = len(s_down_coords) - 1
        
        while _len >= 0:
            s_up_coords += [s_down_coords[_len]]
            _len -= 1
           
        s_up_coords += [(s_up_coords[0][0], s_up_coords[0][1])]

        ###

        _len = len(t_down_coords) - 1
        
        while _len >= 0:
            t_up_coords += [t_down_coords[_len]]
            _len -= 1
           
        t_up_coords += [(t_up_coords[0][0], t_up_coords[0][1])]

        ###
        
        sl = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
        return s_up_coords, t_up_coords, sl

    """
        Generates a hole from the geometry support line.
        Input
            min1                 min up evolve percentage
            max1                max up evolve percentage
            min2                 min down evolve percentage
            max2                max down evolve percentage
        Output
            sl                      support line of the new geometry
            s_up_coords    source coords
            t_up_coords     target coords
    """
    def generate_hole_from_sl(self, min1, max1, min2, max2):
        vertices = []
        
        s_up_coords = []
        t_up_coords = []
            
        s_down_coords = []
        t_down_coords = []
        
        for seg in self.vertices:
            up = seg[0]
            down = seg[1]
            
            _t_up_coords = []
            _t_down_coords = []
            
            for v in up:
                x0 = v.coords[0][0]
                y0 = v.coords[0][1]
                
                x1 = v.coords[1][0]
                y1 = v.coords[1][1]
                
                _x = 0
                _y = 0
                
                dxx = x1 - x0
                dyy = y1 - y0
                
                _f = random.uniform(min1, max1)
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f

                s_up_coords += [(x0, y0)]
                t_up_coords += [(_x, _y)]
                
                _t_up_coords += [LineString([(x0, y0), (_x, _y)])]            
            
            for v in down:
                x0 = v.coords[0][0]
                y0 = v.coords[0][1]
                
                x1 = v.coords[1][0]
                y1 = v.coords[1][1]
                
                _x = 0
                _y = 0
                
                dxx = x1 - x0
                dyy = y1 - y0
                
                _f = random.uniform(min2, max2)
                
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f

                s_down_coords += [(x0, y0)]
                t_down_coords += [(_x, _y)]
                
                _t_down_coords += [LineString([(x0, y0), (_x, _y)])]
        
            vertices += [[_t_up_coords, _t_down_coords]]
        
        ###
        
        _len = len(s_down_coords) - 1
        
        while _len >= 0:
            s_up_coords += [s_down_coords[_len]]
            _len -= 1
           
        s_up_coords += [(s_up_coords[0][0], s_up_coords[0][1])]

        ###

        _len = len(t_down_coords) - 1
        
        while _len >= 0:
            t_up_coords += [t_down_coords[_len]]
            _len -= 1
           
        t_up_coords += [(t_up_coords[0][0], t_up_coords[0][1])]

        ###
        
        sl = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
        return sl, s_up_coords, t_up_coords

    def generate_hole_from_sl_2(self, min1, max1, min2, max2):
        vertices = []
        
        s_up_coords = []
        t_up_coords = []
            
        s_down_coords = []
        t_down_coords = []
        
        N = len(self.segs)
        k = 0
        for seg in self.vertices:
            up = seg[0]
            down = seg[1]
            
            _t_up_coords = []
            _t_down_coords = []
            
            sx1 = self.segs[k].coords[0][0]
            sy1 = self.segs[k].coords[0][1]
            
            sx2 = 0
            sy2 = 0
            
            if k == N - 1:
                sx2 = self.segs[k].coords[1][0]
                sy2 = self.segs[k].coords[1][1]
            else:
                sx2 = self.segs[k + 1].coords[0][0]
                sy2 = self.segs[k + 1].coords[0][1]
            
            for v in up:
                x0 = v.coords[0][0]
                y0 = v.coords[0][1]
                
                x1 = v.coords[1][0]
                y1 = v.coords[1][1]
                
                _x = 0
                _y = 0
                
                if k != N - 1:
                    dxx = sx2 - sx1
                    dyy = sy2 - sy1
                
                    _x = x0
                    _f = (_x - sx1) / dxx
                    _y = sy1 + dyy * _f
                    
                    _x0 = _x
                    _y0 = _y
                    
                    dxx = x1 - _x0
                    dyy = y1 - _y0
                    
                    _f = random.uniform(min1, max1)
                    
                    _x = _x0 + dxx * _f
                    _y = _y0 + dyy * _f
                    
                    s_up_coords += [(_x0, _y0)]
                    t_up_coords += [(_x, _y)]
                else:
                    dxx = x1 - x0
                    dyy = y1 - y0
                    
                    _f = random.uniform(min1, max1)
                    _x = x0 + dxx * _f
                    _y = y0 + dyy * _f
                
                    s_up_coords += [(x0, y0)]
                    t_up_coords += [(_x, _y)]
                
                _t_up_coords += [LineString([(x0, y0), (_x, _y)])]            
            
            for v in down:
                x0 = v.coords[0][0]
                y0 = v.coords[0][1]
                
                x1 = v.coords[1][0]
                y1 = v.coords[1][1]
                
                _x = 0
                _y = 0
                
                """
                dxx = x1 - x0
                dyy = y1 - y0
                
                _f = random.uniform(min2, max2)
                
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f

                s_down_coords += [(x0, y0)]
                t_down_coords += [(_x, _y)]
                """
                
                if k != N - 1:
                    dxx = sx2 - sx1
                    dyy = sy2 - sy1
                
                    _x = x0
                    _f = (_x - sx1) / dxx
                    _y = sy1 + dyy * _f
                    
                    _x0 = _x
                    _y0 = _y
                    
                    dxx = x1 - _x0
                    dyy = y1 - _y0
                    
                    _f = random.uniform(min1, max1)
                    
                    _x = _x0 + dxx * _f
                    _y = _y0 + dyy * _f
                    
                    s_down_coords += [(_x0, _y0)]
                    t_down_coords += [(_x, _y)]
                else:
                    dxx = x1 - x0
                    dyy = y1 - y0
                    
                    _f = random.uniform(min1, max1)
                    _x = x0 + dxx * _f
                    _y = y0 + dyy * _f
                
                    s_down_coords += [(x0, y0)]
                    t_down_coords += [(_x, _y)]
                
                _t_down_coords += [LineString([(x0, y0), (_x, _y)])]
        
            vertices += [[_t_up_coords, _t_down_coords]]
            k += 1
        
        ###
        
        _len = len(s_down_coords) - 1
        
        while _len >= 0:
            s_up_coords += [s_down_coords[_len]]
            _len -= 1
           
        s_up_coords += [(s_up_coords[0][0], s_up_coords[0][1])]

        ###

        _len = len(t_down_coords) - 1
        
        while _len >= 0:
            t_up_coords += [t_down_coords[_len]]
            _len -= 1
           
        t_up_coords += [(t_up_coords[0][0], t_up_coords[0][1])]

        ###
        
        sl = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
        return sl, s_up_coords, t_up_coords

    def generate_geom_from_sp_line(self, min1, max1, min2, max2):
        vertices = []
       
        for seg in self.vertices:
            up = seg[0]
            down = seg[1]
            
            _t_up_coords = []
            _t_down_coords = []
            
            for v in up:
                x0 = v.coords[0][0]
                y0 = v.coords[0][1]
                
                x1 = v.coords[1][0]
                y1 = v.coords[1][1]
                
                _x = 0
                _y = 0
                
                dxx = x1 - x0
                dyy = y1 - y0
                
                _f = random.uniform(min1, max1)
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f
                
                _t_up_coords += [LineString([(x0, y0), (_x, _y)])]

            for v in down:
                x0 = v.coords[0][0]
                y0 = v.coords[0][1]
                
                x1 = v.coords[1][0]
                y1 = v.coords[1][1]
                
                _x = 0
                _y = 0
                
                dxx = x1 - x0
                dyy = y1 - y0
                
                _f = random.uniform(min2, max2)
                
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f
                
                _t_down_coords += [LineString([(x0, y0), (_x, _y)])]

            vertices += [[_t_up_coords, _t_down_coords]]
                
        sl = SupportLine(self.num_segs, self.seg_min_len, self.seg_max_len, self.up_down_min_len, self.up_down_max_len, self.segs, vertices)
        return sl

    def generate_hole_from_sp_line(self):
        vertices = []
        
        s_up_coords = []
        t_up_coords = []
            
        s_down_coords = []
        t_down_coords = []
        
        for seg in self.vertices:
            up = seg[0]
            down = seg[1]
            
            _t_up_coords = []
            _t_down_coords = []
            
            for v in up:
                x0 = v.coords[0][0]
                y0 = v.coords[0][1]
                
                x1 = v.coords[1][0]
                y1 = v.coords[1][1]
                
                _x = 0
                _y = 0
                
                dxx = x1 - x0
                dyy = y1 - y0
                
                _f = random.uniform(0.10, 0.20)
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f

                s_up_coords += [(x0, y0)]
                t_up_coords += [(_x, _y)]
                
                _t_up_coords += [(_x, _y)]
            
            for v in down:
                x0 = v.coords[0][0]
                y0 = v.coords[0][1]
                
                x1 = v.coords[1][0]
                y1 = v.coords[1][1]
                
                _x = 0
                _y = 0
                
                dxx = x1 - x0
                dyy = y1 - y0
                
                _f = random.uniform(0.05, 0.20)
                
                _x = x0 + dxx * _f
                _y = y0 + dyy * _f

                s_down_coords += [(x0, y0)]
                t_down_coords += [(_x, _y)]
                
                _t_down_coords += [(_x, _y)]
        
            vertices += [(_t_up_coords, _t_down_coords)]
        
        ###
        
        _len = len(s_down_coords) - 1
        
        while _len >= 0:
            s_up_coords += [s_down_coords[_len]]
            _len -= 1
           
        s_up_coords += [(s_up_coords[0][0], s_up_coords[0][1])]

        ###

        _len = len(t_down_coords) - 1
        
        while _len >= 0:
            t_up_coords += [t_down_coords[_len]]
            _len -= 1
           
        t_up_coords += [(t_up_coords[0][0], t_up_coords[0][1])]

        ###
        
        return s_up_coords, t_up_coords, vertices

    def get_support_line(self):
        coords = []
        
        if len(self.segs) == 1 and self.segs[0].coords[0][0] == self.segs[0].coords[1][0] and self.segs[0].coords[0][1] == self.segs[0].coords[1][1]:
            return Point(self.segs[0].coords[0][0], self.segs[0].coords[0][1])
        
        for s in self.segs:
            #s.coords[0][0]
            coords += [(s.coords[0][0], s.coords[0][1]), (s.coords[1][0], s.coords[1][1])]
        
        #print(coords)
        return LineString(coords)
    
    def get_transformed_coords(self):
        coords = []
        
        for seg in self.vertices:
            up = seg[0]
            down = seg[1]
            
            up_v = []
            down_v = []
            
            for v in up:
                up_v += [(v.coords[1][0], v.coords[1][1])]
            
            for v in down:
                down_v +=  [(v.coords[1][0], v.coords[1][1])]
            
            coords += [[up_v, down_v]]
        
        return coords
    
    def get_poly(self):
        return Polygon(self.coords)

    def get_coords(self):
        return self.coords

    def get_vertices(self):
        return self.vertices

    def get_segs(self):
        return self.segs
    
    # Debug >>>
    
    def print_vertices_csv(self):
        print(';')
        for v in self.vertices:
            up = v[0]
            down = v[1]
            
            for l in up:
                print(l.wkt + ';')
                
            for l in down:
                print(l.wkt + ';')
    
    def print_segs_csv(self):
        print(';')
        for seg in self.segs:
            print(seg.wkt + ';')
    
    def print_structure(self):
        print('')
        self.print_segs_csv()
        print('')
        self.print_vertices_csv()
        
        print('')
        if self.coords == []:
            self.generate_coords_from_vertices()

        poly = self.get_poly()
        print(';')
        print(poly.wkt + ';')
