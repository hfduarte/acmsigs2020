package gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import generators.CustomDatasetGenerator;
import generators.DatasetLoaderAndViewer;
import generators.PrimitiveEventsGenerator;
import generators.CustomDatasetViewer;

import java.awt.Color;

/*
 * Authors: José Duarte 	(hfduarte@ua.pt)
 * 			Mark Mckenney 	(marmcke@siue.edu)
 * 
 * Version: 1.0 (2020-05-29)
 * 
 */

public class MainWindow extends JFrame 
{
	private static final long serialVersionUID = -7003878987581378628L;

	private JPanel contentPane;
	private JFrame f;
	private JButton c_ds_generator_btn;
	private JButton ds_viewer_btn;
	private JButton p_ev_generator_btn;
	private JButton c_ds_viewer_btn;
	
	public MainWindow() 
	{
		f = this;	
		draw_UI();
		add_listeners();
	}
	
	public void draw_UI() 
	{
		setTitle("Generator Main Window");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    setSize(830, 700);
	    setLocationRelativeTo(null);
	    setExtendedState(f.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(8, 1, 10, 10));
				
		JPanel format = new JPanel();
		format.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		contentPane.add(format);
		
		JPanel at_p = new JPanel();
		at_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(at_p);
		at_p.setLayout(null);
		
		p_ev_generator_btn = new JButton("Primitive Events Generator");
		p_ev_generator_btn.setBounds(231, 6, 223, 25);
		at_p.add(p_ev_generator_btn);
		
		c_ds_generator_btn = new JButton("Custom Dataset Generator");
		c_ds_generator_btn.setBounds(459, 6, 225, 25);
		at_p.add(c_ds_generator_btn);
		
		ds_viewer_btn = new JButton("Dataset Viewer");
		ds_viewer_btn.setBounds(689, 6, 144, 25);
		at_p.add(ds_viewer_btn);
		
		c_ds_viewer_btn = new JButton("Custom Dataset Viewer");
		c_ds_viewer_btn.setBounds(838, 6, 201, 25);
		at_p.add(c_ds_viewer_btn);
	}

	public void add_listeners()
	{
		p_ev_generator_btn.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent event) 
            {   
            	PrimitiveEventsGenerator gen = new PrimitiveEventsGenerator();
        		gen.setVisible(true);       	
            }
        });
		
		c_ds_generator_btn.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent event) 
            {   
            	CustomDatasetGenerator gen = new CustomDatasetGenerator();
        		gen.setVisible(true);
            }
        });
		
		ds_viewer_btn.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent event) 
            {   
            	DatasetLoaderAndViewer viewer = new DatasetLoaderAndViewer();
            	viewer.setVisible(true);   
            }
        });
		
		c_ds_viewer_btn.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent event) 
            {   
            	CustomDatasetViewer viewer = new CustomDatasetViewer();
            	viewer.setVisible(true);   
            }
        });
	}
}