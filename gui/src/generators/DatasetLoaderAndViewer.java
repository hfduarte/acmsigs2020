package generators;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.LiteShape;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.util.AffineTransformation;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;

import tools.ScreenImage;

/*
 * Authors: José Duarte 	(hfduarte@ua.pt)
 * 			Mark Mckenney 	(marmcke@siue.edu)
 * 
 * Version: 1.0 (2020-05-29)
 * 
 */

public class DatasetLoaderAndViewer extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel viz_p;
	private JFrame f;
	private JPanel jp;
	private JLabel status;
	private JPanel tools_p;
	private JLabel zoom_level;
	private int num_invalid_geoms;
	private int n_obs;
	
	public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 0.25;
	public static final double DEFAULT_MIN_ZOOM_LEVEL = 0.25;
	public static final double DEFAULT_MAX_ZOOM_LEVEL = 600;
	
    private double zoomLevel = 1.0;
    private double minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
    private double maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
    private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;
    private GeometryFactory geometryFactory;
    private WKTReader reader; 
    private int w_center;
	private int h_center;
	private double cx = 0;
	private double cy = 0;
    private JCheckBox fill;
    private JSlider zoom_slider;
    private JButton load_bt;
    private Geometry geometry;
    private String geometry_wkt;
    private boolean show_info_about_geometry_validity;
    private JButton save_curr_to_picture;
	private JLabel num_vertices_lb;
	private JButton clear_bt;
	private boolean center_geoms;
	private String[] annotations;
    private JCheckBox use_guides_cb;
    private boolean interpolation_is_valid;
    private JTextField png_filename_tx;
    private JSlider obs_slider;
    private String[] arr;
	private JButton see_annotations_bt;
	private JLabel zoom_slider_lb;
	private JLabel n_obs_lb;
	private JTextField zoom_factor_tx;
	private int v_id;
	private float[] start;
	private float[] end;
	private JLabel aprox_t_lb;
	private JButton save_seq_to_picture;
	private JTextField n_seq_to_save_tx;
	private boolean show_annotations;
    private JButton show_annotations_bt;
	
	public DatasetLoaderAndViewer() 
	{
		f = this;
		
		start = null;
		end = null;
		
		show_annotations = true;
		
		interpolation_is_valid = false;
		n_obs = 0;
		v_id = 0;
		annotations = null;
		arr = null;
		
		center_geoms = false;
		
		geometry = null;
		geometry_wkt = null;
		
	    geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
	    reader = new WKTReader(geometryFactory);
		
		num_invalid_geoms = 0;
	    
		draw_UI();
		
		w_center = (int) (viz_p.getWidth() / 2);
		h_center = (int) (viz_p.getHeight() / 2);
		
	    show_info_about_geometry_validity = false;
	    
		add_listeners();
		draw_geometry();
	}
	
	public void paint(Graphics g) 
	{
	    super.paint(g);
	    jp.repaint();
	}

	public void draw_geometry()
	{
		viz_p.setLayout(new BorderLayout());
		jp = new DrawGraphs();
		viz_p.add(jp, BorderLayout.CENTER);
	}
	
    private class DrawGraphs extends JPanel
    {
    	private int dx = 0;
    	private int dy = 0;
    	
    	private double sx = 1;
    	private double sy = 1; 	
    	   	
    	private int xx = 0;
    	private int yy = 0;
    	
    	private int _dx = 0;
    	private int _dy = 0;
    	
		private static final long serialVersionUID = 3203545490664689791L;
		
		public DrawGraphs() 
		{
			setBackground(Color.white);
			
			setAlignmentY(CENTER_ALIGNMENT);
			setAlignmentX(CENTER_ALIGNMENT);
			
	        addMouseListener(new MouseAdapter() 
	        {
	            public void mousePressed(MouseEvent e) 
	            {
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });

	        addMouseMotionListener(new MouseAdapter() 
	        {
	            public void mouseDragged(MouseEvent e) 
	            {        	
	            	int ddx = (e.getX() - xx);
	            	int ddy = (e.getY() - yy);
	            	
	            	translate(ddx, ddy);
	            	repaint();
	            	
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });
	        
	        addMouseWheelListener(new MouseAdapter()
	        {
	            public void mouseWheelMoved(MouseWheelEvent e) 
	            {
		            int notches = e.getWheelRotation();
					zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
					
		            if (notches > 0) 
		            {
		            	if (zoomLevel < maxZoomLevel) 
		            	{
		            		zoomLevel += zoomMultiplicationFactor; 
			            	scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
		            	}
		            } else {
			           	if (zoomLevel > minZoomLevel) 
			           	{
			           		zoomLevel -= zoomMultiplicationFactor;
							scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
			            }
		            }

		            center_lines(true);
		            repaint();
	            }
	        });       
		};
		
		@Override
        protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);

	        Graphics2D gr = (Graphics2D) g;
	        gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	        
		    gr.setStroke(new BasicStroke(1.0f));	        
		    gr.setPaint(Color.blue);

	        AffineTransform at = new AffineTransform();
	        at.translate(dx, dy);
	        
	        zoom_level.setText("Zoom Level: " + sx);
   
			try 
			{				
			    if (geometry_wkt != null)
			    {	
			    	if(show_annotations)
			    	{
				    	float t = Float.valueOf(v_id) / (n_obs - 1);
				    	int p = 90;
				    	int step = 20;
				    	
			    	    double scale = Math.pow(10, 2);
			    	    float _t = (float) (Math.round(t * scale) / scale);
				    		    	    
				    	aprox_t_lb.setText("Approx. T: " + _t);
				    	
				    	for(int i = 0; i < start.length; i++)
				    	{
				    		//System.out.println(_t + " " + start[i] + " " + end[i]);
				    		//System.out.println((_t >= start[i]) + " " + (_t <= end[i]));
				    		//System.out.println((_t == start[i]) + " " + (_t == end[i]));
				    		
			    			if(_t >= start[i] && _t <= end[i])
				    		{
					   		    gr.setFont(new Font("Arial", Font.BOLD, 14));
								gr.setPaint(new Color(0.2f, 0.23f, 0.78f, 1.0f));
								gr.drawString(annotations[i], 20, p);
								
								p+= step;
				    		}
				    	}
			    	}

			    	geometry = reader.read(geometry_wkt);
			    	
					int n_vertices = geometry.getNumPoints();
					num_vertices_lb.setText("Nº Vertices: " + n_vertices);
			    	
					AffineTransformation affine = new AffineTransformation();
					affine.scale(sx, -sy);
					geometry.apply(affine);;
			    	
			    	gr.setPaint(Color.black);
			    	gr.draw(new LiteShape(geometry, at, false));
			    	
   					if(fill.isSelected() && geometry.getGeometryType() != "LineString")
   					{
   						gr.setPaint(new Color(18, 21, 67));

		   		        for (int j = 0; j < geometry.getNumGeometries(); j++)
				   			gr.fill(new LiteShape(geometry.getGeometryN(j), at, false));
   					}
   					
		   		    gr.setFont(new Font("Arial", Font.BOLD, 14));
					gr.setPaint(new Color(0.27f, 0.27f, 0.29f, 1.0f));
					gr.drawString("Nº Invalid Geoms: " + String.valueOf(num_invalid_geoms), 20, 50);
			    }
			    
			    if(show_info_about_geometry_validity)
			    {
			    	if(interpolation_is_valid)
						gr.setPaint(new Color(6, 40, 94));
					else
						gr.setPaint(new Color(166, 10, 83));
					
					gr.setFont(new Font("Arial", Font.BOLD, 14));
					
					if(interpolation_is_valid)
						gr.drawString("Interpolation is Valid!", 20, 30);
					else
						gr.drawString("Interpolation is not Valid!", 20, 30);
			    }
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
			}
        };
        
        public void translate(int x, int y) 
        {
        	_dx += x;
        	_dy += y;
        	
        	dx += x;
        	dy += y;
        }
        
        public void scale(double x, double y) 
        {
        	sx += x;
        	sy += y;
        }
        
        public void center_lines(boolean translate)
        {
        	if(geometry_wkt == null)
        		return;
        	
			try
			{
			    Geometry g = reader.read(geometry_wkt);

			    // Apply scale factor.
			    
				AffineTransformation affine = new AffineTransformation();		
				affine.scale(sx, -sy);
				
				g.apply(affine);
			    
			    // Get centroid of the polygon envelope.
				
			    Point c = g.getEnvelope().getCentroid();
			
				cx = c.getX();
				cy = c.getY();
			} 
			catch (ParseException e) {
				e.printStackTrace();
			}

			w_center = (int) (viz_p.getWidth() / 2);
			h_center = (int) (viz_p.getHeight() / 2);
			
			if(!translate)
			{
	        	_dx = 0;
	        	_dy = 0;
			}
			
			dx = (int) ((-cx + w_center)) + _dx;
			dy = (int) ((-cy + h_center)) + _dy;
        }
    }
	
	public void draw_UI()
	{
		setTitle("Dataset Viewer");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	    setSize(1296, 720);
	    setLocationRelativeTo(null);
	    setExtendedState(f.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	    
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		viz_p = new JPanel();
		viz_p.setLocation(175, 10);
		viz_p.setSize(930, 590);	
		viz_p.setBackground(Color.WHITE);
		viz_p.setBorder(new LineBorder(Color.black, 1));
		contentPane.add(viz_p);
		
		tools_p = new JPanel();
		tools_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		tools_p.setBounds(10, 616, 1260, 36);
		contentPane.add(tools_p);
		
		zoom_factor_tx = new JTextField();
		zoom_factor_tx.setToolTipText("Zoom factor.");
		zoom_factor_tx.setText("0.25");
		zoom_factor_tx.setForeground(SystemColor.desktop);
		zoom_factor_tx.setColumns(3);
		zoom_factor_tx.setBackground(Color.LIGHT_GRAY);
		tools_p.add(zoom_factor_tx);
		
		n_seq_to_save_tx = new JTextField();
		n_seq_to_save_tx.setToolTipText("Zoom factor.");
		n_seq_to_save_tx.setText("6");
		n_seq_to_save_tx.setForeground(SystemColor.desktop);
		n_seq_to_save_tx.setColumns(3);
		n_seq_to_save_tx.setBackground(Color.LIGHT_GRAY);
		tools_p.add(n_seq_to_save_tx);
		
		save_seq_to_picture = new JButton("Save Sequence To PNG");
		tools_p.add(save_seq_to_picture);
		
		save_curr_to_picture = new JButton("Save Current To PNG");
		tools_p.add(save_curr_to_picture);
		
		png_filename_tx = new JTextField();
		png_filename_tx.setText("/home/user/");
		tools_p.add(png_filename_tx);
		png_filename_tx.setColumns(10);
		
		use_guides_cb = new JCheckBox("Use Guides");
		tools_p.add(use_guides_cb);
		
		status = new JLabel("");
		tools_p.add(status);
		
		fill = new JCheckBox("Fill");
		fill.setBounds(111, 10, 56, 25);
		contentPane.add(fill);
		
		zoom_level = new JLabel("Zoom Level:");
		zoom_level.setForeground(new Color(0, 0, 128));
		zoom_level.setBounds(1115, 10, 155, 14);
		contentPane.add(zoom_level);
		zoom_slider = new JSlider();
		
		zoom_slider.setValue(1);
		zoom_slider.setMinimum((int) minZoomLevel);
		zoom_slider.setMaximum((int) maxZoomLevel);
		
		zoom_slider.setBounds(1117, 62, 155, 26);
		contentPane.add(zoom_slider);
	    
	    load_bt = new JButton("Load Dataset");
	    load_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    load_bt.setForeground(new Color(72, 61, 139));
	    load_bt.setBounds(10, 86, 153, 25);
	    contentPane.add(load_bt);
			
		num_vertices_lb = new JLabel("Nº Vertices: ");
		num_vertices_lb.setHorizontalAlignment(SwingConstants.LEFT);
		num_vertices_lb.setForeground(new Color(25, 25, 112));
		num_vertices_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		num_vertices_lb.setBounds(13, 136, 153, 16);
		contentPane.add(num_vertices_lb);
		
		clear_bt = new JButton("Clear");
		clear_bt.setForeground(SystemColor.desktop);
		clear_bt.setToolTipText("Clear data");
		clear_bt.setFont(new Font("Dialog", Font.BOLD, 11));
		clear_bt.setBounds(10, 465, 156, 23);
		contentPane.add(clear_bt);
		
		obs_slider = new JSlider();
		obs_slider.setValue(1);
		obs_slider.setMinimum(0);
		obs_slider.setMaximum(600);
		obs_slider.setBounds(1117, 117, 155, 26);
		contentPane.add(obs_slider);
		
		see_annotations_bt = new JButton("See Annotations");
		see_annotations_bt.setToolTipText("See Dataset Annotations");
		see_annotations_bt.setForeground(new Color(47, 79, 79));
		see_annotations_bt.setFont(new Font("Dialog", Font.BOLD, 11));
		see_annotations_bt.setBounds(11, 440, 156, 23);
		contentPane.add(see_annotations_bt);
		
		JLabel interpolation_slider_lb = new JLabel("Evolution Slider");
		interpolation_slider_lb.setForeground(new Color(0, 0, 128));
		interpolation_slider_lb.setBounds(1117, 97, 155, 14);
		contentPane.add(interpolation_slider_lb);
		
		zoom_slider_lb = new JLabel("Zoom Slider");
		zoom_slider_lb.setForeground(SystemColor.activeCaption);
		zoom_slider_lb.setBounds(1117, 42, 155, 14);
		contentPane.add(zoom_slider_lb);
		
		n_obs_lb = new JLabel("Nº Samples: ");
		n_obs_lb.setToolTipText("Number of Observations.");
		n_obs_lb.setHorizontalAlignment(SwingConstants.LEFT);
		n_obs_lb.setForeground(new Color(25, 25, 112));
		n_obs_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		n_obs_lb.setBounds(13, 157, 154, 16);
		contentPane.add(n_obs_lb);
		
		aprox_t_lb = new JLabel("Approx. T: ");
		aprox_t_lb.setToolTipText("Approximate time.");
		aprox_t_lb.setHorizontalAlignment(SwingConstants.LEFT);
		aprox_t_lb.setForeground(new Color(25, 25, 112));
		aprox_t_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		aprox_t_lb.setBounds(13, 178, 154, 16);
		contentPane.add(aprox_t_lb);
		
		show_annotations_bt = new JButton("Show Annotations");
		show_annotations_bt.setToolTipText("Show Dataset Annotations on Screen");
		show_annotations_bt.setForeground(new Color(47, 79, 79));
		show_annotations_bt.setFont(new Font("Dialog", Font.BOLD, 11));
		show_annotations_bt.setBounds(11, 500, 156, 23);
		contentPane.add(show_annotations_bt);
	}

	public void add_listeners()
	{
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                setVisible(false);
                dispose();
            }
        });
        
		fill.addItemListener(new ItemListener() 
		{
			@Override 
			public void itemStateChanged(ItemEvent e) 
			{
				jp.repaint();
			}
		});
		
	    show_annotations_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if(show_annotations)
					show_annotations = false;
				else
					show_annotations = true;

				jp.repaint();
			}
		});

		save_seq_to_picture.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				//int v = 0;
				int N = arr.length;
				int show_granularity = Integer.valueOf(n_seq_to_save_tx.getText());
				
				String temp = geometry_wkt;
				
				try
				{
					for(int i = 0; i < N; i = i + show_granularity) 
					{
						geometry_wkt = arr[i];
						jp.repaint();
			    		
						BufferedImage bi = ScreenImage.createImage(viz_p);
	    				ScreenImage.writeImage(bi, png_filename_tx.getText() + "_" + (N + i) + ".png");
					}
    			} 
				catch (IOException ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Save Sequence ERR", JOptionPane.INFORMATION_MESSAGE);
					return;
    			}
				
				geometry_wkt = temp;
				jp.repaint();
				JOptionPane.showMessageDialog(null, "Sequence Saved!", "Save Sequence", JOptionPane.INFORMATION_MESSAGE);	
			}
		});
		
		see_annotations_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (annotations != null)
				{
					String out_str = "<html><body>";
					
					for (String a : annotations) {
						out_str += "<p>" + a + "<br><br>";
					}
					
					out_str += "</body></html>";
		                
					// change to alter the width 
		            int w = 200;
					
		            JOptionPane.showMessageDialog(null, String.format(out_str, w, w), "Dataset Annotations", JOptionPane.INFORMATION_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null, "[]", "Dataset Annotations", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		save_curr_to_picture.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				String filename = png_filename_tx.getText() + "_" + 1 + ".png";
				
				try
				{
					BufferedImage bi = ScreenImage.createImage(viz_p);
	    			ScreenImage.writeImage(bi, filename);
    			} 
				catch (IOException ex) {
    				ex.printStackTrace();
    			}
				
				jp.repaint();
				JOptionPane.showMessageDialog(null, "Saved to: " + filename + "!", "Save", JOptionPane.INFORMATION_MESSAGE);	
			}
		});
		
		load_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{	
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				
				int result = fileChooser.showOpenDialog(f);
								
				if (result == JFileChooser.APPROVE_OPTION) 
				{
					
				    File ds_file = fileChooser.getSelectedFile();
				    
					interpolation_is_valid = false;
					show_info_about_geometry_validity = false;
					geometry_wkt = null;
					arr = null;
					num_vertices_lb.setText("Nº Vertices: ");
					n_obs_lb.setText("Nº Samples: ");
					aprox_t_lb.setText("Approx. T: ");
					annotations = null;
					n_obs = 0;
					v_id = 0;
					start = null;
					end = null;
					
					BufferedReader buf_reader;
					
					try {
						buf_reader = new BufferedReader(new FileReader(ds_file));
						
						String line = buf_reader.readLine();
						
						int i = 0;					
						int n_annotations = 0;
						int n_samples = 0;
						String A = "";
						int k = 0;
						
						while (line != null) 
						{
							if(i == 0)
							{
								String[] number = line.split(":");
								n_annotations = Integer.valueOf(number[0]);
								n_samples = Integer.valueOf(number[1]);
								arr = new String[n_samples];								
							}
							else
							{
								if(i <= n_annotations) 
								{
									A += line;
									
									if(i < n_annotations)
										A += ";";									
								}
								else
								{
									arr[k] = line;
									k++;
								}
							}
							
							line = buf_reader.readLine();
							i++;
						}
						
						buf_reader.close();
						
						// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
						
						n_obs_lb.setText("Nº Samples: " + n_samples);
						n_obs = n_samples;
						
						// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
						
						//System.out.println(A);
						
						annotations = A.split(";");
						
						int m = annotations.length;
						start = new float[m];
						end = new float[m];
						int h = 0;
						
						for (String a : annotations) 
						{
							String[] E = a.split(",");
							
							for (String e : E) 
							{
								if(e.contains("start:"))
								{
									String[] r = e.split(":");
									start[h] = Float.valueOf(r[1]);
								}
								else if(e.contains("end:"))
								{
									String[] r = e.split(":");
									end[h] = Float.valueOf(r[1]);
								}
							}
							h++;
						}
						
						geometry_wkt = arr[0];

						obs_slider.setMaximum(arr.length - 1);
						obs_slider.setValue(0);
										
						DrawGraphs g = (DrawGraphs) jp;
						g.center_lines(true);
						center_geoms = false;
						
						// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
						
						num_invalid_geoms = 0;
						
						for(String wkt : arr)
						{
							try {
								geometry = reader.read(wkt);
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							if(!geometry.isValid())
								num_invalid_geoms++;
						}
						
						if(num_invalid_geoms > 0)
							interpolation_is_valid = false;
						else
							interpolation_is_valid = true;
						
						show_info_about_geometry_validity = true;	
						
						// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
						
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
							
				jp.repaint();
			}
		});
		
		obs_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		    	if (arr == null || arr.length == 0)
		    		return;
		    	
		        JSlider s = (JSlider) e.getSource();
		        int i = (int) s.getValue();

		    	if(i < arr.length)
		    	{
		    		geometry_wkt = arr[i];
		    		v_id = i;
		    	}
				
				DrawGraphs g = (DrawGraphs) jp;
				
				if(center_geoms)
					g.center_lines(true);
				
		    	jp.repaint();
		    }
		});
		
		zoom_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		        JSlider s = (JSlider) e.getSource();
		        int desired_zoom_level = (int) s.getValue();
		        
		        DrawGraphs c = (DrawGraphs) jp;
		        double diff = desired_zoom_level - zoomLevel;
		        		        
		        if(diff > 0 && desired_zoom_level < maxZoomLevel)
		        {
		        	zoomLevel = desired_zoom_level;
            		c.scale(diff, diff);
		        }
		        else if(diff < 0 && desired_zoom_level > minZoomLevel)
		        {
		        	zoomLevel = desired_zoom_level;
            		c.scale(diff, diff);
		        }
				
		        c.center_lines(true);
		    	jp.repaint(); 
		    }
		});
		
		clear_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
		    {
				interpolation_is_valid = false;
				show_info_about_geometry_validity = false;
				geometry_wkt = null;
				arr = null;
				num_vertices_lb.setText("Nº Vertices: ");
				n_obs_lb.setText("Nº Samples: ");
				aprox_t_lb.setText("Approx. T: ");
				annotations = null;
				n_obs = 0;
				v_id = 0;
				start = null;
				end = null;
				
				jp.repaint(); 
		    }
		});
	}
}