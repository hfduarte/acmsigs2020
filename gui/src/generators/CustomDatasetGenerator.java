package generators;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.LiteShape;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.util.AffineTransformation;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;

import tools.ScreenImage;
import javax.swing.JTextArea;
import java.awt.FlowLayout;

/*
 * Authors: José Duarte 	(hfduarte@ua.pt)
 * 			Mark Mckenney 	(marmcke@siue.edu)
 * 
 * Version: 1.0 (2020-05-29)
 * 
 */

public class CustomDatasetGenerator extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel viz_p;
	//private Data d;
	private JFrame f;
	private JPanel jp;
	private JLabel status;
	private JPanel tools_p;
	private JLabel zoom_level;
	private JComboBox<String> methods_cb;
	
	private int num_invalid_geoms;
	private int num_complex_geoms;
	
	public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 0.25;
	public static final double DEFAULT_MIN_ZOOM_LEVEL = 0.25;
	public static final double DEFAULT_MAX_ZOOM_LEVEL = 600;
	
    private double zoomLevel = 1.0;
    private double minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
    private double maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
    private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;
    //private int geom_to_show_id = 0;
    //private int bbox_to_show_id = 0;
    private GeometryFactory geometryFactory;
    private WKTReader reader; 
    //private String[] geometries;
    //private String[] bboxes;
    //private String wkt;
    //private int max;
    //private TimerTask play_task;
	private int w_center;
	private int h_center;
	private double cx = 0;
	private double cy = 0;
    //private Timer timer;
    private JCheckBox fill;
    //private int show_granularity;
    //private boolean show_footprint;
    private JSlider zoom_slider;
    private JButton generate_bt;
    //private String ref_geom_wkt;
    //private boolean show_bbox;
    private JTextField wkt_save_in_file;
    private Geometry geometry;
    private String geometry_wkt;
    //private Geometry aabb;
    private JButton geometry_valid_bt;
    private JCheckBox num_vertices_cb;
    //private boolean show_info_about_interpolation_validity;
    private boolean show_info_about_geometry_validity;
    //private boolean is_p_valid;
    //private boolean is_q_valid;

    //private boolean is_interpolation_valid;
    //private int granularity;
    //private int num_invalid_geometries;
    //private Boolean[] geometries_validity_info;
    private JButton save_to_picture;
    
	private JLabel num_vertices_lb;
	private JLabel num_holes_lb;
	private JCheckBox num_holes_cb;
	private JButton clear_bt;
	private boolean center_geoms;
	private String[] annotations;
    
    /*
    private methods.PySpatiotemporalGeom pyspatiotemporalgeom_lib;
    private methods.Librip librip;
    private methods.RigidInterpolation rigid;
    */
    
	/*
    private double db;
    private double de;
    
    private String w_p;
    private String w_q;
    
    private double db_i;
    private double de_i;
    */
    
    //private int dn;
    private JTextField zoom_factor_tx;
    //private boolean show_points;
    private JTextField num_vertices_tx;
    private JTextField num_holes_tx;
    private boolean interpolation_is_valid;
    private JTextField png_filename_tx;
    private JSlider obs_slider;
    //private String[] arr;
    private JTextField min_len_tx;
    private JTextField max_len_tx;
    private JTextField n_samples_tx;
    private JTextField ds_tx;
    private JTextField dy_tx;
    private JTextField rot_tx;
	private JCheckBox change_top_cb;
	private JButton ds_save_bt;
	private JButton see_annotations_bt;
	private JLabel zoom_slider_lb;
	private JLabel rot_lb;
	private JLabel rot_lb_1;
	private JTextArea hist_tx;
	private int[] n_invalid_geoms;
	private int[] n_complex_geoms;
	private boolean[] geoms_validity;
	private String[][] Ant;
	private String[][] geoms;
	private int N_GEOMS;
	private JLabel n_units_lb;
	private JLabel unit_lb;
	private int N_UNITS;
	private int UNIT;
	private int GPOS;
	
	public CustomDatasetGenerator() 
	{
		f = this;
		
		//show_points = false;
		interpolation_is_valid = false;
		
		annotations = null;
		//arr = null;
		
		center_geoms = false;
		
		geometry = null;
		geometry_wkt = null;
		
	    geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
	    reader = new WKTReader(geometryFactory);
		
		num_invalid_geoms = 0;
		num_complex_geoms = 0;
	    
		N_UNITS = 0;
		UNIT = 0;
		n_invalid_geoms = null;
		n_complex_geoms = null;
		Ant = null;
		geoms = null;
		N_GEOMS = 0;
		GPOS = 0;
		
		geoms_validity =  null;
		
		draw_UI();
		
		w_center = (int) (viz_p.getWidth() / 2);
		h_center = (int) (viz_p.getHeight() / 2);
		
	    show_info_about_geometry_validity = false;
	    
		add_listeners();
		draw_geometry();
	}
	
	public void paint(Graphics g) 
	{
	    super.paint(g);
	    jp.repaint();
	}

	public void draw_geometry()
	{
		viz_p.setLayout(new BorderLayout());
		jp = new DrawGraphs();
		viz_p.add(jp, BorderLayout.CENTER);
	}
	
    private class DrawGraphs extends JPanel
    {
    	private int dx = 0;
    	private int dy = 0;
    	
    	private double sx = 1;
    	private double sy = 1; 	
    	   	
    	private int xx = 0;
    	private int yy = 0;
    	
    	private int _dx = 0;
    	private int _dy = 0;
    	
		private static final long serialVersionUID = 3203545490664689791L;
		
		public DrawGraphs() 
		{
			setBackground(Color.white);
			
			setAlignmentY(CENTER_ALIGNMENT);
			setAlignmentX(CENTER_ALIGNMENT);
			
	        addMouseListener(new MouseAdapter() 
	        {
	            public void mousePressed(MouseEvent e) 
	            {
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });

	        addMouseMotionListener(new MouseAdapter() 
	        {
	            public void mouseDragged(MouseEvent e) 
	            {        	
	            	int ddx = (e.getX() - xx);
	            	int ddy = (e.getY() - yy);
	            	
	            	translate(ddx, ddy);
	            	repaint();
	            	
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });
	        
	        addMouseWheelListener(new MouseAdapter()
	        {
	            public void mouseWheelMoved(MouseWheelEvent e) 
	            {
		            int notches = e.getWheelRotation();
					zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
					
		            if (notches > 0) 
		            {
		            	if (zoomLevel < maxZoomLevel) 
		            	{
		            		zoomLevel += zoomMultiplicationFactor; 
			            	scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
		            	}
		            } else {
			           	if (zoomLevel > minZoomLevel) 
			           	{
			           		zoomLevel -= zoomMultiplicationFactor;
							scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
			            }
		            }

		            center_lines(true);
		            repaint();
	            }
	        });       
		};
		
		@Override
        protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);

	        Graphics2D gr = (Graphics2D) g;
	        gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	        
		    gr.setStroke(new BasicStroke(1.0f));	        
		    gr.setPaint(Color.blue);

	        AffineTransform at = new AffineTransform();
	        at.translate(dx, dy);
	        
	        zoom_level.setText("Zoom Level: " + sx);
			//double marker_radius = Double.parseDouble(maker_radius_tx.getText());
			        
			try 
			{				
			    if (geometry_wkt != null)
			    {		
			    	geometry = reader.read(geometry_wkt);
			    	
					AffineTransformation affine = new AffineTransformation();
					affine.scale(sx, -sy);
					geometry.apply(affine);;
			    	
			    	gr.setPaint(Color.black);
			    	gr.draw(new LiteShape(geometry, at, false));
			    	
   					if(fill.isSelected() && geometry.getGeometryType() != "LineString")
   					{
   						gr.setPaint(new Color(18, 21, 67));

		   		        for (int j = 0; j < geometry.getNumGeometries(); j++)
				   			gr.fill(new LiteShape(geometry.getGeometryN(j), at, false));
   					}
   					
		   		    gr.setFont(new Font("Arial", Font.BOLD, 14));
					gr.setPaint(new Color(0.75f, 0.2f, 0.2f, 1.0f));
					gr.drawString("Nº Invalid Geoms: " + String.valueOf(num_invalid_geoms), 20, 50);
					//gr.drawString("Nº complex Geoms: " + String.valueOf(num_complex_geoms), 20, 90);
					
					n_units_lb.setText("Nº Units: " + N_UNITS);
					unit_lb.setText("Unit: " + (UNIT + 1));
					
					if(geoms_validity[UNIT])
						gr.setPaint(new Color(6, 40, 94));
					else
						gr.setPaint(new Color(166, 10, 83));
							
					gr.setFont(new Font("Arial", Font.BOLD, 14));
							
					if(geoms_validity[UNIT])
						gr.drawString("Valid Moving Region Unit!", 20, 30);
					else
						gr.drawString("Invalid Moving Region Unit!", 20, 30);
			    }
			    
			    /*
			    if(show_info_about_geometry_validity)
			    {
			    	if(interpolation_is_valid)
						gr.setPaint(new Color(6, 40, 94));
					else
						gr.setPaint(new Color(166, 10, 83));
					
					gr.setFont(new Font("Arial", Font.BOLD, 14));
					
					if(interpolation_is_valid)
						gr.drawString("Interpolation is Valid!", 20, 30);
					else
						gr.drawString("Interpolation is not Valid!", 20, 30);
			    }
			    */
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
			}
        };
        
        public void translate(int x, int y) 
        {
        	_dx += x;
        	_dy += y;
        	
        	dx += x;
        	dy += y;
        }
        
        public void scale(double x, double y) 
        {
        	sx += x;
        	sy += y;
        }
        
        public void center_lines(boolean translate)
        {
        	if(geometry_wkt == null)
        		return;
        	
			try
			{
			    Geometry g = reader.read(geometry_wkt);

			    // Apply scale factor.
			    
				AffineTransformation affine = new AffineTransformation();		
				affine.scale(sx, -sy);
				
				g.apply(affine);
			    
			    // Get centroid of the polygon envelope.
				
			    Point c = g.getEnvelope().getCentroid();
			
				cx = c.getX();
				cy = c.getY();
			} 
			catch (ParseException e) {
				e.printStackTrace();
			}

			w_center = (int) (viz_p.getWidth() / 2);
			h_center = (int) (viz_p.getHeight() / 2);
			
			if(!translate)
			{
	        	_dx = 0;
	        	_dy = 0;
			}
			
			dx = (int) ((-cx + w_center)) + _dx;
			dy = (int) ((-cy + h_center)) + _dy;
        }
    }
	
	public void draw_UI()
	{
		setTitle("Custom Dataset Generator");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	    setSize(1296, 720);
	    setLocationRelativeTo(null);
	    setExtendedState(f.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	    
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		viz_p = new JPanel();
		viz_p.setLocation(175, 10);
		viz_p.setSize(930, 562);	
		viz_p.setBackground(Color.WHITE);
		viz_p.setBorder(new LineBorder(Color.black, 1));
		contentPane.add(viz_p);
		
		tools_p = new JPanel();
		tools_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		tools_p.setBounds(10, 577, 1260, 82);
		contentPane.add(tools_p);
		tools_p.setLayout(null);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(687, 8, 0, 15);
		tools_p.add(textArea);
		
		status = new JLabel("");
		status.setBounds(692, 15, 0, 0);
		tools_p.add(status);
		
		hist_tx = new JTextArea();
		hist_tx.setText("1,1,0,shrink,0,0.2,0,0,0#1,1,0,split-at-point,0.2,0.5,0,0,2,0,0,3,0,0#1,2,0,evolve,0.5,1,-100,50#1,3,0,shrink,0.5,1,100,-75,0;1,2,0,shrink,0,0.2,-5,10,0#0,4,2,hole-appear-from-line,0.2,0.5,-5,10#1,2,0,shrink,0.2,0.5,-5,10,0#0,4,2,evolve,0.5,1,-5,10#1,2,0,shrink,0.5,1,-5,10,0#1,3,0,shrink,0,0.5,10,-15,0#1,3,0,split-cut,0.5,0.5,5,1#1,3,0,shrink,0.5,1,0,-15,0,0#1,5,0,shrink,0.5,1,0,45,0;1,2,0,shrink,0,0.2,-5,10,0#0,4,2,split-at-point,0,0.2,-5,10,6,0,0,7,0,0#1,2,0,evolve,0.2,1,-5,10#0,6,2,evolve,0.2,1,-15,20#0,7,2,grow,0.2,1,-10,5#1,5,0,shrink,0,0.3,0,15,0#1,5,0,disappear-to-line,0.3,0.9,0,15#1,3,0,shrink,0,0.2,10,-15,0#1,3,0,internal-face-split,0.2,0.2,0,0,8,9#1,3,0,shrink,0.2,1,10,-5,0#1,9,8,shrink,0.2,1,10,-5,1#0,8,3,grow,0.2,1,10,-5");
		hist_tx.setBounds(12, 12, 1236, 58);
		tools_p.add(hist_tx);
		
		fill = new JCheckBox("Fill");
		fill.setBounds(111, 10, 56, 25);
		contentPane.add(fill);
		
		zoom_level = new JLabel("Zoom Level:");
		zoom_level.setForeground(new Color(0, 0, 128));
		zoom_level.setBounds(1115, 10, 155, 14);
		contentPane.add(zoom_level);
		zoom_slider = new JSlider();
		
		zoom_slider.setValue(1);
		zoom_slider.setMinimum((int) minZoomLevel);
		zoom_slider.setMaximum((int) maxZoomLevel);
		
		/*
		zoom_slider.setValue(100);
		zoom_slider.setMinimum((int) 25);
		zoom_slider.setMaximum((int) 60000);
		*/
		
		zoom_slider.setBounds(1117, 62, 155, 26);
		contentPane.add(zoom_slider);
	    
	    wkt_save_in_file = new JTextField();
	    wkt_save_in_file.setToolTipText("Filename in HD to store Data.");
	    wkt_save_in_file.setForeground(new Color(25, 25, 112));
	    wkt_save_in_file.setBackground(new Color(240, 255, 240));
	    wkt_save_in_file.setText("/home/user/wkt.txt");
	    wkt_save_in_file.setBounds(11, 553, 157, 20);
	    contentPane.add(wkt_save_in_file);
	    wkt_save_in_file.setColumns(1);
	    
	    methods_cb = new JComboBox<>();
	    methods_cb.setEnabled(false);
	    methods_cb.setToolTipText("Metrics");
	    methods_cb.setModel(new DefaultComboBoxModel(new String[] {"shrink (non-continuous)", "shrink (continuous)", "disappear to line", "disappear to seg", "hole from line", "disappear to line (change top)", "disappear to line (consume)", "burst at n vertices", "hole split face", "hole split face n parts", "burst 2", "grow 2", "evolve 2", "internal face split", "engulf random", "evolve", "split (fragment)", "split (fragment) 2", "split (fragment) 3", "split to point (no translatio)", "split to point (translation)", "generate hole split", "hole disappear to line", "face split hole", "face split hole 2", "internal face merge hole", "internal face consume hole", "internal face merge one side", "remove vertices", "engulf 3", "split (crack)", "split (crack) 2", "crack", "crack 2", "crack 3"}));
	    methods_cb.setBounds(11, 66, 151, 22);
	    contentPane.add(methods_cb);
	    
	    JLabel method_lbl = new JLabel("Primitive Events");
	    method_lbl.setEnabled(false);
	    method_lbl.setHorizontalAlignment(SwingConstants.LEFT);
	    method_lbl.setForeground(new Color(25, 25, 112));
	    method_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
	    method_lbl.setBounds(11, 43, 155, 16);
	    contentPane.add(method_lbl);
	    
	    generate_bt = new JButton("Generate");
	    generate_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    generate_bt.setForeground(new Color(72, 61, 139));
	    generate_bt.setBounds(10, 95, 153, 25);
	    contentPane.add(generate_bt);
	    
	    geometry_valid_bt = new JButton("Interp. Validity");
	    geometry_valid_bt.setEnabled(false);
	    geometry_valid_bt.setToolTipText("Check Interpolation Validity.");
	    geometry_valid_bt.setForeground(new Color(47, 79, 79));
	    geometry_valid_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    geometry_valid_bt.setBounds(10, 467, 156, 23);
	    contentPane.add(geometry_valid_bt);
	    
	    num_vertices_cb = new JCheckBox("Random");
	    num_vertices_cb.setEnabled(false);
	    num_vertices_cb.setSelected(true);
	    num_vertices_cb.setBounds(84, 159, 82, 23);
	    contentPane.add(num_vertices_cb);
			
		num_vertices_lb = new JLabel("Nº Vertices");
		num_vertices_lb.setHorizontalAlignment(SwingConstants.LEFT);
		num_vertices_lb.setForeground(new Color(25, 25, 112));
		num_vertices_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		num_vertices_lb.setBounds(13, 136, 153, 16);
		contentPane.add(num_vertices_lb);
		
		num_vertices_tx = new JTextField();
		//nText("4");
		num_vertices_tx.setBounds(10, 161, 66, 19);
		num_vertices_tx.setText("50");
		contentPane.add(num_vertices_tx);
		num_vertices_tx.setColumns(10);
		
		num_holes_lb = new JLabel("Nº Holes");
		num_holes_lb.setEnabled(false);
		num_holes_lb.setHorizontalAlignment(SwingConstants.LEFT);
		num_holes_lb.setForeground(new Color(25, 25, 112));
		num_holes_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		num_holes_lb.setBounds(13, 186, 153, 16);
		contentPane.add(num_holes_lb);
		
		num_holes_tx = new JTextField();
		num_holes_tx.setEnabled(false);
		num_holes_tx.setColumns(10);
		num_holes_tx.setBounds(10, 211, 66, 19);
		num_holes_tx.setText("4");
		contentPane.add(num_holes_tx);
		
		num_holes_cb = new JCheckBox("Random");
		num_holes_cb.setEnabled(false);
		num_holes_cb.setSelected(true);
		num_holes_cb.setBounds(85, 209, 82, 23);
		contentPane.add(num_holes_cb);
		
		clear_bt = new JButton("Clear");
		clear_bt.setForeground(SystemColor.desktop);
		clear_bt.setToolTipText("Clear data");
		clear_bt.setFont(new Font("Dialog", Font.BOLD, 11));
		clear_bt.setBounds(10, 494, 156, 23);
		contentPane.add(clear_bt);
		
		obs_slider = new JSlider();
		obs_slider.setValue(1);
		obs_slider.setMinimum(0);
		obs_slider.setMaximum(600);
		obs_slider.setBounds(1117, 117, 155, 26);
		contentPane.add(obs_slider);
		
		min_len_tx = new JTextField();
		min_len_tx.setToolTipText("Min");
		min_len_tx.setText("100");
		min_len_tx.setColumns(10);
		min_len_tx.setBounds(11, 265, 66, 19);
		contentPane.add(min_len_tx);
		
		max_len_tx = new JTextField();
		max_len_tx.setToolTipText("Max");
		max_len_tx.setText("300");
		max_len_tx.setColumns(10);
		max_len_tx.setBounds(101, 265, 66, 19);
		contentPane.add(max_len_tx);
		
		n_samples_tx = new JTextField();
		n_samples_tx.setToolTipText("Nº Observations");
		n_samples_tx.setText("100");
		n_samples_tx.setColumns(10);
		n_samples_tx.setBounds(10, 408, 66, 19);
		contentPane.add(n_samples_tx);
		
		ds_tx = new JTextField();
		ds_tx.setToolTipText("Dx");
		ds_tx.setText("0");
		ds_tx.setColumns(10);
		ds_tx.setBounds(10, 318, 66, 19);
		contentPane.add(ds_tx);
		
		dy_tx = new JTextField();
		dy_tx.setToolTipText("Dy");
		dy_tx.setText("0");
		dy_tx.setColumns(10);
		dy_tx.setBounds(101, 318, 66, 19);
		contentPane.add(dy_tx);
		
		JLabel tras_lb = new JLabel("X-Y Translation");
		tras_lb.setHorizontalAlignment(SwingConstants.LEFT);
		tras_lb.setForeground(new Color(25, 25, 112));
		tras_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		tras_lb.setBounds(10, 292, 153, 16);
		contentPane.add(tras_lb);
		
		rot_tx = new JTextField();
		rot_tx.setToolTipText("Rotation (Degrees)");
		rot_tx.setText("0");
		rot_tx.setColumns(10);
		rot_tx.setBounds(10, 385, 66, 19);
		contentPane.add(rot_tx);
		
		JLabel tras_lb_1 = new JLabel("Seg. Min-Max Length");
		tras_lb_1.setHorizontalAlignment(SwingConstants.LEFT);
		tras_lb_1.setForeground(new Color(25, 25, 112));
		tras_lb_1.setFont(new Font("Dialog", Font.BOLD, 12));
		tras_lb_1.setBounds(12, 237, 153, 16);
		contentPane.add(tras_lb_1);
		
		change_top_cb = new JCheckBox("Change Topology");
		change_top_cb.setEnabled(false);
		change_top_cb.setForeground(SystemColor.activeCaption);
		change_top_cb.setBounds(6, 340, 157, 23);
		contentPane.add(change_top_cb);
		
		ds_save_bt = new JButton("Save Dataset");
		ds_save_bt.setForeground(SystemColor.desktop);
		ds_save_bt.setToolTipText("Save Dataset to file.");
		ds_save_bt.setFont(new Font("Dialog", Font.BOLD, 11));
		ds_save_bt.setBounds(11, 521, 156, 25);
		contentPane.add(ds_save_bt);
		
		see_annotations_bt = new JButton("See Annotations");
		see_annotations_bt.setToolTipText("See Dataset Annotations");
		see_annotations_bt.setForeground(new Color(47, 79, 79));
		see_annotations_bt.setFont(new Font("Dialog", Font.BOLD, 11));
		see_annotations_bt.setBounds(11, 440, 156, 23);
		contentPane.add(see_annotations_bt);
		
		JLabel interpolation_slider_lb = new JLabel("Interpolation Slider");
		interpolation_slider_lb.setForeground(new Color(0, 0, 128));
		interpolation_slider_lb.setBounds(1117, 97, 155, 14);
		contentPane.add(interpolation_slider_lb);
		
		zoom_slider_lb = new JLabel("Zoom Slider");
		zoom_slider_lb.setForeground(SystemColor.activeCaption);
		zoom_slider_lb.setBounds(1117, 42, 155, 14);
		contentPane.add(zoom_slider_lb);
		
		rot_lb = new JLabel("Rotation");
		rot_lb.setToolTipText("Rotation in Degrees.");
		rot_lb.setHorizontalAlignment(SwingConstants.LEFT);
		rot_lb.setForeground(new Color(25, 25, 112));
		rot_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		rot_lb.setBounds(84, 386, 83, 16);
		contentPane.add(rot_lb);
		
		rot_lb_1 = new JLabel("Nº Samples");
		rot_lb_1.setToolTipText("Number of Observations.");
		rot_lb_1.setHorizontalAlignment(SwingConstants.LEFT);
		rot_lb_1.setForeground(new Color(25, 25, 112));
		rot_lb_1.setFont(new Font("Dialog", Font.BOLD, 12));
		rot_lb_1.setBounds(84, 410, 83, 16);
		contentPane.add(rot_lb_1);
		
		save_to_picture = new JButton("Save To PNG");
		save_to_picture.setBounds(1117, 467, 155, 25);
		contentPane.add(save_to_picture);
		
		png_filename_tx = new JTextField();
		png_filename_tx.setBounds(1117, 498, 155, 19);
		contentPane.add(png_filename_tx);
		png_filename_tx.setText("/home/user/");
		png_filename_tx.setColumns(10);
		
		zoom_factor_tx = new JTextField();
		zoom_factor_tx.setBounds(1117, 555, 37, 19);
		contentPane.add(zoom_factor_tx);
		zoom_factor_tx.setBackground(SystemColor.inactiveCaptionBorder);
		zoom_factor_tx.setForeground(SystemColor.desktop);
		zoom_factor_tx.setText("0.25");
		//zoom_factor_tEnabled(false);
		//save_interpolation_to_picture.setx.setText("0.25");
		zoom_factor_tx.setToolTipText("Zoom factor.");
		zoom_factor_tx.setColumns(3);
		
		n_units_lb = new JLabel("Nº Units:");
		n_units_lb.setForeground(SystemColor.activeCaption);
		n_units_lb.setBounds(1115, 187, 155, 14);
		contentPane.add(n_units_lb);
		
		unit_lb = new JLabel("Unit:");
		unit_lb.setForeground(SystemColor.activeCaption);
		unit_lb.setBounds(1115, 209, 155, 14);
		contentPane.add(unit_lb);
	}

	public void add_listeners()
	{
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                setVisible(false);
                dispose();
            }
        });
        
		fill.addItemListener(new ItemListener() 
		{
			@Override 
			public void itemStateChanged(ItemEvent e) 
			{
				jp.repaint();
			}
		});
        
		// Zoom out.
		/*
    	minus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

				zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
				
	           	if (zoomLevel > minZoomLevel) 
	           	{
	           		zoomLevel -= zoomMultiplicationFactor;
					c.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
					c.translate(false);
	            }

	            jp.repaint();
			}
		});
    	*/
        
    	// Zoom in.
    	/*
    	plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

				zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
				
            	if (zoomLevel < maxZoomLevel) 
            	{
            		zoomLevel += zoomMultiplicationFactor; 
	            	c.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
	            	c.translate(true);
            	}

	            jp.repaint();

			}
		});
		*/
		
		ds_save_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (Ant != null && geoms != null)
				{
					String filename = wkt_save_in_file.getText();
					PrintWriter writer = null;
					
					try {
						writer = new PrintWriter(filename, "UTF-8");
					} catch (FileNotFoundException | UnsupportedEncodingException e) 
					{
						JOptionPane.showMessageDialog(null, e.getMessage(), "Save Dataset ERROR", JOptionPane.INFORMATION_MESSAGE);
						jp.repaint();
						
						return;
					}
					
					// Write to File >>>
					
					writer.println(N_UNITS);
					
					for(int i = 0; i < N_UNITS; i++)
					{
						writer.println(n_invalid_geoms[i] + "," + n_complex_geoms[i] + "," + geoms[i].length + "," + Ant[i].length);
						
						String str = "";
						
						annotations = Ant[i];
						
						for(int j = 0; j < annotations.length; j ++)
						{
							str += annotations[j];
							
							if (j < annotations.length - 1)
								str += ";";
						}
						
						writer.println(str);
						
						for (String g : geoms[i])
							writer.println(g);
					}
					
					// Write to File >>>
					
					writer.close();
					JOptionPane.showMessageDialog(null, "Saved to " + filename + "!", "Save to dataset.", JOptionPane.INFORMATION_MESSAGE);	

					// >>>					
				}
				else
					JOptionPane.showMessageDialog(null, "No data to save!", "Save Dataset.", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		see_annotations_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (Ant != null)
				{
					annotations = Ant[UNIT];
					
					String out_str = "<html><body>";
					
					for (String a : annotations) {
						out_str += "<p>" + a + "<br><br>";
					}
					
					out_str += "</body></html>";
		                
					// change to alter the width 
		            int w = 200;
					
		            JOptionPane.showMessageDialog(null, String.format(out_str, w, w), "Annotations In Unit " + (UNIT + 1), JOptionPane.INFORMATION_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null, "[]", "Annotations", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		save_to_picture.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				String filename = png_filename_tx.getText() + "_" + 1 + ".png";
				
				try
				{
					BufferedImage bi = ScreenImage.createImage(viz_p);
	    			ScreenImage.writeImage(bi, filename);
    			} 
				catch (IOException ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Save PMG ERROR", JOptionPane.INFORMATION_MESSAGE);
					jp.repaint();
					
					return;
    			}
				
				jp.repaint();
				JOptionPane.showMessageDialog(null, "Saved to: " + filename + "!", "Save", JOptionPane.INFORMATION_MESSAGE);	
			}
		});
		
		/*
		geometry_valid_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				int n_invalid_geoms = 0;
				
				if(arr != null)
				{
					for(String wkt : arr)
					{
						try {
							geometry = reader.read(wkt);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						if(!geometry.isValid())
							n_invalid_geoms ++;
					}
					
					if(n_invalid_geoms > 0)
						interpolation_is_valid = false;
					else
						interpolation_is_valid = true;
					
					show_info_about_geometry_validity = true;	
				}
				
			    jp.repaint();
			}
		});
		*/
		
		generate_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{	
				geometry_wkt = null;
				//String[] arr = null;
				
				// Call python script.
				
				String[] cmd;
				cmd = new String[12];
					
				for(int j = 0; j < cmd.length; j++)
					cmd[j] = "";
				
				//int op = methods_cb.getSelectedIndex();			
				
				cmd[0] = "python";
				//cmd[1] = "/home/user/src/projects/duneRIP/split.py";
				//cmd[2] = "Polygon ((-0.97169383505172302 1.20841509915086576, -1.17133446098139515 0.96599433909340637, -1.01162196023765749 0.79202179364040626, -0.67508490509906682 0.60949322136184869, -0.38132798408826329 0.74638965057076678, -0.54674450271570607 0.88328607977968498, -0.3927360198556733 0.97740237486081627, -0.17883534921673849 0.92891822284932435, -0.23872753699564031 0.67508942702445529, -0.113239143554132 0.60093719453629135, -0.07045900942634509 0.77776174893114391, -0.06475499154263997 1.04014657158157053, -0.41555209139049287 1.09718675041861968, -0.69219695875018172 0.9374742496748818, -0.74923713758723087 1.04299858052342298, -0.34425186784418127 1.25119523327865245, 0.07214143766627812 1.1399668845464066, 0.01510125882922897 1.04870259840712787, 0.06643741978257323 1.03444255369786542, 0.220445902642606 1.33675550153422651, 0.11492157179406504 1.37668362672016098, 0.00084121411996652 1.25404724222050512, -0.40414405562308309 1.50787603804537418, -0.4526282076345749 1.43657581449906258, -0.31002776054195169 1.35957157306904608, -0.7977212895987229 1.13711487560455415, -0.97169383505172302 1.20841509915086576))";
				
				cmd[1] = "/home/user/generator/src/CustomDatasetGenerator.py";
				
				//double rot = rot_tx.getText();
				//boolean change_topology = change_top_cb.isSelected();
				
				cmd[2] = num_vertices_tx.getText();
				cmd[3] = min_len_tx.getText();
				cmd[4] = max_len_tx.getText();
				cmd[5] = n_samples_tx.getText();
				
				cmd[6] = ds_tx.getText();
				cmd[7] = dy_tx.getText();
				
				cmd[8] = rot_tx.getText();
				
				cmd[9] = "0";
								
				cmd[10] = "30";
				cmd[11] = hist_tx.getText();
				
				Runtime rt = Runtime.getRuntime();
				Process pr = null;
				

				for(int i = 0; i < cmd.length; i++)
					System.out.print(cmd[i] + " ");
				
				System.out.println();


				try
				{
					pr = rt.exec(cmd);
				}
				catch (IOException e1) 
				{
					JOptionPane.showMessageDialog(null, e1.getMessage(), "Gen Dataset ERROR", JOptionPane.INFORMATION_MESSAGE);
					jp.repaint();
					
					return;
				}
				
				BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				String line = "";
				int n = 0;

				boolean err = false;
				
				int k = 0;
				int n_geoms = 0;
				int n_ant = 0;
				
				n_invalid_geoms = null;
				n_complex_geoms = null;
				geoms_validity =  null;
				Ant = null;
				geoms = null;
				N_GEOMS = 0;
				N_UNITS = 0;
				UNIT = 0;
				
				String err_ssg = "";
				
				try
				{
					// Build array of geometries in wkt representation.
					int i = 0;
					int j = 0;
					String[] arr = null;
					
					while((line = bfr.readLine()) != null)
					{
						if(i == 0)
						{
							N_UNITS = Integer.valueOf(line);
							//int N = Integer.valueOf(line);
							
							if(N_UNITS > 0)
							{
								n_invalid_geoms = new int[N_UNITS];
								n_complex_geoms = new int[N_UNITS];
								geoms_validity = new boolean[N_UNITS];
								
								Ant = new String[N_UNITS][];
								geoms = new String[N_UNITS][];
							}
							else
								err = true;
						}
						else if(i == 1)
						{
							if(! err)
							{
								String[] A = line.split(",");
								
								n_invalid_geoms[k] = Integer.valueOf(A[0]);
								n_complex_geoms[k] = Integer.valueOf(A[1]);
								
								n_geoms = Integer.valueOf(A[2]);
								n_ant = Integer.valueOf(A[3]);
								
								arr = new String[n_geoms];
							}
						}
						else if(i == 2)
						{
							if(!err)
							{
								Ant[k] = line.split(";");
							}
						}
						else //if(i == 3)
						{
							if(!err)
							{
								arr[j] = line;
								j++;
								n_geoms--;
								
								if(n_geoms == 0)
								{
									geoms[k] = arr;
									
									//System.out.println(k);
									//System.out.println(arr[0]);
									
									int n_inv_geoms = 0;
									
									for(String wkt : arr)
									{
										//try {
										geometry = reader.read(wkt);
										//} catch (ParseException e) {
										//	e.printStackTrace();
										//}
										
										if(!geometry.isValid())
											n_inv_geoms++;
									}
									
									if(n_inv_geoms > 0)
										geoms_validity[k] = false;
									else
										geoms_validity[k] = true;

									i = 0;
									j = 0;
									k++;
								}
							}
							else
								err_ssg = line;
						}
						
						i++;
					}
				}
				catch (IOException e1)
				{
					JOptionPane.showMessageDialog(null, e1.getMessage(), "Gen Dataset ERROR", JOptionPane.INFORMATION_MESSAGE);
					jp.repaint();
					
					return;
					//e1.printStackTrace();
				}
				catch (ParseException e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Gen Dataset ERROR", JOptionPane.INFORMATION_MESSAGE);
					jp.repaint();
					
					return;
				}
				
				if(err) 
				{
					JOptionPane.showMessageDialog(null, err_ssg, "Err Message", JOptionPane.INFORMATION_MESSAGE);	
					//arr = null;
				}
				else
				{
					geometry_wkt = geoms[0][0];
					UNIT = 0;
					
					for(int i = 0; i < geoms.length; i++)
						N_GEOMS += geoms[i].length;
					
					obs_slider.setMaximum(N_GEOMS - 1);
					obs_slider.setValue(0);
					
					n_units_lb.setText("Nº Units: " + N_UNITS);
					unit_lb.setText("Unit: " + (UNIT + 1));
					
					DrawGraphs g = (DrawGraphs) jp;
					g.center_lines(true);
					center_geoms = false;
				}
				
				jp.repaint();
			}
		});
		
		obs_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		    	if(geoms == null)
		    	{
		    		return;
		    	}
		    	
		        JSlider s = (JSlider) e.getSource();
		        int i = (int) s.getValue();

		        if(i < N_GEOMS)
		        {
		        	int ini = 0;
		        	int curr = 0;
		        	
		        	curr = geoms[0].length;
		        	
		        	for(int j = 0; j < geoms.length; j++)
		        	{
		        		if (i < curr)
		        		{
		        			UNIT = j;
		        			GPOS = i - ini;
		        			geometry_wkt = geoms[UNIT][GPOS];
		        			break;
		        		}
		        		else
		        		{
		        			ini += geoms[j].length;
		        			curr += geoms[j].length;
		        		}
		        	}
		        }
		        
				DrawGraphs g = (DrawGraphs) jp;
				
				if(center_geoms)
					g.center_lines(true);
				
		    	jp.repaint();
		    }
		});
		
		zoom_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		        JSlider s = (JSlider) e.getSource();
		        int desired_zoom_level = (int) s.getValue();
		        
		        DrawGraphs c = (DrawGraphs) jp;
		        double diff = desired_zoom_level - zoomLevel;
		        		        
		        if(diff > 0 && desired_zoom_level < maxZoomLevel)
		        {
		        	zoomLevel = desired_zoom_level;
            		c.scale(diff, diff);
		        }
		        else if(diff < 0 && desired_zoom_level > minZoomLevel)
		        {
		        	zoomLevel = desired_zoom_level;
            		c.scale(diff, diff);
		        }
				
		        c.center_lines(true);
		    	jp.repaint(); 
		    }
		});
		
		clear_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
		    {
				interpolation_is_valid = false;
				show_info_about_geometry_validity = false;
				
				geometry_wkt = null;
				//arr = null;
				
				N_UNITS = 0;
				UNIT = 0;
				
				n_invalid_geoms = null;
				n_complex_geoms = null;
				geoms_validity =  null;
				
				GPOS = 0;
				
				Ant = null;
				geoms = null;
				N_GEOMS = 0;
				
				n_units_lb.setText("Nº Units: ");
				unit_lb.setText("Unit: ");
		    }
		});
	}
}