package generators;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.LiteShape;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.util.AffineTransformation;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;

import tools.ScreenImage;
import java.awt.FlowLayout;

public class CustomDatasetViewer extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel viz_p;
	//private Data d;
	private JFrame f;
	private JPanel jp;
	private JLabel status;
	private JPanel tools_p;
	private JLabel zoom_level;
	
	private int num_invalid_geoms;
	private int num_complex_geoms;
	
	public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 0.25;
	public static final double DEFAULT_MIN_ZOOM_LEVEL = 0.25;
	public static final double DEFAULT_MAX_ZOOM_LEVEL = 600;
	
    private double zoomLevel = 1.0;
    private double minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
    private double maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
    private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;
    //private int geom_to_show_id = 0;
    //private int bbox_to_show_id = 0;
    private GeometryFactory geometryFactory;
    private WKTReader reader; 
    //private String[] geometries;
    //private String[] bboxes;
    //private String wkt;
    //private int max;
    //private TimerTask play_task;
	private int w_center;
	private int h_center;
	private double cx = 0;
	private double cy = 0;
    //private Timer timer;
    private JCheckBox fill;
    //private int show_granularity;
    //private boolean show_footprint;
    private JSlider zoom_slider;
    private JButton load_ds_bt;
    private Geometry geometry;
    private String geometry_wkt;
    //private boolean show_info_about_interpolation_validity;
    private boolean show_info_about_geometry_validity;

    private JButton save_to_picture;
	private JButton clear_bt;
	private boolean center_geoms;
	private String[] annotations;
    
    private JTextField zoom_factor_tx;
    private boolean interpolation_is_valid;
    private JTextField png_filename_tx;
    private JSlider obs_slider;
	private JButton show_ant_bt;
	private JButton see_annotations_bt;
	private JLabel zoom_slider_lb;
	private int[] n_invalid_geoms;
	private int[] n_complex_geoms;
	private boolean[] geoms_validity;
	private String[][] Ant;
	private String[][] geoms;
	private int N_GEOMS;
	private JLabel n_units_lb;
	private JLabel unit_lb;
	private int N_UNITS;
	private int UNIT;
	private int GPOS;
	private boolean show_annotations;
	private JTextField gran_tx;
	private JLabel num_vertices_lb;
	private JLabel n_obs_lb;
	private JLabel aprox_t_lb;
	private JButton save_seq_to_picture;
	private float[] start;
	private float[] end;
	
	public CustomDatasetViewer() 
	{
		f = this;
		
		//show_points = false;
		interpolation_is_valid = false;
		
		start = null;
		end = null;
		
		annotations = null;
		//arr = null;
		
		show_annotations = true;
		
		center_geoms = false;
		
		geometry = null;
		geometry_wkt = null;
		
	    geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
	    reader = new WKTReader(geometryFactory);
		
		num_invalid_geoms = 0;
		num_complex_geoms = 0;
	    
		N_UNITS = 0;
		UNIT = 0;
		n_invalid_geoms = null;
		n_complex_geoms = null;
		Ant = null;
		geoms = null;
		N_GEOMS = 0;
		GPOS = 0;
		
		geoms_validity =  null;
		
		draw_UI();
		
		w_center = (int) (viz_p.getWidth() / 2);
		h_center = (int) (viz_p.getHeight() / 2);
		
	    show_info_about_geometry_validity = false;
	    
		add_listeners();
		draw_geometry();
	}
	
	public void paint(Graphics g) 
	{
	    super.paint(g);
	    jp.repaint();
	}

	public void draw_geometry()
	{
		viz_p.setLayout(new BorderLayout());
		jp = new DrawGraphs();
		viz_p.add(jp, BorderLayout.CENTER);
	}
	
    private class DrawGraphs extends JPanel
    {
    	private int dx = 0;
    	private int dy = 0;
    	
    	private double sx = 1;
    	private double sy = 1; 	
    	   	
    	private int xx = 0;
    	private int yy = 0;
    	
    	private int _dx = 0;
    	private int _dy = 0;
    	
		private static final long serialVersionUID = 3203545490664689791L;
		
		public DrawGraphs() 
		{
			setBackground(Color.white);
			
			setAlignmentY(CENTER_ALIGNMENT);
			setAlignmentX(CENTER_ALIGNMENT);
			
	        addMouseListener(new MouseAdapter() 
	        {
	            public void mousePressed(MouseEvent e) 
	            {
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });

	        addMouseMotionListener(new MouseAdapter() 
	        {
	            public void mouseDragged(MouseEvent e) 
	            {        	
	            	int ddx = (e.getX() - xx);
	            	int ddy = (e.getY() - yy);
	            	
	            	translate(ddx, ddy);
	            	repaint();
	            	
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });
	        
	        addMouseWheelListener(new MouseAdapter()
	        {
	            public void mouseWheelMoved(MouseWheelEvent e) 
	            {
		            int notches = e.getWheelRotation();
					zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
					
		            if (notches > 0) 
		            {
		            	if (zoomLevel < maxZoomLevel) 
		            	{
		            		zoomLevel += zoomMultiplicationFactor; 
			            	scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
		            	}
		            } else {
			           	if (zoomLevel > minZoomLevel) 
			           	{
			           		zoomLevel -= zoomMultiplicationFactor;
							scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
			            }
		            }

		            center_lines(true);
		            repaint();
	            }
	        });       
		};
		
		@Override
        protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);

	        Graphics2D gr = (Graphics2D) g;
	        gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	        
		    gr.setStroke(new BasicStroke(1.0f));	        
		    gr.setPaint(Color.blue);

	        AffineTransform at = new AffineTransform();
	        at.translate(dx, dy);
	        
	        zoom_level.setText("Zoom Level: " + sx);
			//double marker_radius = Double.parseDouble(maker_radius_tx.getText());
			        
			try 
			{				
			    if (geometry_wkt != null)
			    {		
			    	
			    	if(show_annotations)
			    	{
						annotations = Ant[UNIT];
						
						int m = annotations.length;
						start = new float[m];
						end = new float[m];
						int h = 0;
						
						for (String a : annotations) 
						{
							String[] E = a.split(",");
							
							for (String e : E) 
							{
								if(e.contains("start:"))
								{
									String[] r = e.split(":");
									start[h] = Float.valueOf(r[1]);
								}
								else if(e.contains("end:"))
								{
									String[] r = e.split(":");
									end[h] = Float.valueOf(r[1]);
								}
							}
							h++;
						}

				    	float t = Float.valueOf(GPOS) / (geoms[UNIT].length - 1);
				    	int p = 90;
				    	int step = 20;
				    	
			    	    double scale = Math.pow(10, 2);
			    	    float _t = (float) (Math.round(t * scale) / scale);
				    		    	    
				    	aprox_t_lb.setText("Approx. T: " + _t);
				    	
				    	for(int i = 0; i < start.length; i++)
				    	{
			    			if(_t >= start[i] && _t <= end[i])
				    		{
					   		    gr.setFont(new Font("Arial", Font.BOLD, 14));
								gr.setPaint(new Color(0.2f, 0.23f, 0.78f, 1.0f));
								gr.drawString(annotations[i], 20, p);
								
								p+= step;
				    		}
				    	}
			    	}
			    	
					n_units_lb.setText("Nº Units: " + N_UNITS);
					unit_lb.setText("Unit: " + (UNIT + 1));
			    	
			    	n_obs_lb.setText("Nº Samples: " + geoms[UNIT].length);
			    	
			    	geometry = reader.read(geometry_wkt);
			    	
					//int n_vertices = geometry.getNumPoints();
					//num_vertices_lb.setText("Nº Vertices: " + n_vertices);
					
					num_vertices_lb.setText("Nº Vertices: " + geometry.getNumPoints());
			    	
					AffineTransformation affine = new AffineTransformation();
					affine.scale(sx, -sy);
					geometry.apply(affine);;
			    	
			    	gr.setPaint(Color.black);
			    	gr.draw(new LiteShape(geometry, at, false));
			    	
   					if(fill.isSelected() && geometry.getGeometryType() != "LineString")
   					{
   						gr.setPaint(new Color(18, 21, 67));

		   		        for (int j = 0; j < geometry.getNumGeometries(); j++)
				   			gr.fill(new LiteShape(geometry.getGeometryN(j), at, false));
   					}
   					
		   		    gr.setFont(new Font("Arial", Font.BOLD, 14));
					gr.setPaint(new Color(0.75f, 0.2f, 0.2f, 1.0f));
					gr.drawString("Nº Invalid Geoms: " + String.valueOf(num_invalid_geoms), 20, 50);
					//gr.drawString("Nº complex Geoms: " + String.valueOf(num_complex_geoms), 20, 90);
					
					if(geoms_validity[UNIT])
						gr.setPaint(new Color(6, 40, 94));
					else
						gr.setPaint(new Color(166, 10, 83));
							
					gr.setFont(new Font("Arial", Font.BOLD, 14));
							
					if(geoms_validity[UNIT])
						gr.drawString("Valid Moving Region Unit!", 20, 30);
					else
						gr.drawString("Invalid Moving Region Unit!", 20, 30);
			    }
			    
			    /*
			    if(show_info_about_geometry_validity)
			    {
			    	if(interpolation_is_valid)
						gr.setPaint(new Color(6, 40, 94));
					else
						gr.setPaint(new Color(166, 10, 83));
					
					gr.setFont(new Font("Arial", Font.BOLD, 14));
					
					if(interpolation_is_valid)
						gr.drawString("Interpolation is Valid!", 20, 30);
					else
						gr.drawString("Interpolation is not Valid!", 20, 30);
			    }
			    */
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
			}
        };
        
        public void translate(int x, int y) 
        {
        	_dx += x;
        	_dy += y;
        	
        	dx += x;
        	dy += y;
        }
        
        public void scale(double x, double y) 
        {
        	sx += x;
        	sy += y;
        }
        
        public void center_lines(boolean translate)
        {
        	if(geometry_wkt == null)
        		return;
        	
			try
			{
			    Geometry g = reader.read(geometry_wkt);

			    // Apply scale factor.
			    
				AffineTransformation affine = new AffineTransformation();		
				affine.scale(sx, -sy);
				
				g.apply(affine);
			    
			    // Get centroid of the polygon envelope.
				
			    Point c = g.getEnvelope().getCentroid();
			
				cx = c.getX();
				cy = c.getY();
			} 
			catch (ParseException e) {
				e.printStackTrace();
			}

			w_center = (int) (viz_p.getWidth() / 2);
			h_center = (int) (viz_p.getHeight() / 2);
			
			if(!translate)
			{
	        	_dx = 0;
	        	_dy = 0;
			}
			
			dx = (int) ((-cx + w_center)) + _dx;
			dy = (int) ((-cy + h_center)) + _dy;
        }
    }
	
	public void draw_UI()
	{
		setTitle("Custom Dataset Viewer");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	    setSize(1296, 720);
	    setLocationRelativeTo(null);
	    setExtendedState(f.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	    
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		viz_p = new JPanel();
		viz_p.setLocation(175, 12);
		viz_p.setSize(930, 598);	
		viz_p.setBackground(Color.WHITE);
		viz_p.setBorder(new LineBorder(Color.black, 1));
		contentPane.add(viz_p);
		
		tools_p = new JPanel();
		tools_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		tools_p.setBounds(10, 622, 1260, 37);
		contentPane.add(tools_p);
		tools_p.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		//JTextArea textArea = new JTextArea();
		//tools_p.add(textArea);
		
		zoom_factor_tx = new JTextField();
		tools_p.add(zoom_factor_tx);
		zoom_factor_tx.setBackground(SystemColor.inactiveCaptionBorder);
		zoom_factor_tx.setForeground(SystemColor.desktop);
		zoom_factor_tx.setText("0.25");
		//zoom_factor_tEnabled(false);
		//save_interpolation_to_picture.setx.setText("0.25");
		zoom_factor_tx.setToolTipText("Zoom factor.");
		zoom_factor_tx.setColumns(3);
		
		gran_tx = new JTextField();
		gran_tx.setToolTipText("Zoom factor.");
		gran_tx.setText("6");
		gran_tx.setForeground(SystemColor.desktop);
		gran_tx.setColumns(3);
		gran_tx.setBackground(Color.LIGHT_GRAY);
		tools_p.add(gran_tx);
		
		save_seq_to_picture = new JButton("Save Sequence To PNG");
		tools_p.add(save_seq_to_picture);
		
		save_to_picture = new JButton("Save Current To PNG");
		tools_p.add(save_to_picture);
		
		png_filename_tx = new JTextField();
		tools_p.add(png_filename_tx);
		png_filename_tx.setText("/home/user/");
		png_filename_tx.setColumns(10);
		
		status = new JLabel("");
		tools_p.add(status);
		
		fill = new JCheckBox("Fill");
		fill.setBounds(111, 10, 56, 25);
		contentPane.add(fill);
		
		zoom_level = new JLabel("Zoom Level:");
		zoom_level.setForeground(new Color(0, 0, 128));
		zoom_level.setBounds(1115, 10, 155, 14);
		contentPane.add(zoom_level);
		zoom_slider = new JSlider();
		
		zoom_slider.setValue(1);
		zoom_slider.setMinimum((int) minZoomLevel);
		zoom_slider.setMaximum((int) maxZoomLevel);
		
		/*
		zoom_slider.setValue(100);
		zoom_slider.setMinimum((int) 25);
		zoom_slider.setMaximum((int) 60000);
		*/
		
		zoom_slider.setBounds(1117, 62, 155, 26);
		contentPane.add(zoom_slider);
	    
		/*
	    JLabel method_lbl = new JLabel("Primitive Events");
	    method_lbl.setEnabled(false);
	    method_lbl.setHorizontalAlignment(SwingConstants.LEFT);
	    method_lbl.setForeground(new Color(25, 25, 112));
	    method_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
	    method_lbl.setBounds(11, 43, 155, 16);
	    contentPane.add(method_lbl);
	    */
	    
	    load_ds_bt = new JButton("Load Dataset");
	    load_ds_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    load_ds_bt.setForeground(new Color(72, 61, 139));
	    load_ds_bt.setBounds(10, 95, 153, 25);
	    contentPane.add(load_ds_bt);
		
		clear_bt = new JButton("Clear");
		clear_bt.setForeground(SystemColor.desktop);
		clear_bt.setToolTipText("Clear data");
		clear_bt.setFont(new Font("Dialog", Font.BOLD, 11));
		clear_bt.setBounds(10, 494, 156, 23);
		contentPane.add(clear_bt);
		
		obs_slider = new JSlider();
		obs_slider.setValue(1);
		obs_slider.setMinimum(0);
		obs_slider.setMaximum(600);
		obs_slider.setBounds(1117, 117, 155, 26);
		contentPane.add(obs_slider);
		
		show_ant_bt = new JButton("Show Annotations");
		show_ant_bt.setForeground(SystemColor.desktop);
		show_ant_bt.setToolTipText("");
		show_ant_bt.setFont(new Font("Dialog", Font.BOLD, 11));
		show_ant_bt.setBounds(11, 521, 156, 25);
		contentPane.add(show_ant_bt);
		
		see_annotations_bt = new JButton("See Annotations");
		see_annotations_bt.setToolTipText("See Dataset Annotations");
		see_annotations_bt.setForeground(new Color(47, 79, 79));
		see_annotations_bt.setFont(new Font("Dialog", Font.BOLD, 11));
		see_annotations_bt.setBounds(11, 440, 156, 23);
		contentPane.add(see_annotations_bt);
		
		JLabel interpolation_slider_lb = new JLabel("Interpolation Slider");
		interpolation_slider_lb.setForeground(new Color(0, 0, 128));
		interpolation_slider_lb.setBounds(1117, 97, 155, 14);
		contentPane.add(interpolation_slider_lb);
		
		zoom_slider_lb = new JLabel("Zoom Slider");
		zoom_slider_lb.setForeground(SystemColor.activeCaption);
		zoom_slider_lb.setBounds(1117, 42, 155, 14);
		contentPane.add(zoom_slider_lb);
		
		n_units_lb = new JLabel("Nº Units:");
		n_units_lb.setForeground(SystemColor.activeCaption);
		n_units_lb.setBounds(1115, 187, 155, 14);
		contentPane.add(n_units_lb);
		
		unit_lb = new JLabel("Unit:");
		unit_lb.setForeground(SystemColor.activeCaption);
		unit_lb.setBounds(1115, 209, 155, 14);
		contentPane.add(unit_lb);
		
		num_vertices_lb = new JLabel("Nº Vertices: ");
		num_vertices_lb.setHorizontalAlignment(SwingConstants.LEFT);
		num_vertices_lb.setForeground(new Color(25, 25, 112));
		num_vertices_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		num_vertices_lb.setBounds(13, 153, 153, 16);
		contentPane.add(num_vertices_lb);
		
		n_obs_lb = new JLabel("Nº Samples: ");
		n_obs_lb.setToolTipText("Number of Observations.");
		n_obs_lb.setHorizontalAlignment(SwingConstants.LEFT);
		n_obs_lb.setForeground(new Color(25, 25, 112));
		n_obs_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		n_obs_lb.setBounds(13, 174, 154, 16);
		contentPane.add(n_obs_lb);
		
		aprox_t_lb = new JLabel("Approx. T: ");
		aprox_t_lb.setToolTipText("Approximate time.");
		aprox_t_lb.setHorizontalAlignment(SwingConstants.LEFT);
		aprox_t_lb.setForeground(new Color(25, 25, 112));
		aprox_t_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		aprox_t_lb.setBounds(13, 195, 154, 16);
		contentPane.add(aprox_t_lb);
	}

	public void add_listeners()
	{
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                setVisible(false);
                dispose();
            }
        });
        
		fill.addItemListener(new ItemListener() 
		{
			@Override 
			public void itemStateChanged(ItemEvent e) 
			{
				jp.repaint();
			}
		});
        
		// Zoom out.
		/*
    	minus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

				zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
				
	           	if (zoomLevel > minZoomLevel) 
	           	{
	           		zoomLevel -= zoomMultiplicationFactor;
					c.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
					c.translate(false);
	            }

	            jp.repaint();
			}
		});
    	*/
        
    	// Zoom in.
    	/*
    	plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

				zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
				
            	if (zoomLevel < maxZoomLevel) 
            	{
            		zoomLevel += zoomMultiplicationFactor; 
	            	c.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
	            	c.translate(true);
            	}

	            jp.repaint();

			}
		});
		*/
		
		show_ant_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if(show_annotations)
					show_annotations = false;
				else
					show_annotations = true;

				jp.repaint();
			}
		});
		
		see_annotations_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (Ant != null)
				{
					annotations = Ant[UNIT];
					
					String out_str = "<html><body>";
					
					for (String a : annotations) {
						out_str += "<p>" + a + "<br><br>";
					}
					
					out_str += "</body></html>";
		                
					// change to alter the width 
		            int w = 200;
					
		            JOptionPane.showMessageDialog(null, String.format(out_str, w, w), "Annotations In Unit " + (UNIT + 1), JOptionPane.INFORMATION_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null, "[]", "Annotations", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		save_seq_to_picture.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if(geoms == null)
				{
					JOptionPane.showMessageDialog(null, "Nothing to save!", "Save Sequence", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
					
				
				int show_granularity = Integer.valueOf(gran_tx.getText());
				
				String temp = geometry_wkt;
				String[] arr = null;
				int N = 0;
				int j = 0;
				
				try
				{
					for(int h = 0; h < geoms.length; h++)
					{
						arr = geoms[h];
						N = arr.length;
						
						for(int i = 0; i < N; i = i + show_granularity) 
						{
							geometry_wkt = arr[i];
							jp.repaint();
				    		
							BufferedImage bi = ScreenImage.createImage(viz_p);
		    				ScreenImage.writeImage(bi, png_filename_tx.getText() + "_" + (N + j) + ".png");
		    				j++;
						}
					}
    			} 
				catch (IOException ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Save Sequence ERR", JOptionPane.INFORMATION_MESSAGE);
					return;
    			}
				
				geometry_wkt = temp;
				jp.repaint();
				JOptionPane.showMessageDialog(null, "Sequence Saved!", "Save Sequence", JOptionPane.INFORMATION_MESSAGE);	
			}
		});
		
		save_to_picture.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				String filename = png_filename_tx.getText() + "_" + 1 + ".png";
				
				try
				{
					BufferedImage bi = ScreenImage.createImage(viz_p);
	    			ScreenImage.writeImage(bi, filename);
    			} 
				catch (IOException ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Save Current PNG ERROR", JOptionPane.INFORMATION_MESSAGE);
					jp.repaint();
					
					return;
    			}
				
				jp.repaint();
				JOptionPane.showMessageDialog(null, "Saved to: " + filename + "!", "Save", JOptionPane.INFORMATION_MESSAGE);	
			}
		});
		
		/*
		geometry_valid_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				int n_invalid_geoms = 0;
				
				if(arr != null)
				{
					for(String wkt : arr)
					{
						try {
							geometry = reader.read(wkt);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						if(!geometry.isValid())
							n_invalid_geoms ++;
					}
					
					if(n_invalid_geoms > 0)
						interpolation_is_valid = false;
					else
						interpolation_is_valid = true;
					
					show_info_about_geometry_validity = true;	
				}
				
			    jp.repaint();
			}
		});
		*/
		
		load_ds_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{	
				geometry_wkt = null;
				String[] arr = null;
				
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				
				int result = fileChooser.showOpenDialog(f);
					
				if (result == JFileChooser.APPROVE_OPTION) 
				{
					
				    File ds_file = fileChooser.getSelectedFile();
				    
					BufferedReader buf_reader = null;
					
					String line = "";
					//int n = 0;

					//boolean err = false;
					
					int k = 0;
					int n_geoms = 0;
					//int n_ant = 0;
					
					n_invalid_geoms = null;
					n_complex_geoms = null;
					geoms_validity =  null;
					
					Ant = null;
					geoms = null;
					
					N_GEOMS = 0;
					N_UNITS = 0;
					UNIT = 0;
					
					//String err_ssg = "";
					
					try 
					{
						buf_reader = new BufferedReader(new FileReader(ds_file));
						line = buf_reader.readLine();
						
						int i = 0;
						int j = 0;
						
						while (line != null) 
						{
							if(i == 0)
							{
								N_UNITS = Integer.valueOf(line);

								if(N_UNITS > 0)
								{
									n_invalid_geoms = new int[N_UNITS];
									n_complex_geoms = new int[N_UNITS];
									geoms_validity = new boolean[N_UNITS];
									
									Ant = new String[N_UNITS][];
									geoms = new String[N_UNITS][];
								}
								/*
								else
									err = true;
								*/
							}
							else if(i == 1)
							{
								//if(! err)
								//{
								String[] A = line.split(",");
								
								//System.out.println(line);
								
								
								n_invalid_geoms[k] = Integer.valueOf(A[0]);
								n_complex_geoms[k] = Integer.valueOf(A[1]);
									
								n_geoms = Integer.valueOf(A[2]);
								//n_ant = Integer.valueOf(A[3]);
									
								arr = new String[n_geoms];
								//}
							}
							else if(i == 2)
							{
								//if(!err)
								//{
								Ant[k] = line.split(";");
								//}
							}
							else //if(i == 3)
							{
								//if(!err)
								//{
								arr[j] = line;
								j++;
								n_geoms--;
									
								if(n_geoms == 0)
								{
									geoms[k] = arr;
										
									int n_inv_geoms = 0;
									
									for(String wkt : arr)
									{
										//try {
										geometry = reader.read(wkt);
										//} catch (ParseException e) {
											//e.printStackTrace();
										//}
										
										if(!geometry.isValid())
											n_inv_geoms++;
									}
										
									if(n_inv_geoms > 0)
										geoms_validity[k] = false;
									else
										geoms_validity[k] = true;

									i = 0;
									j = 0;
									k++;
								}
								//}
								/*
								else
									err_ssg = line;
								*/
							}
							
							line = buf_reader.readLine();
							i++;
						}
						
						geometry_wkt = geoms[0][0];
						UNIT = 0;
						
						for(int p = 0; p < geoms.length; p++)
							N_GEOMS += geoms[p].length;
						
						obs_slider.setMaximum(N_GEOMS - 1);
						obs_slider.setValue(0);
						
						//System.out.println(N_GEOMS - 1);
						
						n_units_lb.setText("Nº Units: " + N_UNITS);
						unit_lb.setText("Unit: " + (UNIT + 1));
						
						DrawGraphs g = (DrawGraphs) jp;
						g.center_lines(true);
						center_geoms = false;
						
						buf_reader.close();
					} catch (FileNotFoundException e) {
						JOptionPane.showMessageDialog(null, e.getMessage(), "Load Dataset ERROR", JOptionPane.INFORMATION_MESSAGE);
						jp.repaint();
						
						return;
						//e.printStackTrace();
					} catch (IOException e) {
						JOptionPane.showMessageDialog(null, e.getMessage(), "Load Dataset ERROR", JOptionPane.INFORMATION_MESSAGE);
						jp.repaint();
						
						return;
						//e.printStackTrace();
					}
					catch (ParseException e) {
						JOptionPane.showMessageDialog(null, e.getMessage(), "Load Dataset ERROR", JOptionPane.INFORMATION_MESSAGE);
						jp.repaint();
						
						return;
						//e.printStackTrace();
					}
				}
				
				jp.repaint();
			}
		});
		
		obs_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		    	if(geoms == null)
		    	{
		    		return;
		    	}
		    	
		        JSlider s = (JSlider) e.getSource();
		        int i = (int) s.getValue();

		        if(i < N_GEOMS)
		        {
		        	int ini = 0;
		        	int curr = 0;
		        	
		        	curr = geoms[0].length;
		        	
		        	for(int j = 0; j < geoms.length; j++)
		        	{		        		
		        		//System.out.println(i + ", " + curr + ", " + ini + ", " + UNIT + ", " + GPOS);
		        		
		        		if (i < curr)
		        		{
		        			UNIT = j;
		        			GPOS = i - ini;
		        			geometry_wkt = geoms[UNIT][GPOS];
		        			break;
		        		}
		        		else
		        		{
		        			ini += geoms[j].length;
		        			curr += geoms[j].length;
		        		}
		        		
		        		//System.out.println(UNIT + ", " + GPOS);
		        	}
		        }
		        
				DrawGraphs g = (DrawGraphs) jp;
				
				if(center_geoms)
					g.center_lines(true);
				
		    	jp.repaint();
		    }
		});
		
		zoom_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		        JSlider s = (JSlider) e.getSource();
		        int desired_zoom_level = (int) s.getValue();
		        
		        DrawGraphs c = (DrawGraphs) jp;
		        double diff = desired_zoom_level - zoomLevel;
		        		        
		        if(diff > 0 && desired_zoom_level < maxZoomLevel)
		        {
		        	zoomLevel = desired_zoom_level;
            		c.scale(diff, diff);
		        }
		        else if(diff < 0 && desired_zoom_level > minZoomLevel)
		        {
		        	zoomLevel = desired_zoom_level;
            		c.scale(diff, diff);
		        }
				
		        c.center_lines(true);
		    	jp.repaint(); 
		    }
		});
		
		clear_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
		    {
				interpolation_is_valid = false;
				show_info_about_geometry_validity = false;
				
				geometry_wkt = null;
				//arr = null;
				
				N_UNITS = 0;
				UNIT = 0;
				
				n_invalid_geoms = null;
				n_complex_geoms = null;
				geoms_validity =  null;
				
				num_vertices_lb.setText("Nº Vertices: ");
				n_obs_lb.setText("Nº Samples: ");
				aprox_t_lb.setText("Approx. T: ");
				
				GPOS = 0;
				
				Ant = null;
				geoms = null;
				N_GEOMS = 0;
				
				start = null;
				end = null;
				
				n_units_lb.setText("Nº Units: ");
				unit_lb.setText("Unit: ");
		    }
		});
	}
}