package generators;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.LiteShape;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.util.AffineTransformation;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;

import tools.ScreenImage;

/*
 * Authors: José Duarte 	(hfduarte@ua.pt)
 * 			Mark Mckenney 	(marmcke@siue.edu)
 * 
 * Version: 1.0 (2020-05-29)
 * 
 */

public class PrimitiveEventsGenerator extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel viz_p;
	private JFrame f;
	private JPanel jp;
	private JLabel status;
	private JPanel tools_p;
	private JLabel zoom_level;
	private JComboBox<String> events_cb;
	
	private int num_invalid_geoms;
	
	public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 0.25;
	public static final double DEFAULT_MIN_ZOOM_LEVEL = 0.25;
	public static final double DEFAULT_MAX_ZOOM_LEVEL = 600;
	
    private double zoomLevel = 1.0;
    private double minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
    private double maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
    private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;
    private String event;
    private GeometryFactory geometryFactory;
    private WKTReader reader; 
    private int w_center;
	private int h_center;
	private double cx = 0;
	private double cy = 0;
    private JCheckBox fill;
    private JSlider zoom_slider;
    private JButton wkt_save_bt;
    private JButton generate_bt;
    private JTextField wkt_save_in_file;
    private Geometry geometry;
    private String geometry_wkt;
    private JCheckBox num_vertices_cb;
    private boolean show_info_about_geometry_validity;
    private JButton save_to_picture;   
	private JLabel num_vertices_lb;
	private JLabel num_holes_lb;
	private JCheckBox num_holes_cb;
	private JButton clear_bt;
	private boolean center_geoms;
	private String[] annotations;
    
    private JTextField zoom_factor_tx;
    private JTextField num_vertices_tx;
    private JTextField num_holes_tx;
    private boolean interpolation_is_valid;
    private JTextField png_filename_tx;
    private JSlider obs_slider;
    private String[] arr;
    private JTextField min_len_tx;
    private JTextField max_len_tx;
    private JTextField n_samples_tx;
    private JTextField ds_tx;
    private JTextField dy_tx;
    private JTextField rot_tx;
	private JCheckBox change_top_cb;
	private JButton ds_save_bt;
	private JButton see_annotations_bt;
	private JLabel zoom_slider_lb;
	private JLabel rot_lb;
	private JLabel nsamples_lb;
    
	public PrimitiveEventsGenerator() 
	{
		f = this;
		event = "";
		interpolation_is_valid = false;
		
		annotations = null;
		arr = null;
		
		center_geoms = false;
		
		geometry = null;
		geometry_wkt = null;
		
	    geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
	    reader = new WKTReader(geometryFactory);
		
		num_invalid_geoms = 0;
	    
		draw_UI();
		
		w_center = (int) (viz_p.getWidth() / 2);
		h_center = (int) (viz_p.getHeight() / 2);
		
	    show_info_about_geometry_validity = false;
	    
		add_listeners();
		draw_geometry();
	}
	
	public void paint(Graphics g) 
	{
	    super.paint(g);
	    jp.repaint();
	}

	public void draw_geometry()
	{
		viz_p.setLayout(new BorderLayout());
		jp = new DrawGraphs();
		viz_p.add(jp, BorderLayout.CENTER);
	}
	
    private class DrawGraphs extends JPanel
    {
    	private int dx = 0;
    	private int dy = 0;
    	
    	private double sx = 1;
    	private double sy = 1; 	
    	   	
    	private int xx = 0;
    	private int yy = 0;
    	
    	private int _dx = 0;
    	private int _dy = 0;
    	
		private static final long serialVersionUID = 3203545490664689791L;
		
		public DrawGraphs() 
		{
			setBackground(Color.white);
			
			setAlignmentY(CENTER_ALIGNMENT);
			setAlignmentX(CENTER_ALIGNMENT);
			
	        addMouseListener(new MouseAdapter() 
	        {
	            public void mousePressed(MouseEvent e) 
	            {
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });

	        addMouseMotionListener(new MouseAdapter() 
	        {
	            public void mouseDragged(MouseEvent e) 
	            {        	
	            	int ddx = (e.getX() - xx);
	            	int ddy = (e.getY() - yy);
	            	
	            	translate(ddx, ddy);
	            	repaint();
	            	
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });
	        
	        addMouseWheelListener(new MouseAdapter()
	        {
	            public void mouseWheelMoved(MouseWheelEvent e) 
	            {
		            int notches = e.getWheelRotation();
					zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
					
		            if (notches > 0) 
		            {
		            	if (zoomLevel < maxZoomLevel) 
		            	{
		            		zoomLevel += zoomMultiplicationFactor; 
			            	scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
		            	}
		            } else {
			           	if (zoomLevel > minZoomLevel) 
			           	{
			           		zoomLevel -= zoomMultiplicationFactor;
							scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
			            }
		            }

		            center_lines(true);
		            repaint();
	            }
	        });       
		};
		
		@Override
        protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);

	        Graphics2D gr = (Graphics2D) g;
	        gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	        
		    gr.setStroke(new BasicStroke(1.0f));	        
		    gr.setPaint(Color.blue);

	        AffineTransform at = new AffineTransform();
	        at.translate(dx, dy);
	        
	        zoom_level.setText("Zoom Level: " + sx);
   
			try 
			{				
			    if (geometry_wkt != null)
			    {		
			    	geometry = reader.read(geometry_wkt);
			    	
					AffineTransformation affine = new AffineTransformation();
					affine.scale(sx, -sy);
					geometry.apply(affine);;
			    	
			    	gr.setPaint(Color.black);
			    	gr.draw(new LiteShape(geometry, at, false));
			    	
   					if(fill.isSelected() && geometry.getGeometryType() != "LineString")
   					{
   						gr.setPaint(new Color(18, 21, 67));

		   		        for (int j = 0; j < geometry.getNumGeometries(); j++)
				   			gr.fill(new LiteShape(geometry.getGeometryN(j), at, false));
   					}
   					
		   		    gr.setFont(new Font("Arial", Font.BOLD, 14));
					gr.setPaint(new Color(0.2f, 0.2f, 0.2f, 1.0f));
					gr.drawString("Nº Invalid Geometries: " + String.valueOf(num_invalid_geoms), 20, 50);

		   		    gr.setFont(new Font("Arial", Font.BOLD, 14));
					gr.setPaint(new Color(0.6f, 0.4f, 0.9f, 1.0f));
					gr.drawString("Event: " + event, 20, 90);
			    }
			    
			    if(show_info_about_geometry_validity)
			    {
			    	if(interpolation_is_valid)
						gr.setPaint(new Color(6, 40, 94));
					else
						gr.setPaint(new Color(166, 10, 83));
					
					gr.setFont(new Font("Arial", Font.BOLD, 14));
					
					if(interpolation_is_valid)
						gr.drawString("Valid Moving Region!", 20, 30);
					else
						gr.drawString("Invalid Moving Region!", 20, 30);
			    }
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
			}
        };
        
        public void translate(int x, int y) 
        {
        	_dx += x;
        	_dy += y;
        	
        	dx += x;
        	dy += y;
        }
        
        public void scale(double x, double y) 
        {
        	sx += x;
        	sy += y;
        }
        
        public void center_lines(boolean translate)
        {
        	if(geometry_wkt == null)
        		return;
        	
			try
			{
			    Geometry g = reader.read(geometry_wkt);

			    // Apply scale factor.
			    
				AffineTransformation affine = new AffineTransformation();		
				affine.scale(sx, -sy);
				
				g.apply(affine);
			    
			    // Get centroid of the polygon envelope.
				
			    Point c = g.getEnvelope().getCentroid();
			
				cx = c.getX();
				cy = c.getY();
			} 
			catch (ParseException e) {
				e.printStackTrace();
			}

			w_center = (int) (viz_p.getWidth() / 2);
			h_center = (int) (viz_p.getHeight() / 2);
			
			if(!translate)
			{
	        	_dx = 0;
	        	_dy = 0;
			}
			
			dx = (int) ((-cx + w_center)) + _dx;
			dy = (int) ((-cy + h_center)) + _dy;
        }
    }
	
	public void draw_UI()
	{
		setTitle("Primitive Events Generator");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	    setSize(1296, 720);
	    setLocationRelativeTo(null);
	    setExtendedState(f.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	    
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		viz_p = new JPanel();
		viz_p.setLocation(175, 10);
		viz_p.setSize(930, 590);	
		viz_p.setBackground(Color.WHITE);
		viz_p.setBorder(new LineBorder(Color.black, 1));
		contentPane.add(viz_p);
		
		tools_p = new JPanel();
		tools_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		tools_p.setBounds(10, 616, 1260, 36);
		contentPane.add(tools_p);
		
		zoom_factor_tx = new JTextField();
		zoom_factor_tx.setBackground(SystemColor.inactiveCaptionBorder);
		zoom_factor_tx.setForeground(SystemColor.desktop);
		zoom_factor_tx.setText("0.25");
		zoom_factor_tx.setToolTipText("Zoom factor.");
		tools_p.add(zoom_factor_tx);
		zoom_factor_tx.setColumns(3);
		
		save_to_picture = new JButton("Save Current To PNG");
		tools_p.add(save_to_picture);
		
		png_filename_tx = new JTextField();
		png_filename_tx.setText("/home/user/");
		tools_p.add(png_filename_tx);
		png_filename_tx.setColumns(10);
		
		status = new JLabel("");
		tools_p.add(status);
		
		fill = new JCheckBox("Fill");
		fill.setBounds(111, 10, 56, 25);
		contentPane.add(fill);
		
		zoom_level = new JLabel("Zoom Level:");
		zoom_level.setForeground(new Color(0, 0, 128));
		zoom_level.setBounds(1115, 10, 155, 14);
		contentPane.add(zoom_level);
		zoom_slider = new JSlider();
		
		zoom_slider.setValue(1);
		zoom_slider.setMinimum((int) minZoomLevel);
		zoom_slider.setMaximum((int) maxZoomLevel);
		
		zoom_slider.setBounds(1117, 62, 155, 26);
		contentPane.add(zoom_slider);
	    
	    wkt_save_in_file = new JTextField();
	    wkt_save_in_file.setToolTipText("Filename in HD to store Data.");
	    wkt_save_in_file.setForeground(new Color(25, 25, 112));
	    wkt_save_in_file.setBackground(new Color(240, 255, 240));
	    wkt_save_in_file.setText("/home/user/wkt.txt");
	    wkt_save_in_file.setBounds(11, 580, 157, 20);
	    contentPane.add(wkt_save_in_file);
	    wkt_save_in_file.setColumns(1);
	    
	    wkt_save_bt = new JButton("Save WKT");
	    wkt_save_bt.setForeground(new Color(47, 79, 79));
	    wkt_save_bt.setToolTipText("Save Observations WKT to file.");
	    wkt_save_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    wkt_save_bt.setBounds(10, 551, 156, 25);
	    contentPane.add(wkt_save_bt);
	    
	    events_cb = new JComboBox<>();
	    events_cb.setToolTipText("Metrics");
	    events_cb.setModel(new DefaultComboBoxModel(new String[] {"Non-Continuous Shrink", "Continuous Shrink", "Disappear to line", "Disappear to Seg", "Hole from Open Line", "Hole from Open Line 2", "Hole Consume Face", "Burst", "Hole Split Face", "Hole Split Face in N Faces", "Burst 2", "Grow", "Evolve", "Face Internal Split from cCosed Line", "Split (Fragment)", "Split (Fragment) 2", "Split (Fragment) 3", "Split to Point", "Split to Point 2", "Hole Appear and Split", "Hole Disappear to Open Line", "Face Split Hole", "Face Split Hole and Consumes It", "Face Internal Merge", "Hole Consume", "Internal Face Split", "Remove Vertices", "Engulf", "Hole Consume Face (Non-Deterministic)"}));
	    events_cb.setBounds(11, 66, 151, 22);
	    contentPane.add(events_cb);
	    
	    JLabel events_lbl = new JLabel("Primitive Events");
	    events_lbl.setHorizontalAlignment(SwingConstants.LEFT);
	    events_lbl.setForeground(new Color(25, 25, 112));
	    events_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
	    events_lbl.setBounds(11, 43, 155, 16);
	    contentPane.add(events_lbl);
	    
	    generate_bt = new JButton("Generate");
	    generate_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    generate_bt.setForeground(new Color(72, 61, 139));
	    generate_bt.setBounds(10, 95, 153, 25);
	    contentPane.add(generate_bt);
	    
	    num_vertices_cb = new JCheckBox("Random");
	    num_vertices_cb.setEnabled(false);
	    num_vertices_cb.setSelected(true);
	    num_vertices_cb.setBounds(84, 159, 82, 23);
	    contentPane.add(num_vertices_cb);
			
		num_vertices_lb = new JLabel("Nº Vertices");
		num_vertices_lb.setHorizontalAlignment(SwingConstants.LEFT);
		num_vertices_lb.setForeground(new Color(25, 25, 112));
		num_vertices_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		num_vertices_lb.setBounds(13, 136, 153, 16);
		contentPane.add(num_vertices_lb);
		
		num_vertices_tx = new JTextField();
		num_vertices_tx.setBounds(10, 161, 66, 19);
		num_vertices_tx.setText("50");
		contentPane.add(num_vertices_tx);
		num_vertices_tx.setColumns(10);
		
		num_holes_lb = new JLabel("Nº Holes");
		num_holes_lb.setHorizontalAlignment(SwingConstants.LEFT);
		num_holes_lb.setForeground(new Color(25, 25, 112));
		num_holes_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		num_holes_lb.setBounds(13, 186, 153, 16);
		contentPane.add(num_holes_lb);
		
		num_holes_tx = new JTextField();
		num_holes_tx.setEnabled(false);
		num_holes_tx.setColumns(10);
		num_holes_tx.setBounds(10, 211, 66, 19);
		num_holes_tx.setText("4");
		contentPane.add(num_holes_tx);
		
		num_holes_cb = new JCheckBox("Random");
		num_holes_cb.setEnabled(false);
		num_holes_cb.setSelected(true);
		num_holes_cb.setBounds(85, 209, 82, 23);
		contentPane.add(num_holes_cb);
		
		clear_bt = new JButton("Clear");
		clear_bt.setForeground(SystemColor.desktop);
		clear_bt.setToolTipText("Clear data");
		clear_bt.setFont(new Font("Dialog", Font.BOLD, 11));
		clear_bt.setBounds(10, 494, 156, 23);
		contentPane.add(clear_bt);
		
		obs_slider = new JSlider();
		obs_slider.setValue(1);
		obs_slider.setMinimum(0);
		obs_slider.setMaximum(600);
		obs_slider.setBounds(1117, 117, 155, 26);
		contentPane.add(obs_slider);
		
		min_len_tx = new JTextField();
		min_len_tx.setToolTipText("Min");
		min_len_tx.setText("100");
		min_len_tx.setColumns(10);
		min_len_tx.setBounds(11, 265, 66, 19);
		contentPane.add(min_len_tx);
		
		max_len_tx = new JTextField();
		max_len_tx.setToolTipText("Max");
		max_len_tx.setText("300");
		max_len_tx.setColumns(10);
		max_len_tx.setBounds(101, 265, 66, 19);
		contentPane.add(max_len_tx);
		
		n_samples_tx = new JTextField();
		n_samples_tx.setToolTipText("Nº Observations");
		n_samples_tx.setText("100");
		n_samples_tx.setColumns(10);
		n_samples_tx.setBounds(10, 408, 66, 19);
		contentPane.add(n_samples_tx);
		
		ds_tx = new JTextField();
		ds_tx.setToolTipText("Dx");
		ds_tx.setText("0");
		ds_tx.setColumns(10);
		ds_tx.setBounds(10, 318, 66, 19);
		contentPane.add(ds_tx);
		
		dy_tx = new JTextField();
		dy_tx.setToolTipText("Dy");
		dy_tx.setText("0");
		dy_tx.setColumns(10);
		dy_tx.setBounds(101, 318, 66, 19);
		contentPane.add(dy_tx);
		
		JLabel tras_lb = new JLabel("X-Y Translation");
		tras_lb.setHorizontalAlignment(SwingConstants.LEFT);
		tras_lb.setForeground(new Color(25, 25, 112));
		tras_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		tras_lb.setBounds(10, 292, 153, 16);
		contentPane.add(tras_lb);
		
		rot_tx = new JTextField();
		rot_tx.setToolTipText("Rotation (Degrees)");
		rot_tx.setText("0");
		rot_tx.setColumns(10);
		rot_tx.setBounds(10, 385, 66, 19);
		contentPane.add(rot_tx);
		
		JLabel tras_lb_1 = new JLabel("Seg. Min-Max Length");
		tras_lb_1.setHorizontalAlignment(SwingConstants.LEFT);
		tras_lb_1.setForeground(new Color(25, 25, 112));
		tras_lb_1.setFont(new Font("Dialog", Font.BOLD, 12));
		tras_lb_1.setBounds(12, 237, 153, 16);
		contentPane.add(tras_lb_1);
		
		change_top_cb = new JCheckBox("Change Topology");
		change_top_cb.setForeground(SystemColor.activeCaption);
		change_top_cb.setBounds(6, 340, 157, 23);
		contentPane.add(change_top_cb);
		
		ds_save_bt = new JButton("Save Dataset");
		ds_save_bt.setForeground(SystemColor.desktop);
		ds_save_bt.setToolTipText("Save Dataset to file.");
		ds_save_bt.setFont(new Font("Dialog", Font.BOLD, 11));
		ds_save_bt.setBounds(11, 521, 156, 25);
		contentPane.add(ds_save_bt);
		
		see_annotations_bt = new JButton("See Annotations");
		see_annotations_bt.setToolTipText("See Dataset Annotations");
		see_annotations_bt.setForeground(new Color(47, 79, 79));
		see_annotations_bt.setFont(new Font("Dialog", Font.BOLD, 11));
		see_annotations_bt.setBounds(11, 440, 156, 23);
		contentPane.add(see_annotations_bt);
		
		JLabel interpolation_slider_lb = new JLabel("Interpolation Slider");
		interpolation_slider_lb.setForeground(new Color(0, 0, 128));
		interpolation_slider_lb.setBounds(1117, 97, 155, 14);
		contentPane.add(interpolation_slider_lb);
		
		zoom_slider_lb = new JLabel("Zoom Slider");
		zoom_slider_lb.setForeground(SystemColor.activeCaption);
		zoom_slider_lb.setBounds(1117, 42, 155, 14);
		contentPane.add(zoom_slider_lb);
		
		rot_lb = new JLabel("Rotation");
		rot_lb.setToolTipText("Rotation in Degrees.");
		rot_lb.setHorizontalAlignment(SwingConstants.LEFT);
		rot_lb.setForeground(new Color(25, 25, 112));
		rot_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		rot_lb.setBounds(84, 386, 83, 16);
		contentPane.add(rot_lb);
		
		nsamples_lb = new JLabel("Nº Samples");
		nsamples_lb.setToolTipText("Number of Observations.");
		nsamples_lb.setHorizontalAlignment(SwingConstants.LEFT);
		nsamples_lb.setForeground(new Color(25, 25, 112));
		nsamples_lb.setFont(new Font("Dialog", Font.BOLD, 12));
		nsamples_lb.setBounds(84, 410, 83, 16);
		contentPane.add(nsamples_lb);
	}

	public void add_listeners()
	{
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                setVisible(false);
                dispose();
            }
        });
        
		fill.addItemListener(new ItemListener() 
		{
			@Override 
			public void itemStateChanged(ItemEvent e) 
			{
				jp.repaint();
			}
		});
        		
		ds_save_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (annotations != null && arr != null)
				{
					String filename = wkt_save_in_file.getText();
					PrintWriter writer = null;
					
					try {
						writer = new PrintWriter(filename, "UTF-8");
					} catch (FileNotFoundException | UnsupportedEncodingException e) 
					{
						e.printStackTrace();
					}
					
					// Write to File >>>
					
					writer.println(annotations.length + ":" + arr.length);
					
					for (String a : annotations)
						writer.println(a);
										
			    	for(int i = 0; i < arr.length; i++) 
			    		writer.println(arr[i]);
					
					// Write to File >>>
					
					writer.close();
					JOptionPane.showMessageDialog(null, "Saved to " + filename + "!", "Save to dataset.", JOptionPane.INFORMATION_MESSAGE);	

					// >>>					
				}
				else
					JOptionPane.showMessageDialog(null, "No data to save!", "Save Dataset.", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		see_annotations_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (annotations != null)
				{
					String out_str = "<html><body>";
					
					for (String a : annotations) {
						out_str += "<p>" + a + "<br><br>";
					}
					
					out_str += "</body></html>";
		                
					// change to alter the width 
		            int w = 200;
					
		            JOptionPane.showMessageDialog(null, String.format(out_str, w, w), "Dataset Annotations", JOptionPane.INFORMATION_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null, "[]", "Dataset Annotations", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		save_to_picture.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				String filename = png_filename_tx.getText() + "_" + 1 + ".png";
				
				try
				{
					BufferedImage bi = ScreenImage.createImage(viz_p);
	    			ScreenImage.writeImage(bi, filename);
    			} 
				catch (IOException ex) {
    				ex.printStackTrace();
    			}
				
				jp.repaint();
				JOptionPane.showMessageDialog(null, "Saved to: " + filename + "!", "Save", JOptionPane.INFORMATION_MESSAGE);	
			}
		});
		
		generate_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{				
				geometry_wkt = null;
				arr = null;
				
				// Call python script.
				
				String[] cmd;
				cmd = new String[11];
					
				for(int j = 0; j < cmd.length; j++)
					cmd[j] = "";
				
				int op = events_cb.getSelectedIndex();			
				
				cmd[0] = "python";
				cmd[1] = "/home/user/generator/src/PrimitiveEventsGenerator.py";

				cmd[2] = num_vertices_tx.getText();
				cmd[3] = min_len_tx.getText();
				cmd[4] = max_len_tx.getText();
				cmd[5] = n_samples_tx.getText();
				
				cmd[6] = ds_tx.getText();
				cmd[7] = dy_tx.getText();
				
				cmd[8] = rot_tx.getText();
				
				if(change_top_cb.isSelected())
					cmd[9] = "1";
				else
					cmd[9] = "0";
				
				cmd[10] = String.valueOf(op + 1);
				
				switch(op + 1)
				{
					case 1:
						event = "Non-Continuous Shrink";
						break;
					case 2:
						event = "Continuous Shrink";
						break;
					case 3:
						event = "Face Disappear to line";
						break;
					case 4:
						event = "Face Disappear to Line Segment";
						break;
					case 5:
						event = "Hole Appear from Open Line (Fissure), Remove/Insert Vertices";
						break;
					case 6:
						event = "Hole Appear from Open Line (Fissure), Insert Vertices";
						break;
					case 7:
						event = "Hole consume Face";
						break;
					case 8:
						event = "Burst";
						break;
					case 9:
						event = "Hole split face";
						break;
					case 10:
						event = "Hole split face in 2+ faces";
						break;
					case 11:
						event = "Burst";
						break;
					case 12:
						event = "Face Grow";
						break;
					case 13:
						event = "Face Evolve";
						break;
					case 14:
						event = "Face Internal Split from Closed Line";
						break;
					case 15:
						event = "Face Split from Open Line";
						break;
					case 16:
						event = "Face Split from Open Line 2 Cuts";
						break;
					case 17:
						event = "Face Split from Open Line 2 Simultaneous Cuts";
						break;
					case 18:
						event = "Face Split at a Point (Stay Connected)";
						break;
					case 19:
						event = "Face Split at a Point (Move Away)";
						break;
					case 20:
						event = "Hole Appear and Split at a Point (Move Away)";
						break;
					case 21:
						event = "Hole Disappear to open Line";
						break;
					case 22:
						event = "Face Split Hole (Face Internal Merge)";
						break;
					case 23:
						event = "Face Split and Consume Hole";
						break;
					case 24:
						event = "Face Internal Merge";
						break;
					case 25:
						event = "Face Consume Hole";
						break;
					case 26:
						event = "Face Internal Merge 2";
						break;
					case 27:
						event = "Remove Vertices";
						break;
					case 28:
						event = "Engulf";
						break;
					case 29:
						event = "Hole Consume Face (Non-Deterministic)";
						break;
				}
				
				Runtime rt = Runtime.getRuntime();
				Process pr = null;
				
				for(int i = 0; i < cmd.length; i++)
					System.out.print(cmd[i] + " ");
				
				System.out.println();

				try
				{
					pr = rt.exec(cmd);
				}
				catch (IOException e1) 
				{
					e1.printStackTrace();
				}
				
				BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				String line = "";
				int n = 0;
				
				try
				{
					int i = 0;
					int j = 0;
					
					while((line = bfr.readLine()) != null)
					{
						if(i == 0)
						{
							String A = line;
							annotations = A.split(";");
						}
						else if(i == 1)
						{
							n = Integer.valueOf(line);
							arr = new String[n];
						}
						else if(i == 2)
						{
							num_invalid_geoms = Integer.valueOf(line);
						}
						else if(i == 3)
						{
							//num_complex_geoms = Integer.valueOf(line);
						}
						else
						{
							arr[j] = line;
							j++;
						}
						
						i++;
					}
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}
				
				if(n == 1) 
				{
					String err_ssg = arr[0];
					JOptionPane.showMessageDialog(null, err_ssg, "Err Message", JOptionPane.INFORMATION_MESSAGE);	
					arr = null;
				}
				else
				{
					num_invalid_geoms = 0;
					
					for(String wkt : arr)
					{
						try {
							geometry = reader.read(wkt);
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
						if(!geometry.isValid())
							num_invalid_geoms++;
					}
					
					if(num_invalid_geoms > 0)
						interpolation_is_valid = false;
					else
						interpolation_is_valid = true;
					
					show_info_about_geometry_validity = true;	
					
					// >>>

					geometry_wkt = arr[0];
					
					obs_slider.setMaximum(arr.length - 1);
					obs_slider.setValue(0);
									
					DrawGraphs g = (DrawGraphs) jp;
					g.center_lines(true);
					center_geoms = false;
				}
				
				jp.repaint();
			}
		});
		
		obs_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		        JSlider s = (JSlider) e.getSource();
		        int i = (int) s.getValue();

		    	if(i < arr.length)
		    		geometry_wkt = arr[i];
				
				DrawGraphs g = (DrawGraphs) jp;
				
				if(center_geoms)
					g.center_lines(true);
				
		    	jp.repaint();
		    }
		});
		
		zoom_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		        JSlider s = (JSlider) e.getSource();
		        int desired_zoom_level = (int) s.getValue();
		        
		        DrawGraphs c = (DrawGraphs) jp;
		        double diff = desired_zoom_level - zoomLevel;
		        		        
		        if(diff > 0 && desired_zoom_level < maxZoomLevel)
		        {
		        	zoomLevel = desired_zoom_level;
            		c.scale(diff, diff);
		        }
		        else if(diff < 0 && desired_zoom_level > minZoomLevel)
		        {
		        	zoomLevel = desired_zoom_level;
            		c.scale(diff, diff);
		        }
				
		        c.center_lines(true);
		    	jp.repaint(); 
		    }
		});
		
		clear_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
		    {
				interpolation_is_valid = false;
				show_info_about_geometry_validity = false;
				geometry_wkt = null;
				arr = null;
				event = "";
		    }
		});
		
		wkt_save_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
		    {
				if(arr == null)
				{
					JOptionPane.showMessageDialog(null, "Nothing to save!", "Save", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				String filename = wkt_save_in_file.getText();

				PrintWriter writer = null;
				
				try {
					writer = new PrintWriter(filename, "UTF-8");
				} catch (FileNotFoundException | UnsupportedEncodingException e) 
				{
					e.printStackTrace();
				}
				
		    	for(int i = 0; i < arr.length; i++) 
		    		writer.println(arr[i]);
				
				writer.close();
				JOptionPane.showMessageDialog(null, "Saved to " + filename + "!", "Save", JOptionPane.INFORMATION_MESSAGE);	
		    }
		});
	}
}